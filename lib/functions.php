<?php
include 'rss.php';
$self=$_SERVER['SCRIPT_NAME'];
$self_ex=explode('/',$self);
$self_page=end($self_ex);

$bad=array('"',"'",'.',',','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',':',';');
$chars=array('"',"'",'$','^','*','(',')','+','=','`',';','?','|','>','<','}','{',']','[',);
$en_day=array('saturday','sunday','monday','tuesday','wednesday','thursday','friday');
$ar_day=array('السبت','الأحد','الإثنين','الثلاثاء','الأربعاء','الخميس','الجمعة');
$time_en=array('am','pm');
$time_ar=array('ص','م');
$en_month=array('january','february','march','april','may','june','july','august','september','october','november','december');
$ar_month=array('يناير','فبراير','مارس','أبريل','مايو','يونيو','يوليو','أغسطس','سبتمبر','أكتوبر','نوفمبر','ديسمبر');
$enNum=array('0','1','2','3','4','5','6','7','8','9');
$arNum=array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');



function getLastQuestion($userData,$les){
	if(!$userData){return'';}
	$chk=objreturn($userData->id,'user_lesson','user_id',"&& `les_id`='$les->id' && `passed`='0'");
	if(!$chk){
		$chkLessons=getLessonView($les->tut_id,$les->id);
		if($chkLessons->prev==0){// it,s The First Lesson at This Tutorial
		$chkPass=valxreturn($userData->id,'user_lesson','user_id','id',"&& `les_id`='$les->id' && `passed`='1'");
		if($chkPass){return 'passed_lesson';}
		$qIn=mysql_query("INSERT INTO `user_lesson`(`user_id`,`les_id`) VALUES ('$userData->id','$les->id')");
		$up=mysql_query("UPDATE `tutorials` SET `attend`=`attend`+1 WHERE `id`='$les->tut_id'");
		
		$inTut=mysql_query("INSERT INTO `user_tutorial`(`user_id`,`tut_id`) VALUES ('$userData->id','$les->tut_id')");
		addActivityLog('[TUT_SUB]','users','id',$userData->id,$les->tut_id);
		
		$chk=objreturn($userData->id,'user_lesson','user_id',"&& `les_id`='$les->id'");
		}else{
			$checkLessonUser=objreturn($userData->id,'user_lesson','user_id',"&& `les_id`='$chkLessons->prev'");
			if($checkLessonUser->passed==1){
				$qIn=mysql_query("INSERT INTO `user_lesson`(`user_id`,`les_id`) VALUES ('$userData->id','$les->id')");
				$chk=objreturn($userData->id,'user_lesson','user_id',"&& `les_id`='$les->id'");
			}else{return 'last_question';}
		}
	}elseif($chk->passed==1){return 'passed_lesson';}
	$level="`level`='$chk->level'";
	
	$lastAnswer=objreturn($userData->id,'users_answers','user_id',"&& `q_id` IN (SELECT `id` FROM `questions` WHERE `les_id`='$les->id' && $level) ORDER BY `id` DESC LIMIT 0,1");

	if(!$lastAnswer){
		$question=objreturn($les->id,'questions','les_id'," && `id` NOT IN(SELECT `q_id` FROM `users_answers` WHERE `user_id`='$userData->id') && $level ORDER BY `order` ASC LIMIT 0,1");
		
		if($question){return $question;}else{return 'last_question';}
	}
	else{
		
		$lastOrder=objreturn($lastAnswer->q_id,'questions','id',"&& `les_id`='$les->id'");
		
		$question=objreturn($les->id,'questions','les_id',"&& `id` NOT IN (SELECT `q_id` FROM `users_answers` WHERE `user_id`='$userData->id') && `order`>='$lastOrder->order' && $level && `id`!='$lastOrder->id' ORDER BY `order` ASC LIMIT 0,1");
		
		if($question){return $question;}else{return 'last_question';}
	}
	return 'last_question';
}



function getLessonView($tutorial,$les){
	$q=mysql_query("SELECT `id` FROM `lessons` WHERE `tut_id`='$tutorial' && `deleted`='0' ORDER BY `order`");
	$x=array('prev'=>0,'next'=>0);$one;$two;$addNext=0;
	while($l=mysql_fetch_object($q)){
		$two=$one;
		$one=$l->id;
		if($addNext==1){$x['next']=$l->id;$addNext=0;}
		if($l->id==$les){$x['prev']=$two;$addNext=1;}
	}
	return (object)$x;
}
/*Function To Get The Prev Lesson And The Next Lesson From The Current Lesson*/

function datePlus($date,$stamp)
{
	//stamp like +1 day  or -5 day or -5 month
	$date_new=date('Y-m-d', strtotime($stamp,strtotime($date)));
	return $date_new;
}

function dateTimePlus($date,$stamp)
{
	//stamp like +1 day  or -5 day or -5 month
	$date_new=date('Y-m-d H:i:s', strtotime($stamp,strtotime($date)));
	return $date_new;
}
	
function ardate($date,$time=NULL)
{
	
	global $en_day,$ar_day,$en_month,$ar_month,$time_ar,$time_en;
	$date=strtolower($date);
	if($time!=NULL)
	{
		$date=str_replace($time_en,$time_ar,$date);
	}
	else
	{
	$date=explode('/',$date);
	$date=$date[0];
	}
	$date=str_replace($en_day,$ar_day,$date);
	$date=str_replace($en_month,$ar_month,$date);
	
	$ex=explode(',',$date);
	
	$ex1=explode(' ',$ex[1]);
	
	return $ex1[2].' '.$ex1[1].'  '.$ex[2];
}

function userDis($id)
{
	return 'user.php?id='.$id;
}

/*
This Function Creates A 2D Array var named $foldersTotal
That Contains Every folder in a dir with all it,s contents
*/
function checkFolder($file)
{	
	global $foldersTotal;
	$bad = array(".", "..", ".DS_Store", "_notes", "Thumbs.db","__MACOSX");
	$scan=scandir($file);
	$files = array_diff($scan, $bad);
	foreach($files as $f)
	{
		$fd=$file.'/'.$f;
		if(is_dir($fd))
		{
			$dh  = opendir($fd);
			while (false !== ($filename = readdir($dh)))
			{
			if(!in_array($filename,$bad) && is_file($fd.'/'.$filename)) {$foldersTotal[$fd][] = $filename;}
			}
			checkFolder($fd);
		}	
	}
}

function getLocalIp()
{ return gethostbyname(trim(`hostname`)); }

function userLink($id,$class)
{
	$user=objreturn($id,'users','id');
	return '<a class="'.$class.'" href="#">'.$user->username.'</a>';
}

function extractZip($file,$file_path,$folder_path)
{
	$path=$file_path.$file;
	$folder=str_replace('.zip','',$file);
	$zip = new ZipArchive;
	if ($zip->open($path) === true)
	{
		for($i = 0; $i < $zip->numFiles; $i++)
		{
			$zip->extractTo($folder_path.$folder, array($zip->getNameIndex($i)));
		}	
		$zip->close();					
	}
	return $folder;
}



function addLog($user_id,$status)
{
	$time=time();
	$date=date('Y-m-d H:i:s');
	$ip=(strlen($_SERVER['REMOTE_ADDR'])>5)?$_SERVER['REMOTE_ADDR']:'88.88.88.88';
	$ipData=getDataIP($ip);
	$agent=$_SERVER['HTTP_USER_AGENT'];
	$country=$ipData->country;
	$city=$ipData->city.' \,\, '.$ipData->region;
	$q=mysql_query("INSERT INTO `login`(`user_id`,`status`,`time`,`agent`,`ip`,`country`,`city`,`date`) VALUES ('$user_id','$status','$time','$agent','$ip','$country','$city','$date')");
}

function getDataIP($ip)
{
	$file_string=file_get_contents("http://ipinfo.io/$ip/json");
	$data=(object)json_decode($file_string,true);
	return $data;
}

function addActivityLog($type,$table,$col,$text,$comment){
	$ip=$_SERVER['REMOTE_ADDR'];
	$agent=$_SERVER['HTTP_USER_AGENT'];
	connect();
	$qin=mysql_query("INSERT INTO `notify`(`type`,`table`,`col`,`text`,`comment`,`ip`,`agent`) VALUES ('$type','$table','$col','$text','$comment','$ip','$agent')") or die(mysql_error());
}
function objFreturn($tablename,$cond=NULL)
{
	connect();
	$sql=mysql_query("SELECT * FROM `$tablename` $cond LIMIT 0,1");
	if(mysql_num_rows($sql)<1){return false;}else{$obj=mysql_fetch_object($sql); return $obj;}
}


function chkLog($user_id)
{
	$now=time();
	$valid_attempts = $now - (5 * 60);
	$qChk=mysql_query("SELECT `time` FROM `login` WHERE `user_id`='$user_id' && `status`='0' && `time` > '$valid_attempts'") or die(mysql_error());
	if(mysql_num_rows($qChk) > 2)
	{return true;}
	else
	{return false;}
}


function GH($page,$location)
{
	global $self_page;
	if($page==$self_page)
	{
		echo '
		<script>
			window.location="'.$location.'"
		</script>
		';
		exit;
	}
}

function GL($location)
{
	echo '
	<script>
		window.location="'.$location.'"
	</script>
	';
	exit;

}

function chgetX($name,$val,$name2,$val2,$word)
{
	if((isset($_REQUEST[$name]) && $_REQUEST[$name]==$val) && (isset($_REQUEST[$name2]) && $_REQUEST[$name2]==$val2)){echo $word;}
}

function cldate($v)
{
	global $bad;
	$value=str_replace($bad,'',$v);
	return trim($value);
}

function cl2($v)
{
	global $chars;
	$value=str_replace($chars,'',$v);
	return $value;
}

function getRandNumber($len)
{
	$code="";
	$picharset="12346789";
	for($i=0; $i<$len ; $i++)
	{
	$rand=rand() % strlen($picharset);
	$tmp=substr($picharset, $rand, 1);
	$code.=$tmp;
	}
	return $code;
}

function sendHmail($to,$from,$subject,$msg)
{
    $headers = "From: " . $from . "\r\n";
    $headers .= "Reply-To: ". $from . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= 'Content-Type: text/html; charset=UTF-8'."\r\n";
	$mailsend=mail($to, $subject, $msg, $headers);
	return $mailsend;
}


function connect(){
	@mysql_connect(HOST,USER,PASS) or die(@mysql_error()) ;
	
	@mysql_select_db(BASE) or die(@mysql_error()) ;
	@mysql_query("SET NAMES 'utf8'"); 
}

function arNumbers($val)
{
	global $enNum,$arNum;
	return str_replace($enNum,$arNum,$val);
}


function seloption($tname,$val,$view,$post,$cond=NULL){
	connect();
	$q=mysql_query("SELECT `$val`,`$view` FROM `$tname` $cond") or die(mysql_error());
	while($obj=mysql_fetch_object($q))
	{
		echo'<option '.((isset($_REQUEST[$post]) && $_REQUEST[$post]==$obj->$val)?'selected':'').' value="'.$obj->$val.'">'.$obj->$view.'</option>';
	}
	mysql_close();
}

function selOptionMulti($tname,$val,$view,$post,$cond)
{
	if(isset($cond)){$tail=" $cond";}else{$tail="";}
	connect();
	$q=mysql_query("SELECT `$val`,`$view` FROM `$tname` $tail");
	$i=0;
	while($obj=mysql_fetch_object($q))
	{
		echo'<option '.((count($_POST[$post])>0 && $_POST[$post][$i]==$obj->$val)?'selected':'').' value="'.$obj->$val.'">'.$obj->$view.'</option>';
		$i++;
	}
	mysql_close();
}

function seloptionx($tname,$val,$view,$x,$cond)
{
	if(isset($cond)){$tail=" $cond";}else{$tail="";}
	connect();
	$q=mysql_query("SELECT `$val`,`$view` FROM `$tname` $tail");
	while($obj=mysql_fetch_object($q))
	{?>
		<option <?php if($obj->$val==$x){echo 'selected';} ?> value="<?php echo $obj->$val ?>"><?php echo $obj->$view ?></option>
	<?php }
	mysql_close();
}

function clearString($val)
{	
	global $chars;
	$value=str_replace($chars,'',$val);
	$value1=urldecode(str_replace('%5C','', urlencode($value)));
	return trim($value1);
}

function nospace($var)
{
	$value=str_replace(' ','',$var);
	return $value;
}


function objreturn($val,$tablename,$cell,$cond=NULL)
{
	connect();
	$val=cl2($val);
	$sql=mysql_query("SELECT * FROM `$tablename` where `$cell`='$val' $cond") or die(mysql_error());
	if(mysql_num_rows($sql)<1){return false;}else{$obj=mysql_fetch_object($sql); return $obj;}
	
}

function chget($name,$val,$word){
	if(isset($_REQUEST[$name]) && $_REQUEST[$name]==$val){echo $word;}
}

function wordsNum($val,$num){
	$exval=explode(' ',$val);
	
	if(count($exval) >= $num)
	{
		$rv='';
		for($i=0; $i<$num; $i++)
		{
			$rv.=' '.$exval[$i];
		}
		return $rv;
	}
	else
	{return $val;}
}


function valexsist($val,$tablename,$cell)
{
	connect();
	$val=cl2($val);
	$sql=mysql_query("SELECT * FROM `$tablename` where `$cell`='$val'");
	if(mysql_num_rows($sql)<1){return false;}else{$arr=mysql_fetch_array($sql); return $arr[0];}
	
}

function funcReturn($val,$tablename,$cell,$xreturn,$func,$cond=NULL)
{
	connect();
	$val=clearString($val);
	$sql=mysql_query("SELECT $func($xreturn) as XmaN FROM $tablename where `$cell`='$val' $cond") or die(mysql_error());
	if(mysql_num_rows($sql)<1){return false;}else{$arr=mysql_fetch_array($sql); return $arr['XmaN'];}
}


function valxreturn($val,$tablename,$cell,$xreturn,$cond=NULL)
{
	if($cond!=NULL){$tail=" $cond";}else{$tail="";}
	connect();
	$val=clearString($val);
	$sql=mysql_query("SELECT * FROM `$tablename` where `$cell`='$val' $tail");
	if(mysql_num_rows($sql)<1){return false;}else{$arr=mysql_fetch_array($sql); return $arr[$xreturn];}
}

function valsexsist($val1,$cell1,$val2,$cell2,$tablename,$return)
{
	connect();
	$sql=mysql_query("SELECT * FROM `$tablename` where `$cell1`='$val1' && `$cell2`='$val2' ");
	if(mysql_num_rows($sql)<1){return false;}else{$arr=mysql_fetch_array($sql); if(isset($return)){return $arr[$return];}else{ return $arr[0];}}
	
}

function NiceDate($datexman,$notime=NULL)
{
$ex1=explode(' ',$datexman);
$d=explode('-',$ex1[0]);
$t=explode(':',$ex1[1]);
$mkd=mktime(0,0,0,$d[1],$d[2],$d[0]);
$thedate=date("l, F d,Y", $mkd);
$xtime=$t[0];
if($xtime<= 12){$state='am';}else{$state='pm';}
if($xtime > 12){$xtime=$xtime-12;}
elseif($xtime == 00){$xtime= 12;}
$timeall=$xtime .':'.$t[1].' '.$state;
if($notime!=NULL){return $thedate;}else{return $thedate .' / '.$timeall;}
}

function checho($val)
{
	if(isset($_REQUEST[$val])){echo $_REQUEST[$val];}
	else{return false;}
}

function selcho($val,$chk)
{
	if(isset($_REQUEST[$val]) && $_REQUEST[$val]==$chk){echo 'selected';}
}

function selchonp($val,$chk)
{
	if($val==$chk){echo 'selected';}
}

function keyless($val,$num,$tag)
{
	if(strlen($val) > $num){return '<'.$tag.' title="'.$val.'">'.substr($val,0,$num).'..</'.$tag.'>';}else{return '<'.$tag.'>'.$val.'</'.$tag.'>';}
}

function paging($number,$table,$get_page,$cond)
{
	if (!isset($_REQUEST[$get_page])){$page=1;}else{$page= (int)$_REQUEST[$get_page];}
	if(isset($cond)){$qtail=$cond;}else{$qtail='';}
	$record_at_page=$number;
	$start=($page - 1) * $record_at_page ;
	$end= $record_at_page;
	connect();
	$q=mysql_query("SELECT * FROM `$table` $qtail");
	mysql_close();
	$records_count=@mysql_num_rows($q);
	@mysql_free_result($q);
	$pages_count = (int)ceil($records_count / $record_at_page);
	$arr=array(
	"pages"=>$pages_count,
	"start"=>$start,
	"end"=>$end,
	"page"=>$page
	
	);
	return $arr;
}

function getcount($table,$cell,$val,$cond){
	if(isset($cond)){$qtail=" $cond";}else{$qtail='';}
	connect();
	$Q=mysql_query("SELECT * FROM `$table` WHERE `$cell`='$val' $qtail");
	mysql_close();
	$x=mysql_num_rows($Q);
	if($x > 0){return $x;}else{return '0';}
}

function paging_linksBS3($link,$get,$pages_count)
{
	if (!isset($_REQUEST[$get])){$page=1;}else{$page= (int)$_REQUEST[$get];}
	$next=($page+1);
	$prev=($page-1);
	if($pages_count>0)
	{
	?>

      <ul class="pagination pagination-centered">
        <li<?php if($page==1){echo' class="disabled"';}?>><a href="<?php if($page!=1)echo $link.'&'.$get.'='.$prev; else echo'#'; ?>">« Prev.</a></li>
        <?php
        for ($i=1 ; $i <= $pages_count ; $i++ )
		{
			?>
            <li<?php if($page==$i){echo' class="active"';}?>><a href="<?php if($page!=$i) echo $link.'&'.$get.'='.$i;else echo '#'; ?>"><?php echo $i?></a></li>
			<?php
		}
		?>
        <li<?php if($page==$pages_count){echo' class="disabled"';}?>><a href="<?php if($page!=$pages_count) echo $link.'&'.$get.'='.$next; else echo '#'; ?>">Next »</a></li>
      </ul>
    <?php
	}
}

function paging_linksArBS3($link,$get,$pages_count)
{
	if (!isset($_REQUEST[$get])){$page=1;}else{$page= (int)$_REQUEST[$get];}
	$next=($page+1);
	$prev=($page-1);
	if($pages_count>0)
	{
	?>

      <ul class="pagination pagination-centered">
        <li<?php if($page==1){echo' class="disabled"';}?>><a href="<?php if($page!=1)echo $link.'&'.$get.'='.$prev; else echo'#'; ?>">السابق</a></li>
        <?php
        for ($i=1 ; $i <= $pages_count ; $i++ )
		{
			?>
            <li<?php if($page==$i){echo' class="active"';}?>><a href="<?php if($page!=$i) echo $link.'&'.$get.'='.$i;else echo '#'; ?>"><?php echo $i?></a></li>
			<?php
		}
		?>
        <li<?php if($page==$pages_count){echo' class="disabled"';}?>><a href="<?php if($page!=$pages_count) echo $link.'&'.$get.'='.$next; else echo '#'; ?>">التالى</a></li>
      </ul>
    <?php
	}
}

function ajaxPagingBS3($class,$get,$pages_count)
{
	if (!isset($_REQUEST[$get])){$page=1;}else{$page= (int)$_REQUEST[$get];}
	$next=($page+1);
	$prev=($page-1);
	if($pages_count>0)
	{
	?>

      <ul class="pagination pagination-centered">
        <li<?php if($page==1){echo' class="disabled"';}?>><a href="<?php if($page!=1)echo $prev; ?>" class="<?php echo $class?>">« Prev.</a></li>
        <?php
        for ($i=1 ; $i <= $pages_count ; $i++ )
		{
			?>
            <li<?php if($page==$i){echo' class="active"';}?>><a href="<?php if($page!=$i) echo $i; ?>" class="<?php echo $class?>"><?php echo $i?></a></li>
			<?php
		}
		?>
        <li<?php if($page==$pages_count){echo' class="disabled"';}?>><a href="<?php if($page!=$pages_count) echo $next;  ?>" class="<?php echo $class?>">Next »</a></li>
      </ul>
    <?php
	}
}

function ajaxPaging($class,$get,$pages_count)
{
	if (!isset($_REQUEST[$get])){$page=1;}else{$page= (int)$_REQUEST[$get];}
	$next=($page+1);
	$prev=($page-1);
	if($pages_count>0)
	{
	?>

      <ul style="list-style:none;width: 100%;text-align: center;min-height: 30px;">
        <li style="display: inline-block;"<?php if($page==1){echo' class="disabled"';}?>><a href="<?php if($page!=1)echo $prev; ?>" class="<?php echo $class?>">« Prev . </a></li>
        <?php
        for ($i=1 ; $i <= $pages_count ; $i++ )
		{
			?>
            <li style="display: inline-block;" <?php if($page==$i){echo' class="active"';}?>><a href="<?php if($page!=$i) echo $i; ?>" class="<?php echo $class?>"><?php echo $i?></a></li>
			<?php
			if($i<$pages_count){echo'-';}
		}
		?>
        <li style="display: inline-block;" <?php if($page==$pages_count){echo' class="disabled"';}?>><a href="<?php if($page!=$pages_count) echo $next;  ?>" class="<?php echo $class?>"> . Next »</a></li>
      </ul>
    <?php
	}
}
function ajaxPagingAr($class,$get,$pages_count)
{
	if (!isset($_REQUEST[$get])){$page=1;}else{$page= (int)$_REQUEST[$get];}
	$next=($page+1);
	$prev=($page-1);
	if($pages_count>0)
	{
	?>

      <ul style="list-style:none;width: 100%;text-align: center;border-top: 1px #808080 solid;min-height: 30px;">
        <li style="display: inline-block;"<?php if($page==1){echo' class="disabled"';}?>><a href="<?php if($page!=1)echo $prev; ?>" class="<?php echo $class?>">السابق </a></li>
        <?php
        for ($i=1 ; $i <= $pages_count ; $i++ )
		{
			?>
            <li style="display: inline-block;" <?php if($page==$i){echo' class="active"';}?>><a href="<?php if($page!=$i) echo $i; ?>" class="<?php echo $class?>"><?php echo $i?></a></li>
			<?php
			if($i<$pages_count){echo'-';}
		}
		?>
        <li style="display: inline-block;" <?php if($page==$pages_count){echo' class="disabled"';}?>><a href="<?php if($page!=$pages_count) echo $next;  ?>" class="<?php echo $class?>"> التالى</a></li>
      </ul>
    <?php
	}
}

function paging_links($link,$get,$pages_count)
{
	if (!isset($_GET[$get])){$page=1;}else{$page= (int)$_GET[$get];}
	$next=($page+1);
	$prev=($page-1);
	if($prev > 0)
	
	?>
    <div class="pagination pagination-centered">
      <ul>
        <li<?php if($page==1){echo' class="disabled"';}?>><a href="<?php if($page!=1)echo $link.'&'.$get.'='.$prev; else echo'#'; ?>">« Prev.</a></li>
        <?php
        for ($i=1 ; $i <= $pages_count ; $i++ )
		{
			?>
            <li<?php if($page==$i){echo' class="active"';}?>><a href="<?php if($page!=$i) echo $link.'&'.$get.'='.$i;else echo '#'; ?>"><?php echo $i?></a></li>
			<?php
		}
		?>
        <li<?php if($page==$pages_count){echo' class="disabled"';}?>><a href="<?php if($page!=$pages_count) echo $link.'&'.$get.'='.$next; else echo '#'; ?>">Next »</a></li>
      </ul>
    </div>
    <?php
}

function htmsgR($type,$msg,$style=NULL)
{
	return '<div class="alert alert-'.$type.'" style="'.$style.'"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$msg.'</div><div class="clearfix"></div>';
}


function getPass($p){
	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
	$password = hash('sha512',$p);
	$password1 = hash('sha512',$password.$random_salt);
	return array('pass'=>$password1,'salt'=>$random_salt);
}

function chkPass($pass,$salt,$dbPass){
	$password=hash('sha512',$pass);
	$password1 = hash('sha512',$password.$salt);
	if($password1==$dbPass){return true;}else{return false;}
}

function validEmail($val)
{
	if(!filter_var($val, FILTER_VALIDATE_EMAIL)){return false;}else{return true;}
}

function limit_words($string, $word_limit)
{
	$string = strip_tags($string);
	$words = explode(" ",$string);
	return implode(" ",array_splice($words,0,$word_limit));
}

function getRand($len)
{
	$code="";
	$picharset="12346789ABCDEFGHIJLKMNPQRSTUVWXYZ";
	for($i=0; $i<$len ; $i++)
	{
	$rand=rand() % strlen($picharset);
	$tmp=substr($picharset, $rand, 1);
	$code.=$tmp;
	}
	return $code;
}


function htmsg($type,$msg)
{
	echo'<div class="alert alert-'.$type.'"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$msg.'</div>';
}

function getRatingFA($val,$val2)
{
	$gap=$val2-$val;
	$get_minus=$val+1;
	$haveExt=strpos($gap,'.');
	if($haveExt!==false)
	{$numEx=explode('.',$val);}
	for($i=0; $i<$val2; $i++)
	{
		if($gap >= 1){$get_minus--;}
		?>
		<i class="fa fa-star<?php if($haveExt!==false && $i==$numEx[0]){echo'-half';}if($get_minus<1){echo'-o ';}?>"></i>
        
		<?php
	}
}

function getRatingFAR($val,$val2)
{
	$data='';
	$gap=$val2-$val;
	$get_minus=$val+1;
	$haveExt=strpos($gap,'.');
	if($haveExt!==false)
	{$numEx=explode('.',$val);}
	for($i=0; $i<$val2; $i++)
	{
		if($gap >= 1){$get_minus--;}
		$data.=' <i class="fa fa-star';
		if($haveExt!==false && $i==$numEx[0]){$data.='-half';}
		if($get_minus<1){$data.='-o ';}
		$data.='"></i> ';
	}
	return $data;
}


function getRatingBS3($val,$val2)
{
	$gap=$val2-$val;
	$get_minus=$val+1;
	$haveExt=strpos($gap,'.');
	for($i=0; $i<$val2; $i++)
	{
		if($gap > 0){$get_minus--;}
		?>
		<i class="glyphicon glyphicon-star<?php if($get_minus<1){echo'-empty';}?>"></i>
        
		<?php
	}
}
function tagsSlash($val){
	return htmlentities(strip_tags($val));
}

function gets($word)
{
	$gets='';$z=0;
	if(is_array($word))
	{
	foreach($_GET as $key=>$val){$key=tagsSlash($key);$val=tagsSlash($val);if(in_array($key,$word)){continue;}$gets.="&$key=$val";}
	}
	else
	{
	foreach($_GET as $key=>$val){$key=tagsSlash($key);$val=tagsSlash($val);if($key==$word){continue;}$gets.="&$key=$val";}
	}
	return $gets;
}

function getsQ($word)
{
	$gets='';$z=0;
	if(is_array($word))
	{
	foreach($_GET as $key=>$val){if(in_array($key,$word)){continue;}$z++; if($z==1){$gets.="?$key=$val";}else{$gets.="&$key=$val";}}
	}
	else
	{
	foreach($_GET as $key=>$val){if($key==$word){continue;}$z++; if($z==1){$gets.="?$key=$val";}else{$gets.="&$key=$val";}}
	}
	return $gets;
}




function sec_session_start()
{
        /*$session_name = 'secloginofadmin'; // Set a custom session name
        $secure = false; // Set to true if using https.
        $httponly = true; // This stops javascript being able to access the session id. 
 
        ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
        $cookieParams = session_get_cookie_params(); // Gets current cookies params.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); // Sets the session name to the one set above.
        session_start(); // Start the php session
        session_regenerate_id(true); // regenerated the session, delete the old one.
		*/
		session_start();
}

function sToTime($seconds)
{	
	$t='';
	if($seconds<60){$t= '00:00';}
	else
	{
		$minits=$seconds/60;
		if($minits<60)
		{
			if($minits<10){$t= '00:0'.(int)$minits;}
			else{$t= '00:'.(int)$minits;}
		}
		else
		{
			$hours=$minits/60;
			if(strpos($hours,'.')!==false)
			{
			$ex=explode('.',$hours);
			$h=$ex[0];
			$mx='0.'.$ex[1];
			$m=$mx*60;
			if($h<10){$t= '0'.(int)$h.':';}else{$t= (int)$h.':';}
			if($m<10){$t.= '0'.(int)$m;}else{$t.= (int)$m;}
			
			}
			else
			{
				if($hours<10){$t= '0'.$hours.':00';}
			}
		}
	}
	return $t;
}

function TimeToCash($seconds,$salary)
{
	$money=number_format((float)($seconds/3600*$salary), 2, '.', '');
	return $money;
}

function fObj($q){
	return mysql_fetch_object($q);
}

function delData(){
	$data=array();
	
	connect();
	$q_categories=mysql_query("UPDATE `categories` SET `deleted`='1' WHERE `main`!='0' && `main` NOT IN (SELECT `id` FROM `categories` WHERE `deleted`='0')");
	$data['cat']=mysql_affected_rows($q_categories);
	
	$q_tutorials=mysql_query("UPDATE `tutorials` SET `deleted`='1' WHERE `cat_id` NOT IN (SELECT `id` FROM `categories` WHERE `deleted`='0')");
	$data['tut']=mysql_affected_rows($q_tutorials);
	
	$q_lessons=mysql_query("UPDATE `lessons` SET `deleted`='1' WHERE `tut_id` NOT IN (SELECT `id` FROM `tutorials` WHERE `deleted`='0')");
	$data['les']=mysql_affected_rows($q_lessons);
	
	$q_holders=mysql_query("UPDATE `holders` SET `deleted`='1' WHERE (`les_id` NOT IN (SELECT `id` FROM `lessons` WHERE `deleted`='0') && `tut_id` NOT IN (SELECT `id` FROM `tutorials` WHERE `deleted`='0'))");
	$data['hol']=mysql_affected_rows($q_holders);
	
	$q_sections=mysql_query("UPDATE `sections` SET `deleted`='1' WHERE `hol_id` NOT IN (SELECT `id` FROM `holders` WHERE `deleted`='0')");
	$data['sec']=mysql_affected_rows($q_sections);
	
	$q_columns=mysql_query("UPDATE `columns` SET `deleted`='1' WHERE `sec_id` NOT IN (SELECT `id` FROM `sections` WHERE `deleted`='0')");
	$data['col']=mysql_affected_rows($q_columns);
	
	$q_titles=mysql_query("UPDATE `titles` SET `deleted`='1' WHERE `col_id` NOT IN (SELECT `id` FROM `columns` WHERE `deleted`='0')");
	$data['tit']=mysql_affected_rows($q_titles);
	
	$q_content=mysql_query("UPDATE `content` SET `deleted`='1' WHERE `col_id` NOT IN (SELECT `id` FROM `columns` WHERE `deleted`='0')  &&  `tit_id` NOT IN (SELECT `id` FROM `titles` WHERE `deleted`='0')");
	$data['con']=mysql_affected_rows($q_content);
	mysql_close();
	
	return $data;
}

function getWidTypes($t)
{
$arrx=explode(',',$t);$i=0;$types='';
foreach($arrx as $val){$i++;$types.="'".$val."'"; if($i==count($arrx)) continue; $types.=',';}
return $types;
}



function getTutId($id,$type){
	switch ($type){
		case'les': return valxreturn((int)$id,'lessons','id','tut_id','');
		break;
		case'hol': return valxreturn(valxreturn((int)$id,'holders','id','les_id',''),'lessons','id','tut_id','');
		break;
		case'sec': return valxreturn(valxreturn(valxreturn((int)$id,'sections','id','hol_id',''),'holders','id','les_id',''),'lessons','id','tut_id','');
		break;
		case'col': return valxreturn(valxreturn(valxreturn(valxreturn((int)$id,'columns','id','sec_id',''),'sections','id','hol_id',''),'holders','id','les_id',''),'lessons','id','tut_id','');
		break;
		case'tit':return valxreturn(valxreturn(valxreturn(valxreturn(valxreturn((int)$id,'titles','id','col_id',''),'columns','id','sec_id',''),'sections','id','hol_id',''),'holders','id','les_id',''),'lessons','id','tut_id','');
		break;
		case'con': $tit_id=valxreturn((int)$id,'content','id','tit_id','');
		if(!empty($tit_id)){
			return valxreturn(valxreturn(valxreturn(valxreturn(valxreturn($tit_id,'titles','id','col_id',''),'columns','id','sec_id',''),'sections','id','hol_id',''),'holders','id','les_id',''),'lessons','id','tut_id','');
		}
		else{
			return valxreturn(valxreturn(valxreturn(valxreturn(valxreturn((int)$id,'content','id','col_id',''),'columns','id','sec_id',''),'sections','id','hol_id',''),'holders','id','les_id',''),'lessons','id','tut_id','');
		}
		break;
	}
	return false;
}


function addTutChange($tut,$type,$table=NULL,$id=NULL,$v1=NULL,$v2=NULL,$v3=NULL,$ex=NULL){
	switch($type){
		case'add':$type=1;break;
		case'edit':$type=2;break;
		case'delete':$type=3;break;
	}
	
	$chkTut=objreturn((int)$tut,'tutorials','id',"&& `deleted`='0'");
	if($chkTut->approved==1 && $chkTut->edited==0){
		$upTut=mysql_query("UPDATE `tutorials` SET `approved`='0',`edited`='1' WHERE `id`='$chkTut->id'");
		$qIn=mysql_query("INSERT INTO `tut_changes`(`tut_id`,`type`,`table`,`x_id`,`val1`,`val2`,`val3`,`extra`,`undone`) VALUES ('$chkTut->id','$type','$table','$id','$v1','$v2','$v3','$ex','1')");
	}
	elseif($chkTut->approved==0 && $chkTut->edited==1){
		$qIn=mysql_query("INSERT INTO `tut_changes`(`tut_id`,`type`,`table`,`x_id`,`val1`,`val2`,`val3`,`extra`,`undone`) VALUES ('$chkTut->id','$type','$table','$id','$v1','$v2','$v3','$ex','1')");
	}
}

function toUrl($val){
	$val=str_replace('/','|',$val);
	return str_replace(' ','-',$val);
}
function fromUrl($val){
	$val=str_replace('|','/',$val);
	return str_replace('-',' ',$val);
}

function htGetLast($getName){
	$v=$_GET[$getName];
	if(substr($v,-1)=='/'){$v=substr($v,0,-1);}
	$ex=explode('/',$v);
	return end($ex);
}


function getTutLink($id){
	$tut=objreturn((int)$id,'tutorials','id','');
	$cat=objreturn($tut->cat_id,'categories','id','');
	$main=objreturn($cat->main,'categories','id','');
	return SITE_NAME.'Tutorials/'.toUrl($main->link_title).'/'.toUrl($cat->link_title).'/'.toUrl($tut->title).'/';
}

/* Rss Function Start  ... */ 

///////// Function Insert Rss Data Into DB Start >>>
function news($link){
    if (strpos($link, 'biomedcentral') != false)
    {
        return biomedcentral($link);
    }
    elseif (strpos($link, 'reuters') != false)
    {
        return reuters($link);
    }
    elseif (strpos($link, 'fdanews') != false)
    {
        return fdanews($link);
    }else
    {
        return false;
    }

	}
//////////// <<< End 

 function biomedcentral($link)
 {
 	$article = array();
	 	$html 	= file_get_html($link);
	 	###  Select Header >>>>
		foreach ($html->find('#Test-ImgSrc > div:nth-child(2) > div.MainTitleSection > h1') as $h1)
		{
		
		$articlesHeader = strip_tags($h1);
		
		}
		$i=0;
    	###  Select Content For Article >>>>
    	foreach( $html->find('.FulltextWrapper') as $content)
    	{
    		$i++;
    		if($i==3)
    		
    		{
	    		$article[] = strip_tags($content);
	    	}
    	}
    	$data = array('title' => $articlesHeader, 'content' => array($article));
    	echo  $data;
 }

 function fdanews($link)
 {
 	$article = array();
 	$html 	= file_get_html($link);
 		###  Select Header >>>>
		foreach ($html->find('h1') as $h1) {
			$articlesHeader = strip_tags($h1);
		}

		///Count P Element To Remove Button
		$count = count($html->find('.body p'));
		$i = 0;
		###  Select Content For Article >>>>
    	foreach( $html->find('.body p') as $content)
    	{

    		$i++;
    		if($i < $count)
    		{

	    		$article[] = strip_tags($content);
	    	}

    	}

    	$data = array('title' => $articlesHeader, 'content' => array($article));
    	return $data;
 }

 function reuters($link)
 {
 	$article = array();
 	$html 	= file_get_html($link);
 	###  Select Header >>>>
		foreach ($html->find('#articleContent > div > h1') as $h1) {
			$articlesHeader = strip_tags($h1);
		}
		
	###  Select Content For Article >>>>
    	foreach( $html->find('#articleText p') as $content)
    	{

    		
	    	$article[] = strip_tags($content);
	    	

    	}
    	$data = array('title' => $articlesHeader, 'content' => array($article));
    	return $data;


 }
/* Rss Function End */
?>