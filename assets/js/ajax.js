function alertHtml(status,msg){
	return message = '<div class="alert alert-'+status+'"><strong>! '+msg+'</strong></div>';
}
// OPEN PRINT AND CLOSE WINDOW
function loadPrint() {
    
    $(document).ready(function(){
        window.print();
        setTimeout(function () { window.close(); }, 4000);    
    });
    
}

function perloader(color,size){
	return '<div class="preloader pl-'+size+' pls-'+color+'">'+
                      '<svg class="pl-circular" viewBox="25 25 50 50">'+
                        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
                      '</svg>'+
                    '</div>';
}


function topFunction() {
	$('html,body').animate({ scrollTop: 0 }, 'slow');
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}


function headerLoader(remove){
	if(remove == true) 
		$('.headerLoader').html('<div class="loader loader-pink"></div>');
	else
		$('.headerLoader').html('');
}

// TABLE CONTENT 
function tableContent(){
	return '<div class="card card-data-tables "><div class="card-boty p-0">'+
	'<div class="table-responsive">'+
		'<table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">'+
	        '<thead class="thead_append">'+
	        '</thead>'+
			'<tbody class="TableResult">'+
            '</tbody>'+
    	'</table>'+
    '</div>'+
    '</div></div>';
}

// DELETE FUNCTION 
$("body").on('click','.delete',function() {
	var $this = $(this);
	var Element = $this.closest("tr");
	var id 		= $this.attr('data-id');
	var table 	= $this.attr('data-table');
	var column 	= $this.attr('data-column');
	var join 	= $this.attr('data-join');
	var url 	= $this.attr('data-url');
	var type    = $this.attr('data-type');
	var type_name    = $this.attr('type-name');
	var img_name    = $this.attr('img-name');

	if(url != '' && id != '' && table !='' && column != '')
	{
		$('#sa-warning').click();
		$('.confirm').click(function(){
		Element.hide(1000);
		$.ajax({
			type: "POST",
			url: url,
			data: {id:id,table:table,column:column,join,join,type:type,type_name:type_name,img_name:img_name},
			success: function(data)
			{
				if(type == 'image')
				{
					$this.parent().parent().find('form').css('visibility','visible');
					$this.parent().parent().find('.dz-message').removeAttr('style');
					$this.parent().parent().find('.progress-barUpload').removeAttr('style');
					$this.parent().parent().find('.status').html('0%');
					$this.parent().remove();
				}
				else
					Element.hide("slow");
			}
		});
	
		});
	}
});

$("body").on('submit','.form_in',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $(this).attr("action");	
	resultClass = $(this).attr('result-data');
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			success: function(data){
				if(data.response == true){
					$('.'+resultClass+'').html(alertHtml('success',data.message));
					if(data.redirect != ''){
						setTimeout(function(){
							window.location.href = ''+data.redirect+'';
						});
					}
				}
				else
					$('.'+resultClass+'').html(alertHtml('danger',data.message));
			}	
		});
	}
	return false;

});

$(document).ready(function(){
	$('body > sweet-alert.showSweetAlert > button.confirm.btn.btn-lg.btn-primary').html('انهاء');
});

/////////////// LOAD MORE FUNCTION 

$('.loadmore').on('click',function(){
    var table= $(this).attr('table');
    var count= +$(this).attr('count');
    var dataResponse = $(this).attr('response');
    var url = $(this).attr('url');
    var loading = $(this).attr('data-img');
    if(dataResponse == 'send'){
        $('.loadmore').attr('response','sent');
        $('.loadmore').html('<img src="'+loading+'">');
        $.ajax({
            type: "GET",
            url: url,
            data: {table:table,count:count},
            success: function(data)
            {
                $('.tablaResult').append(data);
                if(data.length > 2){
                    $('.loadmore').attr('count',parseInt(count+10));
                    $('.loadmore').attr('response','send');
                    $('.loadmore').html('Load more');
                }
                else{

                    $('.loadmore').html('Load more');

                    $('.loadmore').attr('disabled','disabled');
                }
            },
        });
    }

});


// OPEN FILE _ RECEPTION > AJAX :::: SECTION 1
$('.changeAjax').change(function(){
	$this = $(this);
	val  		= $this.val();
	url 		= $this.attr('data-url');
	resultArea  = $this.attr('data-resultArea');
	table  		= $this.attr('data-table');
	column  	= $this.attr('data-column');
	get_id		= $this.attr('data-get-id');
	get_name	= $this.attr('data-get-name');
	data_tr		= $this.attr('data-tr');
	$.ajax({
		cache: false,
		type: "GET",
		url: url,
		dataType: 'json',
		data: {id:val,table:table,column:column},
		success: function(data){
			if(data.response == true){
				$('.'+resultArea).html('');

				$('.'+resultArea).attr('data-from-id',val);

				if(get_id == 'administration_id'){

					$.each(data.result.administrations, function(key,value){
						$('.'+resultArea).append('<option data-id="'+val+'" value="'+value.administration_id+'">'+value.administration_name+'</option>');

					});
					$('#student_school').html('');
					$.each(data.result.school, function(key,value){
						$('#student_school').append('<option data-id="'+val+'" value="'+value.school_id+'">'+value.school_name+'</option>');
					});
					$('#student_school').append('<option data-adminid="'+data.result.not_exist+'" value="not_exist">اخرى</option>');
				}
				else
				{
					$.each(data.result, function(key,value){
						if(data_tr == 'name'){
							$('.'+resultArea).append('<option data-id="'+val+'" value="'+value.city_name+'">'+value.city_name+'</option>');
						}
						else{
							$('.'+resultArea).append('<option data-id="'+val+'" value="'+value.city_id+'">'+value.city_name+'</option>');
						}
					});
				}


				
				
				$('.'+resultArea).append('<option data-id="'+val+'" value="not_exist">اخرى</option>');


			}
		}
	});
});

$(document).ready(function(){
	 $("#print").click(function(){
	 	printSpace = $(this).attr('data-print');
		var mode = 'iframe'; // popup
		var close = mode == "popup";
		var options = { mode : mode, popClose : close};
		$("#"+printSpace).printArea( options );
	});
});

// END SECTION 1


// THEME SETTING FUNCTION 
$('body').on('click','#theme-color',function(){
	theme_setting = $(this).attr('data-load-css');
	url 		  = $(this).attr('data-url');
	$.ajax({
		cache: false,
		type: "GET",
		url: url,
		dataType: 'json',
		data: {theme_setting:theme_setting},
		success: function(data){
				
		}
	});
});