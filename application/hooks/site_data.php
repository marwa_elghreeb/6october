<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class main_info{

    function init() {
    	$CI =& get_instance();
    	$CI->userData = false;
        
    	// START BACKEND ADMIN USERDATA 
        	if($CI->session->userdata('username')!==false && $CI->session->userdata('logged_in')){
                $CI->userData=$CI->main_m->getRow($CI->session->userdata('user_id'),'user','user_id');
            }
            else
            {
                if(isset($_COOKIE['userCookies'])){ ## IF SESSION DESTROY OPEN SESSION WITH COOKIES 
        		    $CI->userData=$CI->main_m->getRow($_COOKIE['userCookies'],'user','user_id');
                    $sessionData = array('username' => $CI->userData->username , 'real_name' => $CI->userData->real_name, 'user_id'=>$CI->userData->user_id, 'logged_in' => TRUE);
                    $CI->session->set_userdata($sessionData);


                    // USER PERMISSION
                    $CI->pagesArray = array('home','home/','','/','auth/','auth');
                    if(!in_array($CI->uri->segment(1),json_decode($CI->userData->permission)) && !in_array($CI->uri->segment(1),$CI->pagesArray)){
                        redirect(base_url().'home/page_not_found');
                    }

                }
        		if($CI->userData == false){ ## IF SESSION DESTROY AND COOKIES 

		            if($CI->uri->segment(1) != 'auth'){
		                redirectJs(base_url().'auth');
                        
                    }
		        }
    		}

        
        // END BACKEND ADMIN USERDATA
    }
}