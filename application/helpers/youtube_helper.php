<?php

define("YT_API_KEY", "AIzaSyDCqipxQEgyBALAyBRya6xRCKEPrKMhPr4");

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('loadRestfulUrl')) {
    function loadRestfulUrl($url) {
        $data = file_get_contents($url);
        if ($data) {
            return json_decode($data);
        } else {
            return NULL;
        }
    }

}

function getVideoList($playlistId, $maxResults) {
    $url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=" . $maxResults . "&playlistId=" . $playlistId . "&key=" . YT_API_KEY;
    $jsonResponse = loadRestfulUrl($url);
    return $jsonResponse -> items;
}

function getplayListyoutube($channelId = '', $maxResults = '') {
    $url = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&maxResults=" . $maxResults . "&channelId=" . $channelId . "&key=" . YT_API_KEY;
    $jsonResponse = loadRestfulUrl($url);
    return $jsonResponse -> items;
}

function getChannelId($username) {
    $url = "https://www.googleapis.com/youtube/v3/channels?part=id&forUsername=" . $username . "&key=" . YT_API_KEY;
    $jsonResponse = loadRestfulUrl($url);
    return $jsonResponse -> items[0] -> id;
}

function getUploadsId($channelId) {
    $url = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=" . $channelId . "&key=" . YT_API_KEY;
    $jsonResponse = loadRestfulUrl($url);
    return $jsonResponse -> items[0] -> contentDetails -> relatedPlaylists -> uploads;
}

function getplaylist($play = '', $maxResults = '10') {
    $play = htmlspecialchars($play);
    $videoList = getVideoList($play, $maxResults);

    $parsedVideoList = array();
    foreach ($videoList as $video) {
        $videoDetails = $video -> snippet;
        if (!empty($videoDetails -> thumbnails -> high -> url)) {
            //$data['publishedAt'] = $videoDetails->publishedAt;
            $data['publishedAt'] = strtotime((string)$videoDetails -> publishedAt);
            $data['title'] = $videoDetails -> title;
            $data['thumbnails'] = $videoDetails -> thumbnails -> high -> url;
            $data['videoId'] = $videoDetails -> resourceId -> videoId;
            $data['videolink'] = getUrl($videoDetails -> resourceId -> videoId);
            array_push($parsedVideoList, $data);
        }

    }

    return $parsedVideoList;

}

function getallplaylist($channelId = '', $maxResults = '') {
    $allplaylist = getplayListyoutube($channelId,$maxResults);
    $parsedplayList = array();
    foreach ($allplaylist as $list) {
        if (!empty($list -> snippet -> title)) {
            $data['publishedAt'] = strtotime((string)$list -> snippet -> publishedAt);
            $data['title'] = $list -> snippet -> title;
            $data['listid'] = $list -> id;
            array_push($parsedplayList, $data);
        }

    }

    return $parsedplayList;

}

function getUrl($videoId) {
    return "https://youtube.com/watch?v=" . $videoId;
}

/* End of file date_helper.php */
/* Location: ./system/helpers/date_helper.php */
