<?php
    function getPass($p)
    {
		$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$password = hash('sha512',$p);
		$password1 = hash('sha512',$password.$random_salt);
		return array('pass'=>$password1,'salt'=>$random_salt);
	}

	function chkPass($pass,$salt,$dbPass)
	{
		$password = hash('sha512',$pass);
		$password1 = hash('sha512',$password.$salt);
		if($password1==$dbPass){return true;}else{return false;}
	}

	function dayAr($day)
	{
		switch ($day) {
			case 'Sat':
				$day = 'السبت' ;
				break;
			case 'Sun':
				$day = 'الاحد' ;
				break;
			case 'Mon':
				$day = 'الاثنين' ;
				break;
			case 'Tue':
				$day = 'الثلاثاء' ;
				break;
			case 'Wed':
				$day = 'الاربعاء' ;
				break;
			case 'Thu':
				$day = 'الخميس' ;
				break;
			default:
				$day = 'الجمعة' ;
				break;
		}

		return $day;
	}

	function loadStaticContent($custom,$data = NULL,$script = NULL){
		$CI =& get_instance();
		$CI->load->view('main/head',$data);
		$CI->load->view('main/header');
		$CI->load->view('main/nav');
		$CI->load->view($custom);
		$CI->load->view('main/footer',$script);
	}

	function student_year(){
		$array = array(1,2,3,4,5);
		if(in_array(date('m'),$array))
			return date('Y').'-'.student_codeDate();
		else
			return (date('Y')+1).'-'.student_codeDate();
	}
	
	function controllers()
	{
		return array(
            array('name' => 'reception', 'ar' => 'الاستقبال'),
            array('name' => 'students_affairs', 'ar' => 'لجنة النظام والمراقبة'),
            array('name' => 'accounts', 'ar' => 'الحسابات'),
            array('name' => 'members', 'ar' => 'الاعضاء'),
            array('name' => 'security', 'ar' => 'الامن'),
            array('name' => 'call_center', 'ar' => 'call center'),
        );
	}

	function student_codeDate()
	{
		$array = array(1,2,3,4,5);
		if(in_array(date('m'),$array))
			return date('Y')-1;
		else
			return date('Y');
	}

	function WordIN($number){
		switch ($number) {
			case 1:
				$val = 'الاول';
			break;
			case 2:
				$val = 'الثانى';
			break;
			case 3:
				$val = 'الثالث';
			break;
			case 4:
				$val = 'الرابع';
			break;
			case 5:
				$val = 'الخامس';
			break;
	
			default:
				$va = 'الاول';
			break;
		}
		return $val;
	}


	function tuition_fees_type($type){
		switch ($type) {
			case 'normal':
				$val = 'مصاريف عادية';
			break;
			case 'foreigner':
				$val = 'طالب اجنبى';
			break;
			case 'intensified':
				$val = 'منهج مكثف';
			break;
			default:
				$va = 'مصروفات عادية';
			break;
		}
		return $val;
	}

	function loadContentArray($array,$data = NULL){
		$CI =& get_instance();
		$i=0;
		foreach ($array as $array_key => $array_value) { $i++;
			if($i == 1 && $data != NULL)
				$CI->load->view($array_value,$data);
			else
				$CI->load->view($array_value);
		}

	}

	function uploadMedia($file)
	{
		
		$dir 	= './dist/uploads/';	
		$path 	= $file['tmp_name'];
		$name 	= $file['name'];
		$size 	= $file['size'];
		$type 	= $file['type'];
		$error 	= $file['error'];
		if (!$error)
		{
			$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
			$result = '';
			for ($i = 0; $i < 20; $i++)
			{
				$result .= $characters[mt_rand(0, 61)];
			}
			$ex=explode ('.',$name);
			$end = end($ex);
			$forname=time()."_".$result.'.'.$end;
			if(move_uploaded_file($path , $dir.$forname))
			{return $forname;}
		}
		else
		{
			return '';
		}
	}

	// EXCEL STYLE FUNCTION 
	function ExcelFontStyle ($bold = true, $color = 'FF0000', $size = 15, $name = 'Verdana'){
        $fontStyle = array(
            'font'  => array(
            'bold'  => $bold,
            'color' => array('rgb' => $color),
            'size'  => $size,
            'name'  => $name
        ));
        return $fontStyle;
    }


	function uploadFile($path,$fileName){
		$Up = &get_instance();
		$config['upload_path']          = $path;
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['encrypt_name'] 		= TRUE;
		$Up->load->library('upload', $config);
        if ( ! $Up->upload->do_upload($fileName))
            $error = array('response' => false,'error' => $Up->upload->display_errors());
        else
            $imageData = array('upload_data' => $Up->upload->data());
    	
    	if(isset($imageData['upload_data']['file_name']))
    		return array('response' => true, 'filename' => $imageData['upload_data']['file_name']);
    	else
    		return $error;
	}

	function sendEmailHelper($from,$to,$subject,$message){
	$Int = &get_instance();
	$message = '';

	    $Int->load->library('email');
	    $Int->email->set_newline("\r\n");
	    $Int->email->from($from); // change it to yours
	    $Int->email->to($to);// change it to yours
	    $Int->email->subject($subject);
	    $Int->email->message($message);
	    if($Int->email->send())
	      	return true;
	    else
	     	return false;


	}


	function validArgs($chk){
		if(empty($chk)){
			redirect(base_url('/'));
			exit;
		}else{
			return linkText($chk);
		}
	}	


	function textLink($str){
		$l=array('-',' ');
		$f=array('_','-');
		return strtolower(str_replace($l,$f,$str));
	}

	function linkText($str){
		$f=array('-','_');
		$l=array(' ','-');
		return strtolower(str_replace($f,$l,$str));
	}


	function textLinkCapital($str){
		$l=array('-',' ');
		$f=array('_','-');
		return str_replace($l,$f,$str);
	}

	function linkTextCapital($str){
		$f=array('-','_');
		$l=array(' ','-');
		return str_replace($f,$l,$str);
	}

	function returnMonthFormate($date){
		$date = explode(',', $date);
		if(isset($date[2])){
			unset($date[2]);
			return implode("",str_replace(' ',' / ',$date));
		}
		else{
			return implode("",str_replace(' ',' / ',$date));
			
		}
	}

	function wordLimitLink($str,$count){
		$strx='';
		$ex=explode(' ',$str);
		for($i=0; $i<$count; $i++){
			if(isset($ex[$i])){
				$strx.=' '.$ex[$i];
			}
		}
		return strtolower(textLink(trim($strx)));
	}

	function metaKey($str){
		return str_replace('-',',',wordLimitLink($str,5));
	}


	function addBR($str){
		$str=str_replace('
','<br>',htmlentities($str));
		return $str;
	}

	function alert_success($msg)
	{
		echo "<script>$('.form_result').removeClass('error');</script>";
		echo "<script>$('.form_result').addClass('success');</script>";
		echo "<span>".$msg."...</span>";
	}
	function alert_error($msg)
	{
		echo "<script>$('.form_result').removeClass('success');</script>";
		echo "<script>$('.form_result').addClass('error');</script>";
		echo "<span>".$msg."...</span>";
	}

	function alert($type='info',$msg,$return=FALSE){
		$alert='<div class="alert alert-'.$type.'"><button type="button" class="close" data-dismiss="alert">&times;</button>'.$msg.'</div>';
		if($return){
			return $alert;
		}else{
			echo $alert;
		}
	}

	function CheckImg($img)
	{
		$avatar = base_url().'assets/uploads/avatar.png';

		if(!empty($img))
		{
			if(file_exists('assets/uploads/'.$img))
				return base_url().'assets/uploads/'.$img;
			else
				return $avatar;
		}
		else
			return $avatar;
	}

	////Random String
	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	function generateRandomInt($length = 10) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function siteURL() {
	    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	    $domainName = $_SERVER['HTTP_HOST'] . '/';
	    return $protocol . $domainName;
	}

	function unique_multidim_array($array, $key) { 
	    $temp_array = array(); 
	    $i = 0; 
	    $key_array = array(); 
	    
	    foreach($array as $val) {
	    	$val=(array)$val;
	        if (!in_array($val[$key], $key_array)) {
	            $key_array[$i] = $val[$key];
	            $temp_array[$i] = $val;
	        }
	        $i++;
	    }
	    return $temp_array;
	}

	function img($link,$width=300,$str=false){
		if(isMobile()){$width=$width/2;}
		$link=str_replace('/','__',$link);
		$link=base_url('/image/'.$width.'/'.$link);
		if(!$str){
			echo $link;
		}else{
			return $link;
		}
	}

	function isMobile(){
		$useragent=$_SERVER['HTTP_USER_AGENT'];

		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
			return true;
		}else{
			return false;
		}
	}

	function noSpaces($str){
		return str_replace([' ','&'], '', $str);
	}

	function CheckYearDate()
	{
		
		$date = date('Y-m-d');

		$ex = explode('-',$date);
		
		$date = ($ex[0] - 1).'-'.$ex[1].'-'.$ex[2];
		return $date;
	}

	function selcho($val,$chk)
	{
		if(isset($_REQUEST[$val]) && $_REQUEST[$val]==$chk){echo 'selected';}
	}

	function checkRecentDate(array $date)
	{
		if($date[1] == date('Y') OR (date('Y') - $date[1]) == 1)
		{
			return TRUE;
		}else{return FALSE;}

	}

	function returnResponse($result,$msg,$status,$redirect = NULL, $window = NULL)
	{
		$data = array('result'=>$result , 'message' => $msg,'response' => $status, 'redirect' => $redirect, 'window' => $window);
		echo  json_encode($data);
	}

	function billResponse($result,$count,$msg,$status)
	{
		$data = array('result'=>$result, 'count'=>$count, 'message' => $msg,'response' => $status, );
		echo  json_encode($data);
	}


	function calcTime($time){
		
		$bet = explode('.',(time() - $time)/60); return $bet[0].' Min';
	
	}
	// PAGINATION
	function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
	{
	    $pagination = '';
	    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
	        $pagination .= '<ul class="pagination">';
	        
	        $right_links    = $current_page + 3; 
	        $previous       = $current_page - 3; //previous link 
	        $next           = $current_page + 1; //next link
	        $first_link     = true; //boolean var to decide our first link
	        
	        if($current_page > 1){
	            $previous_link = ($previous==0)?1:$previous;
	            $pagination .= '<li class="first"><a href="javascript:;" class="paginat" data-page="1" title="First">&laquo;</a></li>'; //first link
	                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
	                    if($i > 0){
	                        $pagination .= '<li><a href="javascript:;" class="paginat" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
	                    }
	                }   
	            $first_link = false; //set first link to false
	        }
	        
	        if($first_link){ //if current active page is first link
	            $pagination .= '<li class="first active"><a href="javascript:;">'.$current_page.'</a></li>';
	        }elseif($current_page == $total_pages){ //if it's the last active link
	            $pagination .= '<li class="last active"><a href="javascript:;">'.$current_page.'</a></li>';
	        }else{ //regular current link
	            $pagination .= '<li class="active"><a href="javascript:;">'.$current_page.'</a></li>';
	        }
	                
	        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
	            if($i<=$total_pages){
	                $pagination .= '<li><a href="javascript:;" class="paginat" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
	            }
	        }
	        if($current_page < $total_pages){ 
	                $next_link = ($i > $total_pages)? $total_pages : $i;
	                $pagination .= '<li class="last"><a href="javascript:;" class="paginat" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
	        }
	        
	        $pagination .= '</ul>'; 
	    }
	    return $pagination; //return pagination links
	}
	// PAGINATION

	function calcCost($price,$time){
		$calcTime = (time() - $time);
		$Time = $calcTime/60;
		$getPrice = $price/60;
		return $price = number_format($getPrice * $Time,2).' <sup>(EGP)</sup>';
	}

	function TimeHr($Time){
		$hours = floor($Time/60);
		$Minute= $Time%60;
		$total = $hours.','.$Minute;
		return $total;
	} 
	function redirectJs($url){
		echo "<script>window.location.href = '".($url)."';</script>";
	}


	//ERORR 404 
	function error404()
	{
		echo "<h1>Page Not Found</h1>";
		echo "<span>We're sorry, we couldn't find the page you requested.</span>";
	}

	function dd($array,$exit = true){
		echo "<pre>";
		print_r($array);
		echo "</pre>";
		
		if($exit == true)
			exit();
	}