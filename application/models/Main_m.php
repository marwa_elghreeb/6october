<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function select_status($table,$data,array $join)
	{
		$this->db->join($join[0],$join[1],'right');
		$this->db->where($data);
		return $this->db->get($table)->result();
	}

	public function getRow($id,$table,$col)
	{ 
		// A General Function To Get Any Row From Any Table With One Condition
		return $this->db->where($col,$id)->get($table)->row();
	}

	public function countRows($table,$data = NULL)
	{
	 	// A General Function To Get Any Row From Any Table With Array Of Conditions
		if($data != NULL)
			$this->db->where($data);
		return $this->db->get($table)->num_rows();
	}

	public function getRowCond($table,$data = null)
	{ 
		if($data != null)
			$this->db->where($data);
		// A General Function To Get Any Row From Any Table With Array Of Conditions
		return $this->db->get($table)->row();
	}

	public function getResultCond($table,$data,$col,$order)
	{ 
		// A General Function To Get Any Row From Any Table With Array Of Conditions
		return $this->db->where($data)->order_by($col,$order)->get($table)->result();
	}
	
	public function getRowOrder($table,$data,$col,$order)
	{ 
		// Table With Array of Conditions AND ORDER COLUMN
		return $this->db->where($data)->order_by($col,$order)->get($table)->row();
	}
	
	public function getResultLimitEx($table,$input)
	{
		return  $this->db->query("SELECT * FROM `$table` ORDER BY $order LIMIT $limit[0],$limit[1]")->result();	
	}

	public function getResultLimitCount($table,$inputs = NULL)
	{
		$var = array();
		if($inputs != NULL){

			foreach ($inputs as $key => $value) {
				if(!empty($value))
					$var[$key] = $value;
			}
			if(isset($var['student_status']) && $var['student_status'] == 'null')
			{
				$var['student_status'] = null;
				$this->db->where($var);
				unset($var['student_status']);
				$var['student_status'] = 1;

				$this->db->or_where($var);
			}
			if(isset($var['student_name']) && !empty($var['student_name']) )
			{ 
				$this->db->like('student_name' ,$var['student_name'] ,'after');
				unset($var['student_name']);
			}

			else
				$this->db->where($var);

			return $this->db->get($table)->num_rows();

		}	
		else
			$query = $this->db->query("SELECT * FROM `$table`");

		return $query->num_rows();
	}

	public function getResultLimit($table,$limit,$order,$inputs = NULL)
	{
		if($inputs != NULL){

			foreach ($inputs as $key => $value) {
				if(!empty($value))
					$var[$key] = $value;
			}

			
			if(isset($var['student_status']) && $var['student_status'] == 'null')
			{
				$var['student_status'] = null;
				$this->db->where($var);
				unset($var['student_status']);
				$var['student_status'] = 1;

				$this->db->or_where($var);
			}
			if(isset($var['student_name']) && !empty($var['student_name']) )
			{ 
				$this->db->like('student_name' ,$var['student_name'] ,'after');
				unset($var['student_name']);
			}

			else
				$this->db->where($var);
			
			return $this->db->order_by($order)->get($table,$limit[1],$limit[0])->result(); 
		}	
		else
			return $this->db->order_by($order)->get($table,$limit[1],$limit[0])->result();
		
	}

	public function getResultCondLimit($table,$limit,$data,$order)
	{
		$query = $this->db->query("SELECT * FROM `$table` WHERE $data ORDER BY $order LIMIT $limit[0],$limit[1]");
		return $query->result();
	}

	public function getOneRow($select,$table,$where = NULL)
	{ 
		// A General Function To Get Results From Any Table With One Condition
		if($where != NULL )
			$this->db->where($where);
		return $this->db->select($select)->get($table)->row();
	}

	public function getRows($id,$table,$col)
	{ 
		// A General Function To Get Results From Any Table With One Condition
		return $this->db->where($col,$id)->get($table)->result();
	}

	public function getAll($select,$table)
	{ 
		// A General Function To Get Results From Any Table WithOut Condition
		return $this->db->select($select)->get($table)->result();
	}

	public function getAllArray($select,$table)
	{ 
		// A General Function To Get Results From Any Table WithOut Condition
		return $this->db->select($select)->get($table)->result_array();
	}

	public function limitOrderBy($table,$order_by,$sort,$limit) // A General Function To Get Results table Any Table coustom Condition
	{

		$query = $this->db->order_by($order_by,$sort); 
		$query = $this->db->limit($limit); 
		$query = $this->db->get($table);
		return $query->result();

	}

	public function limitOrderWhere($select,$from,$where,$order_by,$sort,$limit) 
		// A General Function To Get Results From Any Table coustom Condition
	{
		$query = $this->db->select($select);
		$query = $this->db->from($from);
		$query = $this->db->where('type',$where);
		$query = $this->db->order_by($order_by,$sort); 
		$query = $this->db->limit($limit); 
		$query = $this->db->get();
		return $query->result();
	}

	public function getLimit($select,$table,$limit)
	{ 
		// A General Function To Get Results From Any Table With Limit Data
		$query = $this->db->select($select);
		$query = $this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result();
	}

	public function getRowsCondLimit($table,$data = null,$limit)
	{ 
		if($data != null)
			$this->db->Where($data);
		// A General Function To Get Results From Any Table With Array of Conditions
		return $this->db->get($table,$limit[1],$limit[0])->result();
	}

	public function getRowsCond($table,$data = null)
	{ 
		if($data != null)
			$this->db->Where($data);
		// A General Function To Get Results From Any Table With Array of Conditions
		return $this->db->get($table)->result();
	}

	public function getRowsCondArray($table,$data = null)
	{ 
		if($data != null)
			$this->db->Where($data);
		// A General Function To Get Results From Any Table With Array of Conditions
		return $this->db->get($table)->result_array();
	}

	public function searchWhere($select,$from,$column,$like,$where)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->like($column,$like);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function CheckLoginInfo($username,$password)
	{
        $row = $this->db->where('username',$username)->get('user')->row();
        if(!$row){return false;}

        $chk=chkPass($password,$row->salt,$row->password);
        if($chk){return $row;}
        else{return false;}
   }

   public function CheckRows(array $data,$table)
   {
   		$row = $this->db->where($data[0],$data[1])->get($table)->row();
        if(!$row)
        	return false;
        else
        	return $row;
   }



}

/* End of file Main_m.php */
/* Location: ./application/models/Main_m.php */