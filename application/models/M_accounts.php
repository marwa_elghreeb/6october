<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_accounts extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function getResultLimitExCount($table,$input){
		
		return $this->db->where('ex_invoice_payment',$input)->query("SELECT * FROM `$table`")->num_rows();
	}



	public function getResultLimitEx($table,$type,$limit,$order){
		if($this->userData->owner != 1)
			return  $this->db->query("SELECT * FROM `$table` WHERE `ex_invoice_created_by` = $this->userData->real_name AND `ex_invoice_created_date` = date('Y-m-d') ORDER BY $order LIMIT $limit[0],$limit[1]")->result();	
		else
			return  $this->db->query("SELECT * FROM `$table` WHERE `ex_invoice_payment` = '$type' ORDER BY $order LIMIT $limit[0],$limit[1]")->result();	
	}


	// INCOME MODULE 
	public function getResultLimitIncomeCount($table ,$where=null){
		if($where != null)
			$this->db->where($where);

		return $this->db->get($table)->num_rows();
	}

	public function getResultLimitIncome($table,$income_id,$limit,$order){
		if($this->userData->owner != 1)
			return  $this->db->query("SELECT * FROM `$table` WHERE `income_invoice_created_by` = $this->userData->real_name AND `ex_invoice_date` = date('Y-m-d') ORDER BY $order LIMIT $limit[0],$limit[1]")->result();	
		else
			return  $this->db->query("SELECT * FROM `$table` WHERE `income_id` = '$income_id' ORDER BY $order LIMIT $limit[0],$limit[1]")->result();	
	}

	public function getResultIncome($table,$income_id,$order){
		if($this->userData->owner != 1)
			return  $this->db->query("SELECT * FROM `$table` WHERE `income_id` = '$income_id' AND `income_invoice_created_by` = $this->userData->real_name AND `ex_invoice_date` = date('Y-m-d') ORDER BY $order")->result();	
		else
			return  $this->db->query("SELECT * FROM `$table` WHERE `income_id` = '$income_id' ORDER BY $order")->result();	
	}


	//GET ALL INVOICES PER DAYS
	public function income_date_invoice_res($table,$date = null,$limit){
		if($this->userData->owner != 1)
		{
			$date = (object) array('from' => date('Y-m-d'), 'to' => date('Y-m-d'));
		}

		if(!empty($date))
			return  $this->db->query("SELECT * FROM `oct_$table` WHERE `date` >= '$date->from' AND `date` <= '$date->to' ORDER BY `date` DESC LIMIT $limit[0],$limit[1]")->result();
		else
			return  $this->db->query("SELECT * FROM `oct_$table` ORDER BY `date` DESC LIMIT $limit[0],$limit[1]")->result();
	}


	// COUNT INVOICES PER DAY 
	public function income_date_invoice_numrows($table,$date = null)
	{
		if($this->userData->owner != 1)
		{
			$date = (object) array('from' => date('Y-m-d'), 'to' => date('Y-m-d'));
		}

		if(!empty($date))
			return  $this->db->query("SELECT * FROM `oct_$table` WHERE `date` >= '$date->from' AND `date` <= '$date->to' ORDER BY `income_date_invoice_id` DESC")->num_rows();
		else
			return  $this->db->query("SELECT * FROM `oct_$table` ORDER BY `income_date_invoice_id` DESC")->num_rows();
	}


	// NUM ROWS STUDENT INCOME 
	public function student_income_invoice_numrows($table,$val = null)
	{

		if(!empty($val)){
			$val['income_invoices'] = 1;
			if(!empty($val['student_name']))
				$this->db->like('student_name',$val['student_name'],'both');

			if(!empty($val['school_year']))
				$val['school_year'] = ($val['school_year']+1).'-'.$val['school_year'];

			return $this->db->where($val)->order_by('student_id','DESC')->get($table)->num_rows();
		}
		else
			return $this->db->where('income_invoices',1)->order_by('student_id','DESC')->get($table)->num_rows();
	}

	// GET STUDENT INCOME 
	public function student_income_invoice_result($table,$limit,$val = null)
	{
		if(!empty($val)){
			$val['income_invoices'] = 1;
			if(!empty($val['student_name']))
				$this->db->like('student_name',$val['student_name'],'both');

			if(!empty($val['school_year']))
				$val['school_year'] = ($val['school_year']+1).'-'.$val['school_year'];

			return $this->db->where($val)->order_by('student_id','DESC')->get($table,$limit[1],$limit[0])->result();
		}
		else
			return $this->db->where('income_invoices',1)->order_by('student_id','DESC')->get($table,$limit[1],$limit[0])->result();
	}



}