<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller
{

    public function index()
    {   
        $data['users'] = $this->main_m->getRowsCond('user',array('delete' => 0));
        loadStaticContent('members/members',$data,array('script' => 'ajax/membersJs.php'));
    }

    public function add_member()
    {   
        $data['controllers'] = controllers();

        loadStaticContent('members/add_member',$data,array('script' => 'ajax/membersJs.php'));
    }

    public function edit_member($user_id)
    {   
        $data['controllers'] = controllers();

        $data['member'] = $this->main_m->getRowCond('user',array('user_id' => $user_id));

        loadStaticContent('members/edit_member',$data,array('script' => 'ajax/membersJs.php'));
    }


    public function insert_member()
    {
        $real_name= $this->input->post('real_name');
        $username = $this->input->post('username');
        $permission = $this->input->post('permission');
        $password = $this->input->post('password');
        $conf_password = $this->input->post('conf_password');
        
        
        $this->form_validation->set_rules('real_name', 'الاسم بالكامل', 'trim|required|xss_clean');
        $this->form_validation->set_rules('username', 'اسم المستخدم', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'كلمية المرور', 'trim|required|xss_clean');
        $this->form_validation->set_rules('conf_password', 'تأكيد كلمية المرور', 'trim|required|xss_clean');
            
        if($this->form_validation->run() == FALSE)
            returnResponse(NULL,'<div class="alert alert-danger" role="alert"> '.validation_errors().' </div>',FALSE,NULL);
            
        else
        {

            if($password != $conf_password )
            {
                returnResponse(NULL,' كلمة المرور غير متطابقة ',FALSE,NULL);
            }
            else
            {
                $password = getPass($this->input->post('password'));
                if($this->main_m->countRows('user',array('username' => $username)) <= 0)
                {
                    $data = array(
                        'username'  => $username,
                        'real_name' => $real_name,
                        'password'  => $password['pass'],
                        'salt'      => $password['salt'],
                        'created_by'=> $this->userData->real_name,
                        'permission'=> json_encode($permission),
                    );
                    $this->db->insert('user',$data);  

                    returnResponse($data,'تم تسجيل العضو بنجاح',true,base_url().'members'); 
                }
                else
                {
                    returnResponse(NULL,'العضو مسجل مسبقا',false,NULL); 

                }
            }
        }
    }

    public function edit_member_data()
    {
        $data = array();
        $real_name= $this->input->post('real_name');
        $username = $this->input->post('username');
        $permission = $this->input->post('permission');
        $password = $this->input->post('password');
        $user_id = $this->input->post('user_id');
        
        $this->form_validation->set_rules('real_name', 'الاسم بالكامل', 'trim|required|xss_clean');
        $this->form_validation->set_rules('username', 'اسم المستخدم', 'trim|required|xss_clean');
        $this->form_validation->set_rules('user_id', 'USID', 'trim|required|xss_clean');
            
        if($this->form_validation->run() == FALSE)
            returnResponse(NULL,'<div class="alert alert-danger" role="alert"> '.validation_errors().' </div>',FALSE,NULL);
            
        else
        {
            if($this->main_m->countRows('user',array('username' => $username, 'user_id !=' => $user_id)) <= 0)
            {
                if(!empty($password))
                {

                    if($password == $this->input->post('conf_password') )
                    {
                        $password = getPass($this->input->post('password'));
                        $data['password']  = $password['pass'];
                        $data['salt']      = $password['salt'];
                    }
                    else
                    {
                        returnResponse(NULL,'كلمية المرور غير متطابقة',FALSE,NULL);
                        exit;
                    }
                }

                $data['username']  = $username;
                $data['real_name'] = $real_name;
                $data['permission'] = json_encode($permission);
                $this->db->where('user_id',$user_id)->update('user',$data);  
                returnResponse($data,'تم تعديل بيانات العضو',true,base_url().'members'); 
            }
            else
                returnResponse(NULL,'العضو مسجل مسبقا',false,NULL); 
        }
    }


}


