<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function acquaintance()
	{

		$year = $this->input->get('year_tb_id');

		if($year == null)
		{
			$year_tb = $this->db->where('active',1)->get('year_tb')->row();
			$year_tb_id = $year_tb->year_tb_id;
		}
		else
		{
			$year_tb = $this->main_m->getRowCond('year_tb',array('year_tb_id' => $year));
			$data['year_type'] = $year_tb->year_type;
			$year_tb_id = $year_tb->year_tb_id;
		}

		$data['years'] = $this->db->order_by('year_type','DESC')->get('year_tb')->result();

		$data['acquaintance'] = $this->db->get('acquaintance')->result();
		
		foreach ($data['acquaintance'] as $acquaintance_key => $acquaintance_value) {
			$data['acquaintance'][$acquaintance_key]->num = $this->main_m->countRows('student',array('acquaintance_way' => $acquaintance_value->acquaintance_id, 'year_tb_id' => $year_tb_id));
			$array_count[] = $data['acquaintance'][$acquaintance_key]->num;
		}

		$data['year_tb_id'] = $year_tb_id;
		$data['total'] = array_sum($array_count); 
		loadStaticContent('statistics/statistics_acquaintance',$data,array('script' => 'ajax/statisticsJs.php'));	
	}

	public function print_acquaintance($year)
	{

		$year = $year;

		if($year == null)
		{
			$year_tb = $this->db->where('active',1)->get('year_tb')->row();
			$year_tb_id = $year_tb->year_tb_id;
		}
		else
		{
			$year_tb = $this->main_m->getRowCond('year_tb',array('year_tb_id' => $year));
			$data['year_type'] = $year_tb->year_type;
			$year_tb_id = $year_tb->year_tb_id;
		}

		$data['acquaintance'] = $this->db->get('acquaintance')->result();
		
		foreach ($data['acquaintance'] as $acquaintance_key => $acquaintance_value) {
			$data['acquaintance'][$acquaintance_key]->num = $this->main_m->countRows('student',array('acquaintance_way' => $acquaintance_value->acquaintance_id, 'year_tb_id' => $year_tb_id));
			$array_count[] = $data['acquaintance'][$acquaintance_key]->num;
		}

		$data['total'] = array_sum($array_count); 
		loadStaticContent('print/print_acquaintance',$data,array('script' => 'ajax/statisticsJs.php'));	
	}


	public function statistics_schools()
	{
		$data['years'] = $this->db->order_by('year_type','ASC')->get('year_tb')->result();
		loadStaticContent('statistics/statistics_schools',$data,array('script' => 'ajax/statisticsJs.php'));
	}
	public function print_statistics_schools()
	{
		$years = $this->input->post('years');
		if($years != null)
		{
			sort($years);
			foreach($years as $value)
			{
				$post[] = $value; 
			}
		}

		if(isset($post) & !empty($post))
			$this->db->where_in('year_type',$post);

		$data['years'] = $this->db->order_by('year_type','ASC')->get('year_tb')->result();
		$governorate = $this->db->get('governorate')->result();

			foreach ($governorate as $governorate_key => $governorate)
			{
				$city[] = $this->main_m->getRowsCond('city',array('governorate_id' => $governorate->governorate_id));
				$data['governorate'][] = $governorate;
			}

		loadStaticContent('print/print_statistics_schools',$data,array('script' => 'ajax/statisticsJs.php'));	

	
	}
	public function statistics_administration()
	{
		$data['years'] = $this->db->order_by('year_type','ASC')->get('year_tb')->result();
		loadStaticContent('statistics/statistics_administration',$data,array('script' => 'ajax/statisticsJs.php'));	
	}

	public function print_statistics_administration()
	{

		$years = $this->input->post('years');
		if($years != null)
		{
			sort($years);
			foreach($years as $value)
			{
				$post[] = $value; 
			}
		}

		if(isset($post) & !empty($post))
			$this->db->where_in('year_type',$post);

		$data['years'] = $this->db->order_by('year_type','ASC')->get('year_tb')->result();
		$governorate = $this->db->get('governorate')->result();

			foreach ($governorate as $governorate_key => $governorate)
			{
				$city[] = $this->main_m->getRowsCond('city',array('governorate_id' => $governorate->governorate_id));
				$data['governorate'][] = $governorate;
			}

		loadStaticContent('print/print_statistics_administration',$data,array('script' => 'ajax/statisticsJs.php'));	

	}
	public function statistics_citys()
	{
		$data['years'] = $this->db->order_by('year_type','ASC')->get('year_tb')->result();
		loadStaticContent('statistics/statistics_citys',$data,array('script' => 'ajax/statisticsJs.php'));	
	}
	public function print_statistics_citys()
	{
		$years = $this->input->post('years');
		if($years != null)
		{
			sort($years);
			foreach($years as $value)
			{
				$post[] = $value; 
			}
		}

		if(isset($post) & !empty($post))
			$this->db->where_in('year_type',$post);

		$data['years'] = $this->db->order_by('year_type','ASC')->get('year_tb')->result();
		$governorate = $this->db->get('governorate')->result();

			foreach ($governorate as $governorate_key => $governorate)
			{
				$city[] = $this->main_m->getRowsCond('city',array('governorate_id' => $governorate->governorate_id));
				$data['governorate'][] = $governorate;
			}

		loadStaticContent('print/print_statistics_citys',$data,array('script' => 'ajax/statisticsJs.php'));	

	}


	public function section_statistics(){

		$data['year_tb_id'] = $this->input->post('year_tb_id');
		
		$data['years'] = $this->db->order_by('year_type','DESC')->get('year_tb')->result();

		loadStaticContent('statistics/statistics_section',$data,array('script' => 'ajax/statisticsJs.php'));	
	}

	public function print_sections_statistics()
	{
		
		$data['year_tb_id'] = $this->input->get('year_tb_id');
		$data['year_type'] = $this->main_m->getRowCond('year_tb',array('year_tb_id' => $data['year_tb_id']));
		$data['years'] = $this->db->order_by('year_type','DESC')->get('year_tb')->result();

		$data['months'][0] = array('يناير ' => 'January', 'فبراير ' => 'February',); 
		$data['months'][1] = array('مارس '  => 'March', 'ابريل ' => 'April',);
		$data['months'][2] = array('مايو ' => 'May', 'يونيو ' => 'Jun',);
		$data['months'][3] = array('يوليو ' => 'July', 'اغسطس '  => 'August',);
		$data['months'][4] = array('سبتمبر ' => 'September','اكتوبر  ' => 'October',);
		$data['months'][5] = array( 'نوفمبر  ' => 'November', 'ديسمبر  ' => 'December');

		$data['sections'] = $this->db->get('section')->result();
		

		loadStaticContent('print/print_sections_statistics',$data,array('script' => 'ajax/statisticsJs.php'));	
	}


	public function birth_blace_statistics(){

		$year = date('Y');
		echo $year;
		exit();
		for ($i=0; $i < 30 ; $i++) { 
			
		}
	}
}
