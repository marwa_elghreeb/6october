<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Students_affairs extends CI_Controller
{

    public function index()
    {
        $data = $this->main_m->limitOrderBy('student', 'created_date', 'DESC', 10);
        $data['section'] = $this->main_m->getAll('*', 'section');
        $data['class'] = $this->main_m->getAll('*', 'class');
        loadStaticContent('affairs/students', $data, array('script' => 'ajax/affairsJs.php'));
    }

    public function edit_student($student_id)
    {
        if (!intval($student_id))
            redirectJs(base_url() . 'tudents_affairs');

        $data['governorate'] = $this->main_m->getAll('*', 'governorate');
        $data['qualification'] = $this->main_m->getAll('*', 'qualification');
        $data['section'] = $this->main_m->getAll('*', 'section');
        $data['class'] = $this->main_m->getAll('*', 'class');
        $data['certificate_type'] = $this->main_m->getAll('*', 'certificate_type');
        $data['study_type'] = $this->main_m->getAll('*', 'study_type');
        $data['study_status'] = $this->main_m->getAll('*', 'study_status');
        $data['acquaintance'] = $this->main_m->getAll('*', 'acquaintance');
        $data['student'] = $this->main_m->getRowCond('student', array('student_id' => $student_id));
        $data['administration'] = $this->main_m->getRowCond('administrations', array('administration_id' => $data['student']->management_learning));
        $data['school'] = $this->main_m->getRowsCond('school', array('administration_id' => $data['student']->management_learning));
        $data['state_administration'] = @$this->main_m->getRowsCond('administrations', array('governorate_id' => $data['administration']->governorate_id));

        loadStaticContent('affairs/edit_student', $data, array('script' => 'ajax/affairsJs.php'));
    }


    public function studentAjax()
    {
        $post_data = array();
        $result = array();
        $result['table'] = '';
        $result['pagination'] = '';
        $item_per_page = 10; //item to display per page
        $page = $this->input->post('page');
        $post_data = (array)json_decode(str_replace("'", '"', $this->input->post('post_data')));
        //Get page number from Ajax
        if ($page != NULL) {
            $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
            if (!is_numeric($page_number)) {
                die('Invalid page number!');
            } //incase of invalid page number
        } else {
            $page_number = 1; //if there's no page number, set it to 1
        }

        //get total number of records from database
        $get_total_rows = $this->main_m->getResultLimitCount('oct_student', $post_data); //hold total records in variable

        //break records into pages
        $total_pages = ceil($get_total_rows / $item_per_page);

        //position of records
        $page_position = (($page_number - 1) * $item_per_page);
        $limit = array($page_position, $item_per_page);
        //Limit our results within a specified range.
        $student_data = $this->main_m->getResultLimit('oct_student', $limit, 'student_code DESC', $post_data);

        $i = 0;
        foreach ($student_data as $student_key => $student_value) {
            $i++;
            if ($i % 2 == 0)
                $className = 'even';
            else
                $className = 'odd';

            $section = $this->main_m->getRowCond('section', array('section_id' => $student_value->section_id));
            $class = $this->main_m->getRowCond('class', array('class_id' => $student_value->class_id));
            $class = $this->main_m->getRowCond('class', array('class_id' => $student_value->class_id));
            $invoice_open_file = $this->main_m->getRowCond('invoice_open_file', array('student_id' => $student_value->student_id));

            $result['table'] .=
                '<tr role="row" class="' . $className . '">
             			<td>' . $i . '</td>
                      	<td><img src="' . CheckImg($student_value->profile_photo) . '" alt="" class="img-thumbnail" /></td>
                      	<td>' . $student_value->student_name . '</td>
                      	<td>' . @$section->section_name . '</td>
                      	<td>' . @$class->class_name . '</td>
                      	<td>' . @$student_value->created_by . '</td>
                      	<td>' . @$student_value->student_code . '</td>
                      	<td>
                      		<div class="btn-group">
	                            <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
	                                اعدادات
	                                <span class="caret"></span>
	                            </button>
	                            <ul class="dropdown-menu" role="menu">
	                                <li>
	                                    <a href="' . base_url() . 'students_affairs/student_page/' . $student_value->student_id . '">مشاهدة ملف الطالب <i class="zmdi zmdi-eye zmdi-hc-fw"></i></a>

	                                </li>
	                                <li>
                      					<a href="' . base_url() . 'students_affairs/edit_student/' . $student_value->student_id . '" > تعديل <i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
	                                </li>
	                                <li>
                      					<a href="javascript:;" data-url="' . base_url() . 'home/delete/" data-id="' . $student_value->student_id . '" data-table="student" data-join="invoice_open_file" data-column="student_id" class="delete"> حذف  <i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>
	                                </li>
	                            </ul>
	                        </div>

                      	</td> 
                    </tr>';

        }

        //EDIT BUTTON

        $result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
        echo returnResponse($result, $get_total_rows . ' Data Found', true);
    }


    public function student_page($student_id)
    {
        $data = $this->main_m->getRowCond('student', array('student_id' => $student_id));
        $data->invoice_file = $this->main_m->getRowCond('invoice_open_file', array('student_id' => $student_id));

        $city = $this->main_m->getRowCond('city', array('city_id' => $data->city));
        $state = $this->main_m->getRowCond('governorate', array('governorate_id' => $data->state));
        if (!empty($data->birth_place))
            $birth_place = $this->main_m->getRowCond('city', array('city_id' => $data->birth_place));

        $class = $this->main_m->getRowCond('class', array('class_id' => $data->class_id));
        $section = $this->main_m->getRowCond('section', array('section_id' => $data->section_id));
        $study_type = $this->main_m->getRowCond('study_type', array('study_id' => $data->study_type));
        $study_status = $this->main_m->getRowCond('study_status', array('status_id' => $data->study_status));
        $certificate_type = $this->main_m->getRowCond('certificate_type', array('certificate_id' => $data->certificate_type));

        $created_date = explode(' ', $data->created_date);
        $data->created_date = $created_date[0];
        $data->birth_place = @$birth_place->city_name;
        $data->class = $class->class_name;
        $data->city = $city->city_name;
        $data->state = $state->governorate_name;
        $data->section = @$section->section_name;
        $data->certificate_type = @$certificate_type->certificate_type;
        $data->study_type = @$study_type->study_type;
        $data->study_status = $study_status->study_status;

        $data->tuition_fees = $this->main_m->getRowCond('tuition_fees', array('tuition_fees_id =' => $data->tuition_fees_id));
        $data->installment = $this->db->order_by('type', 'ASC')->where(array('tuition_fees_id' => $data->tuition_fees_id))->get('installment')->result();
        $data->cash_invoice = $this->main_m->getRowCond('invoices', array('student_id' => $student_id, 'class_id' => $data->class_id, 'payment' => 'tuition_fees_cash'));
        $data->installment_invoice = $this->main_m->getRowCond('invoices', array('student_id' => $student_id, 'class_id' => $data->class_id, 'payment' => 'tuition_fees_installment'));

        $count_installment = $this->main_m->getAllArray('*', 'installment');
        foreach ($count_installment as $installment_key => $value) {
            $installment_value[] = $value['value'];
        }

        $data->count_installment = array_sum($installment_value);
        loadStaticContent('affairs/student_page', $data, array('script' => 'ajax/affairsJs.php'));
    }

    // Student Graduates
    public function graduates()
    {
        $data = $this->main_m->limitOrderBy('student', 'created_date', 'DESC', 10);
        $data['section'] = $this->main_m->getAll('*', 'section');
        $data['class'] = $this->main_m->getAll('*', 'class');
        loadStaticContent('affairs/graduated_students', $data, array('script' => 'ajax/affairsJs.php'));
    }


    public function graduated_students_Ajax()
    {
        $post_data = array();
        $result = array();
        $result['table'] = '';
        $result['pagination'] = '';
        $item_per_page = 10; //item to display per page
        $page = $this->input->post('page');
        $post_data = (array)json_decode(str_replace("'", '"', $this->input->post('post_data')));
        //Get page number from Ajax
        if ($page != NULL) {
            $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
            if (!is_numeric($page_number)) {
                die('Invalid page number!');
            } //incase of invalid page number
        } else {
            $page_number = 1; //if there's no page number, set it to 1
        }

        //get total number of records from database
        $get_total_rows = $this->main_m->getResultLimitCount('oct_student', $post_data); //hold total records in variable

        //break records into pages
        $total_pages = ceil($get_total_rows / $item_per_page);

        //position of records
        $page_position = (($page_number - 1) * $item_per_page);
        $limit = array($page_position, $item_per_page);
        //Limit our results within a specified range.
        $student_data = $this->main_m->getResultLimit('oct_student', $limit, 'student_code DESC', $post_data);

        $i = 0;
        foreach ($student_data as $student_key => $student_value) {
            $i++;
            if ($i % 2 == 0)
                $className = 'even';
            else
                $className = 'odd';

            $section = $this->main_m->getRowCond('section', array('section_id' => $student_value->section_id));
            $class = $this->main_m->getRowCond('class', array('class_id' => $student_value->class_id));
            $class = $this->main_m->getRowCond('class', array('class_id' => $student_value->class_id));
            $invoice_open_file = $this->main_m->getRowCond('invoice_open_file', array('student_id' => $student_value->student_id));

            if (!file_exists('assets/uploads/' . $student_value->profile_photo)) {
                $student_value->profile_photo = 'no_avatar.jpg';
            }
            $result['table'] .=
                '<tr role="row" class="' . $className . '">
	             			<td>' . $i . '</td>
	                      	<td><img src="' . base_url() . 'assets/uploads/' . $student_value->profile_photo . '" alt="" class="img-thumbnail" /></td>
	                      	<td>' . $student_value->student_name . '</td>
	                      	<td>' . @$section->section_name . '</td>
	                      	<td>' . @$class->class_name . '</td>
	                      	<td>' . @$student_value->created_by . '</td>
	                      	<td>' . @$student_value->student_code . '</td>
	                      	<td>
	                      		<a ata-toggle="tooltip" data-placement="top" title="حذف" href="javascript:;" data-url="' . base_url() . 'home/delete/" data-id="' . $student_value->student_id . '" data-table="student" data-join="invoice_open_file" data-column="student_id" style="min-width:38px;width:38px;height:38px;font-size:22px" class="btn btn-primary btn-fab delete"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>
	                      		<a ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="تعديل" href="' . base_url() . 'students_affairs/edit_graduated_student/' . $student_value->student_id . '" class="btn btn-info btn-fab"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>

	                      		<a ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="مشاهدة ملف الطالب" href="' . base_url() . 'students_affairs/student_page/' . $student_value->student_id . '" class="btn btn-default btn-fab"><i class="zmdi zmdi-eye zmdi-hc-fw"></i></a>

	                      	</td> 
	                    </tr>';

        }

        //EDIT BUTTON

        $result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
        echo returnResponse($result, $get_total_rows . ' Data Found', true);

    }

    public function edit_graduated_student($student_id)
    {
        if (!intval($student_id))
            redirectJs(base_url() . 'students_affairs');

        $data['governorate'] = $this->main_m->getAll('*', 'governorate');
        $data['qualification'] = $this->main_m->getAll('*', 'qualification');
        $data['section'] = $this->main_m->getAll('*', 'section');
        $data['class'] = $this->main_m->getAll('*', 'class');
        $data['certificate_type'] = $this->main_m->getAll('*', 'certificate_type');
        $data['study_type'] = $this->main_m->getAll('*', 'study_type');
        $data['study_status'] = $this->main_m->getAll('*', 'study_status');
        $data['acquaintance'] = $this->main_m->getAll('*', 'acquaintance');
        $data['student'] = $this->main_m->getRowCond('student', array('student_id' => $student_id));
        $data['student_graduated'] = $this->main_m->getRowCond('graduated_students', array('student_id' => $student_id));

        $data['administration'] = $this->main_m->getRowCond('administrations', array('administration_id' => $data['student']->management_learning));

        $data['state_administration'] = @$this->main_m->getRowsCond('administrations', array('governorate_id' => $data['administration']->governorate_id));

        loadStaticContent('affairs/edit_graduated_student', $data, array('script' => 'ajax/affairsJs.php'));
    }

    public function student_search()
    {
        $student_name = $this->input->post('student_name');
        $student_code = $this->input->post('student_code');
        $class = $this->input->post('class');
        $section = $this->input->post('section');
    }


    public function export_data()
    {

        $allYear = $this->db->get('oct_year_tb');
        $data = '';
        $i = 1;

        $data = '<table bordered="1" class="table" >';

        foreach ($allYear->result() as $year) {
            $data .= '<tr >
                         <td colspan = "41" > ' . $year->year_type . '</td >
                      </tr >';

            $this->db->where(array('year_tb_id' => $year->year_tb_id));  //get('oct_student')->
            $studentData = $this->db->get('oct_student');
            $data .= ' <tr> 
                 
                    <th>مسلسل</th>
                    <th>السنه الدراسيه </th> 
                    <th> كود  الطالب</th>
					<th>   كود الكارت</th>
                    <th>اسم الطالب</th>
                    <th> الجنسيه</th>
				    <th> الديانه </th>
					<th> النوع</th>
					<th> محافظه محل الميلاد </th>
					<th> محل الميلاد</th>
					<th>  تاريخ الميلاد</th>
					<th> الرقم القومى</th> 
					<th> محل الرقم القومى</th>
                    <th>اسم والى الامر </th>
					<th> وظيفة والى الامر</th>
                    <th>صلة القرابة </th>
					<th> المحافظة</th>
                    <th>المدينة </th>
					<th> المركز</th>
                    <th>العنوان </th>
					<th> الهاتف</th>
                    <th>البريد الالكترونى </th>
					<th> المؤهل الدراسى</th>
                    <th>المجموع </th>
					<th> المدرسة</th>
                    <th>محافظة الادارة التعليمية </th>
                    <th> الفرقه</th>
                    <th>القسم </th>
					<th> نوع الدراسة</th>
                    <th>حالة الدراسة </th>
					<th> الشهادة المراد الحصول عليها</th>
                    <th>وسيلة التعارف  (عن طريق)</th>
					<th> وسيلة التعارف</th>
                    <th>فاتوره فتح الملف </th>
                    <th>تاريخ التخرج </th>
					<th> السنه الدراسيه</th>
					<th> ملاحظات</th>
                    <th>اضافه بواسطه </th>
					<th> تاريخ الانشاء</th>
                    <th>تاريخ التسجيل </th>
                    <th>اجمالى المدفوع  </th>
                 </tr>
            ';
            $t = 0;
            foreach ($studentData->result() as $row) {

                $section = $this->main_m->getRowCond('section', array('section_id' => $row->section_id));
                $class = $this->main_m->getRowCond('class', array('class_id' => $row->class_id));
                $governorate = $this->main_m->getRowCond('governorate', array('governorate_id' => $row->birth_place_governorate));
                $state = $this->main_m->getRowCond('governorate', array('governorate_id' => $row->state));
                $city = $this->main_m->getRowCond('city', array('city_id' => $row->city));
                $qualification = $this->main_m->getRowCond('qualification', array('qualification_id' => $row->qualification));
                $study_type = $this->main_m->getRowCond('study_type', array('study_id' => $row->study_type));
                $study_status = $this->main_m->getRowCond('study_status', array('status_id' => $row->study_status));
                $certificate_type = $this->main_m->getRowCond('certificate_type', array('certificate_id' => $row->certificate_type));
                $acquaintance = $this->main_m->getRowCond('acquaintance', array('acquaintance_id' => $row->acquaintance_way));
                $year = $this->main_m->getRowCond('year_tb', array('year_tb_id' => $row->year_tb_id));
                
                $this->db->select_sum('income_invoice_value');
                $this->db->where(array('income_student_id' => $row->student_id)); 
                $result = $this->db->get('oct_income_invoice')->row();  
               
              //  $total  =  $this->db->query('SELECT SUM(income_invoice_value) as total  FROM oct_income_invoice WHERE `income_student_id` = '.$row->student_id);
                  

                if ($row->religion == 0) {
                    $religion = 'مسلم';
                } else {
                    $religion = 'مسيحى';
                }
                if ($row->gender == 0) {
                    $gender = 'ذكر';
                } else {
                    $gender = 'انثى';
                }

                $data .= '<tr> 
             	
                        <td>' . $i . '</td>
                        <td>' . @$year->year_type . '</td>
						<td>' . $row->student_code . '</td>
						<td>' . $row->card_code . '</td>
                        <td>' . $row->student_name . '</td>
						<td>' . $row->nationality . '</td>
			           	<td>' . $religion . '</td>
                        <td>' . $gender . '</td>
						<td> ' . @$governorate->governorate_name . '</td>
						<td>' . $row->birth_place . '</td>
                        <td>' . $row->date_of_birth . '</td>
						<td>' . $row->national_id . '</td>
						<td>' . $row->national_place . '</td>
                        <td>' . $row->guardian_name . '</td>
						<td>' . $row->guardian_work . '</td>
						<td>' . $row->relative_relation . '</td>
                        <td>' . @$state->governorate_name . '</td>
						<td>' . @$city->city_name . '</td>
						<td>' . $row->area_name . '</td>
                        <td>' . $row->address . '</td>
						<td>' . $row->student_phone . '</td>
						<td>' . $row->student_email . '</td>
                        <td>' . @$qualification->qualification . '</td>
						<td>' . $row->qualification_grade . '</td>
						<td>' . $row->student_school . '</td>
                        <td>' . $row->management_learning . '</td>
						<td>' . @$class->class_name . '</td>
						<td>' . @$section->section_name . '</td>
						<td>' . @$study_type->study_type . '</td>
						<td>' . @$study_status->study_status . '</td>
                        <td>' . @$certificate_type->certificate_type . '</td>
						<td>' . $row->acquaintance_student_name . '</td>
						<td>' . @$acquaintance->acquaintance_name . '</td>
                        <td>' . $row->open_file_bill_number . '</td>
						<td>' . $row->graduation_date . '</td>
                        <td>' . $row->school_year . '</td>
						<td>' . $row->comment . '</td>
                        <td>' . $row->created_by . '</td>
                        <td>' . $row->created_in . '</td>
						<td>' . $row->created_date . '</td>
						<td>' .  $result->income_invoice_value . '</td>
                      </tr>';

                $i++;
            }


        }

        $data .= '<tr >
                         <td colspan = "41" > </td >
                      </tr >';
        $data .= '</table>';

        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=student_names.xls');

        echo $data;
        // exit();

    }


}