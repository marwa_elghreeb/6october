<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{

		if($this->userData !== false ){redirect(base_url('/home'),'refresh');}
		$this->load->view('login');
	
	}
	public function logout(){
		# CHECK OUT USER Attendees 
		$this->db->where('username',$this->session->userdata('username'))->update('attendees',array('check_out' => date("F j, Y, g:i a")));
		
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('logged_in');
		setcookie('userCookies', null, -1, '/');
		$this->session->sess_destroy();
		redirect(base_url().'auth/');exit;
	}
	
	public function login()
	{
		//load helpers 
		$this->load->helper('form');
	    $this->load->library('form_validation');
	    $this->load->helper('security');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if($this->form_validation->run() == FALSE)
            returnResponse(NULL,' يجب كتابة اسم المستخدم وكلمة المرور',FALSE,NULL);
        
        else{

        	$username 		= $this->input->post('username');
        	$password 		= $this->input->post('password');
        	$rememberMe 	= $this->input->post('remember');

        	// Check username NotFound In DATA BASE
	        	$checkLogin = $this->main_m->CheckLoginInfo($username,$password);
	        	
	    		if($checkLogin !== FALSE)
	    		{
	    			if($rememberMe != NULL){
	    				setcookie('userCookies', $checkLogin->user_id, time() + (86400 * 30), "/");
	    			}
	    			// INSERT USER Attendees
	    			$this->db->insert('attendees',array(
	    				'username' 	=> $checkLogin->username,
	    				'real_name' => $checkLogin->real_name,
	    				'check_in' 	=> date("F j, Y, g:i a"),
	    				'date' 		=> date('Y-m-d')));
				   	// Set user Session
				   	$sessionData = array('username' => $checkLogin->username ,'real_name' => $checkLogin->real_name, 'user_id'=>$checkLogin->user_id, 'logged_in' => TRUE);
				   	$this->session->set_userdata($sessionData);
				   	returnResponse(NULL,' تم تسجيل الدخول بنجاح', true, base_url('home'));
			   	}
			   	else
			   		returnResponse(NULL ,' يرجى التأكد من اسم المستخدم وكلمة المرور ' ,FALSE ,NULL);
        	}
    }
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */