<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_accounts');
		require_once('num_to_ar.php');
	}


	public function reception_student()
	{
		loadStaticContent('accounts/account_reception_students',NULL,array('script' => 'ajax/receptionJs.php'));
	}
	public function reception_studentAjax()
	{

		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page      = 10; //item to display per page
		$page = $this->input->post('page');
		$post_data = (array)json_decode(str_replace("'",'"',$this->input->post('post_data')));

		$post_data['student_status'] = null;
		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}

		//get total number of records from database
		$get_total_rows = $this->main_m->getResultLimitCountOpenedFile('oct_student',$post_data); //hold total records in variable
		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range.
		$student_data = $this->main_m->getResultLimitOpenedFile('oct_student',$limit,'student_id DESC',$post_data);

		$i = 0; foreach ($student_data as $student_key => $student_value) { $i++;
			if ($i % 2 == 0)
				$className = 'even';
			else
				$className = 'odd';

			$section = $this->main_m->getRowCond('section',array('section_id' => $student_value->section_id));
			$class = $this->main_m->getRowCond('class',array('class_id' => $student_value->class_id));
			$class = $this->main_m->getRowCond('class',array('class_id' => $student_value->class_id));
			$invoice_open_file = $this->main_m->getRowCond('invoice_open_file',array('student_id' => $student_value->student_id));

			$open_filePayment = $this->main_m->getRowCond('invoice_open_file',array('student_id' => $student_value->student_id));
			if(empty($open_filePayment))
			{
				$icon = '<i style="font-size:20px;color:red" class="zmdi zmdi-close"></i>';
				$open_file = '<a data-id="'.$student_value->student_id.'" id="payOpenFilePayment" href="javascript:;" data-url="'.base_url().'reception/openStudentFile/">دفع رسوم فتح الملف   <i class="zmdi zmdi-file"></i></a>';
			}
			else
			{
				$icon = '<i style="font-size:20px;color:green" class="zmdi zmdi-check"></i>';
				$open_file = '<a target="_blank" href="'.base_url().'accounts/print_invoices/open_file/'.$student_value->student_id.'/'.$open_filePayment->invoice_file_id.'" data-url="'.base_url().'reception/openStudentFile/">طباعة الايصال  <i class="zmdi zmdi-print"></i></a>';
			}

			$result['table'] .=
						'<tr role="row" class="'.$className.'">
	             			<td>'.$i.'</td>
	                      	<td><img src="'.CheckImg($student_value->profile_photo).'" alt="" class="img-thumbnail" /></td>
	                      	<td>'.$student_value->student_name.'</td>
	                      	<td>'.@$section->section_name.'</td>
	                      	<td>'.@$class->class_name.'</td>
	                      	<td>'.@$student_value->created_by.'</td>
	                      	<td>'.@returnMonthFormate($student_value->created_in).'</td>
	                      	<td>'.@$icon.'</td>
	                      	<td>
		                      	<div class="btn-group">
		                            <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
		                                اعدادات
		                                <span class="caret"></span>
		                            </button>
		                            <ul class="dropdown-menu" role="menu">
		                            <li>
		                                '.$open_file.'
		                            </li>
		                                <li>
		                                	<a title="التقديم "  href="'.base_url().'accounts/apply_student_page/'.$student_value->student_id.'"> التقديم <i class="zmdi zmdi-assignment-o"></i></a>
		                                </li>
		                                <li>
		                                	<a title="حذف" href="javascript:;" data-url="'.base_url().'home/delete/" data-id="'.$student_value->student_id.'" data-table="student" data-join="invoice_open_file" data-column="student_id" style="" class="delete"> حذف  <i class="zmdi zmdi-delete"></i></a>
		                                </li>
		                            </ul>
		                        </div>
	                      	</td>
	                    </tr>';

		}





		$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
		echo returnResponse($result,$get_total_rows.' Data Found',true);
	}

	public function apply()
	{
		$data = $this->main_m->limitOrderBy('student','created_date','DESC',10);
		loadStaticContent('accounts/apply',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function apply_student(){
		$data = array();
		$where = array();
		$result = array();
		// $where['student_status'] = 1;
		foreach ($this->input->post() as $input_key => $input_value)
		{
			if(!empty($input_value))
				$data[$input_key] = $input_value;
		}
		// CHECK STUDENT NAME
		if(isset($data['student_name']))
		{
			$this->db->like('student_name' ,$data['student_name'] ,'after');
		}

		// CHECK IF ( REQUEST FROM INCOME METHOD )
		if(isset($data['student_code']))
			$where['student_code'] = $data['student_code'];

		// CHECK STUDENT NATIONAL ID
		if(isset($data['national_id']))
			$where['national_id'] = $data['national_id'];

		// CHECK STUDENT BILL NUMBER
		if(isset($data['open_file_bill_number']))
			$where['open_file_bill_number'] = $data['open_file_bill_number'];

		if(!empty($where))
			$this->db->where($where);

		$query = $this->db->get('student')->result();
		if(count($query) > 0 && !empty($query))
		{

			foreach ($query as $query_key => $query_value)
			{
				$class = $this->main_m->getRowCond('class',array('class_id' => $query_value->class_id));
				$section = $this->main_m->getRowCond('section',array('section_id' => $query_value->section_id));
				$result[$query_key] = $query_value;
				$result[$query_key]->class = $class->class_name;
				$result[$query_key]->section = @$section->section_name;
				$result[$query_key]->profile_photo = base_url().'assets/uploads/'.$query_value->profile_photo;

			}
			returnResponse($result,count($result).' Data Found',true);
		}
		else
			returnResponse(NULL,'لا توجد بيانات',false);


	}

	public function apply_student_page($student_id){

		$data = $this->main_m->getRowCond('student',array('student_id' => $student_id));
	//	$data = $this->main_m->getRowCond('student',array('student_id' => $student_id , 'student_code' => null));
		$data->invoice_file = $this->main_m->getRowCond('invoice_open_file',array('student_id' => $student_id));
		$city = $this->main_m->getRowCond('city',array('city_id' => $data->city));
		$state = $this->main_m->getRowCond('governorate',array('governorate_id' => $data->state));

		if(!empty($data->birth_place))
			$birth_place = $this->main_m->getRowCond('city',array('city_id' => $data->birth_place));

		$class = $this->main_m->getRowCond('class',array('class_id' => $data->class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data->section_id));
		$study_type = $this->main_m->getRowCond('study_type',array('study_id' => $data->study_type));
		$study_status = $this->main_m->getRowCond('study_status',array('status_id' => $data->study_status));
		$certificate_type = $this->main_m->getRowCond('certificate_type',array('certificate_id' => $data->certificate_type));

		$created_date 			= explode(' ', $data->created_date);
		$data->created_date 	= $created_date[0];
		$data->birth_place      = @$birth_place->city_name;
		$data->class 			= $class->class_name;
		$data->city 			= $city->city_name;
		$data->state 			= $state->governorate_name;
		$data->section 			= @$section->section_name;
		$data->certificate_type = @$certificate_type->certificate_type;
		$data->study_type 		= @$study_type->study_type;
		$data->study_status 	= $study_status->study_status;

		$data->year_tb = $this->main_m->getRowCond('year_tb',array('active' => 1));

		$data->tuition_fees = $this->main_m->getRowCond('tuition_fees',array('year_tb_id =' => $data->year_tb->year_tb_id, 'tuition_fees_id' => $data->tuition_fees_id, 'class_id' => $data->class_id,'section_id' => $data->section_id));

		// INSTALLMENT QUERY
		$data->installment = $this->db->where(array('tuition_fees_id' => $data->tuition_fees->tuition_fees_id))->order_by('installment_id','ASC')->get('installment')->result();
		$data->installment_invoice = $this->main_m->getRowCond('invoices',array('student_id' => $student_id,'class_id' => $data->class_id, 'payment' => 'tuition_fees_installment', 'year_tb_id' => $data->year_tb->year_tb_id));


		$data->cash_invoice = $this->main_m->getRowCond('invoices',array('student_id' => $student_id,'class_id' => $data->class_id, 'payment' => 'tuition_fees_cash' , 'year_tb_id' => $data->year_tb->year_tb_id));

		$installment_data = $this->main_m->getRowsCond('installment',array('tuition_fees_id' => $data->tuition_fees->tuition_fees_id));
		foreach ($installment_data as $installment_key => $value)
		{
			$installment_value[] = $value->value;
		}

		$data->count_cash = $data->tuition_fees->tuition_fees;
		$data->count_installment = array_sum($installment_value);

		loadStaticContent('accounts/apply_student_page',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function tuition_fees()
	{
		$result = array();
		$value = $this->input->post('value');
		$student_id = $this->input->post('student_id');
		$tuition_fees = $this->input->post('tuition_fees');
		$studentData = $this->main_m->getRowCond('student',array('student_id' => $student_id));
		$class = $this->main_m->getRowCond('class',array('class_id' => $studentData->class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $studentData->section_id));

		$tuition_fees_value = $this->main_m->getRowCond('tuition_fees',array('tuition_fees_id =' => $studentData->tuition_fees_id));
		$year_tb = $this->main_m->getRowCond('year_tb',array('active' => 1, 'year_tb_id' => $tuition_fees_value->year_tb_id));

		if($tuition_fees == 'tuition_fees_cash')
		{

			$ar_number = new convert_ar($tuition_fees_value->tuition_fees, "male");

			$data = array(
				'student_id' 	=> $student_id,
				'class_id'	 	=> $studentData->class_id,
				'payment' 	 	=> 'tuition_fees_cash',
				'payment_value' => '0',
				'year_tb_id'	=> $year_tb->year_tb_id,
				'invoice_value' => $tuition_fees_value->tuition_fees,
				'invoice_value_ar' => $ar_number->convert_number().' جنية فقط لاغير',
			);

			$invoice = $this->main_m->countRows('invoices',$data);
			if($invoice <= 0)
			{

				$data['invoice_number']  = generateRandomInt(7).substr(student_codeDate(),2);
				$data['created_by'] 	 = $this->userData->real_name;
				$data['created_in']  	 = date("F j, Y, g:i a");

				$this->db->insert('invoices',$data);
				$data['invoice_id'] = $this->db->insert_id();

				$log_indata = $data;
				$log_indata['section_id'] 	= $studentData->section_id;
				$log_indata['student_code'] = $studentData->student_code;
				$log_indata['national_id'] 	= $studentData->national_id;
				$log_indata['student_name']	= $studentData->student_name;
				$this->db->insert('invoice_log',$log_indata);

				$checkIncomeInvoices = $this->main_m->getRowCond('income_date_invoice',array('date' => date('Y-m-d')));
	        	if(!empty($checkIncomeInvoices))
	        	{
	            	$this->db->where('date' , date('Y-m-d'))->update('income_date_invoice',array('income_invoices_counter' => $checkIncomeInvoices->income_invoices_counter + 1));
	                        $income_date_invoice_id = $checkIncomeInvoices->income_date_invoice_id;
	            }
	            else
	            {
	                $this->db->insert('income_date_invoice',array('income_invoices_counter' => 1, 'date' => date('Y-m-d')));
	                $income_date_invoice_id = $this->db->insert_id();
	            }

	            $income_data = array(
	                'income_id'             => 2,
	                'income_invoice_number' => $data['invoice_number'],
	                'income_student_name'   => $studentData->student_name,
	                'income_student_id'     => $studentData->student_id,
	                'income_comment'        => $studentData->student_name.' تم دفع رسوم ( مصاريف دراسية) من الطالب /',
	                'income_invoice_payment'=> 'مصاريف دراسية',
	                'remission'             => 0,
	                'income_invoice_value'          => $data['invoice_value'],
	                'income_invoice_value_ar'       => $data['invoice_value_ar'],
	                'income_date_invoice_id'        => $income_date_invoice_id,
	                'income_invoice_created_by'     => $this->userData->real_name,
	                'income_invoice_created_in'     => date("F j, Y, g:i a"),
	                'income_invoice_year_id'        => $data['year_tb_id'],
	            );

	            $this->db->insert('income_invoice',$income_data);

				$data['student_code'] = substr($year_tb->year_type,2).generateRandomInt(4);
				// GENERATE AND UPDATE STUDENT CODE
				$result = $data;

				if($studentData->student_code == NULL OR $studentData->student_code == '')
					$arrayChange = array('student_code' => $data['student_code'],'student_status' => 2);
				else
					$arrayChange = array('student_status' => 2);


				// CEHCK STUDENT CODE GENERATOR MATCHED OR NOT
				if($this->main_m->countRows('student',array('student_code' => $data['student_code'])) == 0)
				{
					$this->db->where('student_id' , $student_id)->update('student',$arrayChange);
					// INSERT STUDENT CODE
					returnResponse($result,'تم دفع المصروفات',true,base_url().'accounts/pay_tuition_fees_bill/'.$student_id);

				}
				else
				{
					$this->db->where(array('student_id' => $student_id))->update('student',array('student_status' => 2));
					returnResponse('installment',array('invoice_id' => $invoice_id,'student_id' => $student_id,'link' => base_url().'accounts/generate_new_code'),false);
				}

			}
			else
				returnResponse($result,'تم دفع المصروفات من قبل',false,base_url().'accounts/pay_tuition_fees_bill/'.$student_id);
		}

	}

	public function pay_residual()
	{
		$newData = '';
		$data = array();
		$tuition_fees_id 	= $this->input->post('installment_id');
		$student_id     	= $this->input->post('student_id');
		$tuition_fees_value = $this->input->post('installment_value');
		$installment_parte_input = $this->input->post('installment_parte_input');
		$year_tb_id = $this->input->post('year_tb_id');

		$student_data = $this->main_m->getRowCond('student',array('student_id' => $student_id));

		if($this->input->post('checkbox') != NULL)
			$tuition_fees_value = $installment_parte_input;
		else
			$tuition_fees_value = $tuition_fees_value;


		$invoice = $this->db->order_by('invoice_id','DESC')->where(array('student_id' => $student_id, 'year_tb_id' => $year_tb_id))->get('invoices')->row();

		$total = $tuition_fees_value + $invoice->invoice_value;
		$invoice_total_ar = new convert_ar( $total, "male");

		$this->db->where(array('student_id' => $student_id, 'year_tb_id' => $year_tb_id))->update('invoices',array(
			'invoice_value' => ($tuition_fees_value + $invoice->invoice_value),
			'invoice_value_ar' => $invoice_total_ar->convert_number().' جنية فقط لاغير',
			'residual' => ($invoice->residual - $tuition_fees_value),
		));

		$invoice_log_total_ar = new convert_ar( ($tuition_fees_value), "male");
		$data_log = (array) $invoice;

		$data_log['invoice_value'] 	= $tuition_fees_value;
		$data_log['invoice_value_ar']  = $invoice_log_total_ar->convert_number().' جنية فقط لاغير';
		$data_log['residual'] 	 = $data_log['residual'] - $tuition_fees_value;
		$data_log['created_by'] = $this->userData->real_name;
		$data_log['created_in'] = date("F j, Y, g:i a");
		$data_log['invoice_number'] = generateRandomInt(7).substr(student_codeDate(),2);

		$data_log['section_id'] 	= $student_data->section_id;
		$data_log['student_name'] 	= $student_data->student_name;
		$data_log['student_code'] 	= $student_data->student_code;
		$data_log['national_id'] 	= $student_data->national_id;
		$this->db->insert('invoice_log',$data_log);

		$checkIncomeInvoices = $this->main_m->getRowCond('income_date_invoice',array('date' => date('Y-m-d')));
    	if(!empty($checkIncomeInvoices))
    	{
        	$this->db->where('date' , date('Y-m-d'))->update('income_date_invoice',array('income_invoices_counter' => $checkIncomeInvoices->income_invoices_counter + 1));
                    $income_date_invoice_id = $checkIncomeInvoices->income_date_invoice_id;
        }
        else
        {
            $this->db->insert('income_date_invoice',array('income_invoices_counter' => 1, 'date' => date('Y-m-d')));
            $income_date_invoice_id = $this->db->insert_id();
        }

        $income_data = array(
            'income_id'             => 2,
            'income_invoice_number' => $data_log['invoice_number'],
            'income_student_name'   => $student_data->student_name,
            'income_student_id'     => $student_data->student_id,
            'income_comment'        => $student_data->student_name.' تم دفع رسوم ( مصاريف دراسية) من الطالب /',
            'income_invoice_payment'=> 'مصاريف دراسية',
            'remission'             => 0,
            'income_invoice_value'          => $data_log['invoice_value'],
            'income_invoice_value_ar'       => $data_log['invoice_value_ar'],
            'income_date_invoice_id'        => $income_date_invoice_id,
            'income_invoice_created_by'     => $this->userData->real_name,
            'income_invoice_created_in'     => date("F j, Y, g:i a"),
            'income_invoice_year_id'        => $data_log['year_tb_id'],
        );

        $this->db->insert('income_invoice',$income_data);


		returnResponse('cash','تم دفع مبلغ '.$tuition_fees_value.' جنية',true,base_url().'accounts/pay_tuition_fees_bill/'.$student_id);
	}

	public function insert_installment(){
		$newData = '';
		$data = array();
		$installment_id = $this->input->post('installment_id');
		$student_id     = $this->input->post('student_id');
		$installment_value = $this->input->post('installment_value');
		$installment_parte_input = $this->input->post('installment_parte_input');
		$year_tb_id = $this->input->post('year_tb_id');

		$installment = $this->main_m->getRowCond('installment',array('installment_id' => $installment_id));
		$installment_type = $installment->type;
		$dbIV = $installment->value;

		$yearType = $this->main_m->getRowCond('year_tb',array('year_tb_id' => $year_tb_id));
		$year_tb_type = $yearType->year_type;
		if($this->input->post('checkbox') != NULL)
			$invoice_total = $installment_parte_input;
		else
			$invoice_total = $installment_value;


		// count remission value
		$student_installment_invoices = $this->main_m->getRowsCond('invoice_log',array('student_id' => $student_id, 'payment_value' => $installment_id));

		foreach($student_installment_invoices as $value)
		{
			$tRemission[] = $value->invoice_value;
		}
		if(!empty($tRemission))
			$totalR = array_sum($tRemission);
		else
			$totalR = 0;

		$residual = $dbIV - ($invoice_total + $totalR);
		// END remission counter

		$invoice_total_ar = new convert_ar($invoice_total, "male");
		$studentData = $this->main_m->getRowCond('student',array('student_id' => $student_id));
		$data = array(
			'student_id' 		=> $student_id,
			'class_id'	 		=> $studentData->class_id,
			'payment' 	 		=> 'tuition_fees_installment',
			'payment_value' 	=> $installment_id,
			'residual'			=> $residual,
			'invoice_value' 	=> $invoice_total,
			'invoice_value_ar' 	=> $invoice_total_ar->convert_number().' جنية فقط لاغير',
			'invoice_number' 	=> generateRandomInt(7).substr($year_tb_type,2),
			'created_by'	 	=> $this->userData->real_name,
			'created_in' 	 	=> date("F j, Y, g:i a"),
			'year_tb_id' 		=> $year_tb_id,
		);

		//CHECK STUDENT CODE IF = NULL INSERT ::DB
		if($studentData->student_code == NULL)
		{

			$this->db->insert('invoices',$data);
			$data['invoice_id'] = $this->db->insert_id();
			// INSERT TO INVOICE LOG
			// GET RAND KEY
			if(strlen($data['invoice_id']) <= 2)
				$invoice_id = $data['invoice_id'];
			else
				$invoice_id = substr($data['invoice_id'],2);

			$student_code = substr($year_tb_type,2).generateRandomInt(4);
			// CHECK STUDENT CODE BEFORE INSERT

			if($this->main_m->countRows('student',array('student_code' => $student_code)) == 0)
			{
				$this->db->where('student_id' , $student_id)->update('student',array('student_code' => $student_code, 'student_status' => 2));
				$data['student_name'] 	= $studentData->student_name;
				$data['national_id'] 	= $studentData->national_id;
				$data['class_id'] 		= $studentData->class_id;
				$data['section_id'] 	= $studentData->section_id;
				$data['student_code'] 	= $student_code;

				$this->db->insert('invoice_log',$data);
				$invoice_log_id = $this->db->insert_id();

				$checkIncomeInvoices = $this->main_m->getRowCond('income_date_invoice',array('date' => date('Y-m-d')));
	        	if(!empty($checkIncomeInvoices))
	        	{
	            	$this->db->where('date' , date('Y-m-d'))->update('income_date_invoice',array('income_invoices_counter' => $checkIncomeInvoices->income_invoices_counter + 1));
	                        $income_date_invoice_id = $checkIncomeInvoices->income_date_invoice_id;
	            }
	            else
	            {
	                $this->db->insert('income_date_invoice',array('income_invoices_counter' => 1, 'date' => date('Y-m-d')));
	                $income_date_invoice_id = $this->db->insert_id();
	            }

	            $income_data = array(
	                'income_id'             => 2,
	                'income_invoice_number' => $data['invoice_number'],
	                'income_student_name'   => $studentData->student_name,
	                'income_student_id'     => $studentData->student_id,
	                'income_comment'        => $studentData->student_name.' تم دفع رسوم ( مصاريف دراسية) من الطالب /',
	                'income_invoice_payment'=> 'مصاريف دراسية',
	                'remission'             => 0,
	                'income_invoice_value'          => $data['invoice_value'],
	                'income_invoice_value_ar'       => $data['invoice_value_ar'],
	                'income_date_invoice_id'        => $income_date_invoice_id,
	                'income_invoice_created_by'     => $this->userData->real_name,
	                'income_invoice_created_in'     => date("F j, Y, g:i a"),
	                'income_invoice_year_id'        => $data['year_tb_id'],
	            );

	            $this->db->insert('income_invoice',$income_data);

				// INSERT STUDENT CODE
				returnResponse('installment','تم دفع مبلغ '.$invoice_total.' جنية من القسط '.WordIN($installment_type).'', true, base_url().'accounts/student_tuition_fees/', base_url().'accounts/print_invoices/installment/'.$student_id.'/'.$data['invoice_id'].'/'.$invoice_log_id);
			}
			else
			{
				// REGENERATE STUDENT CODE WITH RETURN AJAX
				returnResponse('installment',array('invoice_id' => $invoice_id,'student_id' => $student_id,'link' => base_url().'accounts/generate_new_code'),false);
			}
		}

		else
		{
			$checkIncomeInvoices = $this->main_m->getRowCond('income_date_invoice',array('date' => date('Y-m-d')));
       	if(!empty($checkIncomeInvoices))
       	{
           	$this->db->where('date' , date('Y-m-d'))->update('income_date_invoice',array('income_invoices_counter' => $checkIncomeInvoices->income_invoices_counter + 1));
                       $income_date_invoice_id = $checkIncomeInvoices->income_date_invoice_id;
           }
           else
           {
               $this->db->insert('income_date_invoice',array('income_invoices_counter' => 1, 'date' => date('Y-m-d')));
               $income_date_invoice_id = $this->db->insert_id();
           }

           $income_data = array(
               'income_id'             => 2,
               'income_invoice_number' => $data['invoice_number'],
               'income_student_name'   => $studentData->student_name,
               'income_student_id'     => $studentData->student_id,
               'income_comment'        => $studentData->student_name.' تم دفع رسوم ( مصاريف دراسية) من الطالب /',
               'income_invoice_payment'=> 'مصاريف دراسية',
               'remission'             => 0,
               'income_invoice_value'          => $data['invoice_value'],
               'income_invoice_value_ar'       => $data['invoice_value_ar'],
               'income_date_invoice_id'        => $income_date_invoice_id,
               'income_invoice_created_by'     => $this->userData->real_name,
               'income_invoice_created_in'     => date("F j, Y, g:i a"),
               'income_invoice_year_id'        => $data['year_tb_id'],
           );

           $this->db->insert('income_invoice',$income_data);

			// CHECK STUDENT INSTALLMENT INVOICES
			$installment_data   = $this->main_m->getRowOrder('invoices',array(
				'student_id' 	=> $student_id,
				'class_id' 		=> $studentData->class_id,
				'payment'		=> 'tuition_fees_installment',
				'payment_value' => $installment_id,
				'year_tb_id' 	=> $year_tb_id,
				),'invoice_id','DESC');
			// CHECK INSTALLMENT NUMBER 1::2::3::4::5::6
			if(!empty($installment_data) || !empty($installment_data))
			{

				// INSTALLMENT POST VALUE  +  INVOICE LOG PAYMENT VALUE = TOTAL PAIED TO INSTALLMENT ID
				$total_installment_paied = $installment_data->invoice_value + $invoice_total;

				// GET TOTAL INSTALLMENT PAIED WITH ARABIC LITTER CONVERT_AR CLASS()
				$total_installment_paied_ar = new convert_ar($total_installment_paied, "male");

				// TRANS DATA VALUE TO  VAR newData With Some Changes
				$newData = $data;
				$newData['invoice_value'] = $total_installment_paied;
				$newData['invoice_value_ar'] = $total_installment_paied_ar->convert_number().' جنية فقط لاغير';
				$this->db->where('invoice_id',$installment_data->invoice_id)->update('invoices',$newData);

				// SET INVOICE ID TO DATA ARRAY ()
				$data['invoice_id'] = $installment_data->invoice_id;
				$data['national_id'] 	= $studentData->national_id;
				$data['student_name'] 	= $studentData->student_name;
				$data['class_id'] 		= $studentData->class_id;
				$data['student_code'] 	= $studentData->student_code;
				$this->db->insert('invoice_log',$data);
				$invoice_log_id = $this->db->insert_id();

				returnResponse('installment','تم دفع مبلغ '.$invoice_total.' جنية من القسط  '.WordIN($installment_type).'',true,base_url().'accounts/pay_tuition_fees_bill/'.$student_id, base_url().'accounts/print_invoices/installment/'.$student_id.'/'.$data['invoice_id'].'/'.$invoice_log_id);
			}
			else
			{
				$this->db->insert('invoices',$data);
				$data['invoice_id'] = $this->db->insert_id();
				// INSERT TO INVOICE LOG
				$data['student_name'] 	= $studentData->student_name;
				$data['national_id'] 	= $studentData->national_id;
				$data['class_id'] 		= $studentData->class_id;
				$data['section_id'] 	= $studentData->section_id;
				$data['student_code'] 	= $studentData->student_code;
				$this->db->insert('invoice_log',$data);
				$invoice_log_id = $this->db->insert_id();
				// GET RAND KEY
				if(strlen($data['invoice_id']) >= 2)
					$invoice_id = $data['invoice_id'];
				else
					$invoice_id = substr($data['invoice_id'],2);

				returnResponse('installment','تم دفع مبلغ '.$invoice_total.' جنية من القسط '.WordIN($installment_type).'',true,base_url().'accounts/pay_tuition_fees_bill/'.$student_id, base_url().'accounts/print_invoices/installment/'.$student_id.'/'.$data['invoice_id'].'/'.$invoice_log_id);

			}
		}
	}

    // GENERATE NEW CODE FOR STUDENT
    public function generate_new_code()
    {
    	$student_id = $this->input->post('student_id');
    	$invoice_id = $this->input->post('invoice_id');

    	$year_tb = $this->main_m->getRowCond('year_tb',array('active' => 1));
    	$year_type = $year_tb->year_type;

    	$student_code = substr(year_type(),2).generateRandomInt(4);
    	if($this->main_m->countRows('student',array('student_code' => $student_code)) == 0)
    		returnResponse('installment','تم دفع مبلغ '.$invoice_total.' جنية من القسط '.WordIN($installment_id).'',true,base_url().'accounts/apply_student_page/'.$student_id);
    	else
    	{
    		returnResponse('installment',array('invoice_id' => $invoice_id,'student_id' => $student_id,'link' => base_url().'accounts/generate_new_code'),false);
    	}
    }


    // INSERT TO STUDENT PRINTED TABLE
    public function printedCard()
    {
		$student_id = $this->input->post('student_id');
		$val  = $this->input->post('val');

		$this->db->where('student_id', $student_id)->update('student',array('printed' => $val));
		echo json_encode(array('response' => true));
	}

	public function print_invoices($payment_method = NULL, $student_id = NULL, $invoice_id = NULL, $log_id = NULL)
	{

			$student = $this->main_m->getRowCond('student',array('student_id' => $student_id));
			$section = $this->main_m->getRowCond('section',array('section_id' => $student->section_id));
			$class   = $this->main_m->getRowCond('class',array('class_id' => $student->class_id));
			$data['student_data'] = $student;
			$data['student_data']->section = $section->section_name;
			$data['student_data']->class   = $class->class_name;
			$data['payment_method'] = $payment_method;

			if($payment_method == 'installment')
			{
                $getInstallmentInvoice = $this->main_m->getRowCond('invoice_log',array('invoice_log_id' => $log_id, 'invoice_id' => $invoice_id));

				$data['invoice_data']   = $getInstallmentInvoice;

				$getInstallmentData = $this->main_m->getRowCond('installment',array('installment_id' => $getInstallmentInvoice->payment_value));
				$data['invoice_status'] = 'ايصال القسط '.WordIN($getInstallmentData->type);

				$data['invoice_data']->residual = $getInstallmentInvoice->residual;

				// EDN Residual
				$data['invoice_data']->total = $getInstallmentData->value;


			}
			elseif($payment_method == 'cash')
			{

				$getCashnvoice = $this->main_m->getRowCond('invoices',array('invoice_id' => $invoice_id));

				$data['invoice_data']   = $getCashnvoice;

				$data['invoice_status'] = 'ايصال المصروفات الدراسية ';

				$data['invoice_data']->total = $getCashnvoice->invoice_value;

				$data['invoice_data']->residual = 0;

			}
			elseif($payment_method == 'open_file')
			{

				$getOpenFileAMount = $this->main_m->getRowCond('invoice_open_file',array('invoice_file_id' => $invoice_id));

				$data['invoice_data'] = $getOpenFileAMount;

				$data['invoice_data']->invoice_value = $getOpenFileAMount->bill_amount;

				$data['invoice_data']->invoice_number = $getOpenFileAMount->bill_number;

				$data['invoice_data']->total = $getOpenFileAMount->bill_amount;

				$data['invoice_data']->invoice_value_ar = $getOpenFileAMount->bill_amount_ar;

				$data['invoice_data']->created_in = $getOpenFileAMount->created_at;

				$data['invoice_data']->residual = 0;

				$data['invoice_status'] = 'رسوم دراسية ';

			}

			loadStaticContent('print/invoices',$data,NULL);
	}

	// INCOME FUNCTION
	public function income()
	{
		$data['income'] = $this->main_m->getRowsCond('income',array('deleted' => 0));
		loadStaticContent('accounts/income',$data,array('script' => 'ajax/accountsJs.php'));
	}

	// ADD INCOME VIEW
	public function add_income()
	{
		$data['income'] = $this->main_m->getAll('*','income');
		loadStaticContent('accounts/add_income',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function add_new_income($student_id)
	{
		// STUDENT DATA
		$data['student_data'] = $this->main_m->getRowCond('student',array('student_id' => $student_id));
		$class = $this->main_m->getRowCond('class',array('class_id' => $data['student_data']->class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data['student_data']->section_id));
		$data['student_data']->class = $class->class_name;
		$data['student_data']->section = @$section->section_name;
		$data['class'] = $this->db->get('class')->result();

		// INCOME DATA TYPE
		$data['income'] = $this->main_m->getAll('*','income');

		loadStaticContent('accounts/add_new_income',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function get_student_tuitionFees_details()
    {
    	$result = '';
        $student_id = $this->input->post('student_id');
		$class_id = $this->input->post('class_id');
		$year_tb = $this->main_m->getRowCond('year_tb',array('active' => 1));
        $data = $this->main_m->getRowCond('student',array('student_id' => $student_id));


        if(empty($data->tuition_fees_type))
        {
        	if($data->nationality == 'مصرى')
	        	$data->tuition_fees_type = 'normal';
    		else
	        	$data->tuition_fees_type = 'foreigner';

        	if($data->studey_status == 2)
        		$data->tuition_fees_type = 'intensified';
        }

        if($class_id)
        {
        	$tuition_fees = $this->main_m->getRowCond('tuition_fees',array(
        		'year_tb_id' 	=> $year_tb->year_tb_id,
        		'class_id' 		=> $class_id,
				'section_id'	=> $data->section_id,
        		'type'			=> $data->tuition_fees_type,
        	));
        	$tuition_fees_id = $tuition_fees->tuition_fees_id;
        	$this->db->where('student_id',$student_id)->update('student',array('class_id' => $class_id, 'tuition_fees_id' => $tuition_fees_id));
        }


		$invoice_file = $this->main_m->getRowCond('invoice_open_file',array('student_id' => $student_id));
		$city = $this->main_m->getRowCond('city',array('city_id' => $data->city));
		$state = $this->main_m->getRowCond('governorate',array('governorate_id' => $data->state));

		if(!empty($data->birth_place))
			$birth_place = $this->main_m->getRowCond('city',array('city_id' => $data->birth_place));

		$class = $this->main_m->getRowCond('class',array('class_id' => $class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data->section_id));
		$study_type = $this->main_m->getRowCond('study_type',array('study_id' => $data->study_type));
		$study_status = $this->main_m->getRowCond('study_status',array('status_id' => $data->study_status));
		$certificate_type = $this->main_m->getRowCond('certificate_type',array('certificate_id' => $data->certificate_type));

		$created_date 		= explode(' ', $data->created_date);
		$created_date 		= $created_date[0];
		$birth_place      	= @$birth_place->city_name;
		$class 				= $class->class_name;
		$city 				= $city->city_name;
		$state 				= $state->governorate_name;
		$section 			= @$section->section_name;
		$certificate_type 	= @$certificate_type->certificate_type;
		$study_type 		= @$study_type->study_type;
		$study_status 		= $study_status->study_status;

		$tuition_fees = $this->main_m->getRowCond('tuition_fees',array('tuition_fees_id' => $data->tuition_fees_id));


		// INSTALLMENT QUERY
		$installment = $this->db->where(array('tuition_fees_id' => $tuition_fees->tuition_fees_id))->order_by('installment_id','ASC')->get('installment')->result();

		$installment_invoice = $this->main_m->getRowCond('invoices',array('student_id' => $student_id,'class_id' => $class_id, 'payment' => 'tuition_fees_installment', 'year_tb_id' => $year_tb->year_tb_id));


		$cash_invoice = $this->main_m->getRowCond('invoices',array('student_id' => $student_id,'class_id' => $class_id, 'payment' => 'tuition_fees_cash' , 'year_tb_id' => $year_tb->year_tb_id));

		$installment_data = $this->main_m->getRowsCond('installment',array('tuition_fees_id' => $tuition_fees->tuition_fees_id));
		foreach ($installment_data as $installment_key => $value)
		{
			$installment_value[] = $value->value;
		}

		$remission = $this->main_m->getRowsCond('student_remission',array('student_id' => $student_id));

		$count_cash = $tuition_fees->tuition_fees;
		$count_installment = array_sum($installment_value);

        $result .= '
	   <div class="card card-transparent m-b-0">
	   <header class="card-heading">
	      <h2 class="card-title m-t-0">المصاريف الدراسية</h2>
	      <small>جميع الارقام بالجنية المصرى</small>
	   </header>
	   <div class="card-body p-t-0" style="    min-height: 950px;">';


	if(empty($cash_invoice)){
	   $payment_way_installment = '';

	      $payment_way_installment = '<div class="col-md-6">
	      <a href="javascript:;" id="push_installment" class="btn btn-success btn-block"> المصروفات بلقسط  '.$count_installment.'</a>
	      </div>';

	   if(empty($installment_invoice))
	      $result .= '<div class="col-md-12">
	      <div class="col-md-6">
	      <a href="javascript:;" id="push_cash" class="btn btn-success btn-block"> المصروفات كاش   '.$tuition_fees->tuition_fees.'</a>
	      </div>'.$payment_way_installment.'</div>';



	   $result .='<div class="col-md-12 push_cash hidden">
	   <div class="col-md-6">
	      <a href="javascript:;" disabled class="btn btn-success btn-block"> المصروفات الدراسية  '.$tuition_fees->tuition_fees.'</a>
	   </div>
	   <div class="col-md-6">
	      <input type="text" class="hidden" disabled class="form-control push_cash" name="tuition_fees_cash" value="'.$tuition_fees->tuition_fees.'">
	   </div>
	   <div class="col-md-6">
	      <a href="javascript:;" data-result="resultDiv" data-url="'.base_url().'accounts/tuition_fees" data-fees="tuition_fees_cash" data-value="0" data-student="'.$student_id.'" id="tuition_fees_payment" class="btn btn-primary btn-block"> دفع  المصروفات</a>
	   </div>
	      <div class="col-md-12 resultDiv"></div>
	   </div>';

	   $result .='<header class="card-heading push_installment hidden" style="float: right;width: 100%;">
	         <div class="p-t-0" style="width: 100%;float: right;padding: 10px 0px;border: 0px dotted;"></div>
	         <h2 class="card-title m-t-0" style="float: right;">الاقساط :</h2>
	      </header>';



	   if(!empty($installment) && isset($installment))
	   {
	      $i=0;
	      foreach ($installment as $installment_key => $installment_value)
	      {
	         if($installment_value->value > 0){

	         $i++; $disabledClass = ''; $Residual = '';
	         if(!empty($installment_invoice)){$classHidden='';}else $classHidden='hidden';
	         $invoices = $this->main_m->getRowsCond('invoice_log',
	         array(
	            'student_id' => $student_id,
	            'class_id' => $class_id,
	            'payment' => 'tuition_fees_installment',
	            'payment_value' => $installment_value->installment_id,
	            'year_tb_id' => $year_tb->year_tb_id
	         ));


	         $result .= '<div class="col-md-12 push_installment '.$classHidden.'">
	            <div class="col-md-12">
	               <a href="javascript:;" disabled class="btn btn-success btn-block"> القسط   '. $installment_value->installment_type.' '.$installment_value->value .'</a>
	            </div>';

	         if(!empty($invoices)){
	            $totalPaid=array(); $z=0;
	            foreach ($invoices as $key => $value){
	            $totalPaid[] = $value->invoice_value;
	            $z++;
	            $result .='<div class="col-md-6"><p style="padding-top: 0px;margin: 0px;">تم دفع مبلغ وقدرة  '.$value->invoice_value.'</p>
	                        <span style="color: #ccc;">تحريرا فى  '.returnMonthFormate($value->created_in).' </span>
	                     </div>
	                     <div class="col-md-6">
	                        <a href="'.base_url().'accounts/print_invoices/installment/'.$student_id.'/'.$value->invoice_id.'/'.$value->invoice_log_id .'" target="_blank" class="btn btn-info btn-block"> طباعة ايصال القسط  '.wordIn($installment_value->type) .' رقم <?=$z?> <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
	                     </div>';
	            }
	         }
	         if(!empty($invoices) && isset($totalPaid)){ // CHECK INSTALLMENT INVOICE AND PAIED vALUE
	            $total = array_sum($totalPaid);// COUNT PAIED VALUES
	            $Residual = $installment_value->value - $total; // COUNT RESIDUAL vALUE
	            if($Residual == 0){$disabledClass = 'disabled';} else{$disabledClass = '';}

	            $result .='<span style="font-size: 14px;font-weight: bold;color: red;">اجمالى المدفوع من القسط  '.$installment_value->installment_type. ' ' . $total .'</span>
	               <p style="font-weight: bold">المتبقى   '.$Residual .'</p>';
	         }

	         if($disabledClass !='disabled')
	            $disabledClass = 'data-toggle="modal" data-target="#tab_modal"';

	         if($Residual > 0 )
	            $Residual = $installment_value->value;
	        else
	            $Residual = $installment_value->value;

	         $result .= '<div class="col-md-6">
	                  <a href="javascript:;" '.$disabledClass.' data-value=" '.$installment_value->installment_id.'" data-number="'.$Residual.'" year-tb="'.$year_tb->year_tb_id.'" data-type="قيمة  '.$installment_value->installment_type .'" class="btn btn-primary btn-block installment"> دفع   '.$installment_value->installment_type .'</a>
	               </div>
	            </div>';

	         $result .= '<div class="p-t-0" style="width: 100%;float: right;padding: 0px 0px;border: 1px dotted;"></div><hr>';
	     	}
	      }
	   }
	}
	else
	   {

	      $result .= '<div class="col-md-12" id="resultDiv">';
	      if(!empty($remission)){
	         foreach($remission as $remission_val){
	            if($remission_val->type == 'remission'){
	            $result .= '<span style="font-size: 14px;font-weight: bold;color: #0c3263;">اعفاء  مبلغ  ( '.$remission_val->value .' )</span><br>';
	            }
	            else
	            {
	               $result .= '<span style="font-size: 14px;font-weight: bold;color: #0c3263;">المصروفات المقدرة ( '.$remission_val->value.' )</span><br>';
	            }

	         }
	      }
	      $result .= '<span style="font-size: 14px;font-weight: bold;color: red;">اجمالى المدفوع  '.$cash_invoice->invoice_value_ar .' (  '.$cash_invoice->invoice_value .' )</span>';

	      if($cash_invoice->residual != 0){
	         $result .= '<br>
	               <span style="font-size: 14px;font-weight: bold;color: red;">المتبقى  ( '.$cash_invoice->residual .' )</span>
	               <a href="javascript:;" url="'.base_url().'accounts/pay_residual" data-toggle="modal" data-target="#tab_modal" data-value="'.$tuition_fees->tuition_fees_id .'" data-number=" '.$cash_invoice->residual .'" year-tb="'.$year_tb->year_tb_id.'" data-type="قيمة  المصروفات" class="btn btn-primary btn-block installment"> دفع  المتبقى</a>';


	      }

	      $result .= '<a href="'.base_url().'accounts/print_invoices/cash/'.$student_id.'/'.$cash_invoice->invoice_id.'" class="btn btn-info btn-block" target="_blank"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
	      </div>';
	   }
	   $result .= '</div></div>';

	   $html = '<div class="modal fullscreen fade" id="fullscreen_modal" style="padding-left:0px;" tabindex="-1" role="dialog" aria-labelledby="fullscreen_modal">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="myModalLabel-2">المصروفات الدراسية</h4>
							</div>
							<div class="modal-body scrollbar">
							'.$result.'
							</div>
							<div class="modal-footer" style="box-shadow: -13px 7px 21px 6px #7d7d7d;">
								<button type="button" class="btn btn-default btn-flat close_bill_modal" style="float: right;background: #546575;color: #fff;" data-dismiss="modal">اغلاق</button>
							</div>
						</div>
						<!-- modal-content -->
					</div>
					<!-- modal-dialog -->
				</div>
				<div class="modal fade" id="tab_modal" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     	<div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">

                 <h4 class="modal-title" id="myModalLabel-2">رسوم دراسية</h4>
                 <ul class="card-actions icons left-top">
                     <li>
                     <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                         <i class="zmdi zmdi-close"></i>
                     </a>
                 </li>
             </ul>
         </div>
         <form class="form_ajax" method="POST" result-data="form_result" action="'.base_url().'accounts/insert_installment">
         <div class="modal-body p-0" id="">
                 <div class="tab-pane fadeIn active" id="tab-1">
                    <div class="form-group">
                       <label for="installment_input" class="col-sm-4 control-label installment_label"> </label>
                       <div class="col-sm-8">
                           <input id="installment_input" type="text" name="" disabled placeholder="" value=""  class="form-control" >
                           <input id="year_tb_id" type="hidden" name="year_tb_id" placeholder="" value=""  class="form-control" >
                           <input id="installment_id" type="hidden" name="installment_id" placeholder="" value=""  class="form-control" >



                           <input id="installment_value" type="hidden" name="installment_value" placeholder="" value=""  class="form-control" >

                           <input type="hidden" name="student_id" value="'.$student_id.'">

                       </div>
                   </div>
                   <div class="inline-block col-sm-offset-4">
                     <label class="login-label">
                        <input type="checkbox" id="installment_checkBox" name="checkbox" class="checkbox-inline" value="1">
                        <span class="installment_parte"></span>
                     </label>
                   </div>
                   <div class="form-group installment_parte_input hidden">
                       <label for="installment_parte_input" class="col-sm-4 control-label installment_label2"> </label>
                       <div class="col-sm-8">
                           <input id="installment_parte_input" min="0" type="number" name="installment_parte_input" placeholder="" value="0"  class="form-control hidden" >
                       </div>
                   </div>

             </div>
         </div>
         <div class="form_result col-md-12"></div>
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">اغلاق</button>
             <button type="submit" class="btn btn-primary">حفظ</button>
         </div>
         </form>
         </div>
         <!-- modal-content -->

     </div>
         <!-- modal-dialog -->
     </div>';

       returnResponse($html, 'popup',true);
    }
	public function add_income_invoice()
	{
        $income_id = $this->input->post('income_id');
        $student_id = $this->input->post('student_id');
        $student_name = $this->input->post('student_name');
        $remission = $this->input->post('remission');
		$comment = $this->input->post('comment');

		if(empty($remission))
			$remission = 0;

		$this->form_validation->set_rules('student_id', 'SID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('income_id', 'نوع الايراد ', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
        	# RETURN FORM VALIDATINO ERROR ::JSON
        	returnResponse(NULL , '<div class="alert alert-danger" role="alert"> '.validation_errors().' </div>' ,false);
        }
        else
        {
	        // THIS YEAR ID
	        $year_tb = $this->db->order_by('year_type','DESC')->get('year_tb')->row();
	        $income = $this->main_m->getRowCond('income',array('income_id' => $income_id));

	        $income_amount = $income->income_amount;
			$data['income_comment'] = 'تم دفع رسوم ( '.$income->income_type.') من الطالب /'.$student_name;
	        // CHANGE SECTION TO STUDENT
	        if($income->feature == 'section')
	        {
	        	$section_id = $this->input->post('section_id');
	        	$section = $this->main_m->getRowCond('section',array('section_id' => $section_id));
	        	$this->db->where('student_id',$student_id)->update('student',array('section_id' => $section_id));

	        	$data['income_comment'] = 'الطالب /'.$student_name.'.. تم التحويل الى قسم '.$section->section_name.'';
	        }

	        // ADD FINES TO STUDENT
	        elseif($income->feature == 'fines')
	        {
	        	$fines = $this->input->post('fines');
	        	$income_amount = $fines;
	        	$data['income_comment'] = 'تم اضافة غرامة قيمتها '.$fines.' جنية مصرى' .'للطالب /'.$student_name;
			}

			elseif($income->feature == 'tuition_fees')
	        {
	        	$tuition_amount = $this->input->post('tuition_amount');
	        	$income_amount = $tuition_amount;
	        	$data['income_comment'] = 'تم دفع رسوم ( '.$income->income_type.') من الطالب /'.$student_name;
	        }
            elseif($income->feature == 'enrollment')
            {
                $this->db->where('student_id',$student_id)->update('student',array('study_type' => 2));
                $data['income_comment'] = 'تم دفع رسوم ( '.$income->income_type.') من الطالب /'.$student_name;
            }



	        // Invoice Value
	        $data['income_invoice_value'] = $income_amount - $remission;
	        // Invoice Value Ar convert_ar();
	        $in_invoiceV = new convert_ar(str_replace('.00','',$data['income_invoice_value']), "male");
			$data['income_invoice_value_ar'] = $in_invoiceV->convert_number().' جنية مصرى فقط لاغير';
	        // Remission
	        $data['remission'] = $remission;

	        // DB PROPERTIES >>
	        $data['income_id'] = $income_id;
	        $data['income_invoice_number'] = substr(date("Y"),2).generateRandomInt(7);
	        $data['income_student_name'] = $student_name;
	        $data['income_student_id'] = $student_id;
			$data['income_invoice_payment'] = $income->income_type;
			$data['income_invoice_comment'] = $comment;
	        $data['income_invoice_created_by'] = $this->userData->real_name;
	        $data['income_invoice_created_in'] = date("F j, Y, g:i a");
			$data['income_invoice_year_id'] = $year_tb->year_tb_id;

			// INCOME income_date_invoice FOR DAYS >>
			$income_date_invoice = $this->main_m->getRowCond('income_date_invoice',array('date' => date('Y-m-d')));
			$income_date_invoiceData['date'] = date('Y-m-d');
			if(!empty($income_date_invoice))
			{
				$income_date_invoiceData['income_invoices_counter'] = $income_date_invoice->income_invoices_counter+1;
				$this->db->where('date',date('Y-m-d'))->update('income_date_invoice',array('income_invoices_counter' => $income_date_invoice->income_invoices_counter+1));
				$data['income_date_invoice_id'] = $income_date_invoice->income_date_invoice_id;
			}

			else
			{
				$income_date_invoiceData['income_invoices_counter'] = 1;
				$this->db->insert('income_date_invoice',$income_date_invoiceData);
				$data['income_date_invoice_id'] = $this->db->insert_id();
			}

			// CHECK STUDENT INCOME INVOICES COLUMN
			$CSIIC = $this->main_m->getRowCond('student',array('student_id' => $student_id, 'income_invoices' => 1));
			if(empty($CSIIC))
				$this->db->where('student_id',$student_id)->update('student',array('income_invoices' => 1));

			$this->db->insert('income_invoice',$data);
			// FOR AJAX CODE
			$data['back'] = base_url().'accounts/add_income';


			returnResponse($data,'تم اضافة الايراد', true,base_url().'accounts/print_income_invoice/'.$this->db->insert_id());
		}
	}

	public function income_invoices()
	{
		$data['income'] = $this->main_m->getAll('*','income');
		loadStaticContent('accounts/income_invoices',$data,array('script' => 'ajax/accountsJs.php'));
	}



	public function incomeInvoicesAjax()
	{

		$date = array();
		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page      = 10; //item to display per page
		$page = $this->input->post('page');
		$type = $this->input->post('data_type');

		$type = str_replace("'",'"',$type);
		$searchArray = json_decode($type);
		if(isset($searchArray->date_from) && !empty($searchArray->date_from)){
			$date = (object) array('from' => $searchArray->date_from,'to' => $searchArray->date_to);
		}
		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}

		//get total number of records from database
		$get_total_rows = $this->m_accounts->income_date_invoice_numrows('income_date_invoice',$date); //hold total records in variable

		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range.


		// GET INCOME STRING
		$income = $this->main_m->getRowsCond('income');
		// GET INCOME INVOICE PER DAY
		$income_date = $this->m_accounts->income_date_invoice_res('income_date_invoice',$date,$limit);
		// GET < INVOICES >

		foreach ($income_date as $income_date_key => $income_date_value)
		{
			foreach ($income as $income_key => $income_value)
			{
				$income_invoice[$income_date_key][] = $this->main_m->getRowsCond('income_invoice',array(
					'income_id' => $income_value->income_id,
					'income_date_invoice_id' => $income_date_value->income_date_invoice_id,
				));
			}
		}
		// Filter < Invoices > per day
		if(isset($income_invoice)){
			foreach ($income_invoice as $income_invoice_key => $income_invoice_value) {

				foreach ($income_invoice_value as $each_income_invoice_key => $each_income_invoice_value) {

					if(!empty($each_income_invoice_value))
					{
						foreach ($each_income_invoice_value as $end_loop_key => $end_loop_value)
						{
							$invoices_array[$income_invoice_key][$income[$each_income_invoice_key]->income_type][] = $end_loop_value;
						}

					}
					else
					{
						$invoices_array[$income_invoice_key][$income[$each_income_invoice_key]->income_type][] = (object)
						array(
							'income_invoice_id' => 0,
	                    	'income_id' => 0,
	                    	'income_invoice_number' => 0,
	                    	'income_student_name' => 0,
	                    	'income_student_id' => 0,
	                    	'income_comment' => 0,
	                    	'income_invoice_payment' => 0,
	                    	'remission' => 0,
	                    	'income_invoice_value' => 0,
	                    	'income_invoice_value_ar' =>  0,
	                    	'income_date_invoice_id' => 0,
	                    	'income_invoice_created_by' => 0,
	                    	'income_invoice_created_in' => 0,
	                    	'income_invoice_created_date' => 0,
	                    	'income_invoice_year_id' => 0,
	                	);

					}

				}
			}
		}

		// Filter total value
		$total = array();
		for ($i=0; $i < count($income_date) ; $i++)
		{
			foreach ($invoices_array[$i] as $invoices_array_key => $invoices_array_value)
			{
				foreach ($invoices_array_value as $each_invoice_array_key => $end_each_invoice_array_value) {
					if(!empty($end_each_invoice_array_value)){
						$invoice_data[$i][$invoices_array_key][] = $end_each_invoice_array_value;
						$invoice_data[$i][$invoices_array_key]['total'][] = $end_each_invoice_array_value->income_invoice_value;
					}

					$Tvalue[$i][$invoices_array_key] = array_sum($invoice_data[$i][$invoices_array_key]['total']);
				}
			}

		}

		$z=0;
		$Num=0;
		for ($i=0; $i < count($income_date) ; $i++){ $Num++;
			if ($z % 2 == 0){
				$color = 'fff';
				$className = 'even';
			}
			else{
				$color = '#e9f7ff';
				$className = 'odd';
			}

				$result['table'] .= '<tr style="background:'.$color.'" role="row" class="Info '.$className.'">';
         		$result['table'] .= '<td>'.$Num.'</td>';
         		$result['table'] .= '<td>'.$income_date[$i]->date.'</td>';


         		foreach ($Tvalue[$i] as $invoices_total_key => $invoices_total_value) {
         			$countArray_value[$invoices_total_key] = $invoices_total_value;
         		}


         		$result['table'] .= '<td><a style="color:#2ca90b;font-weight:bold;text-decoration: underline;" href="javascript:;">'.@array_sum($countArray_value).'</a></td>';

         		foreach ($Tvalue[$i] as $invoices_total_key => $invoices_total_value) {
         			if($invoices_total_value > 0)
         				$result['table'] .= '<td><a style="color:blue;font-weight:bold;text-decoration: underline;" href="javascript:;">'.$invoices_total_value.'</a></td>';
         			else
         				$result['table'] .= '<td>'.$invoices_total_value.'</td>';
         			$countArray_value[$invoices_total_key] = $invoices_total_value;

         		}

         		$result['table'] .= '<td><a ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="الايصالات" href="'.base_url().'accounts/income_day_invoices/'.$income_date[$i]->income_date_invoice_id.'" class="btn btn-primary btn-fab"><i class="zmdi zmdi-receipt zmdi-hc-fw"></i></a></td>';

                $result['table'] .= '</tr>';
        $z++;
		}


		// PAGINATION RESULT
		$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
		echo returnResponse($result,$get_total_rows.' Data Found',true);

	}

	public function income_day_invoices($income_date_invoice_id){
		$data = array();
		$income = $this->main_m->getRowsCond('income_invoice',array('income_date_invoice_id' => $income_date_invoice_id));
		$IDI = $this->main_m->getRowCond('income_date_invoice',array('income_date_invoice_id' => $income_date_invoice_id));

		$out_income = $this->main_m->getRowsCond('ex_invoice',array('ex_invoice_date' => $IDI->date));

		// OUTCOME TOTAL VALUE
		foreach ($out_income as $out_key => $out_value) {
			$OTV[] = $out_value->ex_invoice_value;
		}
		if(!empty($OTV))
			$data['OTV'] = array_sum($OTV);
		else
			$data['OTV'] = 0;

		foreach ($income as $income_key => $income_value)
		{

			$student = $this->main_m->getRowCond('student',array('student_id' => $income_value->income_student_id));

			if(empty($student)){

			dd($student,false);
			echo $income_value->income_student_id;
			}

			$section = $this->main_m->getRowCond('section',array('section_id' => $student->section_id));

			$class   = $this->main_m->getRowCond('class',array('class_id' => $student->class_id));

			$date_in = explode(',',$income_value->income_invoice_created_in);

			$time = end($date_in);

			$date   = $date_in[0].' '.$date_in[1];

			$data['invoices'][] = (object)array(
				'income_invoice_id'		=> $income_value->income_invoice_id,
				'income_invoice_value'	=> $income_value->income_invoice_value,
				'value_ar'				=> $income_value->income_invoice_value_ar,
				'income_invoice_payment'=> $income_value->income_invoice_payment,
				'date'					=> $date,
				'time'					=> $time,
				'comment'				=> $income_value->income_invoice_comment,
				'income_invoice_created_by' => $income_value->income_invoice_created_by,
				'income_id'				=> $income_value->income_id,
				'income_invoice_number'	=> $income_value->income_invoice_number,
				'student_name' 			=> $student->student_name,
				'student_code' 			=> @$student->student_code,
				'section' 				=> @$section->section_name,
				'class' 				=> $class->class_name,
			);

			$total[] = $income_value->income_invoice_value;
		}

		$data['date'] = $date;
		$data['total'] = array_sum($total);
	//	dd($data);
		loadStaticContent('accounts/income_day_invoices',$data,array('script' => 'ajax/accountsJs.php'));
	}


	public function student_tuition_fees()
	{
		$data['section'] = $this->main_m->getAll('*','section');
		$data['class'] = $this->main_m->getAll('*','class');
		$data['year_studey'] = $this->db->order_by('year_type','DESC')->get('year_tb')->result();

		loadStaticContent('accounts/student_tuition_fees',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function student_tuition_feesAjax($value='')
	{


		$income_val = array();
		$post_data = array();
		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page = 30; //item to display per page
		$page = $this->input->post('page');
		$type = $this->input->post('data_type');

		$type = str_replace("'",'"',$type);
		$searchArray = json_decode($type);

		// SEARCH WITH MIN AND MAX VALUE
		if(isset($searchArray->value_max) && (!empty($searchArray->value_max) OR !empty($searchArray->value_min))){
			if(!empty($searchArray->value_max))
				$max = $searchArray->value_max;

			if(!empty($searchArray->value_min))
				$min = $searchArray->value_min;
		}

		if(isset($income_val))
		{
			foreach (json_decode($type) as $post_key => $post_value) {
				if(!empty($post_value))
					$invoice_da[$post_key] = $post_value;

			}
			// UNSET INCOME INVOICES TABLE POST VALUE TO WHERE FUNCTION STUDENT TABLE
				unset($post_data['value_max']);
				unset($post_data['value_min']);
		}

		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}

		//get total number of records from database
		if(isset($max))
			$invoice_da['invoice_value <='] = $max;
		if(isset($min))
			$invoice_da['invoice_value >='] = $min;


		if(!isset($invoice_da))
			$get_total_rows = $this->db->order_by('invoice_log_id','DESC')->get('invoice_log')->num_rows(); //hold total records in variable
		else
			$get_total_rows = $this->db->order_by('invoice_log_id','DESC')->where($invoice_da)->get('invoice_log')->num_rows(); //hold total records in variable

		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range.

		// GET INVOICES
		if(!isset($invoice_da))
			$invoice_data = $this->db->order_by('invoice_log_id','DESC')->get('invoice_log',$limit[1],$limit[0])->result();
		else
			$invoice_data = $this->db->order_by('invoice_log_id','DESC')->where($invoice_da)->get('invoice_log',$limit[1],$limit[0])->result();

		$z=0;
		$Num=0;
		if(!empty($invoice_data)){
			foreach ($invoice_data as $invoice_data_key => $invoice_data_value) {

				$student_class = $this->main_m->getRowCond('class',array('class_id' => $invoice_data_value->class_id));
				$student_section = $this->main_m->getRowCond('section',array('section_id' => $invoice_data_value->section_id));
				@$class = $student_class->class_name;
				@$section = $student_section->section_name;

				if(!empty($invoice_data_value)){

					if($invoice_data_value->payment == 'tuition_fees_cash'){
						$type = 'cash';
						$payment = 'كاش';
						$log_id = '';
					}
					else{
						$installment = $this->main_m->getRowCond('installment',array('installment_id' => $invoice_data_value->payment_value));
						$payment_value = @$installment->type;
						@$payment = 'القسط '.wordIn($payment_value);
						$type = 'installment';
						$log_id = $invoice_data_value->invoice_log_id;
					}

				if ($z % 2 == 0)
				{
					$color = 'fff';
					$className = 'even';
				}
				else
				{
					$color = '#e9f7ff';
					$className = 'odd';
				}
				$Num++;
				$result['table'] .= '<tr style="background:'.$color.'" role="row" class="Info '.$className.'">';
		        	$result['table'] .= '<td>'.$Num.'</td>';
		        	$result['table'] .= '<td>'.$invoice_data_value->student_name.'</td>';
		        	$result['table'] .= '<td>'.$invoice_data_value->student_code.'</td>';
		        	$result['table'] .= '<td>'.$invoice_data_value->national_id.'</td>';
		        	$result['table'] .= '<td>'.$class.'</td>';
		        	$result['table'] .= '<td>'.$section.'</td>';
		        	$result['table'] .= '<td>'.$payment.'</td>';
		        	$result['table'] .= '<td>'.$invoice_data_value->invoice_value.'</td>';
		        	$result['table'] .= '<td>'.$invoice_data_value->residual.'</td>';
					$result['table'] .= '
					<td>
						<a style="min-width:38px;width:38px;height:38px;font-size:22px" target="_blank" title="طباعة الايصال" href="'.base_url().'accounts/print_invoices/'.$type.'/'.$invoice_data_value->student_id.'/'.$invoice_data_value->invoice_id.'/'.$log_id.'" class="btn btn-primary btn-fab"><i class="zmdi zmdi-print zmdi-hc-fw"></i>
						</a>

						<a ata-toggle="tooltip" data-placement="top" data-value="'.$invoice_data_value->invoice_value.'" data-id="'.$invoice_data_value->invoice_id.'"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="تعديل" href="javascript:;" class="btn btn-info btn-fab"><i class="zmdi zmdi-edit zmdi-hc-fw"></i>
						</a>

						<a ata-toggle="tooltip" data-placement="top" title="حذف" href="javascript:;" data-url="'.base_url().'home/delete/" data-id="'.$invoice_data_value->invoice_id.'" data-table="invoices" data-column="invoice_id" data-join="invoice_log" style="min-width:38px;width:38px;height:38px;font-size:22px" class="btn btn-danger btn-fab delete"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>

					</td>';
		        $result['table'] .= '</tr>';

	        $z++;
                }

			}
			// PAGINATION RESULT
			$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
			echo returnResponse($result,$get_total_rows.' Data Found',true);
		}
		else
			echo returnResponse($result,'0 Data Found',true);
	}


	public function pay_tuition_fees()
	{
		$data['section'] = $this->main_m->getAll('*','section');
		$data['class'] = $this->main_m->getAll('*','class');
		$data['year_studey'] = $this->main_m->getAll('*','year_tb');
		loadStaticContent('accounts/pay_tuition_fees',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function pay_tuition_feesAjax()
	{


		$income_val = array();
		$post_data = array();
		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page = 10; //item to display per page
		$page = $this->input->post('page');
		$type = $this->input->post('data_type');
		$student_name = '';
		$type = str_replace("'",'"',$type);
		$searchArray = (array) json_decode($type);


		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}

		foreach ($searchArray as $key => $value) {
			if(!empty($value))
			$post_data[$key] = $value;
		}
		//get total number of records from database
		if(isset($post_data) && !empty($post_data))
		{
			if(isset($post_data['student_name']) && !empty($post_data['student_name']))
			{
				$this->db->like('student_name',$post_data['student_name'],'after');
				$student_name = $post_data['student_name'];
				unset($post_data['student_name']);
			}

			$this->db->where($post_data);
			$get_total_rows = $this->db->get('student')->num_rows(); //hold total records in variable
		}

		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range.


		// GET INCOME INVOICES
		//get total number of records from database
		if(isset($post_data) OR isset($student_name))
		{
			if(isset($student_name) && !empty($student_name))
				$this->db->like('student_name',$student_name,'after');

			$this->db->where($post_data);
			$student_data = $this->db->get('student',$limit[1],$limit[0])->result();
		}
		// GET < INVOICES > PER STUDENT ID AND INCOME MAX VALUE MIN VALUE

		$z=0;
		$Num=0;
		foreach ($student_data as $student_key => $student_data_value)
		{
			$Num++;
			$class = $this->main_m->getRowCond('class',array('class_id' => $student_data_value->class_id));
			$section = $this->main_m->getRowCond('section',array('section_id' => $student_data_value->section_id));
				if ($z % 2 == 0){
					$color = 'fff';
					$className = 'even';
				}
				else{
					$color = '#e9f7ff';
					$className = 'odd';
				}

					$result['table'] .= '<tr style="background:'.$color.'" role="row" class="Info '.$className.'">';
	         		$result['table'] .= '<td>'.$Num.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value->student_name.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value->student_code.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value->national_id.'</td>';
	         		$result['table'] .= '<td>'.$class->class_name.'</td>';
	         		$result['table'] .= '<td>'.$section->section_name.'</td>';

	         		$result['table'] .= '<td><a   title="الايصالات" href="'.base_url().'accounts/pay_tuition_fees_bill/'.$student_data_value->student_id.'" class="btn btn-primary"><i class="zmdi zmdi-money zmdi-hc-fw"></i> المصروفات</a></td>';

	                $result['table'] .= '</tr>';
	        $z++;
			}


			// PAGINATION RESULT
			$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);


			echo returnResponse($result,$get_total_rows.' Data Found',true);
	}


	public function pay_tuition_fees_bill($student_id)
	{
		$data = $this->main_m->getRowCond('student',array('student_id' => $student_id));
	//	$data = $this->main_m->getRowCond('student',array('student_id' => $student_id , 'student_code' => null));
		$data->invoice_file = $this->main_m->getRowCond('invoice_open_file',array('student_id' => $student_id));
		$city = $this->main_m->getRowCond('city',array('city_id' => $data->city));
		$state = $this->main_m->getRowCond('governorate',array('governorate_id' => $data->state));

		if(!empty($data->birth_place))
			$birth_place = $this->main_m->getRowCond('city',array('city_id' => $data->birth_place));

		$class = $this->main_m->getRowCond('class',array('class_id' => $data->class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data->section_id));
		$study_type = $this->main_m->getRowCond('study_type',array('study_id' => $data->study_type));
		$study_status = $this->main_m->getRowCond('study_status',array('status_id' => $data->study_status));
		$certificate_type = $this->main_m->getRowCond('certificate_type',array('certificate_id' => $data->certificate_type));

		$created_date 			= explode(' ', $data->created_date);
		$data->created_date 	= $created_date[0];
		$data->birth_place      = @$birth_place->city_name;
		$data->class 			= $class->class_name;
		$data->city 			= $city->city_name;
		$data->state 			= $state->governorate_name;
		$data->section 			= @$section->section_name;
		$data->certificate_type = @$certificate_type->certificate_type;
		$data->study_type 		= @$study_type->study_type;
		$data->study_status 	= $study_status->study_status;

		$data->year_tb = $this->main_m->getRowCond('year_tb',array('active' => 1));

		$data->tuition_fees = $this->main_m->getRowCond('tuition_fees',array('year_tb_id =' => $data->year_tb->year_tb_id, 'tuition_fees_id' => $data->tuition_fees_id, 'class_id' => $data->class_id));


		// INSTALLMENT QUERY
		$data->installment = $this->db->where(array('tuition_fees_id' => $data->tuition_fees->tuition_fees_id))->order_by('installment_id','ASC')->get('installment')->result();
		$data->installment_invoice = $this->main_m->getRowCond('invoices',array('student_id' => $student_id,'class_id' => $data->class_id, 'payment' => 'tuition_fees_installment', 'year_tb_id' => $data->year_tb->year_tb_id));


		$data->cash_invoice = $this->main_m->getRowCond('invoices',array('student_id' => $student_id,'class_id' => $data->class_id, 'payment' => 'tuition_fees_cash' , 'year_tb_id' => $data->year_tb->year_tb_id));

		$installment_data = $this->main_m->getRowsCond('installment',array('tuition_fees_id' => $data->tuition_fees->tuition_fees_id));
		foreach ($installment_data as $installment_key => $value)
		{
			$installment_value[] = $value->value;
		}

		$data->remission = $this->main_m->getRowsCond('student_remission',array('student_id' => $data->student_id));

		$data->count_cash = $data->tuition_fees->tuition_fees;
		$data->count_installment = array_sum($installment_value);

		loadStaticContent('accounts/pay_tuition_fees_bill',$data,array('script' => 'ajax/accountsJs.php'));


	}


	public function print_income_invoices($income_date_invoice_id)
	{
		$income = $this->main_m->getRowsCond('income_invoice',array('income_date_invoice_id' => $income_date_invoice_id));
		$IDI = $this->main_m->getRowCond('income_date_invoice',array('income_date_invoice_id' => $income_date_invoice_id));
		$out_income = $this->main_m->getRowsCond('ex_invoice',array('ex_invoice_date' => $IDI->date));

		// OUTCOME TOTAL VALUE
		foreach ($out_income as $out_key => $out_value) {
			$OTV[] = $out_value->ex_invoice_value;
		}
		if(!empty($OTV))
			$data['OTV'] = array_sum($OTV);
		else
			$data['OTV'] = 0;
		foreach ($income as $income_key => $income_value)
		{

			$student = $this->main_m->getRowCond('student',array('student_id' => $income_value->income_student_id));

			$section = $this->main_m->getRowCond('section',array('section_id' => $student->section_id));

			$class   = $this->main_m->getRowCond('class',array('class_id' => $student->class_id));

			$date_in = explode(',',$income_value->income_invoice_created_in);
			$time = end($date_in);
			$date   = $date_in[0].' '.$date_in[1];

			$data['invoices'][] = (object)array(
				'income_invoice_id'		=> $income_value->income_invoice_id,
				'income_invoice_value'	=> $income_value->income_invoice_value,
				'value_ar'				=> $income_value->income_invoice_value_ar,
				'income_invoice_payment'=> $income_value->income_invoice_payment,
				'date'					=> $date,
				'time'					=> $time,
				'comment'				=> $income_value->income_invoice_comment,
				'income_invoice_created_by' => $income_value->income_invoice_created_by,
				'income_id'				=> $income_value->income_id,
				'income_invoice_number'	=> $income_value->income_invoice_number,
				'student_name' 			=> $student->student_name,
				'student_code' 			=> @$student->student_code,
				'section' 				=> @$section->section_name,
				'class' 				=> $class->class_name,
			);

			$total[] = $income_value->income_invoice_value;
		}

		$data['date'] = $date;
		$data['total'] = array_sum($total);

		loadStaticContent('print/income_invoices_day',$data,NULL);

	}

	public function student_income()
	{
		$data['section'] = $this->main_m->getAll('*','section');
		$data['class'] = $this->main_m->getAll('*','class');
		$data['year_studey'] = $this->main_m->getAll('*','year_tb');
		loadStaticContent('accounts/student_income',$data,array('script' => 'ajax/accountsJs.php'));
	}


	public function student_incomeAjax()
	{

		$income_val = array();
		$post_data = array();
		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page = 10; //item to display per page
		$page = $this->input->post('page');
		$type = $this->input->post('data_type');

		$type = str_replace("'",'"',$type);
		$searchArray = json_decode($type);

		// SEARCH WITH MIN AND MAX VALUE
		if(isset($searchArray->value_max) && (!empty($searchArray->value_max) OR !empty($searchArray->value_min))){
			if(!empty($searchArray->value_max))
				$max = $searchArray->value_max;

			if(!empty($searchArray->value_min))
				$min = $searchArray->value_min;
		}


		if(isset($income_val)){
			foreach (json_decode($type) as $post_key => $post_value) {
				if(!empty($post_value))
					$post_data[$post_key] = $post_value;
			}
			// UNSET INCOME INVOICES TABLE POST VALUE TO WHERE FUNCTION STUDENT TABLE
				unset($post_data['value_max']);
				unset($post_data['value_min']);
		}

		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}


		//get total number of records from database
		$get_total_rows = $this->m_accounts->student_income_invoice_numrows('student',$post_data); //hold total records in variable
		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range.

		// GET INCOME INVOICES
		$student_income = $this->m_accounts->student_income_invoice_result('student',$limit,$post_data);

		// GET < INVOICES > PER STUDENT ID AND INCOME MAX VALUE MIN VALUE
		foreach ($student_income as $student_key => $student_value)
		{

			$income_val['income_student_id'] = $student_value->student_id;

			if(isset($max) && isset($min))
				$income_invoices_per_student = $this->db->query("SELECT * FROM `oct_income_invoice` WHERE `income_student_id` = $student_value->student_id AND `income_invoice_value` >= $min AND `income_invoice_value` <= $max ORDER BY `income_invoice_id` DESC")->result();
			else
				$income_invoices_per_student = $this->db->query("SELECT * FROM `oct_income_invoice` WHERE `income_student_id` = $student_value->student_id ORDER BY `income_invoice_id` DESC")->result();
			// echo "<pre>";
			// print_r($income_invoices_per_student);
			// echo "</pre>";


			$income_invoices_num_rows = $this->db->where('income_student_id',$student_value->student_id)->order_by('income_invoice_id','DESC')->get('income_invoice')->row();

			if(!empty($income_invoices_per_student))
			{
				$class = $this->main_m->getRowCond('class',array('class_id' => $student_value->class_id));
				$section = $this->main_m->getRowCond('section',array('section_id' => $student_value->section_id));
				$student_data[$student_key]['data'] = $student_value;
				$student_data[$student_key]['data']->class = $class->class_name;
				$student_data[$student_key]['data']->section = $section->section_name;
				$student_data[$student_key]['invoices'] = $income_invoices_per_student;
			}
		}

		$z=0;
		$Num=0;
		if(isset($student_data)){
			foreach ($student_data as $student_data_key => $student_data_value) { $Num++;
				$total = array();
				if ($z % 2 == 0){
					$color = 'fff';
					$className = 'even';
				}
				else{
					$color = '#e9f7ff';
					$className = 'odd';
				}

					$result['table'] .= '<tr style="background:'.$color.'" role="row" class="Info '.$className.'">';
	         		$result['table'] .= '<td>'.$Num.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value['data']->student_name.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value['data']->student_code.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value['data']->national_id.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value['data']->class.'</td>';
	         		$result['table'] .= '<td>'.$student_data_value['data']->section.'</td>';

	         		foreach ($student_data_value['invoices'] as $invoices_total_key => $invoices_total_value) {
	         			$total[] = $invoices_total_value->income_invoice_value;
	         		}
	         		$result['table'] .= '<td>'.array_sum($total).'</td>';


	         		$result['table'] .= '<td><a ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="الايصالات" href="'.base_url().'accounts/student_income_invoices/'.$student_data_value['data']->student_id.'" class="btn btn-primary btn-fab"><i class="zmdi zmdi-receipt zmdi-hc-fw"></i></a></td>';

	                $result['table'] .= '</tr>';
	        $z++;
			}


			// PAGINATION RESULT
			$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
			echo returnResponse($result,$get_total_rows.' Data Found',true);
		}
		else
			echo returnResponse($result,'0 Data Found',true);


	}

	public function student_income_invoices($STID)
	{

		$income = $this->main_m->getRowsCond('income_invoice',array('income_student_id' => $STID));

		$student = $this->main_m->getRowCond('student',array('student_id' => $STID));

		$section = $this->main_m->getRowCond('section',array('section_id' => $student->section_id));

		$class   = $this->main_m->getRowCond('class',array('class_id' => $student->class_id));

		$data['student'] = $student;
		foreach ($income as $income_key => $income_value)
		{

			$date_in = explode(',',$income_value->income_invoice_created_in);
			$time = end($date_in);
			$date   = $date_in[0].' '.$date_in[1];

			$data['invoices'][] = (object)array(
				'income_invoice_id'		=> $income_value->income_invoice_id,
				'income_invoice_value'	=> $income_value->income_invoice_value,
				'value_ar'				=> $income_value->income_invoice_value_ar,
				'income_invoice_payment'=> $income_value->income_invoice_payment,
				'date'					=> $date,
				'time'					=> $time,
				'income_id'				=> $income_value->income_id,
				'income_invoice_number'	=> $income_value->income_invoice_number,
				'student_name' 			=> $student->student_name,
				'student_code' 			=> $student->student_code,
				'section' 				=> $section->section_name,
				'class' 				=> $class->class_name,
			);

			$total[] = $income_value->income_invoice_value;
		}

		$data['date'] = $date;
		$data['total'] = array_sum($total);
		loadStaticContent('accounts/student_income_invoices',$data,array('script' => 'ajax/accountsJs.php'));

	}

	public function print_income_invoice($income_invoice_id){
		$data['income_data'] = $this->main_m->getRowCond('income_invoice',array('income_invoice_id' => $income_invoice_id));
		$data['student_data'] = $this->main_m->getRowCond('student',array('student_id' => $data['income_data']->income_student_id));

		$section = $this->main_m->getRowCond('section',array('section_id' => $data['student_data']->section_id));
		$class   = $this->main_m->getRowCond('class',array('class_id' => $data['student_data']->class_id));
		$data['student_data']->section = @$section->section_name;
		$data['student_data']->class   = @$class->class_name;



		loadStaticContent('print/income_invoice',$data,NULL);
	}



	public function downIncomeExcel(){
        ini_set('memory_limit', '-1');
		/** PHPExcel */
		$this->load->library('PHPExcel');


		// Instantiate a new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// RIGHT TO LEFT AR WORDS
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setRightToLeft(true);

		$LitterArray = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
		$titleArray  = array();

		$income = $this->main_m->getAll('*','income');

		$objPHPExcel->getActiveSheet()->getColumnDimension($LitterArray[0])->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle($LitterArray[0].'1')->applyFromArray(ExcelFontStyle(false,'000000'));
		$objPHPExcel->getActiveSheet()->setCellValue($LitterArray[0].'1', 'التاريخ');

		$z = 0; foreach ($income as $income_key => $income_value) { $z++;

			$income_invoice[] = $this->m_accounts->getResultIncome('oct_income_invoice',$income_value->income_id,'income_invoice_id DESC');

			// SET ARRAY TITLE TO EXCEL FILE

			$objPHPExcel->getActiveSheet()->getColumnDimension($LitterArray[$income_key+1])->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getStyle($LitterArray[$income_key+1].'1')->applyFromArray(ExcelFontStyle(false,'000000'));
			$objPHPExcel->getActiveSheet()->setCellValue($LitterArray[$income_key+1].'1', $income_value->income_type);

			// COUNT ARRAY FOR ALL
			$ArrayCounter[] = count($income_invoice[$income_key], COUNT_RECURSIVE);
		}
		// SET LAST COLUMN 'TOTAL'
		$objPHPExcel->getActiveSheet()->getColumnDimension($LitterArray[$z+1])->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle($LitterArray[$z+1].'1')->applyFromArray(ExcelFontStyle(false,'000000'));
		$objPHPExcel->getActiveSheet()->setCellValue($LitterArray[$z+1].'1', 'الاجمالى');

		// COUNT MAX ARRAY FOR > LOOP
		$MaxArrayValue = max($ArrayCounter);

		$z=0;
		$num = 2;

		for ($i=0; $i < $MaxArrayValue ; $i++)
		{


			foreach ($income as $income_key => $income_value) {
				if(isset($income_invoice[$income_key][$z]->income_invoice_value) && $income_invoice[$income_key][$z]->income_invoice_value > 0)
					$value = $income_invoice[$income_key][$z]->income_invoice_value;
				else
					$value = 0;

						$objPHPExcel->getActiveSheet()->setCellValue($LitterArray[$income_key+1].$num, $value);
						$total[] = $value;

					}
					$objPHPExcel->getActiveSheet()->setCellValue($LitterArray[count($income)+1].$num, array_sum($total));
			$z++;
			$num++;
		}


		//Redirect output to a client’s web browser (Excel5)
        $objPHPExcel->getActiveSheet()->setTitle('Chesse1');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="helloworld.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
	}


	// END INCOME FUNCTIONS
	public function edit_income_type(){
		$income_id= $this->input->post('income_id');
		$income_type= $this->input->post('income_type');
		$income_amount= $this->input->post('income_amount');
		$this->db->where('income_id',$income_id)->update('income',array('income_type' => $income_type, 'income_amount' => $income_amount));

		returnResponse($this->input->post(),'تم تعديل الايراد',true);
	}

	public function add_income_type(){

		$income_type= $this->input->post('income_type');
		$income_amount= $this->input->post('income_amount');
		$checkName = $this->main_m->getRowCond('income',array('income_type' => $income_type));
		if(empty($checkName))
		{
			$this->db->insert('income',array('income_type' => $income_type, 'income_amount' => $income_amount));
			returnResponse($this->input->post(),'تم اضافة البند ',true);
		}
	}


	public function print_expense_invoices($type){
		$data['type'] = str_replace(' ','',$type);
		if($data['type'] == 'normal')
			$data['type_str'] = 'عادية';
		elseif($data['type'] == 'honesty')
			$data['type_str'] = 'امانة';
		else{
			$data['type_str'] = '';
		}
		$data['expenses'] = '';

		$data['expense'] = $this->db->where( array('ex_invoice_date' => date('Y-m-d'), 'ex_invoice_payment' => $type) )->get('ex_invoice')->result();

		loadStaticContent('print/print_expenses_table',$data,NULL);
	}

	/// expenses function start
	#include view and var type
	public function expenses($type){
		$data['type'] = str_replace(' ','',$type);
		if($data['type'] == 'normal')
			$data['type_str'] = 'عادية';
		elseif($data['type'] == 'honesty')
			$data['type_str'] = 'امانة';
		else{
			$data['type_str'] = '';
		}
		$data['expenses'] = '';
		loadStaticContent('accounts/expenses_invoice',$data,array('script' => 'ajax/accountsJs.php'));
	}

	/// add_expense function start
	#include view and var type
	public function add_expense($type){

		$data['type'] = str_replace(' ','',$type);
		if($data['type'] == 'normal')
			$data['type_str'] = 'عادي';
		elseif($data['type'] == 'honesty')
			$data['type_str'] = 'امانة';
		else{
			$data['type_str'] = '';
		}
		$data['expenses'] = $this->main_m->getAll('*','expenses');
		// $data['expenses'] = $this->main_m->getRowsCond('expenses_invoice',array('type' => $type));
		loadStaticContent('accounts/add_expenses_invoice',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function export_expense($type)
	{
		ini_set('memory_limit', '-1');
		/** PHPExcel */
		$this->load->library('PHPExcel');


		// Instantiate a new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// RIGHT TO LEFT AR WORDS
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setRightToLeft(true);

		if($this->userData->owner != 1)
			$searchArray = array('ex_invoice_payment' => $type);
		else
			$searchArray = array('ex_invoice_payment' => $type);

		$ex_invoice = $this->main_m->getRowsCond('ex_invoice',$searchArray);

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'رقم الايصال');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'البند');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'اسم المستلم');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'عنوان المستلم');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'رقم البطاقة');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'ملاحظات');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'نوع الايصال');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'قيمة الايصال');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'اضيف بواسطة');
		$objPHPExcel->getActiveSheet()->setCellValue('J1', 'تاريخ الاضافة');

		$z = 1; foreach ($ex_invoice as $ex_invoice_key => $ex_invoice_value) { $z++;

			// SET ARRAY TITLE TO EXCEL FILE
			$expenses = $this->main_m->getRowCond('expenses',array('expenses_id' => $ex_invoice_value->expenses_id));
			if($type == 'normal')
				$exType = 'عادى';
			else
				$exType = 'امانة';

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$z, 'تاريخ الاضافة');
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$z, $expenses->expenses_name);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$z, $ex_invoice_value->receiver_name);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$z, $ex_invoice_value->receiver_address);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$z, $ex_invoice_value->receiver_national_id);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$z, $ex_invoice_value->comment);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$z, $exType);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$z, $ex_invoice_value->ex_invoice_value );
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$z, $ex_invoice_value->ex_invoice_created_by);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$z, $ex_invoice_value->ex_invoice_created_in);
		}

		//Redirect output to a client’s web browser (Excel5)
        // $objPHPExcel->getActiveSheet()->setTitle('Chesse1');
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="helloworld.xlsx"');
        // header('Cache-Control: max-age=0');
        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        // ob_end_clean();
        // $objWriter->save('php://output');
	}


	public function edit_expense($type,$invoice_id){

		$data['type'] = str_replace(' ','',$type);
		if($data['type'] == 'normal')
			$data['type_str'] = 'عادي';
		elseif($data['type'] == 'honesty')
			$data['type_str'] = 'امانة';
		else{
			$data['type_str'] = '';
		}
		$data['ex_invoice_id'] = $invoice_id;
		$data['expenses'] = $this->main_m->getAll('*','expenses');
		$data['expense_invoice'] = $this->main_m->getRowCond('ex_invoice',array('ex_invoice_id' => $invoice_id));
		loadStaticContent('accounts/edit_expenses_invoice',$data,array('script' => 'ajax/accountsJs.php'));
	}


	public function get_expenses_form(){

		$this->form_validation->set_rules('expense_type', 'نوع الايصال', 'trim|required|xss_clean');
		$this->form_validation->set_rules('expenses_id', 'بند المصروفات', 'trim|required|xss_clean');
		$this->form_validation->set_rules('receiver_name', 'اسم المستلم', 'trim|required|xss_clean');
		$this->form_validation->set_rules('receiver_address', 'عنوان المستلم', 'trim|xss_clean');
        $this->form_validation->set_rules('receiver_national_id', 'الرقم القومى للمستلم', 'trim|required|xss_clean');
        $this->form_validation->set_rules('ex_invoice_value', 'قيمة المبلغ', 'trim|required|xss_clean|numeric');
        $this->form_validation->set_rules('comment', 'ملاحظات', 'trim|xss_clean');

        if ($this->form_validation->run() == FALSE){
        	# RETURN FORM ERROR ::JSON
        	returnResponse(NULL , '<div class="alert alert-danger" role="alert"> '.validation_errors().' </div>' ,false);
        }
        $year_tb = $this->db->order_by('year_type','DESC')->get('year_tb')->row();
        # PASS GET VAR TO ARRAY $DATA
        foreach ($this->input->post() as $input_key => $input_value) {
        	$data[$input_key] = $input_value;
        }

        if($data['expense_type'] == 1)
        	$data['ex_invoice_payment'] = 'honesty';
        elseif($data['expense_type'] == 0)
	        $data['ex_invoice_payment'] = 'normal';

        $redirect = base_url('accounts/expenses/'.$data['ex_invoice_payment']);

	    $ex_invoice_value = new convert_ar($data['ex_invoice_value'], "male");
		$data['ex_invoice_value_ar'] = $ex_invoice_value->convert_number().' جنية فقط لاغير';




	    /// START INSERT AND EDIT OPTION >>>
	    if($this->input->post('ex_invoice_id') == null){

			$data['ex_invoice_number'] = substr(date("Y"),2).generateRandomInt(7);
	    	$data['ex_invoice_created_by'] = $this->userData->real_name;
			$data['ex_invoice_created_in'] = date("F j, Y, g:i a");
			$data['ex_invoice_date'] = date('Y-m-d');
			$data['ex_invoice_year_id'] = $year_tb->year_tb_id;

	    	$this->db->insert('ex_invoice',$data);
	    	$invoice_id = $this->db->insert_id();
	    }
	    else{

	    	$this->db->where('ex_invoice_id',$this->input->post('ex_invoice_id'))->update('ex_invoice',$data);
	    	$invoice_id = $this->input->post('ex_invoice_id');

	    }


	   	// REDIRECT TO PRINT LINK IN HONESTY INVOICE
	    if($data['expense_type'] == 1)
	    	$redirect = base_url().'accounts/print_expense_invoice/'.$invoice_id;

	    // BACK TO MAIN TABLE LINK
	    $data['back'] = base_url().'accounts/expenses/'.$data['ex_invoice_payment'];

	    returnResponse($data , 'تم حفظ الايصال' ,true ,$redirect);

		#$this->db->insert('');
	}

	public function print_expense_invoice($invoice_id){
		$data['invoice_data'] = $this->main_m->getRowCond('ex_invoice',array('ex_invoice_id' => $invoice_id));
		if($data['invoice_data']->ex_invoice_payment == 'normal')
			$data['type'] = 'عادى';
		else
			$data['type'] = 'امانة';
		loadStaticContent('print/expenses_invoice',$data,NULL);
	}


	public function get_expenses_ajax(){
	$result = array();
	$result['table'] = '';
	$result['pagination'] = '';
	$item_per_page      = 10; //item to display per page
	$page = $this->input->post('page');
	$type = $this->input->post('data_type');
	//Get page number from Ajax
	if($page != NULL){
	    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
	    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
	    $page_number = 1; //if there's no page number, set it to 1
	}

	//get total number of records from database
	$get_total_rows = $this->m_accounts->getResultLimitExCount('oct_ex_invoice',$type); //hold total records in variable

	//break records into pages
	$total_pages = ceil($get_total_rows/$item_per_page);

	//position of records
	$page_position = (($page_number-1) * $item_per_page);
	$limit = array($page_position, $item_per_page);
	//Limit our results within a specified range.
	$ex_invoice = $this->m_accounts->getResultLimitEx('oct_ex_invoice',$type,$limit,'ex_invoice_id DESC');

	$i = 0; foreach ($ex_invoice as $ex_invoice_key => $ex_invoice_value) { $i++;
		if ($i % 2 == 0)
			$className = 'even';
		else
			$className = 'odd';
		if($this->userData->owner == 1)
			$delete = '<a ata-toggle="tooltip" data-placement="top" title="حذف" href="javascript:;" data-url="'.base_url().'home/delete/" data-id="'.$ex_invoice_value->ex_invoice_id.'" data-table="ex_invoice" data-column="ex_invoice_id" style="min-width:38px;width:38px;height:38px;font-size:22px" class="btn btn-primary btn-fab delete"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>';
		else
			$delete = '';

		if($type == 'honesty')
			$print = '<a ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="مشاهدة ملف الطالب" target="_blank" href="'.base_url().'accounts/print_expense_invoice/'.$ex_invoice_value->ex_invoice_id.'" class="btn btn-default btn-fab"><i class="zmdi zmdi-eye zmdi-hc-fw"></i></a>';
		else
			$print = '';

		$result['table'] .=
			'<tr role="row" class="'.$className.'">
     			<td>'.$i.'</td>
              	<td>'.$ex_invoice_value->receiver_name.'</td>
              	<td>'.$ex_invoice_value->receiver_national_id.'</td>
              	<td>'.$ex_invoice_value->ex_invoice_value.'</td>
              	<td>'.$ex_invoice_value->comment.'</td>
              	<td>'.$ex_invoice_value->ex_invoice_created_by.'</td>
              	<td>'.$ex_invoice_value->ex_invoice_created_in.'</td>
              	<td>
              		'.$delete.'
              		<a ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="تعديل" href="'.base_url().'accounts/edit_expense/'.$ex_invoice_value->ex_invoice_payment.'/'.$ex_invoice_value->ex_invoice_id.'" class="btn btn-info btn-fab"><i class="zmdi zmdi-edit zmdi-hc-fw"></i></a>
              		'.$print.'
              	</td>
            </tr>';

	}

	//EDIT BUTTON

	$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	echo returnResponse($result,$get_total_rows.' Data Found',true);

	}


	public function student_card($student_id){
		$data = $this->main_m->getRowCond('student',array('student_id' => $student_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data->section_id));
		$class   = $this->main_m->getRowCond('class',array('class_id' => $data->class_id));
		$data->section = @$section->section_name;
		$data->class   = @$class->class_name;
		$this->load->view('print/card',$data,NULL);
	}

	// GET STUDENT INVOICES INFO
	public function gsii(){

		$student_id = $this->input->post('student_id');
		$result = '';
		$count_invoices = $this->main_m->getRowsCond('income_invoice',array('income_student_id' => $student_id));

		if(count($count_invoices) > 0){
			$result .= '<table class="table table-hover">';
			foreach ($count_invoices as $ci_key => $ci_value) {
				if($ci_value->income_invoice_id == 1)
					$result .= "<tr><td>رسوم فتح الملف</td><td>".$ci_value->income_invoice_value." جنية مصرى</td> </tr>";
				else
					$result .= "<tr><td>".$ci_value->income_invoice_payment."</td><td>".$ci_value->income_invoice_value." جنية مصرى</td> </tr>";
				$total[] = $ci_value->income_invoice_value;
			}
			$data = array('html' => $result, 'total' => array_sum($total) ,'response' =>true);
			$result .= '</table>';
		}
		else
			$data = array('response' => false);

		echo json_encode($data);
	}

	public function get_import_money_invoice()
	{

		$student_id = $this->input->post('student_id');
		$value = $this->input->post('value');
		$import_sm_input = $this->input->post('import_sm_input');

		$data['value'] = $value - $import_sm_input;
		$data['invoice_code'] = '100'.$student_id.rand(9999,9999);

		$data['student'] = $this->main_m->getRowCond('student',array('student_id' => $student_id));

		$class = $this->main_m->getRowCond('class',array('class_id' => $data['student']->class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data['student']->section_id));

		$data['student']->class = $class->class_name;
		$data['student']->section = $section->section_name;
		loadStaticContent('print/import_invoices',$data,NULL);
	}


	public function tuition_fees_setting($class_id = 1,$section_id = null)
	{

		$data['class_id'] = $class_id;
		$data['section_id'] = $section_id;
		if($class_id == 1)
		{
			$data['linkNum'] = 2;
			$data['class_name'] = 'الفرقة الثانية';
		}
		else{
			$data['linkNum'] = 1;
			$data['class_name'] = 'الفرقة الاولى';
		}
		// Get First Section ID
		$data['section'] = $this->db->get('section')->result();
		if($section_id == null)
		{
			$section_id = $data['section'][0]->section_id;
			$data['section_id'] = $data['section'][0]->section_id;
		}

		$year_tb = $this->db->order_by('year_type','DESC')->where( array('active ' => 1) )->get('year_tb')->result();

		foreach ($year_tb as $year_key => $year_value) {

			// CHECK SECTION TUITION FEES IF NOT FOUND INSERT
			$checkTuition = $this->main_m->countRows('tuition_fees',array('year_tb_id' => $year_value->year_tb_id, 'class_id' => $class_id, 'section_id' => $section_id));

			if($checkTuition == 0)
			{

				$tuition_feesType = array('normal','foreigner','intensified');
				foreach ($tuition_feesType as $TT_key => $TT_value) {
					$this->db->insert('tuition_fees',array(
						'tuition_fees' 	=> 0,
						'type'			=> $TT_value,
						'class_id'		=> $class_id,
						'section_id'	=> $section_id,
						'year_tb_id'	=> $year_value->year_tb_id,
					));
					$dbID = $this->db->insert_id();
					$instalmentNum = 5;
					for ($i=1; $i <= $instalmentNum; $i++) {
						$this->db->insert('installment',array(
							'installment_type'	=> 'القسط' .' '. WordIN($i),
							'value'				=> 0,
							'type'  			=> $i,
							'tuition_fees_id'	=> $dbID,
						));
					}
				}
			}


			$tuition_fees = $this->main_m->getRowsCond('tuition_fees',array('year_tb_id' => $year_value->year_tb_id, 'class_id' => $class_id,'section_id' => $section_id));
			foreach ($tuition_fees as $key => $tuition_fees_value) {

				$installment = $this->db->order_by('installment_id','ASC')->where(array('tuition_fees_id' => $tuition_fees_value->tuition_fees_id))->get('installment')->result();
				$data['installment'][$tuition_fees_value->tuition_fees_id] = $installment;

			}

			$data['year'][] = $year_value;
			$data['tuition_fees'][] = $tuition_fees;

		}


		loadStaticContent('accounts/tuition_fees_setting',$data,array('script' => 'ajax/accountsJs.php'));
	}

	public function insert_tuition_fees_setting($class_id,$section_id)
	{
		$class_id = $class_id;
		$section_id = $section_id;
		$year_tb_id 	= $this->input->post('year_tb_id');
		$normal 		= $this->input->post('normal');
		$foreigner 		= $this->input->post('foreigner');
		$intensified 	= $this->input->post('intensified');
		$installment 	= $this->input->post('installment');
		$tuition_fees_id= $this->input->post('tuition_fees_id');


		if($normal > array_sum($installment[$tuition_fees_id[0]]))
		{
			echo json_encode(array('response' => false, 'message' => 'يجب كتابة الاقساط صحيحة ', 'redirect' => null));
		}
		else
		{


			$array_data = array('normal' => $normal, 'foreigner' => $foreigner, 'intensified' => $intensified);
			$count_data = $this->main_m->countRows('tuition_fees',array('year_tb_id' => $year_tb_id, 'section_id' => $section_id,'class_id' => $class_id));
			if($count_data > 1)
			{

				$this->db->where(array('year_tb_id' => $year_tb_id ,'section_id' => $section_id,'type' => 'normal','class_id' => $class_id))->update('tuition_fees',
					array('tuition_fees' => $normal));

				$this->db->where(array('year_tb_id' => $year_tb_id,'section_id' => $section_id, 'class_id' => $class_id ,'type' => 'foreigner'))->update('tuition_fees',
					array('tuition_fees' => $foreigner));

				$this->db->where(array('year_tb_id' => $year_tb_id,'section_id' => $section_id, 'class_id' => $class_id ,'type' => 'intensified'))->update('tuition_fees',array('tuition_fees' => $intensified));

				foreach ($tuition_fees_id as $key => $value_tuition_id)
				{

					$z = 0;
					foreach ($installment[$value_tuition_id] as $ins_key => $inst_value) {
					$z++;
						$this->db->where(array('tuition_fees_id' => $value_tuition_id,'type' => $z))->update('installment',array('value' => $inst_value));


					}
				}



			}
			echo json_encode(array('response' => true, 'message' => 'تم تعديل البيانات', 'redirect' => base_url().'accounts/tuition_fees_setting/'.$class_id.'/'.$section_id));
		}
	}




	// DELETE INCOME TYPE FUNCTION
	public function deleteBoolen()
	{
		$table = $this->input->post('table');
		$column = $this->input->post('column');
		$id = $this->input->post('id');
		$this->db->where($column,$id)->update($table,array('deleted' => 1));
	}

}
