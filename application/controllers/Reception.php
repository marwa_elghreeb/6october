<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reception extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once('num_to_ar.php');
	}


	public function index()
	{
		$data = $this->main_m->limitOrderBy('student','created_date','DESC',10);
		loadStaticContent('reception/reception_students',$data,array('script' => 'ajax/receptionJs.php'));
	}

	public function enquiry()
	{
		$data['acquaintance'] = $this->main_m->getAll('*','acquaintance');
		loadStaticContent('reception/enquiry',$data,array('script' => 'ajax/receptionJs.php'));
	}

	public function insert_enquiry()
	{

		foreach ($this->input->post() as $key => $value) {
			$data[$key] = $value;
		}

		$data['created_by'] = $this->userData->real_name;
		if($this->input->post('enquiry_id') != null)
		{
			$enquiry_id = $this->input->post('enquiry_id');
			unset($data['enquiry_id']);
			$this->db->where('enquiry_id',$this->input->post('enquiry_id'))->update('enquiry',$data);
		}
		else
			$this->db->insert('enquiry',$data);

		returnResponse($data ,'تم  تسجيل البيانات بنجاح',true,base_url().'reception/enquiry');


	}

	public function edit_enquiry()
	{

		$id = $this->input->post('id');
		$acquaintance_way = '';
		$enquiry = $this->main_m->getRowCond('enquiry',array('enquiry_id' => $id));

		$acquaintance = $this->main_m->getAll('*','acquaintance');

		$acquaintance_way .= '<div class="form-group">
	                                <label for="date" class="col-sm-4 control-label">سيلة التعارف </label>
	                                <div class="col-sm-8">
										<select name="acquaintance_way" id="acquaintance_way" class="form-control acquaintanceSelect">';

		foreach ($acquaintance as $key => $value) {
			if($value->acquaintance_id == $enquiry->acquaintance_way)
				$selected = 'selected';
			else
				$selected = '';

            $acquaintance_way .= '<option '.$selected.' value="'.$value->acquaintance_id.' ">'.$value->acquaintance_name.'</option>';

		}
		$acquaintance_way .= '</select></div></div>';


		$checked1 = '';
		$checked2 = '';
		$reasone1 = '';
		$reasone2 = '';
		$reasone3 = '';
		$reasone4 = '';

		if($enquiry->another_academy == 0)
			$checked1 = 'checked';
		else
			$checked2 = 'checked';

		$another_academy = '
		<div class="form-group col-md-12">
            <label for="" class="col-sm-4 control-label" style="padding-right: 0px;">زيارة معهد اخر</label>
          	<label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
            <input type="radio" name="another_academy" '.$checked1.'  id="inlineRadio3" value="0"><span class="circle"></span><span class="check"></span> نعم
          	</label>
          	<label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
            <input type="radio" name="another_academy" '.$checked2.'  id="inlineRadio4" value="1"><span class="circle"></span><span class="check"></span> لا
          </label>
        </div>';

        if($enquiry->reasone == 0)
        	$reasone1 = 'checked';
        elseif($enquiry->reasone == 1)
        	$reasone2 = 'checked';
        elseif($enquiry->reasone == 2)
        	$reasone3 = 'checked';
		else
			$reasone4 = 'checked';
        $reasone ='
        <div class="form-group col-md-12">
            <label for="" class="col-sm-4 control-label" style="padding-right: 0px;">الغرض من الزيارة</label>
          	<label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
            <input type="radio" name="reasone" '.$reasone1.'  id="inlineRadio3" value="0"><span class="circle"></span><span class="check"></span> استفسار
          	</label>
          	<label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
            <input type="radio" name="reasone" '.$reasone2.'  id="inlineRadio4" value="1"><span class="circle"></span><span class="check"></span> استفسار بورق
          </label>
			<label for="" class="col-sm-4 control-label" style="padding-right: 0px;"></label>
          <label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
            <input type="radio" name="reasone" '.$reasone3. ' id="inlineRadio4" value="2"><span class="circle"></span><span class="check"></span> تقديم
          </label>
          <label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
            <input type="radio" name="reasone" '.$reasone4. ' id="inlineRadio4" value="3"><span class="circle"></span><span class="check"></span> تقديم بورق
          </label>
        </div>';



		echo '<form class="form_ajax" method="POST" result-data="form_result" action="'.base_url().'reception/insert_enquiry/">
                        <div class="modal-body">
	                            <div class="form-group">
	                                <label for="day" class="col-sm-4 control-label">اليوم </label>
	                                <div class="col-sm-8">
	                                    <input id="day" type="text" data-rule-required="true"  data-msg-required="يجب كتابة  اليوم" name="day" placeholder="اكتب  اليوم" value="'.$enquiry->day.'" class="form-control" >
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="date" class="col-sm-4 control-label">التاريخ </label>
	                                <div class="col-sm-8">
	                                    <input id="date" type="text" data-rule-required="true"  data-msg-required="يجب كتابة  التاريخ" name="date" placeholder="اكتب  التاريخ" value="'.$enquiry->date.'"  class="form-control" >
	                                </div>
	                            </div>
	                        	<div class="form-group">
		                            <label for="name" class="col-sm-4 control-label"> الاسم </label>
	                                <div class="col-sm-8">
	                                    <input id="name" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   الاسم" name="name" placeholder="الاسم" value="'.$enquiry->name.'"  class="form-control" >
	                                </div>
		                        </div>
		                        <input type="hidden" name="enquiry_id" value="'.$enquiry->enquiry_id.'">
	                            <div class="form-group">
	                                <label for="qualification" class="col-sm-4 control-label">المؤهل </label>
	                                <div class="col-sm-8">
	                                    <input id="qualification" type="text" name="qualification" placeholder="المؤهل" value="'.$enquiry->qualification.'"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="grade" class="col-sm-4 control-label">المجموع </label>
	                                <div class="col-sm-8">
	                                    <input id="grade" type="text" name="grade" placeholder="المجموع" value="'.$enquiry->grade.'"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="graduate_year" class="col-sm-4 control-label">سنة التخرج </label>
	                                <div class="col-sm-8">
	                                    <input id="graduate_year" type="text" name="graduate_year" placeholder="سنة التخرج" value="'.$enquiry->graduate_year.'"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="school" class="col-sm-4 control-label">المدرسة </label>
	                                <div class="col-sm-8">
	                                    <input id="school" type="text" name="school" placeholder="المدرسة" value="'.$enquiry->school.'"  class="form-control" >
	                                </div>
	                            </div>
	                            '.$acquaintance_way.'
	                            <div class="form-group">
	                                <label for="phone" class="col-sm-4 control-label">رقم الهاتف </label>
	                                <div class="col-sm-8">
	                                    <input id="phone" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   رقم الهاتف   " name="phone" min="11" placeholder="رقم الهاتف" value="'.$enquiry->phone.'"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="area" class="col-sm-4 control-label">المنطقة </label>
	                                <div class="col-sm-8">
	                                    <input id="area" type="text" name="area" placeholder="المنطقة" value="'.$enquiry->area.'"  class="form-control" >
	                                </div>
	                            </div>
	                            '.$another_academy.'
	                            '.$reasone.'

	                            <div class="form-group">
	                                <label for="first_call" class="col-sm-4 control-label">المكالمة الاولى   </label>
	                                <div class="col-sm-8">
	                                    <textarea id="first_call" type="text" name="first_call" placeholder="المكالمة الاولى  " class="form-control" >'.$enquiry->first_call.'</textarea>
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="second_call" class="col-sm-4 control-label">المكالمة  الثانية   </label>
	                                <div class="col-sm-8">
	                                    <textarea id="second_call" type="text" name="second_call" placeholder="المكالمة الثانية " class="form-control" >'.$enquiry->second_call.'</textarea>
	                                </div>
	                            </div>

                        </div>
                        <div class="form_result col-md-12"></div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="margin-top: 30px;background: whitesmoke;">
                            <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>';

	}

	public function enquiryAjax()
	{

		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page      = 10; //item to display per page
		$page = $this->input->post('page');
		$post_data = (array)json_decode(str_replace("'",'"',$this->input->post('post_data')));

		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}

		//get total number of records from database
		$get_total_rows = $this->main_m->countRows('oct_enquiry',$post_data); //hold total records in variable
		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range.
		$student_data = $this->db->where($post_data)->order_by('enquiry_id DESC')->get('oct_enquiry',$limit[1],$limit[0])->result();
		$i = 0; foreach ($student_data as $student_key => $student_value) { $i++;
			if ($i % 2 == 0)
				$className = 'even';
			else
				$className = 'odd';

			switch ($student_value->reasone) {
				case '0':
					$reason = 'استفسار';
					break;
				case '1':
					$reason = 'استفسار  بورق';
					break;
				case '2':
					$reason = 'تقديم';
					break;
				case '3':
					$reason = 'تقديم بورق';
					break;

				default:
					$reason = 'استفسار';
					break;
			}
			$result['table'] .=
						'<tr role="row" class="'.$className.'">
	             			<td>'.$i.'</td>
	                      	<td>'.$student_value->date.'</td>
	                      	<td>'.$student_value->name.'</td>
	                      	<td>'.@$section->qualification.'</td>
	                      	<td>'.@$class->area.'</td>
	                      	<td>'.@$student_value->phone.'</td>
	                      	<td>'.@$reason.'</td>
	                      	<td>'.@$student_value->first_call.'</td>
	                      	<td>'.@$student_value->second_call.'</td>
	                      	<td>

	                      	<div class="btn-group">
	                            <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
	                                اعدادات
	                                <span class="caret"></span>
	                            </button>
	                            <ul class="dropdown-menu" role="menu">
	                                <li>
		                      			<a  title="حذف" href="javascript:;" class="edit_enquiry" data-url="'.base_url().'reception/edit_enquiry/" data-id="'.$student_value->enquiry_id.'" data-table="enquiry" data-column="enquiry_id" style="" > تعديل <i class="zmdi zmdi-edit"></i></a>
	                                </li>

	                                <li>
		                      			<a  title="حذف" href="javascript:;" data-url="'.base_url().'home/delete/" data-id="'.$student_value->enquiry_id.'" data-table="enquiry" data-column="enquiry_id" style="" class="delete"> حذف  <i class="zmdi zmdi-delete"></i></a>
	                                </li>
	                            </ul>
	                        </div>
	                      	</td>
	                    </tr>';

		}





		$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
		echo returnResponse($result,$get_total_rows.' Data Found',true);
	}

	// PRINT CARDS FUNCTION
	public function print_cards(){
		$student = $this->db->where(array('printed' => 0))->get('student')->result();

		foreach ($student as $stu_key => $stu_value)
		{
			if(file_exists('assets/uploads/'.$stu_value->profile_photo)){
				$data['students'][] = $stu_value;
			}
		}
		loadStaticContent('reception/studetn_print',$data,array('script' => 'ajax/receptionJs.php'));

	}
	// END FUNCTION

	public function add_new_student()
	{
		$data['governorate'] 		= $this->main_m->getAll('*','governorate');
		$data['qualification'] 		= $this->main_m->getAll('*','qualification');
		$data['section'] 			= $this->main_m->getAll('*','section');
		$data['class'] 				= $this->main_m->getAll('*','class');
		$data['certificate_type'] 	= $this->main_m->getAll('*','certificate_type');
		$data['study_type'] 		= $this->main_m->getAll('*','study_type');
		$data['study_status'] 		= $this->main_m->getAll('*','study_status');
		$data['acquaintance'] 		= $this->main_m->getAll('*','acquaintance');
		loadStaticContent('reception/reception_add_student',$data,array('script' => 'ajax/receptionJs.php'));
	}

	public function add_student()
	{
		$data = array();

		$this->form_validation->set_rules('student_name', 'اسم الطالب', 'trim|required|xss_clean');
		$this->form_validation->set_rules('nationality', 'جنسية الطالب', 'trim|required|xss_clean');
		$this->form_validation->set_rules('religion', 'الديانة', 'trim|required|xss_clean');
		$this->form_validation->set_rules('gender', 'الجنس', 'trim|required|xss_clean');
        $this->form_validation->set_rules('date_of_birth', 'تاريخ الميلاد', 'trim|required|xss_clean');
        $this->form_validation->set_rules('national_id', 'رقم البطاقة', 'trim|required|xss_clean');

        if($this->input->post('nationality') == 'مصرى')
        {
        	$this->form_validation->set_rules('state', 'المحافظة', 'trim|required|xss_clean');
        	$this->form_validation->set_rules('city', 'المدينة', 'trim|required|xss_clean');
        }

        $this->form_validation->set_rules('area_name', 'المنطقة', 'trim|required|xss_clean');
        $this->form_validation->set_rules('address', 'العنوان', 'trim|required|xss_clean');
        $this->form_validation->set_rules('section_id', 'القسم', 'trim|required|xss_clean');
        $this->form_validation->set_rules('qualification', 'المؤهل الدراسى', 'trim|required|xss_clean');
        $this->form_validation->set_rules('study_type', 'نوع الدراسة', 'trim|required|xss_clean');
        $this->form_validation->set_rules('study_status', 'حالة الدراسة', 'trim|required|xss_clean');
        $this->form_validation->set_rules('certificate_type', 'نوع الشهادة', 'trim|required|xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
        	# RETURN FORM ERROR ::JSON
        	returnResponse(NULL , '<div class="alert alert-danger" role="alert"> '.validation_errors().' </div>' ,false);

        }
        else{



        	# PASS GET VAR TO ARRAY $DATA
        	foreach ($this->input->post() as $input_key => $input_value)
        	{
        		$data[$input_key] = $input_value;
        	}
        	$data['student_phone'] = json_encode($this->input->post('student_phone'));
        	if($this->input->post('acquaintance_student') != NULL)
        	{
        		unset($data['acquaintance_way']);
        		unset($data['acquaintance_student']);
        	}


        	// INSERT QUALIFICATION TEXT OPTION //
        	if(isset($data['qualification_'])){
        		unset($data['qualification_']);
        		$qualification_data = $this->main_m->getRowCond('qualification',array('qualification' => $data['qualification']));
        		if(isset($qualification_data->qualification_id)){
        			$data['qualification'] = $qualification_data->qualification_id;
        		}else{
        			$this->db->insert('qualification',array('qualification' => $data['qualification']));
        			$data['qualification'] = $this->db->insert_id();
        		}

        	}

        	// INSERT MANAGEMENT LEARNING TEXT
        	# WILL DELETE AFTER INSERT MANAGEMENT INSERT OPTION
        	if(isset($data['management_learning_text'])){
        		$management_learning_data = $this->main_m->getRowCond('administrations',
        			array(
        				'governorate_id' => $data['state_administration'],
        				'administration_name' => $data['management_learning_text'])
        			);

        		if(isset($management_learning_data->administration_id)){
        			$data['management_learning'] = $management_learning_data->administration_id;
        		}else{

        			$this->db->insert('administrations',array('governorate_id' => $data['state_administration'],'administration_name' => $data['management_learning_text']));
        			$data['management_learning'] = $this->db->insert_id();

        		}
        		$governorate_id = $data['state_administration'];
        		unset($data['state_administration']);
        		unset($data['management_learning_text']);


        	}
        	else
        	{
        		$governorate_id = $data['state_administration'];
        		unset($data['state_administration']);
        	}


        	// INSERT city TEXT
        	# WILL DELETE AFTER INSERT city INSERT OPTION
        	if(isset($data['city_text'])){
        		$city_data = $this->main_m->getRowCond('city',array('governorate_id' => $data['state'],'city_name' => $data['city_text']));
        		if(isset($city_data->city_id)){
        			$data['city'] = $city_data->city_id;
        		}else{
        			$this->db->insert('city',array('governorate_id' => $data['state'],'city_name' => $data['city_text']));
        			$data['city'] = $this->db->insert_id();
        		}
        		unset($data['city_text']);

        	}
			else
        		unset($data['city_text']);

        	// INSERT city TEXT TO birth_place column
        	# WILL DELETE AFTER INSERT city INSERT OPTION
        	if(isset($data['birth_place_text'])){
        		$birth_place = $this->main_m->getRowCond('city',array('governorate_id' => $data['state'],'city_name' => $data['birth_place_text']));
        		if(isset($birth_place->city_id)){
        			$data['city'] = $birth_place->city_id;
        		}else{
        			$this->db->insert('city',array('governorate_id' => $data['birth_place_governorate'],'city_name' => $data['birth_place_text']));
        			$data['birth_place'] = $this->db->insert_id();
        		}
        		unset($data['birth_place_text']);

        	}

        	if(isset($data['student_school_text'])){
        		$student_school = $this->main_m->getRowCond('school',array('administration_id' => $data['management_learning'], 'school_name' => $data['student_school_text']));
        		if(isset($student_school->school_id))
        			$data['student_school'] = $student_school->school_id;
        		else
        		{

        			$this->db->insert('school',array('administration_id' => $data['management_learning'],'governorate_id' => $governorate_id, 'school_name' => $data['student_school_text']));
        			$data['student_school'] = $this->db->insert_id();
        		}
        		unset($data['student_school_text']);

        	}


        	# USER CREATED
        	$data['created_by'] = $this->userData->real_name;

        	# DATE WITH DATE STRING TYPE
        	$data['created_in'] = date("F j, Y, g:i a");

        	// GET GOVERNRATE BY NAME IF REQUEST = ID
        	if(is_int($this->input->post('birth_place_state'))){
        		$governorate_name = $this->main_m->getRowCond('governorate',array('governorate_id' => $this->input->post('birth_place_state')));
        		$data['birth_place_state'] = $governorate_name->governorate_name;
        	}
        	//END GOVERNRATE

			// STUDENT TYPE
			$nationality = $this->input->post('nationality');
			$study_status = $this->input->post('study_status');

        	if($nationality == 'مصرى' )
        		$st_type = 'normal';
        	else
        		$st_type = 'foreigner';

			if($study_status == 2)
        		$st_type = 'intensified';

        	$getTb_year = $this->main_m->getRowCond('year_tb',array('active' => 1));

        	$getTuitionFeesId = $this->main_m->getRowCond('tuition_fees',
					array(
						'year_tb_id' => $getTb_year->year_tb_id,
						'type' => $st_type,
						'class_id' => $this->input->post('class_id'),
						'section_id' => $data['section_id']
					));

        	$data['tuition_fees_id']  = $getTuitionFeesId->tuition_fees_id;
        	$data['tuition_fees_type']  = $getTuitionFeesId->type;
        	if($this->input->post('student_id') == NULL)
        	{

        		$data['school_year'] = $getTb_year->year_type.'/'.($getTb_year->year_type + 1);
        		$data['year_tb_id'] = $getTb_year->year_tb_id;
        		$this->db->insert('student',$data);
        		$data['student_id'] = $this->db->insert_id();
        	}
        	else
        	{
        		$data['updated_by'] = $this->userData->real_name;
        		$data['updated_at'] = date('Y-m-d');
				$data['student_id'] = $this->input->post('student_id');
				$data['class_id']   = $this->input->post('class_id');
        		$this->db->where('student_id',$data['student_id'])->update('student',$data);
        	}

			$steps_array = array(
				'student_id' => $data['student_id'],
				'tuition_fees_id' => $data['tuition_fees_id'],
				'amount_status' => null,
				'residual' 		=> null,
				'class_status' 	=> null,
				'class_id' 		=> 1,
			);

			if($this->db->where(array('student_id' => $data['student_id'],'class_id' => $data['class_id']))->get('student_steps')->num_rows() == 0)
				$this->db->insert('student_steps',$steps_array);
			else{
				unset($steps_array['amount_status']);
				unset($steps_array['residual']);
				unset($steps_array['class_status']);
				$this->db->where('student_id',$data['student_id'])->update('student_steps',$steps_array);
			}

        	// GET SECTION NAME WITH SECTION ID ::
        	$section = $this->main_m->getRowCond('section',array('section_id' => $data['section_id']));
        	$data['section_name'] = $section->section_name;

        	$data['breadcrumb'] = 'الطالب : '.$data['student_name'];
        	$data['print_file_url']   = base_url().'reception/print_student_file/'.$data['student_id'];

        	if($nationality != 'مصرى')
        		$data['future_institue']   = base_url().'reception/future_institue_student_file/'.$data['student_id'];
        	else
        		$data['future_institue']   = '';

        	$data['open_file_amount'] = $this->main_m->getOneRow('open_file_amount','open_file');
        	$data['bill_study_year'] = date('Y').'/'.(date('Y') + 1);
        	# RETURN RESPONSE TRUE. ::JSON
        	returnResponse($data ,'تم تسجيل البيانات بنجاح',true);

        }


	}
	public function upload(){

		$InputName  = key($_FILES);
		$uploadFile = uploadFile('./assets/uploads/',$InputName);

		if($uploadFile['response'] == true){

			$student_id = $this->input->post('student_id');
			// GRADUATED STUDENT TABLE
			if($this->input->post('table') != null){
			$table = $this->input->post('table');
				$checkStudentPhoto = $this->main_m->getRowCond($table,array( 'student_id' => $student_id));

				if(isset($checkStudentPhoto->graduated_students_id))
				{

					if(!empty($checkStudentPhoto->$InputName)){
						unlink('./assets/uploads/'.$checkStudentPhoto->$InputName);
						$update = $this->db->where('student_id',$student_id)->update($table,array($InputName => $uploadFile['filename']));
					}
					else
						$update = $this->db->where('student_id',$student_id)->update($table,array($InputName => $uploadFile['filename']));
				}
				else
					$this->db->insert($table,array($InputName => $uploadFile['filename'], 'student_id' => $student_id));

				$graduated_studentTable = array('international_certificate','university_certificate','certificate_ministry','national_id_photo','study_file_photo', 'birth_certificate_photo', 'profile_photo');



				returnResponse(base_url().'assets/uploads/'.$uploadFile['filename'], 'uploaded',$uploadFile['response'],$uploadFile['filename']);

			}
			else{

				$checkStudentPhoto = $this->main_m->getRowCond('student',array( 'student_id' => $student_id));

				if(!empty($checkStudentPhoto->$InputName)){

					if(file_exists('./assets/uploads/'.$checkStudentPhoto->$InputName))
						unlink('./assets/uploads/'.$checkStudentPhoto->$InputName);

					$update = $this->db->where('student_id',$student_id)->update('student',array($InputName => $uploadFile['filename']));
				}
				else
					$update = $this->db->where('student_id',$student_id)->update('student',array($InputName => $uploadFile['filename']));

				returnResponse(base_url().'assets/uploads/'.$uploadFile['filename'], 'uploaded',$uploadFile['response'],$uploadFile['filename']);
			}
		}
		else
			returnResponse(base_url().'assets/img/fileupload/fileupload-bg.jpg', $uploadFile['error'], $uploadFile['response']);
	}

	public function openStudentFile(){
			//POST PROPERTIES
			$student_id = $this->input->post('student_id');
			// Get Student Result
			$student_data = $this->main_m->getRowCond('student',array('student_id' => $student_id));

			// Get Income Result
			$income_ = $this->main_m->getRowCond('income',array('income_id' => 1));
			$open_file_amount = $income_->income_amount;

			if(!empty($student_id) && !empty($open_file_amount)){
				$data = array(
					'student_id' 	=> $student_id,
					'bill_amount' 	=> $open_file_amount,
					'bill_number'	=> generateRandomInt(7).substr(date("Y"),2),
					'date' 			=> date('Y-m-d'),
					'created_at' 	=> date("F j, Y, g:i a"),
					'created_by' 	=> $this->userData->real_name,
				);

				$checkStudentInvoice = $this->main_m->countRows('invoice_open_file',array('student_id' => $student_id));
				if($checkStudentInvoice == 0)
				{
					$this->db->where('student_id' , $student_id)->update('student',array('student_status' => 1,'open_file_bill_number' => $data['bill_number']));

					$bill_amount_ar = new convert_ar($open_file_amount, "male");

					$data['bill_amount_ar'] = $bill_amount_ar->convert_number().' جنية فقط لاغير';

					$this->db->insert('invoice_open_file',$data);
					$data_id = $this->db->insert_id();

					// INSERT INVOICES TO INCOME INVOICES Tables:: (income_invoice),(income_date_invoice)

					$income_date_invoice = $this->db->where('date',date('Y-m-d'))->get('income_date_invoice')->row();
					if(!empty($income_date_invoice))
					{
						$ipd = $income_date_invoice->income_date_invoice_id;
						$this->db->where('date',date('Y-m-d'))->update('income_date_invoice',array('income_invoices_counter' => $income_date_invoice->income_invoices_counter + 1));
					}
					else
					{
						$this->db->insert('income_date_invoice',array('date' => date('Y-m-d'), 'income_invoices_counter' => 1));
						$ipd = $this->db->insert_id();
					}

					// Year Table
					$year_tb = $this->db->order_by('year_tb_id','DESC')->get('year_tb')->row();
					$incomeData = array(
						'income_id' 				=> $income_->income_id,
						'income_invoice_number' 	=> $data['bill_number'],
						'income_student_name'  		=> $student_data->student_name,
						'income_student_id'			=> $student_id,
						'income_comment'			=> 'تم دفع ( '.$income_->income_type.' ) من الطالب /'.$student_data->student_name.'',
						'income_invoice_payment'	=> $income_->income_type,
						'remission'					=> 0,
						'income_invoice_value'		=> $open_file_amount,
						'income_invoice_value_ar'	=> $data['bill_amount_ar'],
						'income_date_invoice_id'	=> $ipd,
						'income_invoice_created_by'	=> $this->userData->real_name,
						'income_invoice_created_in'	=> $data['created_at'],
						'income_invoice_year_id'	=> $year_tb->year_tb_id,
					);
					$this->db->insert('income_invoice',$incomeData);

					$CSIIC = $this->main_m->getRowCond('student',array('student_id' => $student_id, 'income_invoices' => 1));
					if(empty($CSIIC))
						$this->db->where('student_id',$student_id)->update('student',array('income_invoices' => 1));


					returnResponse('Invoice',base_url().'accounts/print_invoices/open_file/'.$student_id.'/'.$data_id,true);
				}
				else
					returnResponse(NULL,'تم دفع الرسوم من قبل',false);

			}
			else
				returnResponse(NULL,'يجب املاء جميع الحقول',false);

	}

	public function  checkFiles(){
		// $student_id = $this->input->post('student_id');
		// $checkStudentFiles = $this->main_m->countRows('student' ,array('profile_photo !=' => '', 'birth_certificate_photo !=' => '', 'study_file_photo !=' => '', 'national_id_photo !=' => '','student_id' => $student_id ));
		// if($checkStudentFiles < 1)
		// 	returnResponse('Invoice','يرجى اضافة جميع الصور المطلوبة',false);
		// else
			returnResponse('Invoice','يرجى اضافة جميع الصور المطلوبة',true);
	}

	public function future_institue_student_file($id){
		$student_id = intval($id);
		$data = $this->main_m->getRowCond('student',array('student_id' => $student_id));
		$this->load->view('print/future_institue',$data,NULL);

	}
	public function print_student_file($id){
		$student_id = intval($id);
		$data = $this->main_m->getRowCond('student',array('student_id' => $student_id));
		$city = $this->main_m->getRowCond('city',array('city_id' => $data->city));
		$state = $this->main_m->getRowCond('governorate',array('governorate_id' => $data->state));
		$class = $this->main_m->getRowCond('class',array('class_id' => $data->class_id));
		$section = $this->main_m->getRowCond('section',array('section_id' => $data->section_id));
		$study_type = $this->main_m->getRowCond('study_type',array('study_id' => $data->study_type));
		$study_status = $this->main_m->getRowCond('study_status',array('status_id' => $data->study_status));
		$certificate_type = $this->main_m->getRowCond('certificate_type',array('certificate_id' => $data->certificate_type));


		$management_learning = $this->main_m->getRowCond('administrations',array('administration_id' => $data->management_learning));
		$qualification = $this->main_m->getRowCond('qualification',array('qualification_id' => $data->qualification));

		$data->qualification = $qualification->qualification;
		$data->management_learning = $management_learning->administration_name;
		$tuition_fees = $this->main_m->getRowCond('tuition_fees',array('year_tb_id' => $data->year_tb_id, 'tuition_fees_id' => $data->tuition_fees_id));
		$data->installment = $this->db->where(array('tuition_fees_id' => $tuition_fees->tuition_fees_id))->order_by('type','ACS')->get('installment')->result();

		if($data->acquaintance_way != null)
		{
			$acquaintance_way = $this->main_m->getRowCond('acquaintance',array('acquaintance_id' => $data->acquaintance_way));
			$data->acquaintance_way = $acquaintance_way->acquaintance_name;
		}

		$created_date = explode(' ', $data->created_date);
		$data->created_date = $created_date[0];

		$data->class 			= $class->class_name;
		$data->city 			= $city->city_name;
		$data->state 			= $state->governorate_name;
		$data->section 			= $section->section_name;
		$data->study_type 		= @$study_type->study_type;
		$data->study_status 	= @$study_status->study_status;


		$this->load->view('print/studentFile',$data);
	}
	public function studentAjax(){
	$result = array();
	$result['table'] = '';
	$result['pagination'] = '';
	$item_per_page      = 10; //item to display per page
	$page = $this->input->post('page');
	$post_data = (array)json_decode(str_replace("'",'"',$this->input->post('post_data')));

	$post_data['student_status'] = 'null';
	//Get page number from Ajax
	if($page != NULL){
	    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
	    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
	    $page_number = 1; //if there's no page number, set it to 1
	}

	//get total number of records from database
	$get_total_rows = $this->main_m->getResultLimitCount('oct_student',$post_data); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows/$item_per_page);

	//position of records
	$page_position = (($page_number-1) * $item_per_page);
	$limit = array($page_position, $item_per_page);
	//Limit our results within a specified range.
	$student_data = $this->main_m->getResultLimit('oct_student',$limit,'student_id DESC',$post_data);

	$i = 0; foreach ($student_data as $student_key => $student_value) { $i++;
		if ($i % 2 == 0)
			$className = 'even';
		else
			$className = 'odd';

		$section = $this->main_m->getRowCond('section',array('section_id' => $student_value->section_id));
		$class = $this->main_m->getRowCond('class',array('class_id' => $student_value->class_id));
		$class = $this->main_m->getRowCond('class',array('class_id' => $student_value->class_id));
		$invoice_open_file = $this->main_m->getRowCond('invoice_open_file',array('student_id' => $student_value->student_id));

		$result['table'] .=
					'<tr role="row" class="'.$className.'">
             			<td>'.$i.'</td>
                      	<td><img src="'.CheckImg($student_value->profile_photo).'" alt="" class="img-thumbnail" /></td>
                      	<td>'.$student_value->student_name.'</td>
                      	<td>'.@$section->section_name.'</td>
                      	<td>'.@$class->class_name.'</td>
                      	<td>'.@$student_value->created_by.'</td>
                      	<td>'.@returnMonthFormate($student_value->created_in).'</td>
                      	<td>
	                      	<div class="btn-group">
	                            <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
	                                اعدادات
	                                <span class="caret"></span>
	                            </button>
	                            <ul class="dropdown-menu" role="menu">
	                                <li>
	                                    <a data-id="'.$student_value->student_id.'" id="payOpenFilePayment" href="javascript:;" data-url="'.base_url().'reception/openStudentFile/">دفع رسوم فتح الملف   <i class="zmdi zmdi-file"></i></a>
	                                </li>
	                                <li>
	                                	<a title="التقديم "  href="'.base_url().'accounts/apply_student_page/'.$student_value->student_id.'"> التقديم <i class="zmdi zmdi-assignment-o"></i></a>
	                                </li>
	                                <li>
	                                	<a title="حذف" href="javascript:;" data-url="'.base_url().'accounts/deleteBoolen/" data-id="'.$student_value->student_id.'" data-table="student" data-join="invoice_open_file" data-column="student_id" style="" class="delete"> حذف  <i class="zmdi zmdi-delete"></i></a>
	                                </li>
	                            </ul>
	                        </div>
                      	</td>
                    </tr>';

	}





	$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
	echo returnResponse($result,$get_total_rows.' Data Found',true);
	}



	public function add_acquaintance_way(){
		$acquaintance_way = $this->input->post('acquaintance_way');

		$acquaintance = $this->main_m->getRowCond('acquaintance',array('acquaintance_name' => $acquaintance_way));
		if(isset($acquaintance->acquaintance_id)){
			$data = $acquaintance;
		}else{
			$this->db->insert('acquaintance',array('acquaintance_name' => $acquaintance_way));
			$data['acquaintance_id'] = $this->db->insert_id();
			$data['acquaintance_name'] = $acquaintance_way;
		}

		echo json_encode($data);

	}


	public function add_city_name(){
		$governorate_id = $this->input->post('birth_place_gid');
		$city = $this->input->post('birth_place_text');

		$birth_place = $this->main_m->getRowCond('city',array('governorate_id' => $governorate_id,'city_name' => $city));
		if(isset($birth_place->city_id)){
			$data = $birth_place;
		}else{
			$this->db->insert('city',array('governorate_id' => $governorate_id,'city_name' => $city));
			$data['city_id'] = $this->db->insert_id();
			$data['city_name'] = $city;
			$data['governorate_id'] = $governorate_id;
		}

		echo json_encode($data);

	}


}





/* End of file Reception.php */
/* Location: ./application/controllers/Reception.php */
