<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CallCenter extends CI_Controller
{



    public function index()
    {
        $this->load->model('callCenter_m');
        $data['call']  = $this->main_m->limitOrderBy('call_center','id','DESC',10);  //$this->db->get('call_center');

     // var_dump(count($call));
     //  die();
        loadStaticContent('callCenter/index' ,$data);
    }


    public function add()
    {
        $data['governorate'] = $this->main_m->getAll('*', 'governorate');
        $data['qualification'] = $this->main_m->getAll('*', 'qualification');
        loadStaticContent('callCenter/add', $data);

    }





    public function add_call()
    {
        $this->load->model('callCenter_m');
        $data = array(
            'name' => $this->input->post('name'),
            'phone' => $this->input->post('phone'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'qualification' => $this->input->post('qualification'),
            'userId' => $this->userData->real_name,
            'date' => date('Y-m-d'),
        );

//Transfering data to Model
        $this->callCenter_m->form_insert($data);
        $data['created_by'] = $this->userData->real_name;
        returnResponse($data ,'تم  تسجيل البيانات بنجاح',true,base_url().'CallCenter');
    }



    public function edit_call()
    {
       /* $data['governorate'] = $this->main_m->getAll('*', 'governorate');
        $data['qualification'] = $this->main_m->getAll('*', 'qualification');
        loadStaticContent('callCenter/edit', $data);*/

        $data['controllers'] = controllers();

        $data['member'] = $this->main_m->getRowCond('user',array('user_id' => $user_id));

        loadStaticContent('members/edit_member',$data,array('script' => 'ajax/membersJs.php'));

    }


}


