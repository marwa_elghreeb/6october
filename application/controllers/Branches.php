<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Branches extends CI_Controller
{

    public function index()
    {
        $this->load->model('Branches_m');
        $data['branches']  =  $this->main_m->limitOrderBy('branches','id','DESC',10);
        loadStaticContent('branches/index' ,$data);
    }


    public function create()
    {
        loadStaticContent('branches/add');

    }


    public function store()
    {

        $data = array(
            'name' 		=> $this->input->post('name'),
        );
        $this->db->insert('branches',$data);
        redirect(base_url().'branches/index', 'refresh');
    }

    public function edit($id)
    {
        $data['edit']  = $this->main_m->getRowCond('branches',array('id' => $id));
        loadStaticContent('branches/edit' , $data);

    }

    public function update()
    {
        $data = array();
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $data['name']  = $name;
        $this->db->where('id',$id)->update('branches',$data);
        redirect(base_url().'branches/index', 'refresh');
    }

}