<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Security extends CI_Controller {

	public function entry_machine()
	{

		loadStaticContent('security/entry_machine',NULL,array('script' => 'ajax/securityJs.php'));

	}

	public function download_machine_file()
	{
		ini_set('memory_limit', '-1');
		/** PHPExcel */
		$this->load->library('PHPExcel');
		
        
		// Instantiate a new PHPExcel object 
		$objPHPExcel = new PHPExcel();  
		// RIGHT TO LEFT AR WORDS
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setRightToLeft(true);

		$columnArray = array('USERID','Badgenumber','SSN','Name','Gender','TITLE','PAGER','BIRTHDAY','HIREDDAY',' ','CITY','STATE','ZIP','OPHONE','FPHONE','VERIFICATIONMETHOD','SECURITYFLAGS','ATT','INLATE','OUTEARLY','OVERTIME','SEP','HOLIDAY','MINZU','PASSWORD','LUNCHDURATION','PHOTO','mverifypass','Notes','privilege','InheritDeptSch','InheritDeptSchClass','AutoSchPlan','MinAutoSchInterval','RegisterOT','InheritDeptRule','EMPRIVILEGE','CardNo');
		$LitterArray = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM');
		
		$objPHPExcel->getActiveSheet()->setCellValue($LitterArray[0].'1', 'التاريخ');
		// foreach ($LitterArray as $litter_key => $litter_value)
		// {
		// 	echo $litter_value.' => '.$columnArray[$litter_key];
		
			
		// }
		$objPHPExcel->getActiveSheet()->setTitle('Chesse1');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="helloworld.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

	}

	public function visit()
	{
		loadStaticContent('security/visit',NULL,array('script' => 'ajax/securityJs.php'));
	}

	public function insert_visit()
	{
		//load helpers 
		$this->load->helper('form');
	    $this->load->library('form_validation');
	    $this->load->helper('security');
        $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('national_id', 'National ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('meet', 'Meeting with ?', 'trim|required|xss_clean');
        $this->form_validation->set_rules('reasone', 'Reasone', 'trim|required|xss_clean');
        if($this->form_validation->run() == FALSE)
            returnResponse(NULL,' يجب كتابة  الحقول المطلوبة ',FALSE,NULL);
        else
        {
        	$data = array(
        		'name' 		=> $this->input->post('name'),
        		'phone' 	=> $this->input->post('phone'),
        		'meet' 		=> $this->input->post('meet'),
        		'reasone' 	=> $this->input->post('reasone'),
        		'company' 	=> $this->input->post('company'),
        		'day_ar' 	=> $this->input->post('day_ar'),
        		'date' 		=> $this->input->post('date'),
        		'check_in' 	=> $this->input->post('check_in'),
        		'national_id'	=> $this->input->post('national_id'),
        	);
        	$this->db->insert('visit',$data);

        	returnResponse($data,'success',true,base_url().'security/visit');
        }

	}

	public function edit_visit()
	{
		$id = $this->input->post('id');
		$visit = $this->main_m->getRowCond('visit',array('visit_id' => $id));

		echo '<form class="form_ajax" method="POST" result-data="form_result" action="'.base_url().'security/insert_visit/">
                        <div class="modal-body">
	                         
	                         	<div class="form-group text-center">
	                                <label for="day" class="col-sm-4 control-label">اليوم </label>
	                                <div class="col-sm-8">
	                                	<span>'.$visit->day_ar.'</span>
	                                    <input id="day" type="hidden" data-rule-required="true"  data-msg-required="يجب كتابة  اليوم" name="day_ar" placeholder="اكتب  اليوم" value="'.$visit->day_ar.'" class="form-control" >
	                                </div>
	                            </div>
	                            <div class="form-group text-center">
	                                <label for="date" class="col-sm-4 control-label">التاريخ </label>
	                                <div class="col-sm-8">
	                                	<span>'.$visit->date.'</span>
	                                    <input id="date" type="hidden" data-rule-required="true"  data-msg-required="يجب كتابة  التاريخ" name="date" placeholder="اكتب  التاريخ" value="'.$visit->date.'"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group text-center">
	                                <label for="date" class="col-sm-4 control-label">الساعة  </label>
	                                <div class="col-sm-8">
	                                	<span>'.$visit->check_in.'</span>
	                                    <input id="date" type="hidden" data-rule-required="true"  data-msg-required="يجب كتابة  الساعة " name="check_in" placeholder="اكتب  الساعة " value="'.$visit->check_in.'"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="clearfix"></div>
		                        <hr>

                        		<input type="hidden" value="'.$visit->visit_id.'" name="visit_id">
	                            <div class="form-group">
		                            <label for="name" class="col-sm-4 control-label"> الاسم </label>
	                                <div class="col-sm-8">
	                                    <input id="name" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   الاسم" name="name" placeholder="الاسم" value="'.$visit->name.'"  class="input-name form-control" >
	                                </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="name" class="col-sm-4 control-label"> رقم البطاقة  </label>
	                                <div class="col-sm-8">
	                                    <input id="national_id" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   رقم البطاقة " name="national_id" placeholder="رقم البطاقة " value="'.$visit->national_id.'"  class="form-control" >
	                                </div>
		                        </div>


		                        <div class="form-group">
	                                <label for="phone" class="col-sm-4 control-label">رقم الهاتف </label>
	                                <div class="col-sm-8">
	                                    <input id="phone" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   رقم الهاتف   " name="phone" min="11" placeholder="رقم الهاتف" value="'.$visit->phone.'"  class="form-control" >
	                                </div>
	                            </div>

		                        <div class="form-group">
		                            <label for="company" class="col-sm-4 control-label"> الشركة  </label>
	                                <div class="col-sm-8">
	                                    <input id="company" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   الشركة " name="company" placeholder="الشركة " value="'.$visit->company.'"  class="form-control" >
	                                </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="meet" class="col-sm-4 control-label"> لمقابلة  </label>
	                                <div class="col-sm-8">
	                                    <input id="meet" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   لمقابلة " name="meet" placeholder="لمقابلة " value="'.$visit->meet.'"  class="form-control" >
	                                </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="reasone" class="col-sm-4 control-label"> سبب الزيارة   </label>
	                                <div class="col-sm-8">
	                                    <input id="reasone" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   سبب الزيارة  " name="reasone" placeholder="سبب الزيارة  " value="'.$visit->reasone.'"  class="form-control" >
	                                </div>
		                        </div>

                        </div>
                        <div class="form_result col-md-12"></div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="margin-top: 30px;background: whitesmoke;">
                            <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>';

	
	}

	public function visitAjax()
	{


		$result = array();
		$result['table'] = '';
		$result['pagination'] = '';
		$item_per_page      = 10; //item to display per page
		$page = $this->input->post('page');
		$post_data = (array)json_decode(str_replace("'",'"',$this->input->post('post_data')));

		//Get page number from Ajax
		if($page != NULL){
		    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
		    $page_number = 1; //if there's no page number, set it to 1
		}

		//get total number of records from database
		$get_total_rows = $this->main_m->countRows('oct_visit',$post_data); //hold total records in variable
		//break records into pages
		$total_pages = ceil($get_total_rows/$item_per_page);

		//position of records
		$page_position = (($page_number-1) * $item_per_page);
		$limit = array($page_position, $item_per_page);
		//Limit our results within a specified range. 
		$visit_data = $this->db->where($post_data)->order_by('visit_id DESC')->get('oct_visit',$limit[1],$limit[0])->result();

		$i = 0; foreach ($visit_data as $visit_key => $visit_value) { $i++;
			if ($i % 2 == 0)
				$className = 'even';
			else
				$className = 'odd';

			$result['table'] .= 
			'<tr role="row" class="'.$className.'">
     			<td>'.$i.'</td>
              	<td>'.$visit_value->date.'</td>
              	<td>'.$visit_value->name.'</td>
              	<td>'.$visit_value->national_id.'</td>
              	<td>'.$visit_value->phone.'</td>
              	<td>'.$visit_value->reasone.'</td>
              	<td>'.$visit_value->meet.'</td>
              	<td>'.$visit_value->company.'</td>
              	<td>'.$visit_value->day_ar.'</td>
              	<td>'.$visit_value->check_in.'</td>
              	<td>'.$visit_value->check_out.'</td>
              	<td>

              	<div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                        اعدادات
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                  			<a  title="حذف" href="javascript:;" class="edit_visit" data-url="'.base_url().'security/edit_visit/" data-id="'.$visit_value->visit_id.'" data-table="visit" data-column="visit_id" style="" > تعديل <i class="zmdi zmdi-edit"></i></a>
                        </li>

                        <li>
                  			<a  title="حذف" href="javascript:;" data-url="'.base_url().'home/delete/" data-id="'.$visit_value->visit_id.'" data-table="visit" data-column="visit_id" style="" class="delete"> حذف  <i class="zmdi zmdi-delete"></i></a>
                        </li>

                    </ul>
                </div>
              	</td> 
            </tr>';
		}

		$result['pagination'] = paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages); 
		echo returnResponse($result,$get_total_rows.' Data Found',true);
	
	}
}
