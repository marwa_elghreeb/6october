<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        ini_set('MAX_EXECUTION_TIME', -1);
        parent::__construct();
        $this->load->model('m_accounts');
        require_once('num_to_ar.php');
    }

    public function index(){

        $this->load->view('main/head');
        $this->load->view('main/header');
        $this->load->view('main/nav');
        $this->load->view('main/footer');
    }


    public function update_student_image_code(){
        $students = $this->db->get('student')->result();
    
        foreach($students as $st_value){
            if($st_value->school_year == '2017-2018')
                $data = array('study_status' => 2);


            $this->db->where('student_code',$st_value->student_code)->update('student',$data);
        }
    }

    public function updateData(){
        $this->db->where(array('ex_invoice_created_date <' =>'2018-07-03'))->delete('ex_invoice');
        $this->db->where(array('income_invoice_created_date <' =>'2018-07-03'))->delete('income_invoice');
        $this->db->where(array('date <' =>'2018-07-03'))->delete('income_date_invoice');
        $this->db->where(array('created_date <' =>'2018-07-03'))->delete('invoices');
        $this->db->where(array('created_date <' =>'2018-07-03'))->delete('invoice_log');
        $this->db->where(array('date <' =>'2018-07-03'))->delete('invoice_open_file');


    }

    public function update_student_tuition(){
        $students = $this->db->get('student')->result();
        
        foreach($students as $st_value){
            $type = '';
            if($st_value->nationality == 'مصرى')
                $type = 'normal';
            else
                $type = 'foreigner';

            if($st_value->study_status == 2)
                $type = 'intensified';

            $tuition_fees = $this->db->where(array('year_tb_id' => 18, 'class_id' => $st_value->class_id, 'type' => $type))->get('tuition_fees')->row();
            $this->db->where('student_id',$st_value->student_id)->update('student',array('tuition_fees_id' => $tuition_fees->tuition_fees_id,'year_tb_id' => 18));

        }
    }

    // RETURN CHANGE SELECT ID
    public function changeSelect(){
        $id     = $this->input->get('id');
        $column = $this->input->get('column');
        $table  = $this->input->get('table');
        $result = $this->main_m->getRowsCond($table,array($column => $id));

        if($table == 'administrations')
        {
            $data['administrations'] = $result;
            if(!empty($result))
                $data['school'] = $this->main_m->getRowsCond('school',array('administration_id' => $result[0]->administration_id));

            $data['not_exist'] = $data['administrations'][0]->administration_id;
            returnResponse($data,'data-selected',true);
        }
        else
            returnResponse($result,'data-selected',true);
    }
    // THEME SETTING
    public function theme_setting(){
        $theme_setting = explode('/',$this->input->get('theme_setting'));
        $theme = $theme_setting[6];
        $this->db->where(array('user_id' => $this->userData->user_id))->update('user',array('theme_setting' => $theme));
    }

    public function sendMail(){
        foreach ($this->input->get() as $key => $value) {
            $data[$key] = $value;
        }

        if(empty($data['name']))
            returnResponse(null ,' يجب كتابة الاسم ', false, null);

        elseif(empty($data['email']))
            returnResponse(null ,' يجب كتابة البريد الالكترونى ', false, null);


        elseif(empty($data['phone']))
            returnResponse(null ,' يجب رقم الهاتف ', false, null);

        
        elseif(empty($data['message']))
            returnResponse(null ,' يجب كتابة نص الرسالة ', false, null);
            
        else
        {

            if(sendEmailHelper($data['email'],$this->contact_info->email,'Contact Page',$data['message']) == true)
            $this->db->insert('messages',$data);
            
            returnResponse(null ,' تم ارسال الرسالة بنجاح ', true, null);
        }

    }

    public function view_image($img,$controller)
    {
        if(file_exists('./assets/uploads/'.$img)){
            $data['image'] = $img;
            
            if(in_array($controller, json_decode($this->userData->permission)))
                $this->load->view('main/view_img',$data);
            
            else
                error404();
             
        }
    }

    public function downloadimage($img,$controller)
    {
        if(file_exists('./assets/uploads/'.$img))
        {
            
            $path = base_url().'assets/uploads/'.$img;
                        
            if(in_array($controller, json_decode($this->userData->permission))){
                $this->load->helper('download');
              
                $name = $img;
                $data = file_get_contents('./assets/uploads/'.$img);
                force_download($name, $data);
            
            }
            
            else
                error404();
        }

    }

    //GET SECTION FUNCTION
    public function get_sections()
    {
        echo returnResponse($this->main_m->getAll('*','section') ,'NULL' ,true);

    }

    public function upsc(){
        $student = $this->db->get('student')->result();
        foreach ($student as $key => $value)
        {
            preg_match_all('!\d+!', $value->student_code, $matches);
            if(isset($matches[0][0]))
                $this->db->where('student_id',$value->student_id)->update('student',array('student_code' => $matches[0][0]));
        }
    }

    // DELETE FUNCTION 
    public function delete(){
        $id     = $this->input->post('id');
        $column = $this->input->post('column');
        $table  = $this->input->post('table');
        $join   = $this->input->post('join');

        if($this->input->post('type') != null)
        {
            if(file_exists('./assets/uploads/'.$this->input->post('img_name')))
                unlink('./assets/uploads/'.$this->input->post('img_name'));
            
            $this->db->where($column,$id)->update($table,array($this->input->post('type_name') => ''));
        }
        else
        {
            if($join != null || !empty($join))
                $this->db->where($column,$id)->delete($join);
            
            $this->db->where($column,$id)->delete($table);
        }

    }

    public function update_student(){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 500);
        $student = $this->db->get('student')->result();
        foreach ($student as $key => $value)
        {
            $year = explode('-',$value->school_year);
            
            $year_tb = $this->main_m->getRowCond('year_tb',array('year_type' => $year[1]));

            if(isset($year_tb->year_tb_id)){

            $tuition_fees = $this->main_m->getRowCond('tuition_fees',array('year_tb_id' => $year_tb->year_tb_id));
            $this->db->where('student_code' ,    $value->student_code)->update('student',array(
                'tuition_fees_type' => $tuition_fees->type,
                'tuition_fees_id'   => $tuition_fees->tuition_fees_id
            ));
            }
        
        }
    }

    public function edit_income_invoices()
    {
        $iv = $this->db->get('student')->result();
        foreach ($iv as $iv_key => $iv_value) {

            $this->db->where('student_id',$iv_value->student_id)->update('invoice_log',array(
                'class_id'      => $iv_value->class_id,
                'section_id'    => $iv_value->section_id,
                'student_code'  => $iv_value->student_code,
                'national_id'   => $iv_value->national_id,
                'national_id'   => $iv_value->national_id,
                'student_name'  =>$iv_value->student_name,
            ));
        }
    }
    
    public function uploadAdministration($folder,$file){
        
        $this->load->library('PHPExcel');

        $student_data = array();

        set_time_limit('4000');

        

        $objPHPExcel = new PHPExcel();

        $FileName = $folder.'/'.$file.'.xlsx';

        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;

        PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

        try {
            $objPHPExcel = PHPExcel_IOFactory::load($FileName);
        } catch(Exception $e){
            die('Error loading file "'.pathinfo($FileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

            $objPHPExcel->setActiveSheetIndex(0);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $i = 0;

            $z=0;
            for ($i=1; $i <= count($allDataInSheet) ; $i++) { 

                    $year_tb = '';
                    $student = $this->main_m->getRowCond('student',array('student_code' => $allDataInSheet[$i]['C']));
                    
                    if(empty($student->student_id))
                        $student = $this->main_m->getRowCond('student',array('student_name' => $allDataInSheet[$i]['B']));
                    
                    if(!empty($student->student_id) && isset($student->student_id))
                    {
//                        if($allDataInSheet[$i]['F'] == '216')
//                            $allDataInSheet[$i]['F'] = '2016';
//
//                        $year_tb = $this->main_m->getRowCond('year_tb',array('year_type' => $allDataInSheet[$i]['F']));
//                        if(empty($year_tb)){
//                            $this->db->insert('year_tb',array('year_type' => $allDataInSheet[$i]['F']));
//                            $year_tb->year_tb_id = $this->db->insert_id();
//                        }
//
//                        $tuition_fees = $this->main_m->getRowCond('tuition_fees',array('year_tb_id' => $year_tb->year_tb_id));
//                        if(empty($tuition_fees))
//                        {
//                            $this->db->insert('tuition_fees',array('year_tb_id' => $year_tb->year_tb_id, 'tuition_fees' => 2000, 'type' => 'normal'));
//                            $tuition_fees = $this->main_m->getRowCond('tuition_fees',array('year_tb_id' => $year_tb->year_tb_id));
//
//                        }
//                        $exTime = explode('.',$allDataInSheet[$i]['AP']);
//                        $invoice_total_ar = new convert_ar($allDataInSheet[$i]['AK'], "male");
//                        $data = array(
//                            'student_id'    => $student->student_id,
//                            'class_id'      => $student->class_id,
//                            'payment'       => 'tuition_fees_cash',
//                            'payment_value' => $tuition_fees->tuition_fees_id,
//                            'invoice_value' => $allDataInSheet[$i]['AK'],
//                            'invoice_value_ar'  => $invoice_total_ar->convert_number().' جنية فقط لاغير',
//                            'discount'      => null,
//                            'invoice_number'=> generateRandomInt(7).substr(student_codeDate(),2),
//                            'created_by'    => $allDataInSheet[$i]['AL'],
//                            'created_in'    => date("F j, Y, g:i a"),
//                            'created_date'  => $allDataInSheet[$i]['AO'].' '.$exTime[0],
//                            'year_tb_id'    => $year_tb->year_tb_id,
//                        );
//
//                        if($allDataInSheet[$i]['AU'] > 0)
//                            $data['residual'] = $allDataInSheet[$i]['AU'] - ($allDataInSheet[$i]['AK'] + $allDataInSheet[$i]['AB']);
//                        else
//                            $data['residual'] = 0;
//
//                        if(!empty($allDataInSheet[$i]['AT']))
//                            $this->db->insert('student_comment',array('student_id' => $student->student_id, 'st_comment' => $allDataInSheet[$i]['AT']));
//
//                        // REMISSION COMMENT DATA
//                        if($allDataInSheet[$i]['AB'] != 0)
//                        {
//                            $this->db->insert('student_remission',array('student_id' => $student->student_id, 'value' => $allDataInSheet[$i]['AB'], 'type' => 'remission'));
//
//                            $this->db->insert('student_remission',array('student_id' => $student->student_id, 'value' => $allDataInSheet[$i]['AU'], 'type' => 'tuition_fees'));
//                        }
//
//
//                        $this->db->insert('invoices',$data);
//
//                        $data['invoice_id'] = $this->db->insert_id();
//
//                        $this->db->insert('invoice_log',$data);
//
//                        $checkIncomeInvoices = $this->main_m->getRowCond('income_date_invoice',array('date' => $allDataInSheet[$i]['AO']));
//                        if(!empty($checkIncomeInvoices)){
//                            $this->db->where('date' , $allDataInSheet[$i]['AO'])->update('income_date_invoice',array('income_invoices_counter' => $checkIncomeInvoices->income_invoices_counter + 1));
//                            $income_date_invoice_id = $checkIncomeInvoices->income_date_invoice_id;
//                        }
//                        else{
//                            $this->db->insert('income_date_invoice',array('income_invoices_counter' => 1, 'date' => $allDataInSheet[$i]['AO']));
//                            $income_date_invoice_id = $this->db->insert_id();
//                        }
//
//                        $income_data = array(
//                            'income_id'             => 2,
//                            'income_invoice_number' => $data['invoice_number'],
//                            'income_student_name'   => $allDataInSheet[$i]['B'],
//                            'income_student_id'     => $student->student_id,
//                            'income_comment'        => $allDataInSheet[$i]['B'].' تم دفع رسوم ( مصاريف دراسية) من الطالب /',
//                            'income_invoice_payment'=> 'مصاريف دراسية',
//                            'remission'             => $allDataInSheet[$i]['AB'],
//                            'income_invoice_value'          => $data['invoice_value'],
//                            'income_invoice_value_ar'       => $data['invoice_value_ar'],
//                            'income_date_invoice_id'        => $income_date_invoice_id,
//                            'income_invoice_created_by'     => $allDataInSheet[$i]['AL'],
//                            'income_invoice_created_in'     => date("F j, Y, g:i a"),
//                            'income_invoice_created_date'   => $allDataInSheet[$i]['AO'].' '.$exTime[0],
//                            'income_invoice_year_id'        => $year_tb->year_tb_id,
//                        );

                      //  $this->db->insert('income_invoice',$income_data);
                    }

                    else
                    {
                        $z++;
                        $Stdata[] = $allDataInSheet[$i];
                    }
            }


                    $this->load->library('PHPExcel');
                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->setTitle('الطلاب');
                    $objPHPExcel->getActiveSheet()->setCellValue('A1','كود الطالب');
                    $objPHPExcel->getActiveSheet()->setCellValue('B1','اسم الطالب');
                    $objPHPExcel->getActiveSheet()->setCellValue('C1','الفرقة');
                    $objPHPExcel->getActiveSheet()->setCellValue('D1','القسم');

                    $i=1;
                    foreach($Stdata as $key => $value){
                        $i++;
                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$value['B']);
                        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$value['C']);
                        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$value['E']);
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$value['F']);

                    }

                    //Save as an Excel BIFF (xls) file
                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="الطلاب-.xls"');
                    header('Cache-Control: max-age=0');

                    $objWriter->save('php://output');
                    exit();
    }


    public function updateImage(){
        $student = $this->db->get('student')->result();

        foreach ($student as $key => $value) {
            $this->db->where('student_id' , $value->student_id)->update('student',array('profile_photo' => $value->student_code.'.jpg'));
        }

    }

    // UPLOAD STUDENT EXCEL FILE
    public function uploadExcel($file){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 500);
        require_once('num_to_ar.php');
        $this->load->library('PHPExcel');
        
        $student_data = array();
        $objPHPExcel = new PHPExcel();

            
            $file_year = str_replace('.xlsx','',$file);

            try {
                $objPHPExcel = PHPExcel_IOFactory::load('students/'.$file);
         
            }
            catch(Exception $e)
            {
                $this->resp->success = FALSE;
                $this->resp->msg = 'Error File';
                echo json_encode($this->resp);
                exit;
            }
         
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $i = 0;
        for ($i=3; $i <= count($allDataInSheet) ; $i++) { 


            // STUDNET RELIGION
            if($allDataInSheet[$i]['E'] == '')
                $allDataInSheet[$i]['E'] = '0';
            elseif($allDataInSheet[$i]['E'] == 'مسلم')
                $allDataInSheet[$i]['E'] = '0';
            else
                $allDataInSheet[$i]['E'] = '1';
            // END STUDNET RELIGION

            // STUDNET sex
            if($allDataInSheet[$i]['E'] == '')
                $allDataInSheet[$i]['E'] = ' ';
            elseif($allDataInSheet[$i]['E'] == 'ذكر')
                $allDataInSheet[$i]['E'] = '0';
            else
                $allDataInSheet[$i]['E'] = '1';
            // END STUDNET sex
            
            /// START CITY NAME
            if(empty($allDataInSheet[$i]['G'])){
                $allDataInSheet[$i]['G'] = 14;
            }
            else{

                $gov1 = $allDataInSheet[$i]['G'];
                $governorate = $this->db->query("SELECT * FROM oct_governorate WHERE `governorate_name` LIKE '%$gov1%'")->row();

                if(!empty($governorate->governorate_name))
                    $allDataInSheet[$i]['G'] = $governorate->governorate_id;
                else
                    $allDataInSheet[$i]['G'] = 14;
            }
            /// END CITY NAME

            // START BIRTH DATE
            if(!empty($allDataInSheet[$i]['H'])){
                if(strpos($allDataInSheet[$i]['H'],'/') != false){
                    $exDate = explode('/',$allDataInSheet[$i]['H']);
                    echo "<pre>";
                    print_r($exDate);
                    echo "</pre>";
                    $allDataInSheet[$i]['H'] = $exDate[2].'-'.$exDate[1].'-'.$exDate[0];
                }
                elseif(strpos($allDataInSheet[$i]['H'],'"\"') != false ){
                    $exDate = explode('"\"',$allDataInSheet[$i]['H']);
                    $allDataInSheet[$i]['H'] = $exDate[2].'-'.$exDate[1].'-'.$exDate[0];
                }
            }
            else
                $allDataInSheet[$i]['H'] = '1990-10-10';
            // END BIRTH DATE

            // START NATIONALTY ID
            if(empty($allDataInSheet[$i]['I'])){
                $allDataInSheet[$i]['I'] = '00000000000000';
            }

            // END NATIONALTY ID
            // ADDRESS
            if(empty($allDataInSheet[$i]['J'])){
                $allDataInSheet[$i]['J'] = ' ';
            }
            // ADDRESS

            /// START CITY NAME
            if(empty($allDataInSheet[$i]['K'])){
                $allDataInSheet[$i]['K'] = 14;
            }
            else{
                $gov2 = $allDataInSheet[$i]['K'];
                $governorate = $this->db->query("SELECT * FROM oct_governorate WHERE `governorate_name` LIKE '%$gov2%'")->row();

                if(!empty($governorate->governorate_name))
                    $allDataInSheet[$i]['K'] = $governorate->governorate_id;
                else
                    $allDataInSheet[$i]['K'] = 14;
            }
            /// END CITY NAME

            /// START CITY NAME
            if(empty($allDataInSheet[$i]['L'])){
                $allDataInSheet[$i]['L'] = 14;
            }
            else{
                $cit = $allDataInSheet[$i]['L'];

                $city = $this->db->query("SELECT * FROM oct_city WHERE `city_name` LIKE '%$cit%'")->row();
                if(!empty($city->city_name))
                    $allDataInSheet[$i]['L'] = $city->city_id;
                else
                    $allDataInSheet[$i]['L'] = 251;
            }
            /// END CITY NAME

            if(empty($allDataInSheet[$i]['M'])){
                $allDataInSheet[$i]['M'] = ' ';
            }

            if(empty($allDataInSheet[$i]['N'])){
                $allDataInSheet[$i]['N'] = ' ';
            }

            if(empty($allDataInSheet[$i]['Q'])){
                $allDataInSheet[$i]['Q'] = ' ';
            }
            
            if(empty($allDataInSheet[$i]['R'])){
                $allDataInSheet[$i]['R'] = ' ';
            }

            if(empty($allDataInSheet[$i]['S'])){
                $allDataInSheet[$i]['S'] = ' ';
            }


            if(empty($allDataInSheet[$i]['T'])){
                $allDataInSheet[$i]['T'] = ' ';
            }

            if(!empty($allDataInSheet[$i]['U'])){
                if($allDataInSheet[$i]['U'] == 'الفرقة الاولي')
                    $allDataInSheet[$i]['U'] = 1;
                else
                    $allDataInSheet[$i]['U'] = 2;
            }
            else{
                    $allDataInSheet[$i]['U'] = 1;

            }

            if(!empty($allDataInSheet[$i]['V'])){

                $sec = $allDataInSheet[$i]['V'];
                $section = $this->db->query("SELECT * FROM oct_section WHERE `section_name` LIKE '%$sec%'")->row();
                if(!empty($section))
                    $allDataInSheet[$i]['V'] = $section->section_id;
                else
                    $allDataInSheet[$i]['V'] = 0;
            }
            else{
                    $allDataInSheet[$i]['V'] = 0;

            }


            if(!empty($allDataInSheet[$i]['Y'])){
                $status = $allDataInSheet[$i]['Y'];
                $study_status =  $this->db->query("SELECT * FROM oct_study_status WHERE `study_status` LIKE '%$status%'")->row();
                if(!empty($study_status))
                    $allDataInSheet[$i]['Y'] = $study_status->status_id;
                else
                    $allDataInSheet[$i]['Y'] = 0;
            }
            else
                $allDataInSheet[$i]['Y'] = 0;

            $qual = $allDataInSheet[$i]['Q'];
            $qualification = $this->db->query("SELECT * FROM oct_qualification WHERE `qualification` LIKE '%$qual%'")->row();
            if(empty($qualification))
                $qualification = '0';
            else
                $qualification = $qualification->qualification_id;

            if(!empty($allDataInSheet[$i]['N']) && strlen($allDataInSheet[$i]['N']) > 9)
                $phone = json_encode(array($allDataInSheet[$i]['N'],$allDataInSheet[$i]['O']));
            else
                $phone = json_encode(array('10101010'));


            $student_data = array(
                'student_code'  => str_replace('-','',$allDataInSheet[$i]['C']),
                'student_name'  => $allDataInSheet[$i]['B'],
                'nationality'   => $allDataInSheet[$i]['D'],
                'religion'      => $allDataInSheet[$i]['E'],
                'gender'        => $allDataInSheet[$i]['F'],
                'birth_place_governorate'   => 0,
                'birth_place'               => $allDataInSheet[$i]['G'],
                'date_of_birth'             => $allDataInSheet[$i]['H'],
                'national_id'               => $allDataInSheet[$i]['I'],
                'state'                     => $allDataInSheet[$i]['K'],
                'city'                      => $allDataInSheet[$i]['L'],
                'area_name'                 => $allDataInSheet[$i]['M'],
                'address'                   => $allDataInSheet[$i]['J'],
                'student_phone'             => $phone,
                'student_email'             => 'info@6oct.com',
                'qualification'             => $qualification,
                'qualification_grade'       => $allDataInSheet[$i]['R'],
                'student_school'            => $allDataInSheet[$i]['S'],
                'management_learning'       => $allDataInSheet[$i]['T'],
                'class_id'                  => $allDataInSheet[$i]['U'],
                'section_id'                => $allDataInSheet[$i]['V'],
                'study_type'                => $allDataInSheet[$i]['X'],
                'study_status'              => $allDataInSheet[$i]['Y'],
                'certificate_type'          => $allDataInSheet[$i]['Z'],
                'open_file_bill_number'     => '111111',
                'student_status'            => 3,
                'school_year'               => $file_year,
                'comment'                   => 'no comment',
                'created_by'                => $this->userData->real_name,
                'created_in'                => date("F j, Y, g:i a"),
            );

            echo "<pre>";
            print_r($student_data);
            echo "</pre>";
            // $this->db->insert('student',$student_data);
            // $student_id = $this->db->insert_id();


            // $total       = $allDataInSheet[$i]['AG'];
            // $bill_amount_ar = new convert_ar($total, "male");
            // $invoice_value   = $allDataInSheet[$i]['AE'];
            // $discount        = $allDataInSheet[$i]['AF'];
            // $residual        = str_replace('-','',$allDataInSheet[$i]['AH']);
            // $data = array(
            //  'invoice_number'    => generateRandomInt(7).substr(date("Y"),2),
            //  'student_id'        => $student_id,
            //  'class_id'          => $allDataInSheet[$i]['U'],
            //  'payment'           => 'tuition_fees_cash',
            //  'payment_value'     => '0',
            //  'invoice_value'     => $total,
            //  'invoice_value_ar'  => $bill_amount_ar->convert_number().' جنية فقط لاغير',
            //  'discount'          => $discount,
            //  'residual'          => $residual,
            //  'created_by'        => $this->userData->real_name,
            //  'created_in'        => date("F j, Y, g:i a"),
            // );

            // $this->db->insert('invoices',$data);
            // $last_id = $this->db->insert_id();
            // $date['invoice_id'] = $last_id;
            // $this->db->insert('invoice_log',$data);
    
            
            
            
            
            
            
            



            
        }

    }

    public function edit_administration(){
        $governorate = $this->db->get('governorate')->result();
        foreach ($governorate as $key => $value) {
            $administrations = $this->main_m->getRowCond('administrations',array('governorate_id' => $value->governorate_id ));
            
            if($this->main_m->countRows('administrations',array('governorate_id' => $value->governorate_id, 'administration_name' => 'قطاع المعاهد الازهرية')) == 0){
                $this->db->insert('administrations',array('governorate_id' => $value->governorate_id, 'administration_name' => 'قطاع المعاهد الازهرية'));
                echo $key;
                echo "<br>";
                echo "Inserted";
                echo "<br>";
            }
        }
    }



    // UPLOAD STUDENT EXCEL FILE
    public function newuploadExcel($file){
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 500);
        require_once('num_to_ar.php');
        $this->load->library('PHPExcel');
        
        $student_data = array();
        $objPHPExcel = new PHPExcel();

            
            try {
                $objPHPExcel = PHPExcel_IOFactory::load($file);
         
            }
            catch(Exception $e)
            {
                $this->resp->success = FALSE;
                $this->resp->msg = 'Error File';
                echo json_encode($this->resp);
                exit;
            }
         
        

        for ($numActive=1; $numActive <= 26 ; $numActive++) { 
            echo "<br>";
            echo $numActive;
            $objPHPExcel->setActiveSheetIndex($numActive);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            for ($i=3; $i <= count($allDataInSheet); $i++) { 
                if(!empty($allDataInSheet[$i]['E']) && !empty($allDataInSheet[$i]['B']))
                {
                    $governorate = $this->db->like('governorate_name',$allDataInSheet[$i]['E'],'after')->get('governorate')->row();

                    if(isset($governorate->governorate_id))
                    {
                        $governorate_id = $governorate->governorate_id;
                    }
                    else
                    {
                        $this->db->insert('governorate',array('governorate_name' => $allDataInSheet[$i]['E']));
                        $governorate_id = $this->db->insert_id();
                    }
                    
                    $likeAr = array('administration_name' => $allDataInSheet[$i]['F'], 'governorate_id' => $governorate_id);
                    $administrations = $this->db->like('administration_name',$allDataInSheet[$i]['F'],'after')->get('administrations')->row();
                    if(!isset($administrations->administration_id))
                    {
                        $this->db->insert('administrations',$likeAr);
                        $administration_id = $this->db->insert_id();
                    }
                    else
                        $administration_id = $administrations->administration_id;


                    $dataArray = array('governorate_id' => $governorate_id, 'administration_id' => $administration_id, 'school_name' => $allDataInSheet[$i]['B'], 'school_type' => $allDataInSheet[$i]['D']);
                    $this->db->insert('school',$dataArray);
                }
            }            
        }
    }




}


