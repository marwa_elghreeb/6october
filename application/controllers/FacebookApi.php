<?php
/**
 * FacebookSDK.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class FacebookApi extends CI_Controller
{
	/**
	 * FacebookSDK class instance.
	*/
	public function index(){

		require_once('facebook/autoload.php');
			$fb = new Facebook\Facebook([
	        'app_id' => '1825384664451011',
	        'app_secret' => 'cfbcc943b18d1bcce8ef0d285f43eddb',
	        'default_graph_version' => 'v2.8',
	        ]);
	    	$helper = $fb->getRedirectLoginHelper();
	    	$permissions = ['publish_actions'];
	    	try {

	            	$accessToken = $helper->getAccessToken();
	        
	        
	    	} catch (Facebook\Exceptions\FacebookResponseException $e) {
	        	echo 'Facebook SDK Returned Err -> '.$e->getMessage();
	        	exit();
	    	}catch (Facebook\Exceptions\FacebookSDKException $e) {
	        	echo 'Facebook SDK Returned Err -> '.$e->getMessage();
	        	exit();
	    	}

	    	if(isset($accessToken)){

	            $fb->setDefaultAccessToken($accessToken);
	        
	        // if(isset($_GET['code'])){
	            
	        //     redirectJs(base_url('/'));
	        // }

	        try {
	            
	            $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
	            
	            $profile = $profile_request->getGraphNode()->asArray();
	        } catch (Facebook\Exceptions\FacebookResponseException $e) {
	            echo "Graph Returned Err ".$e->getMessage();
	            
	            session_destroy();
	            
	            redirectJs(base_url('/'));
	            
	        }
	        catch (Facebook\Exceptions\FacebookSDKException $e) {
	            echo 'Facebook SDK Returned Err -> '.$e->getMessage();
	            exit();
	        }

	        $CheckAccessToken = $this->main_m->getRowCond('social_users',array('user_id' => $profile['id']));
	        if(count($CheckAccessToken) > 0)
	        {	
	        	$this->db->where('user_id',$profile['id']);
	        	$this->db->update('social_users',array('access_token' => $accessToken));
	        	redirectJs(base_url('/'));
	        }
	        else{
	        	$FBDATA = array(
	        		'social_name' 	=> 'facebook',
	        		'user_id' 		=> $profile['id'],
	        		'screen_name' 	=> $profile['name'],
	        		'user_email' 	=> @$profile['email'],
	        		'access_token' 	=> $accessToken,
	        	);
	        	$this->db->insert('social_users',$FBDATA);
	        	redirectJs(base_url('/'));
	        }


	    }
	    else
	    {
	        redirectJs(base_url('/'));
	    }


	}
}
