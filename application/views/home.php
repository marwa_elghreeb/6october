
          <div id="content_wrapper" class="card-overlay">
          <div id="header_wrapper" class="header-md ">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <header id="header">
                    <h1>فتح ملف طالب</h1>
                    <ol class="breadcrumb">
                      <li><a href="index.html">الاستقبال</a></li>
                      <li class="active">فتح الملف</li>
                    </ol>
                  </header>
                </div>
              </div>
            </div>
          </div>

          <div id="content" class="container">
            <div class="content-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="card" id="rootwizard">
                    <div class="card-heading">
                        <div class="form-wizard-nav">
                          <div class="progress" style="width: 75%;">
                            <div class="progress-bar" style="width:0%;"></div>
                          </div>
                          <ul class="nav nav-justified nav-pills">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab" aria-expanded="true">
                                    <span class="step">1</span>
                                    <span class="title">بيانات الطالب</span>
                                </a>
                            </li>

                            <li class="disabled">
                                <a href="#tab2" disabled="disabled" data-toggle="tab" aria-expanded="false">
                                    <span class="step">2</span>
                                    <span class="title">ملفات الطالب</span>
                                </a>
                            </li>
                            <li class="disabled">
                                <a href="#tab3" data-toggle="tab" aria-expanded="false">
                                    <span class="step">3</span>
                                    <span class="title">مصروفات فتح الملف</span>
                                </a>
                            </li>
                            <li class="disabled">
                                <a href="#tab4" data-toggle="tab" aria-expanded="false">
                                    <span class="step">4</span>
                                    <span class="title">طباعة ملف الطاب</span>
                                </a>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div class="card-body p-0">
                        <div class="form-wizard form-wizard-horizontal">
                          <div class="tab-content clearfix p-30">
                            <div class="tab-pane active" id="tab1">
                                <div class="card-body">
                                    <form id="form-horizontal form_insert" method="get" action="reception/add_student"  class="form-horizontal">
                                        <div class="form-group">
                                            <label for="student_name" class="col-sm-2 control-label">اسم الطالب</label>
                                            <div class="col-sm-10">
                                                <input id="student_name" type="text" name="student_name" placeholder="اسم الطالب" minlength="5"  data-rule-required="true" data-msg-required="يجب كتابة اسم الطالب"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nationnality" class="col-sm-2 control-label">جنسية الطالب</label>
                                            <div class="col-sm-10">
                                                <input id="nationnality" type="text" name="nationnality" placeholder="جنسية الطالب"  data-rule-required="true" data-msg-required="يجب كتابة جنسية الطالب"  class="form-control" >
                                            </div>
                                        </div>
                                          
                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">الديانة</label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="religion" checked="checked" id="inlineRadio1" value="0"><span class="circle"></span><span class="check"></span> مسلم
                                          </label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="religion" id="inlineRadio2" value="1"><span class="circle"></span><span class="check"></span> مسيحى
                                          </label>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">الجنس</label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="gender" checked="checked" id="inlineRadio1" value="0"><span class="circle"></span><span class="check"></span> ذكر <i class="zmdi zmdi-male zmdi-hc-fw"></i>
                                          </label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="gender" id="inlineRadio2" value="1"><span class="circle"></span><span class="check"></span> انثى <i class="zmdi zmdi-female zmdi-hc-fw"></i>
                                          </label>
                                        </div>

                                        <div class="form-group">
                                            <label for="birth_place" class="col-sm-2 control-label">محل الميلاد</label>
                                            <div class="col-sm-10">
                                                <input id="birth_place" type="text" name="birth_place" placeholder="محل الميلاد" minlength="2"  data-rule-required="true" data-msg-required="يجب كتابة محل الميلاد"  class="form-control" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Date Of bith" class="col-sm-2 control-label">تاريخ الميلاد</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control datepicker" name="date_of_birth" style="text-align: right;" id="md_input_date"  data-rule-required="true" data-msg-required="يجب اختيار تاريخ الميلاد"  placeholder="تاريخ الميلاد" data-dtp="dtp_DUFhz">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="national_id" class="col-sm-2 control-label">رقم البطاقة</label>
                                            <div class="col-sm-10">
                                                <input id="national_id" type="text" name="national_id" placeholder="رقم البطاقة" minlength="2"  data-rule-required="true" minlength="14" data-msg-required="يجب كتابة رقم البطاقة"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="state" class="col-sm-2 control-label">المحافظة</label>
                                            <div class="col-sm-10">
                                                <select name="state" id="state" class="select form-control">
                                                    <option>اختر المحافظة</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="city" class="col-sm-2 control-label">المدينة</label>
                                            <div class="col-sm-10">
                                                <select name="city" id="city" class="select form-control">
                                                    <option>اختر المدينة</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="area_name" class="col-sm-2 control-label">المنطقة</label>
                                            <div class="col-sm-10">
                                                <input id="area_name" type="text" name="area_name" placeholder="المنطقة"  data-rule-required="true"  data-msg-required="يجب كتابة المنطقة"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="address" class="col-sm-2 control-label">العنوان</label>
                                            <div class="col-sm-10">
                                                <input id="address" type="text" name="address" placeholder="العنوان" data-rule-required="true" data-msg-required="يجب كتابة العنوان"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="phone" class="col-sm-2 control-label">الهاتف</label>
                                            <div class="col-sm-3">
                                                <input id="phone" type="text" name="phone[]" placeholder="الهاتف" minlength="2"  data-rule-required="true"  data-msg-required="يجب كتابة الهاتف"  class="form-control" >
                                            </div>
                                            <div class="col-sm-3">
                                                <input id="phone" type="text" name="phone[]" placeholder="الهاتف" class="form-control" >
                                            </div>
                                            <div class="col-sm-3">
                                                <input id="phone" type="text" name="phone[]" placeholder="الهاتف" class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="student_email" class="col-sm-2 control-label">البريد الالكترونى</label>
                                            <div class="col-sm-10">
                                                <input id="student_email" type="text" name="student_email" placeholder="البريد الالكترونى"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="qualification" class="col-sm-2 control-label">المؤهل الدراسى</label>
                                            <div class="col-sm-10">
                                                <select name="qualification" id="qualification" class="select form-control">
                                                    <option>اختر المؤهل الدراسى</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="qualification_grade" class="col-sm-2 control-label">المجموع</label>
                                            <div class="col-sm-10">
                                                <input id="qualification_grade" type="text" name="qualification_grade" placeholder="المجموع"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="school" class="col-sm-2 control-label">المدرسة</label>
                                            <div class="col-sm-10">
                                                <input id="school" type="text" name="school" placeholder="المدرسة"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="management_learning" class="col-sm-2 control-label">الاداراة التعليمية</label>
                                            <div class="col-sm-10">
                                                <select name="management_learning" id="management_learning" class="select form-control">
                                                    <option>اختر الاداراة التعليمية</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="class_id" class="col-sm-2 control-label">الفرقة</label>
                                            <div class="col-sm-10">
                                                <select name="class_id" id="class_id" class="select form-control">
                                                    <option>اختر الفرقة</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="section_id" class="col-sm-2 control-label">القسم</label>
                                            <div class="col-sm-10">
                                                <select name="section_id" id="section_id" class="select form-control">
                                                    <option>اختر القسم</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="study_type" class="col-sm-2 control-label">نوع الداراسة</label>
                                            <div class="col-sm-10">
                                                <select name="study_type" id="study_type" class="select form-control">
                                                    <option>اختر نوع الداراسة</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="study_status" class="col-sm-2 control-label">حالة الداراسة</label>
                                            <div class="col-sm-10">
                                                <select name="study_status" id="study_status" class="select form-control">
                                                    <option>اختر حالة الداراسة</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="certificate_type" class="col-sm-2 control-label">الشهادة المراد الحصول عليها</label>
                                            <div class="col-sm-10">
                                                <select name="certificate_type" id="certificate_type" class="select form-control">
                                                    <option>اختر الشهادة المراد الحصول عليها</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="acquaintance_way" class="col-sm-2 control-label">وسيلة التعارف</label>
                                            <div class="col-sm-10">
                                                <select name="acquaintance_way" id="acquaintance_way" class="select form-control">
                                                    <option>اختر وسيلة التعارف</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <button type="submit" class="btn btn-primary">حفظ <i class="zmdi zmdi-long-arrow-return zmdi-hc-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end #tab1 -->


































                            
                            <!--end #tab2 -->
                            
                            <!--end #tab3 -->
                            
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            </div>