<style type="text/css">
  .InfoSpanFooter_honesty{
    width: 50%;
    border-bottom:none;
  }
  #honesty{
    min-width: 830px;
  }
  .invoice_border_honesty{
    height: 65px;
    border: none;
    text-align: right;
  }
</style>

<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" id="honesty" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 
                 <h4 class="modal-title" id="myModalLabel-2"> ايصال <?=$type?></h4>
                 <ul class="card-actions icons left-top">
                     <li>
                     <a href="javascript:void(0)" data-dismiss="modal" class="text-white closeModal" aria-label="Close">
                         <i class="zmdi zmdi-close"></i>
                     </a>
                 </li>
             </ul>
         </div>
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
             	<h2 style="text-align: center;background: #ccc;padding: 10px;border: 1px solid #b9b9b9;">ايصالات  ال<?=$type_str?>
             		<br>
             	<span style="font-size: 14px;line-height: 40px;">تاريخ اليوم <?=date('Y-m-d')?></span>
             	</h2>
                 <div class="tab-pane fadeIn active" id="tab-1">
                    <div class="bill_header">
						<table class="table table-hover">
							<thead>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">اسم المستلم</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">عنوان المستلم</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">الرقم القومى	</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">المبلغ</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">البند</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">ملاحظات</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">اضيف بواسطة	</th>
								<th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">تاريخ </th>
							</thead>
							<tbody>
								<?php foreach ($expense as $key => $value): ?>
								<?php $expenses = $this->main_m->getRowCond('expenses',array('expenses_id' => $value->expenses_id)); ?>
								<tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->receiver_name ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->receiver_address ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->receiver_national_id ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->ex_invoice_value ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$expenses->expenses_name ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->comment ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->ex_invoice_created_by ?></td>
									<td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$value->ex_invoice_date ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>                         
                         
                     </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>