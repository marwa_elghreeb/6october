<style type="text/css">
  .InfoSpanFooter_honesty{
    width: 50%;
    border-bottom:none;
  }
  #honesty{
    min-width: 95%;
  }
  .invoice_border_honesty{
    height: 65px;
    border: none;
    text-align: right;
  }
  table th,table tr,table tr td{
    font-size: 12px !important;
    border: 1px solid #ccc !important;
    text-align: center !important;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة  احصائيات وسيلة التعارف <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>


<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" id="honesty" role="document">
         <div class="modal-content">
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1" style="width: 100%;min-height: 345px;">
                     <div>
            <header class="card-heading">
              <h2 class="card-title">الاحصائيات</h2> 
            </header>

            <div class="card-body p-0">
                <h2 style="text-align: center;">احصائية وسيلة التعارف على الطلاب خلال العام  <?=$year_type.' / '.($year_type + 1)?></h2>
              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;" class="col-xs-2">وسيلة التعارف </th>
                            <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;" class="col-xs-2">عدد الطلاب  </th>
                            <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;" class="col-xs-2">اجمالى عدد الطلاب</th>
                        </tr>
                    </thead>
                    <tbody class="TableResult">
                        <?php $i=0; foreach ($acquaintance as $acquaintance_key => $acquaintance_value): $i++;?>                      
                        <tr>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$acquaintance_value->acquaintance_name?></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$acquaintance_value->num?></td>
                            <?php if($i == 1): ?>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$total?></td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                
              </div>
              </div>

            </div>
            </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>