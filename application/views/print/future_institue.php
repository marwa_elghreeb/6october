<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <!-- I.X -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- mobile -->

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content=" piacere al nostro sito bla bal bala ">
        <title>future institue </title>
        <link rel="stylesheet" href="<?=base_url()?>assets/print/css/bootstrap.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/print/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/print/future_institue/style.css" type="text/css"/>   
        <link rel="stylesheet" href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700'>
        <script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script>     
</head>
<body>
    
<!-- header contact : start -->
 <section id="header-contact">
     <div class="container">
         <div class="frist-sec">
             <div class="row">
                 <div class="col-md-4 col-xs-12  text-center">
                    <div class="right-side">
                        <img src="<?=base_url()?>assets/print/future_institue/img/logo.png" class="img-responsiv"/>
                    </div>
                 </div>
                 <div class="col-md-4 "></div>
                 <div class="col-md-4 col-xs-12 text-center">
                     <div class="left-side text-right ">
                        <h4 > رقم :  </h4>
                        <p> ص.ب: 90810  
                          <span> دبى . الامارات</span>
                        </p>
                         <p> رخصة رقم : 668823 -
                          <span> دبى </span>
                        </p>
                    </div>
                 </div>
             </div>
         </div>
     </div>
</section>
<!-- header contact : end -->  
    <div class="container">
        <div clsass="row">
            <div class="title_001 text-center">
                <h3 clasa="text-center">  استمارة تسجيل  /  Registration Form  </h3>
            </div>
        </div>
    </div>
    
<section class="prog_detials">
    <div class="container">
        <div clsass="row">
            <div class="border_0022">
                <div class="col-md-4">
                    <div class="prog_name">
                        <h3> اسم البرنامج  </h3>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="prog_name">
                        <h3> الساعات  </h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="prog_name">
                        <h3>  التاريخ </h3>
                        <p> من .... /..../....   الى .... /..../..... </p>
                    </div>
                </div>
            </div>

        </div>
    </div>    
</section>
<!-- forma : start -->
<section class="all-forma">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="title22">
                        <h3 >البيانات الشخصية :-</h3>
                    </div>
                    <div class="col-md-12">
                        <form class="TeacherName">
                          الاسم باللغة العربية :................
                        </form>
                    </div>
                    <div class="col-md-12">
                        <form class="TeacherName">
                          الاسم باللغة الانجليزية :................
                        </form>
                    </div>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="new_line_001">
                <div class="col-md-4">
                    <div class="">
                         <form>
                              الجنسية :.............................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                              الحالة الاجتماعية :.............................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                              تاريخ وجهة الميلاد :...................
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="SecondForm">
                <div class="col-md-6">
                    <div class="">
                         <form>
                              العنوان :...............................
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                              المحمول :...............................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                            التليفون :...............................
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="">
                         <form>
                            البريد الالكترونى :...............................
                        </form>
                    </div>
                </div>
<!--
                 <div class="col-md-12">
                    <form class="TeacherName">
                      عنوان الدارس:<input type="text" name="firstname">
                    </form>
                </div>
-->
                <div class="col-md-4">
                    <div class="">
                         <form>
                            المؤهل :...................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                             التخصص :...................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                             تاريخ الحصول عليه :...................
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                             رقم البطاقة :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                            محل الاصدار :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                             تاريخ الاصدار :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                            رقم جواز السفر :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                            محل الاصدار :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                            تاريخ الاصدار :...........
                        </form>
                    </div>
                </div>


                <div class="row">
                    
                     <div class="title22">
                        <h3 style="width:50%">  طريقة الدفع </h3><span style="font-size:18px;">تكلفة البرنامج 500  دولار امريكى</span>
                     </div>
                    <div class="">
                        <h4 style="width:50%;display:inline-block;margin: 0px;">  (1) 
                            نقدى لدى المعهد.
                        </h4>
                        <span style="width:50%">التوقيع : .............</span>
                        <h4>  (2) 
                            تحوبل بنكى على حساب المهد الاتى :
                        </h4>
                        <div class="">
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> الدولة </span> الامارات العربية المتحدة  </h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> اسم الحساب </span> معهد المستقبل للجودة  </h4>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> المدينة </span> دبى فرع الممزر </h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> رقم الحساب داخل الامارات  </span> 063520040091902 </h4>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> اسم البنك </span> بنك دبى الاسلامى </h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> رقم الحساب الدولى  </span> IBAN / AE810240063520040091902 </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="other_info_0">
                        <p> - فى حالة التحويل من خارج دزلة الامارات العربية المتحدة يرجى استخدام رقم الحساب الدولى .</p>
                        <p> - يرجى ارسال صورة تحوبل بالبريد الاكترونى (6october@miq.ae) او بالواتساب ( 00971555786060 )
                            لتأكيد الحجز.
                        </p>
                        <p>
                            - يرجى ملاحظة انه يجب دفع الرسوم كامله     حيث ستهمل اى طلبات لتسجيل غير مصحوبة برسوم البرنامج . 
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="gneral_terms">
                        <div class="general_terms_title">
                        <div>
                            <p> شروط عامة  </p>
                            
                        </div>
                        </div>
                        <div class="terms_points">
                            <ol>
                                <li>
                                  قيمة تحديد المستوى غير قابلة للاسترداد .  
                                </li>
                                <li>
                                    يسترد العميل 50% من المصروفات اذا انسحب من البرنامج قبل البدء باسبوعين غلى الاقل باسبوع.
                                </li>
                                <li>
                                    يسترد العميل 25% من المصروفات اذا انسحب من البرنامج قبل البدء باسبوع0.
                                </li>
                                <li>
                                    لا يمكن استرداد اى مبالغ مالية خلال الاسبوع الذى يسبق بداية البرنامج .
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="title22">
                        <h3> إقرار وتعهد :- </h3>
                    </div>
                     <div class="col-md-12">
                        <div class="promise">
                            <p> 
                            اقر بأننى اطلعت على اللائحة الداخلية للمعهد وعلى نظام المصروفات الدراسية وقبلتها وليس لى الحق فى استرداد اى مصروفات 
                            دفعتها بأى حال من الأحوال بعد بداية الدراسة وأقر بأننى على علم تام نافى للجهالة شرعا وقانةنابأن المعهد لا يؤهل لاستكمال
                                دراستى بالجامعة وانه ليس له علاقة بموقف الطالب من التجنيد أو القوات المسلحة وعلى علم تام بعدم تجاوزى نسبة الغياب 25 
                                 وفى حالة تجاوزي هذه السنة احرم من الحصول على الشهادة لتجاوزي نسبة الغياب المقررة .
                                وهذا اقرار منى بذلك.
                                اقر انا بصحة البيانات السابقة  و  تحت مسؤليتى:
                                
                            </p>
                        </div>
                    </div>
                </div>
                


                <div class="col-md-6">
                    <div class="">
                         <form>
                             الاسم :................................................
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                     التوقيع :..............................................

                        </form>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="final_info">
                        <div class="info_img">
                            <img src="<?=base_url()?>assets/print/future_institue/img/QRcode.png" class="img-responsive">
                        </div>
                        <div class="info_de">
                            <p> العنوان : دبى - ديرة .
                            <br>
                                الامارات العربية المتحدة 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <p>  موبايل : 0097555786060  </p>
                    <p>  تليفاكس : 0097142522646  </p>
                </div>
                <div class="col-md-4">
                    <p>  موقع الكترونى : www.miq@ae  </p>
                    <p>  بريد الكترونى : 6october@miq.ae  </p>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- forma : end -->

<section id="header-contact">
     <div class="container">
         <div class="frist-sec">
             <div class="row">
                 <div class="col-md-4 col-xs-12  text-center">
                    <div class="right-side">
                        <img src="<?=base_url()?>assets/print/future_institue/img/logo.png" class="img-responsiv"/>
                    </div>
                 </div>
                 <div class="col-md-4 "></div>
                 <div class="col-md-4 col-xs-12 text-center">
                     <div class="left-side text-right ">
                        <h4 > رقم :  </h4>
                        <p> ص.ب: 90810  
                          <span> دبى . الامارات</span>
                        </p>
                         <p> رخصة رقم : 668823 -
                          <span> دبى </span>
                        </p>
                    </div>
                 </div>
             </div>
         </div>
     </div>
</section>

    <div class="container">
        <div clsass="row">
            <div class="title_001 text-center">
                <h3 clasa="text-center">  استمارة تسجيل  /  Registration Form  </h3>
            </div>
        </div>
    </div>
    
<section class="prog_detials">
    <div class="container">
        <div clsass="row">
            <div class="border_0022">
                <div class="col-md-4">
                    <div class="prog_name">
                        <h3> اسم البرنامج  </h3>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="prog_name">
                        <h3> الساعات  </h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="prog_name">
                        <h3>  التاريخ </h3>
                        <p> من .... /..../....   الى .... /..../..... </p>
                    </div>
                </div>
            </div>

        </div>
    </div>    
</section>
<section class="all-forma">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="title22">
                        <h3 >البيانات الشخصية :-</h3>
                    </div>
                    <div class="col-md-12">
                        <form class="TeacherName">
                          الاسم باللغة العربية :................
                        </form>
                    </div>
                    <div class="col-md-12">
                        <form class="TeacherName">
                          الاسم باللغة الانجليزية :................
                        </form>
                    </div>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="new_line_001">
                <div class="col-md-4">
                    <div class="">
                         <form>
                              الجنسية :.............................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                              الحالة الاجتماعية :.............................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                              تاريخ وجهة الميلاد :...................
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="SecondForm">
                <div class="col-md-6">
                    <div class="">
                         <form>
                              العنوان :...............................
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                              المحمول :...............................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                            التليفون :...............................
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="">
                         <form>
                            البريد الالكترونى :...............................
                        </form>
                    </div>
                </div>
<!--
                 <div class="col-md-12">
                    <form class="TeacherName">
                      عنوان الدارس:<input type="text" name="firstname">
                    </form>
                </div>
-->
                <div class="col-md-4">
                    <div class="">
                         <form>
                            المؤهل :...................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                             التخصص :...................
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="">
                         <form>
                             تاريخ الحصول عليه :...................
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                             رقم البطاقة :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                            محل الاصدار :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                             تاريخ الاصدار :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                            رقم جواز السفر :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="">
                         <form>
                            محل الاصدار :...........
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                            تاريخ الاصدار :...........
                        </form>
                    </div>
                </div>


                <div class="row">
                    
                     <div class="title22">
                        <h3 style="width:50%">  طريقة الدفع </h3><span style="font-size:18px;">تكلفة البرنامج 500  دولار امريكى</span>
                     </div>
                    <div class="">
                        <h4 style="width:50%;display:inline-block;margin: 0px;">  (1) 
                            نقدى لدى المعهد.
                        </h4>
                        <span style="width:50%">التوقيع : .............</span>
                        <h4>  (2) 
                            تحوبل بنكى على حساب المهد الاتى :
                        </h4>
                        <div class="">
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> الدولة </span> الامارات العربية المتحدة  </h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> اسم الحساب </span> معهد المستقبل للجودة  </h4>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> المدينة </span> دبى فرع الممزر </h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> رقم الحساب داخل الامارات  </span> 063520040091902 </h4>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> اسم البنك </span> بنك دبى الاسلامى </h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="country_payment">
                                    <h4> <span> رقم الحساب الدولى  </span> IBAN / AE810240063520040091902 </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="other_info_0">
                        <p> - فى حالة التحويل من خارج دزلة الامارات العربية المتحدة يرجى استخدام رقم الحساب الدولى .</p>
                        <p> - يرجى ارسال صورة تحوبل بالبريد الاكترونى (6october@miq.ae) او بالواتساب ( 00971555786060 )
                            لتأكيد الحجز.
                        </p>
                        <p>
                            - يرجى ملاحظة انه يجب دفع الرسوم كامله     حيث ستهمل اى طلبات لتسجيل غير مصحوبة برسوم البرنامج . 
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="gneral_terms">
                        <div class="general_terms_title">
                        <div>
                            <p> شروط عامة  </p>
                            
                        </div>
                        </div>
                        <div class="terms_points">
                            <ol>
                                <li>
                                  قيمة تحديد المستوى غير قابلة للاسترداد .  
                                </li>
                                <li>
                                    يسترد العميل 50% من المصروفات اذا انسحب من البرنامج قبل البدء باسبوعين غلى الاقل باسبوع.
                                </li>
                                <li>
                                    يسترد العميل 25% من المصروفات اذا انسحب من البرنامج قبل البدء باسبوع0.
                                </li>
                                <li>
                                    لا يمكن استرداد اى مبالغ مالية خلال الاسبوع الذى يسبق بداية البرنامج .
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="title22">
                        <h3> إقرار وتعهد :- </h3>
                    </div>
                     <div class="col-md-12">
                        <div class="promise">
                            <p> 
                            اقر بأننى اطلعت على اللائحة الداخلية للمعهد وعلى نظام المصروفات الدراسية وقبلتها وليس لى الحق فى استرداد اى مصروفات 
                            دفعتها بأى حال من الأحوال بعد بداية الدراسة وأقر بأننى على علم تام نافى للجهالة شرعا وقانةنابأن المعهد لا يؤهل لاستكمال
                                دراستى بالجامعة وانه ليس له علاقة بموقف الطالب من التجنيد أو القوات المسلحة وعلى علم تام بعدم تجاوزى نسبة الغياب 25 
                                 وفى حالة تجاوزي هذه السنة احرم من الحصول على الشهادة لتجاوزي نسبة الغياب المقررة .
                                وهذا اقرار منى بذلك.
                                اقر انا بصحة البيانات السابقة  و  تحت مسؤليتى:
                                
                            </p>
                        </div>
                    </div>
                </div>
                


                <div class="col-md-6">
                    <div class="">
                         <form>
                             الاسم :................................................
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="">
                         <form>
                     التوقيع :..............................................

                        </form>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="final_info">
                        <div class="info_img">
                            <img src="<?=base_url()?>assets/print/future_institue/img/QRcode.png" class="img-responsive">
                        </div>
                        <div class="info_de">
                            <p> العنوان : دبى - ديرة .
                            <br>
                                الامارات العربية المتحدة 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <p>  موبايل : 0097555786060  </p>
                    <p>  تليفاكس : 0097142522646  </p>
                </div>
                <div class="col-md-4">
                    <p>  موقع الكترونى : www.miq@ae  </p>
                    <p>  بريد الكترونى : 6october@miq.ae  </p>
                </div>

            </div>
        </div>
    </div>
</section>


 
<script src="<?=base_url()?>assets/print/js/jquery-1.11.3.min.js"></script> 
<script src="<?=base_url()?>assets/print/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/ajax.js"></script>

</body>

</html>

<script type="text/javascript">
    loadPrint();
</script>