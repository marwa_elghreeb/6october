<style type="text/css">
  .InfoSpanFooter_honesty{
    width: 50%;
    border-bottom:none;
  }
  #honesty{
    min-width: 830px;
  }
  .invoice_border_honesty{
    height: 65px;
    border: none;
    text-align: right;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" id="honesty" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 
                 <h4 class="modal-title" id="myModalLabel-2"> ايصال <?=$type?></h4>
                 <ul class="card-actions icons left-top">
                     <li>
                     <a href="javascript:void(0)" data-dismiss="modal" class="text-white closeModal" aria-label="Close">
                         <i class="zmdi zmdi-close"></i>
                     </a>
                 </li>
             </ul>
         </div>
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1" style="width: 100%;border: 2px solid;padding: 10px;min-height: 345px;">
                     <div class="bill_header">
                         <div class="" style="">
                             <h1 style="text-align:center;display: inline-block;width: 100%;padding: 12px;background: #ccc;border: 1px solid #ccc;">إقرار إستلام نقدية على سبيل الامانة</h1>
                             <br>                             
                         </div>
                         <div class="billFooter" style="padding: 0px 20px;">
                             <span  class="InfoSpanFooter_honesty" style="width: 50%;float: right;">إقر انا / <span style="text-decoration: underline;" class="invoice_value_ar"><?=$invoice_data->receiver_name?></span></span>
                             <span  class="InfoSpanFooter_honesty" style="width: 50%;">المقيم / <span style="text-decoration: underline;" class="invoice_value_ar"><?=$invoice_data->receiver_address?></span></span>
                             <br>
                             <br>
                             <span  class="InfoSpanFooter_honesty">وأحمل بطاقة رقم قومى / <span style="text-decoration: underline;" class="open_file_amount"><?=$invoice_data->receiver_national_id?></span></span>
                             <br>
                             <br>
                             <span  class="InfoSpanFooter_honesty">مبلغ وقدرة / <span style="text-decoration: underline;" class="open_file_amount"><?=$invoice_data->ex_invoice_value_ar?></span></span>
                             <br>
                             <br>
                             <span  class="InfoSpanFooter_honesty">من  السيد/ 
                                <span class="open_file_amount"> احمد محمد سليمان سليمان شلبى</span>
                                <span class="open_file_amount" style="margin-right: 100px;"> رقم قومى  : 28811218800436</span>
                            </span>
                             <br>
                             <br>
                         </div>
                         <div style="padding: 0px 20px;" class="invoice_border_honesty"><span>و ذلك على سبيل الأمانه على ان اردها عند الطلب و فى حالة عدم ردها أصبح خائنا و مبددا للأمانه و أقع تحت المسؤلية القانونية</span></div>
                        <br>
                        <span style="padding: 0px 20px;"  class="InfoSpanFooter_honesty">المقر بما فية / <span class="open_file_amount"></span></span>
                         
                     </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>