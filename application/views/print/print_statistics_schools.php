<style type="text/css">
  .child tbody > tr{
    border: 1px solid #ccc !important;
    
  }
  .TableResult tr{
    font-size: 12px !important;
    text-align: center !important;
  }
  .productsTable thead th{
    font-size: 12px !important;
    border: 1px solid #ccc !important;
    text-align: center !important;
  }
  .InfoSpanFooter_honesty{
    width: 50%;
    border-bottom:none;
  }
  #honesty{
    min-width: 95%;
  }
  .invoice_border_honesty{
    height: 65px;
    border: none;
    text-align: right;
  }
  table th,table tr td{
    font-size: 12px !important;
    border: 1px solid #ccc !important;
    text-align: center !important;
  }
  .child{
    width: 100%;
    padding: 10px 0px;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة  احصائيات وسيلة التعارف <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" id="honesty" role="document">
         <div class="modal-content">
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1" style="width: 100%;min-height: 345px;">
                    <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
                    <a href="<?=base_url().'statistics/statistics_schools'?>" style="float: left;margin-left: 20px;" class="btn btn-default" >رجوع  </a>
                  <div>
            <header class="card-heading">
              <h2 class="card-title">الاحصائيات</h2> 
            </header>

            <div class="card-body p-0">
                <h2 style="text-align: center;">احصائية  المدارس  لعام   (<?php foreach($years as $yy){echo $yy->year_type.' / ';}?>)</h2>
              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <th class="col-xs-2" style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">المحافظة  </th>
                          <th class="col-xs-2" style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"> المدارس  </th>
                          <?php foreach($years as $year_v): ?>
                            <th class="col-xs-2" style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$year_v->year_type?></th>
                          <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody class="TableResult">
                        <?php $i=0; foreach ($governorate as $governorate_key => $governorate_value): $i++;?>                      
                        <?php $school = $this->db->where('governorate_id',$governorate_value->governorate_id)->get('school')->result(); ?>
                        <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                <td style="font-size: 22px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$governorate_value->governorate_name?></td>
                                <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                <?php foreach ($school as $school_key => $school_value):?>
                                    <table class="child" style="width: 100%;">
                                        <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: right !important;"><?=$school_value->school_name ?></td>
                                        </tr>
                                    </table>
                                <?php endforeach; ?>
                                    <table class="child" style="width: 100%;">
                                        <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                            <td style="font-size: 12px !important;border: 1px solid red !important;color: red;text-align: center !important;">الاجمالى </td>
                                        </tr>
                                    </table>
                                </td>
                                <?php $school = $this->db->where('governorate_id',$governorate_value->governorate_id)->get('school')->result(); ?>
                                <?php $countyears = count($years); $i=0; foreach($years as $year_k => $year_v): ?>
                                    <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                    <?php $total =array(); foreach ($school as $school_key => $school_value):  ?>
                                        <?php 
                                            $counter = $this->main_m->countRows('student',array('student_school' => $school_value->school_id, 'year_tb_id' => $year_v->year_tb_id)); 
                                            $total[] = $counter;
                                        ?>
                                                <table class="child" style="width: 100%;">
                                                    <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$counter?></td>
                                                    </tr>
                                                </table>
                                    <?php endforeach; $i++; ?>
                                    <table class="child" style="width: 100%">
                                        <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                            <td style="font-size: 12px !important;border: 1px solid red !important;color: red;text-align: center !important;"><?=array_sum($total)?> </td>
                                        </tr>
                                    </table>
                                    </td>
                                <?php endforeach; ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                
              </div>
              </div>

            </div>
            </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>