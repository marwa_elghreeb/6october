<style type="text/css">
  .child tbody > tr{
    border: 1px solid #ccc !important;
    
  }
  .TableResult tr{
    font-size: 12px !important;
    text-align: center !important;
  }
  .productsTable thead th{
    font-size: 12px !important;
    border: 1px solid #ccc !important;
    text-align: center !important;
  }
  .InfoSpanFooter_honesty{
    width: 50%;
    border-bottom:none;
  }
  #honesty{
    min-width: 95%;
  }
  .invoice_border_honesty{
    height: 65px;
    border: none;
    text-align: right;
  }
  table th,table tr td{
    font-size: 12px !important;
    border: 1px solid #ccc !important;
    text-align: center !important;
  }
  .child{
    width: 100%;
    padding: 10px 0px;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> كشف احصائيات   المتقدمين للاقسام المختلفة  <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" id="honesty" role="document">
         <div class="modal-content">
          <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
          <a href="<?=base_url().'statistics/section_statistics'?>" style="float: left;margin-left: 20px;" class="btn btn-default" >رجوع  </a>

         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1" style="width: 100%;min-height: 345px;">
                     <div>
            <div class="card-body p-0">
                <h2 style="text-align: center;"> كشف احصائيات   المتقدمين للاقسام المختلفة   (<?php foreach($years as $yy){ if($year_tb_id  == $yy->year_tb_id) {echo $yy->year_type;}}?>)</h2>
              
              <?php foreach ($months as $key => $value): ?>
              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                          <?php foreach($value as $mo_key => $mo_value): ?>
                            <th class="col-xs-2" style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">شهر  <?=$mo_key?></th>
                          <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody class="TableResult">
                            <?php foreach($value as $mo_key => $mo_value): ?>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                <table>
                                    <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                        
                                <?php foreach ($sections as $section_key => $section_value): ?>
                                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                            <?=
                                                // mb_substr($section_value->section_name,-1)
                                            $section_value->section_name
                                            ?>
                                            <?php for ($i=1; $i <= 31 ; $i++){ ?>
                                                <table style="width: 100%">
                                                    <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                                        <?php
                                                        $day = $year_type->year_type.'-'.date('m', strtotime($mo_value)).'-'.$i;
                                                        $count = $this->db->query("SELECT * FROM `oct_student` WHERE `created_date` LIKE '%$day%' && `section_id` = '$section_value->section_id' ")->num_rows();
                                                        ?>
                                                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$count?></td>
                                                    </tr>
                                                </table>
                                            <?php } ?>
                                            
                                        </td>
                                <?php endforeach;?>
                                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                            <table style="width: 100%"><tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><td style="color: red;font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">مجموع اليوم</td></tr></table>
                                        <?php for ($i=1; $i <= 31 ; $i++){ 
                                            $day = $year_type->year_type.'-'.date('m', strtotime($mo_value)).'-'.$i;
                                            $count = $this->db->query("SELECT * FROM `oct_student` WHERE `created_date` LIKE '%$day%'")->num_rows();
                                        ?>
                                            <table style="width: 100%">
                                                <tr><td style="color: red;font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$count?></td></tr>
                                            </table>
                                        <?php } ?>
                                        </td>

                                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                                            <table style="width: 100%"><tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><td style="color: red;font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;line-height: 40px;"> التاريخ  </td></tr></table>
                                        <?php for ($i=1; $i <= 31 ; $i++){ 
                                            $day = $year_type->year_type.'-'.date('m', strtotime($mo_value)).'-'.$i;
                                        ?>
                                            <table style="width: 100%">
                                                <tr><td style="color: green;font-size: 10px !important;border: 1px solid #ccc !important;"><?=$i?></td></tr>
                                            </table>
                                        <?php } ?>
                                        </td>

                                    </tr>
                                </table>
                            </td>
                          <?php endforeach; ?>
                    </tbody>
                </table>
                
              </div>
              <?php endforeach; ?>
              </div>

            </div>
            </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>