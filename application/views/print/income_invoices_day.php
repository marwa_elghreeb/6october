<style type="text/css">
  .InfoSpanFooter_honesty{
    width: 50%;
    border-bottom:none;
  }
  #honesty{
    min-width: 95%;
  }
  .invoice_border_honesty{
    height: 65px;
    border: none;
    text-align: right;
  }
  table th,table tr,table tr td{
    font-size: 12px !important;
    border: 1px solid #ccc !important;
    text-align: center !important;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" id="honesty" role="document">
         <div class="modal-content">
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1" style="width: 100%;min-height: 345px;">
                     <div>
            <header class="card-heading">
              <h2 class="card-title">الايرادات </h2> 
              <h6>التاريخ : <span style="font-weight: bold;"><?=$date?></span> </h6>             
              <h6>مجموع الايراد : <span style="font-weight: bold;color: green;"><?=$total?></span> جنية مصرى </h6>      
              <h6>مجموع  المصروفات: <span style="font-weight: bold;color: red;"><?=$OTV?></span> جنية مصرى </h6>      
              <h6 style="font-weight: bold">الاجمالى <span style="font-weight: bold;color: green;"><?=$total - $OTV?></span> جنية مصرى </h6>      
            </header>

            <div class="card-body p-0">

              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">#</th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">التاريخ</th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">كود الطالب </th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">اسم الطالب </th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">الفرقة </th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">الشعبة </th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">نوع الرسوم</th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">ملاحظات</th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">المبلغ</th>
                        <th style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"  class="col-xs-2">اضف بواسطة</th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                    <?php $i=0; foreach ($invoices as $invoices_key => $invoices_value){ $i++;?>
                        
                        <tr style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;">
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$i?></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->date?> <br> <span style="color: green;"><?=$invoices_value->time?></span></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->student_code?></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->student_name?></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->class?></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->section?></td>
                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->income_invoice_payment?></td>
                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->comment?></td>
                        <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->income_invoice_value?></td>
                            <td style="font-size: 12px !important;border: 1px solid #ccc !important;text-align: center !important;"><?=$invoices_value->income_invoice_created_by?></td>
                        </tr>
                        
                    <?php } ?>
                  </tbody>
                </table>
                
              </div>
              </div>

            </div>
            </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>