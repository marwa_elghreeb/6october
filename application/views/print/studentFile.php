
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- I.X -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile -->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" piacere al nostro sito bla bal bala ">
    <title> </title>
    <link rel="stylesheet" href="<?=base_url()?>assets/print/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/print/css/bootstrap-rtl.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/print/css/style.css" type="text/css"/> 
    <link rel="stylesheet" href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700'>
    <script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script>   
</head>
<body>
    
<!-- header contact : start -->
 <section id="header-contact">
     <div class="container">
         <div class="frist-sec">
             <div class="row">
                 <div class="col-md-4 col-xs-12">
                    <div class="right-side text-center">
                        <h3>6 اكتوبر </h3>
                        <h2>للتدريب والتعليم </h2>
                    </div>
                 </div>
                 <div class="col-md-4 "></div>
                 <div class="col-md-4 col-xs-12 text-center">
                    <div class="left-side">
                        <img src="<?=base_url()?>assets/print/img/6oct-academy--logo.png" class="img-responsiv"/>
                    </div>
                 </div>
             </div>
         </div>
     </div>
</section>
<!-- header contact : end -->  
<!-- forma : start -->
<section class="all-forma">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                     
                     <div class="col-md-9">
                        <div class=" PageTitle text-center">
                            <h2>استمارة طلب الالتحاق</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="title22">
                        <h3 >البيانات الشخصية :-</h3>
                    </div>
                    <div class="col-md-12">
                        <div class="name">
                             <p> الاسم:  <span><?=$student_name?></span></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="name">
                             <p> الجنسية :
                              <span><?=$nationality?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> الديانة :
                              <span><?= $religion==0 ? 'مسلم' : 'مسيحى'?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> ذكر \ انثى :
                              <span><?= $gender==0 ? 'ذكر' : 'انثى'?></span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ImageForm" style="width:150px;height:170px;border:1px solid #ccc"></div>
            </div>
        </div>
        <div class="row">
            <div class="SecondForm">
                <div class="col-md-6">
                    <div class="name">
                         <p>محل الميلاد :
                          <span><?=$birth_place?></span></p>
                    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>تاريخ الميلاد :
                          <span><?=str_replace('-','/',$date_of_birth)?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>  رقم البطاقة - ش / ع :
                          <span><?=$national_id?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p> جهة صدورها :
                          <span></span></p>
                    </div>
                </div>
                 <div class="col-md-12">
                    <div class="name">
                         <p>عنوان الدارس : <span><?=$address?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>مركز \ قسم  : <span><?=$city?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>محافظة  : <span><?=$state?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>تليفون الدارس : <span> <?=json_decode($student_phone)[2]?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>المحمول  : <span><?=@json_decode($student_phone)[0]?></span></p>
                    </div>
                </div>
                <hr>
                <div class="col-md-6">
                    <div class="name">
                         <p>اسم ولى الامر  :<span> <?=$guardian_name?> </span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>صلة القرابة  : <span><?=$relative_relation?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>وظيفة والى الامر : <span><?=$guardian_work?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>المحمول  : <span><?=@json_decode($student_phone)[1]?></span></p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="title22">
                        <h3> المؤهل العلمي :- </h3>
                    </div>
                    <div class="col-md-12">
                        <div class="name">
                             <p>المؤهل الدراسى وتاريخ الحصول عليه : <span> <?=$qualification?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> المجموع :
                              <span><?=$qualification_grade?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> المدرسة :
                              <span><?=$student_school?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> الادارة التعليمية :
                              <span><?=$management_learning?></span></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                     <div class="title22">
                        <h3> بيانات القسم:- </h3>
                     </div>
                    <div class="col-md-6">
                        <div class="name">
                             <p>القسم المراد الالتحاق به : <span><?=$section?></span></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="name">
                             <p> التخصص :
                              <span>....................</span></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="name">
                             <p> الفرقة :
                              <span><?=$class?></span></p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="name">
                             <p> نوع الدراسة المطلوبة ( عادية / مكثفة ) :
                              <span><?=$study_status?></span></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="name">
                             <p> ( إنتظام / إنتساب ) :
                              <span><?=$study_type?></span></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="name">
                            <p> الشهادة المراد الحصول عليها :
                                <span>جامعة حكومية</span>
                              <label class="checkbox-inline">
                                  <input <?=$certificate_type == 1 ? 'checked' : ''?> type="checkbox"  value="">
                              </label>
                                <span class="uni">جامعة دولية</span>
                              <label class="checkbox-inline">
                                  <input type="checkbox" <?=$certificate_type == 2 ? 'checked' : ''?> value="">
                              </label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="name">
                             <p> وسيلة التعارف : <span> <?=$acquaintance_way?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                         <p>تاريخ الالتحاق :
                          <span><?=str_replace('-','/',$created_date);?></span></p>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="title22">
                        <h3> إقرار وتعهد :- </h3>
                    </div>
                     <div class="col-md-12">
<div class="promise">
                            <p> أقر بأنني إطلعت على قرار الترخيص الصادر واللائحة الداخلية للمركز / للاكاديمية وقد إطلعت على نظام المصروفات الدراسية وقبلتها وليس لي الحق في إسترداد أي مصروفات دفعتها بأي حال من الآحوال وفي حالة سحب أوراقي ألتزم بسداد مصروفات السحب المقرره وأقر بأنني على علم تام نافى للجهاله شرعاً وقانوناً بأن المركز لا يؤهل لاستكمال دراستي بالجامعة وذلك بعد اطلاعي على الشهادة التي سأحصل عليها بعد اجتياز الاختبارات بنجاح وجهة صدورها وجهات الاعتماد والتوثيق ، وانه ليس له علاقة بموقف الطالب من التجنيد أو القوات المسلحة ، وعلى علم تام بعدم تجاوزي نسبة الغياب 25 %وفي حالة تجاوزي هذه المدة أحرم من دخول الامتحانات وأعتبر مفصولا لتجاوزي نسبة الغياب المقررة . وهذا إقرار منى بذلك ،،،،،،،،،،،،،،،</p>
                        </div>
                    </div>
                </div>
                
                

            <div class="col-md-12">
                    
                <div class="col-md-4" style="float: left;">
                    <div class="name">
                        <p> المقر بما فيه : </p>
                        <p>  الاسم :
                          <span><?=$student_name?></span></p>
                        <p>  التوقيع :
                          <span></span>
                        </p>
                    </div>
                </div>
                </div>

            

                <div class="col-md-4">
                    <div class="name">
                         <p>تاريخ التقديم :
                          <span><?=str_replace('-','/',$created_date)?></span></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="name">
                        <p> الاستقبال : <br><br><?=$created_by?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="name">
                        <p>يعتمد ،،،،،،،<br><br>........................</p>
                    </div>
                </div>
            
            </div>
        </div>
  </section>
<!-- forma : end -->
<!-- END SECTION 1  -->

<!-- START SECTION 2  -->
<!-- header contact : start -->
 <section id="header-contact" style="padding-top: 0px;margin: 0px">
     <div class="container">
         <div class="frist-sec">
             <div class="row">
                 <div class="col-md-5 col-xs-12">
                    <div class="right-side text-center">
                        <h3>محافظة الدقهلية</h3>
                        <h2>6 اكتوبر </h2>
                        <span>للسياحة والفنادق ونظم المعلومات واللغات</span>
                    </div>
                 </div>
                 <div class="col-md-3 "></div>
                 <div class="col-md-4 col-xs-12 text-center">
                    <div class="left-side">
                        <img src="<?=base_url()?>assets/print/img/logo.png" class="img-responsiv"/>
                    </div>
                 </div>
             </div>
         </div>
     </div>
</section>
<!-- header contact : end --> 
<section class="all-forma">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                     
                     <div class="col-md-9">
                        <div class=" PageTitle text-center">
                            <h2>استمارة طلب الالتحاق</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="title22">
                        <h3 >البيانات الشخصية :-</h3>
                    </div>
                    <div class="col-md-12">
                        <div class="name">
                             <p> الاسم:  <span><?=$student_name?></span></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="name">
                             <p> الجنسية :
                              <span><?=$nationality?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> الديانة :
                              <span><?= $religion==0 ? 'مسلم' : 'مسيحى'?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> ذكر \ انثى :
                              <span><?= $gender==0 ? 'ذكر' : 'انثى'?></span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ImageForm" style="width:150px;height:170px;border:1px solid #ccc"></div>
            </div>
        </div>
        <div class="row">
            <div class="SecondForm">
                <div class="col-md-6">
                    <div class="name">
                         <p>محل الميلاد :
                          <span><?=$birth_place?></span></p>
                    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>تاريخ الميلاد :
                          <span><?=str_replace('-','/',$date_of_birth)?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>  رقم البطاقة - ش / ع :
                          <span><?=$national_id?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p> جهة صدورها :
                          <span></span></p>
                    </div>
                </div>
                 <div class="col-md-12">
                    <div class="name">
                         <p>عنوان الدارس : <span><?=$address?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>مركز \ قسم  : <span><?=$city?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>محافظة  : <span><?=$state?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>تليفون الدارس  : <span> <?=json_decode($student_phone)[2]?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>المحمول  : <span><?=@json_decode($student_phone)[0]?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>اسم ولى الامر : <span><?=$guardian_name?>  </span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>صلة القرابة  :                      <span><?=$relative_relation?></span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>وظيفة والى الامر :<?=$guardian_work?><span>.............................................</span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="name">
                         <p>المحمول  :                      <span><?=@json_decode($student_phone)[1]?></span></p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="title22">
                        <h3> المؤهل العلمي :- </h3>
                    </div>
                    <div class="col-md-12">
                        <div class="name">
                             <p>المؤهل الدراسى وتاريخ الحصول عليه : <span> <?=$qualification?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> المجموع :
                              <span><?=$qualification_grade?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> المدرسة :
                              <span><?=$student_school?></span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                             <p> الادارة التعليمية :
                              <span><?=$management_learning?></span></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                     <div class="title22">
                        <h3> بيانات القسم:- </h3>
                     </div>
                    <div class="col-md-6">
                        <div class="name">
                             <p>القسم المراد الالتحاق به : <span><?=$section?></span></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="name">
                             <p> التخصص :
                              <span>....................</span></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="name">
                             <p> الفرقة :
                              <span><?=$class?></span></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="name">
                             <p> نوع الدراسة المطلوبة ( عادية / مكثفة ) :
                              <span><?=$study_status?></span></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="name">
                             <p> ( إنتظام / إنتساب ) :
                              <span><?=$study_type?></span></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="name">
                            <p> الشهادة المراد الحصول عليها :
                                <span>جامعة حكومية</span>
                              <label class="checkbox-inline">
                                  <input <?=$certificate_type == 1 ? 'checked' : ''?> type="checkbox"  value="">
                              </label>
                                <span class="uni">جامعة دولية</span>
                              <label class="checkbox-inline">
                                  <input type="checkbox" <?=$certificate_type == 2 ? 'checked' : ''?> value="">
                              </label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="name">
                             <p> وسيلة التعارف : <span> <?=$acquaintance_way?> </span></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="name">
                         <p>تاريخ الالتحاق :
                          <span><?=str_replace('-','/',$created_date);?></span></p>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="title22">
                        <h3> إقرار وتعهد :- </h3>
                    </div>
                     <div class="col-md-12">
<div class="promise">
                            <p> أقر بأنني إطلعت على قرار الترخيص الصادر طبقاً للقرار الوزاري رقم 420 لسنة 2014 والصادرة من وزارة التربية والتعليم ،وقد
إطلعت على نظام المصروفات الدراسية وقبلتها وليس لي الحق في إسترداد أي مصروفات دفعتها بأي حال من الاحوال وفي حالة سحب
أوراقي ألتزم بسداد مصروفات السحب المقرره وأقر بأنني على علم تام نافى للجهاله شرعاً وقانوناً بأن المركز لا يؤهل لإستكمال دراستي
بالجامعة ، وانه ليس له عالقة بموقف الطالب من التجنيد أو القوات المسلحة ، وعلى علم تام بعدم تجاوزي نسبة الغياب 25 %وفي حالة
تجاوزي هذه المدة أحرم من دخول الامتحانات وأعتبر مفصولا لتجاوزي نسبة الغياب المقررة . وهذا إقرار منى بذلك ،,,,,,,,,</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    
                <div class="col-md-4" style="float: left;">
                    <div class="name">
                        <p> المقر بما فيه : </p>
                        <p>  الاسم :
                          <span><?=$student_name?></span></p>
                        <p>  التوقيع :
                          <span></span>
                        </p>
                    </div>
                </div>
                </div>

            

                <div class="col-md-4">
                    <div class="name">
                         <p>تاريخ التقديم :
                          <span><?=str_replace('-','/',$created_date)?></span></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="name">
                        <p> الاستقبال : <br><br><?=$created_by?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="name">
                        <p>يعتمد ،،،،،،،<br><br>........................</p>
                    </div>
                </div>
            
            </div>
        </div>
  </section>
<!-- END SECTION 2  -->
  

<!-- header contact : start -->
 <section id="header-contact" style="padding-top: 0px">
     <div class="container">
         <div class="frist-sec">
             <div class="row">
                 <div class="col-md-4 col-xs-12">
                    <div class="right-side text-center">
                        <h3>6 اكتوبر </h3>
                        <h2>للتدريب والتعليم</h2>
                    </div>
                 </div>
                 <div class="col-md-4 "></div>
                 <div class="col-md-4 col-xs-12 text-center">
                    <div class="left-side">
                        <img src="<?=base_url()?>assets/print/img/6oct-academy--logo.png" class="img-responsiv"/>
                    </div>
                 </div>
             </div>
         </div>
     </div>
</section>
<!-- header contact : end --> 
<section class="list-form">
    <div class="container">
        <div class="row">
            <div class="title22">
                <h3 >لائحة النظام والسلوك:-</h3>
            </div>
            <div class="col-md-12">
                <div class="list">
                    <UL TYPE = "SQUARE">
                     <LI>على الدارس الإلتزام بالحضور حسب الجدول الدراسي للطالب وفقا لتخصصه ومجموعته وعدم تجاوز نسبة الغياب المحددة في المحاضرات
العملية والنظرية وهي 25 % وإخطار الإدارة في حالة الغياب بسبب المرض أو الوظيفة وتقديم الشهادات الدالة على ذلك وإلا يعرض الدارس
نفسه للفصل او الحرمان دخول امتحان المادة التي تجاوز نسبة العياب لها .
                     <LI> عند تغيير محل الإقامة أو عنوان الدارس أو رقم تليفون يتم إخطار إدارة شئون الطلاب  : بالبيانات الجديدة .
                     <LI> على الدارس الإلتزام بالدخول من البوابة الرئيسية بالكارنيه الخاص به وفي حالة فقده عليه إخطار الإدارة بذلك حتى لا يتعرض للحرمان
من الدراسة .
                     <LI> على الدارس المحافظة على محتويات المكان من أجهزة خاصة ومدرجات دراسية وخلافه وأن يلتزم بدفع تكاليف الإصلاح لما تسبب في
إتلافه من أجهزة أو معدات التدريب أو أي من محتويات المركز اوتحمل تكاليف شراء ما لا يمكن إصلاحه كما ان سوء السلوك او التدخين
يعرض الدارس للفصل .
                  </UL>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="left22">
                    <div class="box clearfix">
                        <table class="table table-bordered table-hover ">
                            <thead>
                                <tr>
                                    <th>قيمة القسط</th>
                                    <th>ميعاد السداد</th>
                                </tr>
                            </thead>
                                <tbody>
                                  <?php $i=0; foreach ($installment as $inst_key => $inst_value): $i++; ?>
                                    <tr>
                                        <td><?=$inst_value->value ?></td>
                                        <td><?=$i == 1 ? 'عند التقديم ' : ''?> </td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>
              </div>
            </div>

            <div class="col-md-6">
                <div class="right22">
                        <h2>الأوراق المقدمة منالطالب اثناء التقديم</h2>
                    <form>
                         المؤهل الدراسي:
                        <label class="checkbox-inline">
                          <input type="checkbox" <?= !empty($study_file_photo) ? 'checked' : ''?> value="">أصل
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" value="">صورة
                        </label>
                    </form>
                    <form>
                         شهادة الميلاد:
                        <label class="checkbox-inline">
                          <input type="checkbox" value="" <?= !empty($birth_certificate_photo) ? 'checked' : ''?>>أصل
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" value="">صورة
                        </label>
                    </form>
                    <form>
                         صورة البطاقة:
                        <label class="checkbox-inline">
                          <input type="checkbox" value="" <?= !empty($national_id_photo) ? 'checked' : ''?>>
                        </label>
                    </form>
                    <form>
                         صور شخصية:
                        <label class="checkbox-inline">
                          <input type="checkbox" value="" <?= !empty($profile_photo) ? 'checked' : ''?>>
                        </label>

                    </form>
                    
                </div>
            </div>
        </div>
       <div class="row">
            <div class="title22">
                <h3 >لائحة النظام والسلوك:-</h3>
           </div>
           <div class="col-md-12">
             <div class="list2">
                 <p>أتعهد أنا الموقع أدناه الطالب : ..............................................</p>
                    <UL TYPE = "SQUARE">
                     <LI>بأنني اطلعت على نظام دفع المصروفات الدراسية وقبلتها وأتعهد بالإلتزام بسدادها في المواعيد المحددة ، وأقر
بأنني في حالة تأخيري إسبوع عن الدفع في المواعيد السابقة بالإلتزام غرامة تأخير قدرها ) 30 جنيه عن
كل اسبوع عن تاريخ الإستحقاق ( .</UL>
                 <p class="text-center">وهذا إقرار مني بذلك ،،،،</p>
            </div>
           </div>
                         
                <div class="col-md-12">
                    
                <div class="col-md-4" style="float: left;">
                    <div class="name">
                        <p> المقر بما فيه : </p>
                        <p>  الاسم :
                          <span><?=$student_name?></span></p>
                        <p>  التوقيع :
                          <span></span>
                        </p>
                    </div>
                </div>
                </div>
                
                <div class="col-md-4">
                    <div class="date-text">
 <p>تاريخ التقديم : <?=str_replace('-','/',$created_date); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="test20">
                        <h3>الاستقبال  : <br><br><?=$created_by?></h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="test20">
                        <h3>يعتمد  ،،،،،،،، <br><br>........................</h3>
                    </div>
                </div>
       </div>
    </div>
</section>

 <!-- header contact : start -->
<section id="header-contact" style="padding-top: 15px;">
     <div class="container">
         <div class="frist-sec">
             <div class="row">
                 <div class="col-md-4 col-xs-12">
                    <div class="right-side text-center">
                        <h3>6 اكتوبر </h3>
                        <h2>للتدريب والتعليم</h2>
                    </div>
                 </div>
                 <div class="col-md-4 "></div>
                 <div class="col-md-4 col-xs-12 text-center">
                    <div class="left-side">
                        <img src="<?=base_url()?>assets/print/img/6oct-academy--logo.png" class="img-responsiv"/>
                    </div>
                 </div>
             </div>
         </div>
     </div>
</section>
<!-- header contact : end --> 
<section class="report">
<div class="container">
    <div class="row">
        <div class="report1">
            <h2 style="font-size: 16px;" class="text-center">اقــــــــرار</h2>
            <p>اقر أنا : <span>......................................................................................</span></p>
            <br>
            <P>بأنني لا أرغب في تأجيل التجنيد وأكون مسئول مسئولية شخصية عن أي إجراءات خاصة بي بالتجنيد ،</P>
            <br>
            <p class="text-center">وهذا إقرار مني بذلك ،،،،</p>
        </div>
        <div class="col-md-12">
                    
                <div class="col-md-4" style="float: left;">
                    <div class="name">
                        <p> المقر بما فيه : </p>
                        <p>  الاسم :
                          <?=$student_name?></p>
                        <p>  التوقيع :
                          <span></span>
                        </p>
                    </div>
                </div>
                </div>
            <div class="col-md-4">
                <div class="date-text">
                    <p>تاريخ التقديم : <?=str_replace('-','/',$created_date);?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="test20">
                    <h3> الاستقبال:  <br><br><?=$created_by?></h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="test20">
                    <h3>يعتمد ،،،،،،،، <br><br>........................</h3>
                </div>
            </div>
   </div>
</div>
</section>
<script src="<?=base_url()?>assets/print/js/jquery-1.11.3.min.js"></script> 
<script src="<?=base_url()?>assets/print/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/ajax.js"></script>

<script type="text/javascript">
//loadPrint();
</script>
</body>

</html>