<style type="text/css">
  .modal-dialog{
    max-width: 350px;
  }
  .invoice_border{
        height: 65px;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة سحب الملف <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 
                 <h4 class="modal-title" id="myModalLabel-2">ايصال سحب الملف</h4>
                 <ul class="card-actions icons left-top">
                     <li>
                     <a href="javascript:void(0)" data-dismiss="modal" class="text-white closeModal" aria-label="Close">
                         <i class="zmdi zmdi-close"></i>
                     </a>
                 </li>
             </ul>
         </div>
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1">
                     <div class="bill_header">
                         <div class="Billlogo">
                             <img src="<?=base_url().'assets/img/logo/6oct-academy- logo.jpg'?>" alt="Academy Logo" class="img-thumbnail pull-left m-r-10 " />
                         </div>
                         <div class="AcademyInfo" style="">
                             <span  class="InfoSpan">رقم الايصال : <span class="randbillnumber"><?=$invoice_code?> </span></span>
                             <br>
                             <span class="InfoSpan">اسم الموظف : <?=$this->userData->real_name?></span>
                             <br>                             
                         </div>
                         <div class="billInfo">
                         <div class="table-responsive">
                             <table class="table table-hover billTable">
                               <tbody>
                                 <tr>
                                  <td> اسم الطالب :</td>
                                   <td class="bill_student_name"><?=$student->student_name?></td>
                                 </tr>
                                 <tr>
                                  <td>الفرقة :</td>
                                   <td class="bill_Class_name"> <?=$student->class?></td>
                                 </tr>
                                 <tr>
                                  <td>الشعبة :</td>
                                   <td class="bill_section_name"><?=$student->section?></td>
                                 </tr>
                               </tbody>
                             </table>
                           </div>                                                                    
                         </div>
                         <div class="billFooter">
                             <span  class="InfoSpanFooter">المبلغ المسترد : <span class="invoice_value_ar"><?=$value?></span></span>
                             <br>
                             <span  class="InfoSpanFooter">التاريخ  : <?=returnMonthFormate(date("F j, Y, g:i a"))?></span>
                         </div>                         
                     </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>