
<!DOCTYPE html>
<html>
<head>
        <meta charset="utf-8">
        <!-- I.X -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- mobile -->

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content=" piacere al nostro sito bla bal bala ">
        <title> </title>
        <link rel="stylesheet" href="<?=base_url()?>assets/print/css/bootstrap.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/print/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/print/css/style.css" type="text/css"/>   
        <link rel="stylesheet" href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700'>
        <script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script>     
    <style type="text/css">
    @page  
{ 
    size: auto;   /* auto is the initial value */ 

    /* this affects the margin in the printer settings */ 
    margin:0mm 0mm 0mm 0mm;  
} 
    body,html
{
  margin: 0px;
}
  body,.PrinterArea{
    width: 455px;
    height: 645px;
  }
    .student-data{
        float: right;
    }
    .student-data span{
        color: #000;
        padding: 7px 0px;
        float: right;
        width: 100%;
        margin-right: 10px;
        font-weight: bold;
        font-size: 14px;
    }
    .student-card-body{
        padding: 10px 0px;
        min-height: 180px;
        margin-top: 50px;
        border-bottom: 8px solid transparent;
    }
    .card-header-logo{
        padding-bottom: 10px;
        display: inline-block;
        width: 100%;
        text-align: center;
        border-bottom: 5px solid;
        border-radius: 35px;
    }
    .card-header-logo img{
        max-width: 50px;
        float: right;
        margin-right: 19px;
    }
    .card-header-logo span{
        font-size: 20px;
        color: #000;
        text-align: center;
        margin-top: 25px;
        float: right;
        margin-right: 128px;
        

    }
    .card-content{
        min-width: 200px;
        min-height: 200px;
        background: #fff;
    }
    .card-header{
        max-height: 100px !important;
        background: white;
    }
    .student-img{
        float: left;
    }
    .student-img img{
        max-width: 100%;
        float: right;
    }
    .card-footer{
        width: 100%;
        margin-top: 12px;
        float:right;
    }
    .card-footer .right{
        float: right;
        margin: 10px 10px 0px 0px; 
        color: #a0a0a0;;
        font-size: 13px;
    }
    .card-footer .left{
        float: left;
        margin: -7px 0px 0px 20px;
        color: #fff;
        font-size: 14px;
    }
</style>
</head>

<body>
<div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1">
                        <div class="card-content">
                            <div class="card-header">
                                
                            </div>
                            <div class="student-card-body">
                                <div class="student-img col-md-4" style="">
                                    <img width="100%" style="margin-top:15px;max-height: 135px;margin-right:-4px;" src="<?=base_url().'assets/uploads/'.$profile_photo?>">
                                </div>
                                <div class="student-data col-md-8">
                                    <span><?=@$student_name?></span>
                                    <br>
                                    <span style="margin-right:25px;width:80%"><?=@$student_code?></span>
                                    <br>
                                    <span><?=@$section?></span>
                                    <br>
                                    <span><?=@$class?></span>
                                    <br>
                                    <span style="font-size: 12px;margin-top:40px;color: #d8d4d4;">2018 / 2019</span>
                                    
                                </div>
                            </div>
                    
                        </div>
                 </div>
                         
             </div>

<script src="<?=base_url()?>assets/print/js/jquery-1.11.3.min.js"></script> 
<script src="<?=base_url()?>assets/print/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/ajax.js"></script>

<script type="text/javascript">
loadPrint();
</script>
</body>

</html>