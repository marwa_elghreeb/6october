<style type="text/css">
  .modal-dialog{
    max-width: 350px;
  }
  .invoice_border{
        height: 85px;
  }
</style>
<script src="<?=base_url()?>assets/print/js/html5shiv.min.js"></script> 
<a href="javascript:;" id="openModal" class="btn btn-info btn-block" data-toggle="modal" data-target="#tab_modal"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>

<div class="modal fade" id="tab_modal" data-backdrop="static" data-keyboard="false" style="background: #e2e2e2;" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 
                 <h4 class="modal-title" id="myModalLabel-2">ايصال رسوم دراسية</h4>
                 <ul class="card-actions icons left-top">
                     <li>
                     <a href="javascript:void(0)" data-dismiss="modal" class="text-white closeModal" aria-label="Close">
                         <i class="zmdi zmdi-close"></i>
                     </a>
                 </li>
             </ul>
         </div>
         <div class="modal-body p-0" id="printSpace">
             <div class="tabpanel">
             </div>
             <div class="tab-content PrinterArea p-20">
                 <div class="tab-pane fadeIn active" id="tab-1">
                     <div class="bill_header">
                        <table style="margin: 0px;" class="table table-hover billTable">
                           <tbody>
                             <tr>
                                <td style="border: 0px;width: 30%">
                                    <div class="Billlogo">
                                        <img style="margin-top: 25px;" src="<?=base_url().'assets/img/logo/6oct-academy- logo.jpg'?>" alt="Academy Logo" class="img-thumbnail pull-left m-r-10 " />
                                    </div>
                                </td>
                               <td style="border: 0px;" class="bill_student_code">
                                <div class="AcademyInfo" style="">
                                    <span  class="InfoSpan">رقم الايصال : <span class="randbillnumber"><?=$invoice_data->invoice_number?> </span></span>
                                    <br>
                                    <span  class="InfoSpan">المبلغ بالجنيه : <span class="invoice_value"><?=$invoice_data->invoice_value?></span></span>
                                    <br>
                                    <span class="InfoSpan">اسم الموظف : <?=$invoice_data->created_by?></span>
                                    <br>                             
                                </div>
                                </td>
                             </tr>
                             
                           </tbody>
                         </table>
                         
                         <div class="billInfo">
                         <div class="table-responsive">
                             <table class="table table-hover billTable">
                               <tbody>
                                 <tr>
                                   <?php if($payment_method != 'open_file'): ?>
                                    <td>كود الطالب :</td>
                                   <td class="bill_student_code"><?=$student_data->student_code?></td>
                                   <?php endif; ?>
                                 </tr>
                                 <tr>
                                  <td> اسم الطالب :</td>
                                   <td class="bill_student_name"><?=$student_data->student_name?></td>
                                 </tr>
                                 <tr>
                                  <td>الفرقة :</td>
                                   <td class="bill_Class_name"> <?=$student_data->class?></td>
                                 </tr>
                                 <tr>
                                  <td>الشعبة :</td>
                                   <td class="bill_section_name"><?=$student_data->section?></td>
                                 </tr>
                                 <tr>
                                  <td>المبلغ :</td>
                                   <td class="open_file_amount"><?=$invoice_data->invoice_value?></td>
                                 </tr>
                               </tbody>
                             </table>
                           </div>                                                                    
                         </div>
                         <div class="billFooter">
                             <span  class="InfoSpanFooter">مبلغ وقدرة : <span class="invoice_value_ar"><?=$invoice_data->invoice_value_ar?></span></span>
                             <br>
                             <span  class="InfoSpanFooter"> ذلك قيمة : <span class="invoice_payment"><?=$invoice_status?></span></span>
                             <br>
                             <span  class="InfoSpanFooter">تاريخ  : <?=returnMonthFormate($invoice_data->created_in)?></span>
                             <br>
                             <span  class="InfoSpanFooter">المجموع : <span class="open_file_amount"><?=$invoice_data->total?></span></span>
                             <br>
                             <span  class="InfoSpanFooter">مدفوع : <span class="open_file_amount"><?=$invoice_data->invoice_value?></span></span>
                             <br>
                             <span  class="InfoSpanFooter">المتبقى : <span class="open_file_amount"><?=$invoice_data->residual?></span></span>
                             <br>
                         </div>
                         <div class="invoice_border"><span>ملحوظة : المصروفات الدراسية لاتسترد باى حال من الاحوال</span></div>
                         
                     </div>    
                 </div>
                         
             </div>
         </div>
         
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat closeModal" data-dismiss="modal">اغلاق</button>
             <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
         </div>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>
     <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <script type="text/javascript">
        $('body').on('click','.closeModal',function(){
            window.close();
        });
     </script>