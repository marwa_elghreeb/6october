<div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <header id="header">
                        <h1>الفروع</h1>
                        <a class="btn btn-success search-student-btn" href="<?=base_url().'branches/create/'?>"> اضافة فرع جديد
                            <i class="zmdi zmdi-plus zmdi-hc-fw"></i></a>
                    </header>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="card card-data-tables ">

                        <div class="card-body p-0">

                            <div class="table-responsive">
                                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th >الاسم</th>

                                        <th class="col-xs-2">
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(count($branches) > 0)
                                    { $i=0; foreach($branches as $item){ $i++; ?>
                                        <tr>
                                            <td><?=$i?></td>
                                            <td><?=$item->name?></td>

                                            <td>

                                                <a ata-toggle="tooltip" data-placement="top" title="حذف" href="javascript:;"
                                                   data-url="<?=base_url().'home/delete/'?>"
                                                   data-id="<?=$item->id?>" data-table="branches"
                                                   data-column="id" style="min-width:38px;width:38px;height:38px;font-size:22px"
                                                   class="btn btn-primary btn-fab delete"><i class="zmdi zmdi-delete zmdi-hc-fw"></i></a>

                                                <a ata-toggle="tooltip" data-placement="top"
                                                   style="min-width:38px;width:38px;height:38px;font-size:22px" title="تعديل" href="<?=base_url().'branches/edit/'.$item->id?>" class="btn btn-info btn-fab"><i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                                                </a>


                                            </td>
                                        </tr>
                                    <?php } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>