<div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <header id="header">
                        <h1> أضافة   جديد</h1>
                    </header>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="card p-b-20">
                        <header class="card-heading ">
                            <h2 class="card-title" style="display: inline-block;"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> أضافة جديد</h2>
                            <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'Branches/'?>"> رجوع <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>

                        </header>
                        <div class="card-body">
                            <form class="form-horizontal insert_data" result-data="form_result" method="POST" action="<?=base_url().'Branches/store'?>">

                                <div class="form-group is-empty">
                                    <label for="real_name" class="col-md-2 control-label">الاسم</label>
                                    <div class="col-md-10">
                                        <input type="text" name="name" class="form-control" id="name" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="الاسم ">
                                    </div>
                                </div>





                                <div class="form-group">
                                    <div class="form_result"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary"> اضافة <i class="zmdi zmdi-save zmdi-hc-fw"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
