<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>المصروفات</h1>
            <a class="btn btn-success search-student-btn" href="<?=base_url().'accounts/add_expense/'.$type?>"> اضافة ايصال <i class="zmdi zmdi-plus zmdi-hc-fw"></i></a>  
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">ايصالات <?=$type_str?></h2>  
              <span><?=date("F j, Y");?></span>            
              <!-- <div class="card-search">
                <div id="productsTable_wrapper" class="form-group label-floating is-empty">
                  <i class="zmdi zmdi-search search-icon-left"></i>
                  <input type="text" class="form-control filter-input" placeholder="بحث ..." autocomplete="off">
                  <a href="javascript:void(0)" class="close-search" data-card-search="close" data-toggle="tooltip" data-placement="top" title="Close"><i class="zmdi zmdi-close"></i></a>
                </div>
              </div> -->
              <!-- <div class="col-md-12">
                <form class="student_search" action="<?=base_url().'students_affairs'?>" method="GET">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="student_name" class="col-sm-3 control-label">اسم الطالب</label>
                    <div class="col-sm-9">
                        <input id="student_name" type="text" name="student_name" placeholder="اسم الطالب" value="<?=$this->input->get('student_name') != null ? $this->input->get('student_name') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="student_code" class="col-sm-3 control-label">كود الطالب</label>
                    <div class="col-sm-9">
                        <input id="student_code" type="text" name="student_code" placeholder="كود الطالب" value="<?=$this->input->get('student_code') != null ? $this->input->get('student_code') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="class_id" class="col-sm-3 control-label">الفرقة</label>
                      <div class="col-sm-9">
                          <select name="class_id" id="class_id" class="select form-control">
                              <option>اختر الفرقة</option>
                              <?php foreach ($class as $class_key => $class_value): ?>
                                  <option <?=$this->input->get('class_id') == $class_value->class_id ? 'selected' : '' ?> value="<?=$class_value->class_id?>"><?=$class_value->class_name?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="section_id" class="col-sm-3 control-label">القسم</label>
                      <div class="col-sm-9">
                          <select name="section_id" id="section_id" class="select form-control">
                              <option>اختر القسم</option>
                              <?php foreach ($section as $section_key => $section_value): ?>
                                  <option <?=$this->input->get('section_id') == $section_value->section_id ? 'selected' : '' ?> value="<?=$section_value->section_id?>"><?=$section_value->section_name?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                      </div>
                  </div>
                </div>
                </form>
                
                
              </div> -->
              <hr>
              <ul class="card-actions icons left-top">
                
                <li>
                  <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="Filter Products">
                    <i class="zmdi zmdi-filter-list"></i>
                  </a>
                </li>
                <li>
                  <a href="<?=base_url().'accounts/print_expense_invoices/'.$type?>" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="طباعة">
                    <i class="zmdi zmdi-print"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div class="card-body p-0">

              <div class="table-responsive">
                <table id="productsTable" class="mdl-data-table product-table m-t-30 TableAjax" page="<?=base_url().'accounts/get_expenses_ajax'?>" data-type='<?=$type?>' cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="col-xs-2">اسم المستلم</th>
                      <th class="col-xs-2">الرقم القومى</th>
                      <th class="col-xs-2">المبلغ</th>
                      <th class="col-xs-2">ملاحظات</th>
                      <th class="col-xs-2">اضيف بواسطة</th>
                      <th class="col-xs-2">تاريخ الاضافة</th>
                      <th class="col-xs-2">
                      </th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  </tbody>
                </table>
                <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
                  
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>