<div id="content_wrapper" class="card-overlay">
<div class="content-body">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <?php  ?>
                <h1>اعداد المصروفات الدراسية  للفرقة  (<?=$class_id == 1 ? 'الاولى' : 'الثانية' ?>)</h1>
              </header>
            </div>
          </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12">

                    <div class="card p-b-20" >
                        <div class="card-body">
                            <header id="header">
                                <h1>المصروفات   للفرقة  (<?=$class_id == 1 ? 'الاولى' : 'الثانية' ?>) <i class="zmdi zmdi-money"></i></h1>
                                <a href="<?=base_url().'accounts/tuition_fees_setting/'.$linkNum?>" class="btn btn-primary">مصروفات  <?=$class_name?></a>
                            </header>
                            <hr>
                            <div class="col-md-12">
                                <?php $i=0; foreach ($section as $section_key => $section_value){ $i++; ?>
                                    <div class="col-md-3">

                                        <a <?=$i==1 && $section_id == null ? 'disabled' : $section_value->section_id == $section_id ? 'disabled' : ''?> style="width: 100%;" href="<?=$i==1 && $section_id == null ? base_url().'accounts/tuition_fees_setting/'.$class_id.'/'.$section_value->section_id : $section_value->section_id == $section_id ? 'javascript:;' : base_url().'accounts/tuition_fees_setting/'.$class_id.'/'.$section_value->section_id?>" class="btn btn-info"><?=$section_value->section_name?></a>
                                        
                                    </div>
                                <?php } ?>
                            </div>


                            <?php foreach ($year as $year_key => $year_value): ?>

                            <?php if($year_value->active == 1){ ?>
                            <form class="form_in2 form-horizontal" style="border: 2px solid #ccc;padding: 30px;" result-data="form_result" method="POST" action="<?=base_url().'accounts/insert_tuition_fees_setting/'.$class_id.'/'.$section_id?>">
                                <header id="header" style="margin: 30px 0px;">
                                    <span style="font-weight: bold;color: #5867c3">المصروفات الدراسية لسنة <?=$year_value->year_type.' / '.($year_value->year_type +1)?></span>
                                </header>
                            <input type="hidden" name="year_tb_id" value="<?=$year_value->year_tb_id?>">

                            <?php foreach ($tuition_fees[$year_key] as $tuition_fees_key => $tuition_fees_value): ?>
                                <input type="hidden" name="tuition_fees_id[]" value="<?=$tuition_fees_value->tuition_fees_id?>">
                                <div class="col-md-4" style="border-left:1px solid #ccc ">
                                    
                                    <div class="form-group is-empty">
                                        <label for="" class="col-md-5 control-label"><?=tuition_fees_type($tuition_fees_value->type)?> </label>
                                        <div class="col-md-7">
                                          <input type="number" name="<?=$tuition_fees_value->type?>" class="form-control" value="<?=$tuition_fees_value->tuition_fees?>" id="<?=$tuition_fees_value->type?>_input" placeholder="Name">
                                        </div>
                                    </div>    
                                    
                                    
                                    <?php foreach ($installment[$tuition_fees_value->tuition_fees_id] as $installment_key => $installment_value): ?>
                                    <?php if($installment_value->tuition_fees_id == $tuition_fees_value->tuition_fees_id){ ?>
                                    
                                    <div class="form-group is-empty <?=$tuition_fees_value->type?>">
                                        <label for="" class="col-md-4 control-label">القسط <?=WordIN($installment_value->type)?> </label>
                                        <div class="col-md-5">
                                          <input type="number" name="installment[<?=$tuition_fees_value->tuition_fees_id?>][]" class="form-control" value="<?=$installment_value->value?>">
                                        </div>
                                        <!-- <div class="col-md-3">
                                            <a class="btn btn-danger btn-fab btn-fab-sm" href="javascript:;" id="deleteInstallment"> <i class="zmdi zmdi-close"></i> </a>
                                        </div> -->
                                    </div>

                                    <?php } ?>
                                    <?php endforeach; ?>
                                    <!-- <div class="installment_inputs" id="<?=$tuition_fees_value->type?>"></div>
                                    
                                    <div class="col-md-12">
                                        <a href="javascript:;" data-id="<?=$tuition_fees_value->type?>" data-id-num="<?=$tuition_fees_value->tuition_fees_id?>" class="add_installment_btn btn btn-primary">اضافة قسط </a>
                                    </div>   -->                                  
                                    
                                </div>
                            <?php endforeach; ?>
                                
                                
                                
                                

                                <div class="clearfix"></div>
                                <div class="card-footer" style="text-align: center;">
                                    <button class="btn btn-info"> حفظ <i class="zmdi zmdi-search"></i></button>
                                </div>
                                <div class="form_result"></div>
                            </form>

                            <?php }else{ ?>
                                <header id="header" style="margin: 30px 0px;">
                                    <span style="font-weight: bold;color: #5867c3">المصروفات الدراسية لسنة <?=$year_value->year_type.' / '.(($year_value->year_type) + (1) )?></span>
                                </header>

                            <?php foreach ($tuition_fees[$year_key] as $tuition_fees_key => $tuition_fees_value): ?>
                                <?php if($tuition_fees_value->type == 'normal'){ ?>
                                <div class="col-md-4" style="border-left:1px solid #ccc ">
                                    
                                    <div class="form-group is-empty">
                                        <label for="" class="col-md-5 control-label"><?=tuition_fees_type($tuition_fees_value->type)?> </label>
                                        <div class="col-md-7">
                                          <input disabled type="number" name="<?=$tuition_fees_value->type?>" class="form-control" value="<?=$tuition_fees_value->tuition_fees?>" id="<?=$tuition_fees_value->type?>" placeholder="Name">
                                        </div>
                                    </div>    
                                    
                                    
                                    <?php foreach ($installment[$year_key] as $installment_key => $installment_value): ?>
                                    
                                    
                                    <div class="form-group is-empty">
                                        <label for="" class="col-md-5 control-label">القسط <?=WordIN($installment_value->type)?> </label>
                                        <div class="col-md-7">
                                          <input disabled type="number" name="<?=$installment_value->type?>" class="form-control" value="<?=$installment_value->value?>">
                                        </div>
                                    </div>

                                    <?php endforeach; ?>
                                    
                                </div>

                                <?php } else{ ?>
                                
                                <div class="col-md-4">
                                    <div class="form-group is-empty">
                                        <label for="" class="col-md-5 control-label"><?=tuition_fees_type($tuition_fees_value->type)?> </label>
                                        <div class="col-md-7">
                                          <input disabled type="number" name="<?=$tuition_fees_value->type?>" class="form-control" value="<?=$tuition_fees_value->tuition_fees?>" id="<?=$tuition_fees_value->type?>" placeholder="Name">
                                        </div>
                                    </div>

                                    
                                </div>
                                <?php } ?>

                            <?php endforeach; ?>
                                <div class="clearfix"></div>                            
                            <?php } ?>
                        <?php endforeach; ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>