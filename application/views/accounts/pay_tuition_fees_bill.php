<style type="text/css">
	#content_outer_wrapper #content_wrapper.card-overlay #header_wrapper.header-xl+#content{
		padding-top: 196px;
	}
	#header_wrapper.header-xl{
		    height: 240px;
	}
</style>
<div id="content_wrapper" class="card-overlay">
   <div id="header_wrapper" style="    background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0,transparent),color-stop(30%,transparent),color-stop(100%,rgba(0,0,0,.45))),url(<?=base_url().'assets/'?>/img/logo/cover.jpg)!important;" class="header-xl  profile-header">
      <span style="
    color: #fff;
    /* float: right; */
">العام الدراسى  <?=$year_tb->year_type.' / '. ($year_tb->year_type+1) ?></span>
   </div>
   <div id="content" class="container-fluid">
      <div class="row">
         <div class="col-xs-12">
            <div class="card card-transparent">
               <div class="card-body wrapper">
                  <div class="row">
                     <div class="col-md-12 col-lg-3">
                        <div class="card type--profile">
                           <header class="card-heading">
                              <img src="<?=CheckImg($profile_photo)?>" alt="<?=base_url().'assets/uploads/'.$profile_photo?>" class="img-circle">
                           </header>
                           <div class="card-body">
                              <h3 class="name">الاسم : <?=$student_name?></h3>
                              <span class="title"><?=$class?> / <?=$section?></span>
                           </div>
                           
                        </div>
                     </div>
                     <div class="col-md-12 col-lg-9">
                        <div class="card">
                           <header class="card-heading p-0">
                              <div class="tabpanel">
                                 <ul class="nav nav-tabs nav-justified" style="background: #222f3c; color: #fff">
                                    <li class="active" role="presentation"><a style="color: #fff" href="#profile-price-plan" data-toggle="tab" aria-expanded="true">المصروفات</a></li>
                                    <li role="presentation"><a style="color: #fff" href="#profile-timeline" data-toggle="tab" aria-expanded="true">بيانات الطالب</a></li>
                                    <li role="presentation"><a style="color: #fff" href="#student-profile-files" data-toggle="tab" aria-expanded="true">ملفات الطالب</a></li>
                                 </ul>
                              </div>
                              <div class="card-body" style="background: #fff;float: right;width: 100%;">
                                 <div class="tab-content">
                                    
                                    <div class="tab-pane fadeIn active" id="profile-price-plan">
                                       <div class="card card-transparent m-b-0">
                                          <header class="card-heading">
                                             <h2 class="card-title m-t-0">المصاريف الدراسية</h2>
                                             <small>جميع الارقام بالجنية المصرى</small>
                                          </header>
                                          <div class="card-body p-t-0" style="    min-height: 950px;">
                                       <?php if(empty($cash_invoice)){ if(empty($installment_invoice)){ ?>
                                          <div class="col-md-12">
                                             <div class="col-md-6">
                                                <a href="javascript:;" id="push_cash" class="btn btn-success btn-block"> المصروفات كاش <?=$tuition_fees->tuition_fees?></a>
                                             </div>

                                             <?php if($tuition_fees_type == 'normal'): ?>
                                             <div class="col-md-6">
                                                <a href="javascript:;" id="push_installment" class="btn btn-success btn-block"> المصروفات بلقسط <?=$count_installment?></a>
                                             </div>
                                             <?php endif; ?>
                                          </div>


                                          <?php } ?>

                                          <div class="col-md-12 push_cash hidden">
                                                   <div class="col-md-6">
                                                      <a href="javascript:;" disabled class="btn btn-success btn-block"> المصروفات الدراسية <?=$tuition_fees->tuition_fees?></a>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <input type="text" class="hidden" disabled class="form-control push_cash" name="tuition_fees_cash" value="<?=$tuition_fees->tuition_fees?>">
                                                   </div>
                                                   <div class="col-md-6">
                                                      <a href="javascript:;" data-result="resultDiv" data-url="<?=base_url().'accounts/tuition_fees'?>" data-fees="tuition_fees_cash" data-value="0" data-student="<?=$student_id?>" id="tuition_fees_payment" class="btn btn-primary btn-block"> دفع  المصروفات</a>
                                                   </div>
                                                   <div class="col-md-12 resultDiv"></div>
                                                   <!-- <div class="hidden col-md-6" id="resultDiv">
                                                      <a href="<?=base_url().'accounts/print_invoices/cash/'.$student_id.'/'.$cash_invoice->invoice_id?>" class="btn btn-info btn-block" target="_blank"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                                   </div> -->
                                             </div>

                                             
                                             <header class="card-heading push_installment hidden" style="float: right;width: 100%;">
                                                <div class="p-t-0" style="width: 100%;float: right;padding: 10px 0px;border: 0px dotted;"></div>

                                                <h2 class="card-title m-t-0" style="float: right;">الاقساط :</h2>
                                             </header>
                                             <?php 
                                             if(!empty($installment) && isset($installment)){ $i=0;
                                                foreach ($installment as $installment_key => $installment_value)
                                                { 
                                                   if($installment_value->value > 0){
                                                   $i++; $disabledClass = ''; $Residual = '';
                                                
                                                if(!empty($installment_invoice)){$classHidden='';}else $classHidden='hidden';
                                                $invoices = $this->main_m->getRowsCond('invoice_log',
                                                array(
                                                   'student_id' => $student_id,
                                                   'class_id' => $class_id,
                                                   'payment' => 'tuition_fees_installment',
                                                   'payment_value' => $installment_value->installment_id,
                                                   'year_tb_id' => $year_tb->year_tb_id
                                                ));
                                                
                                             ?>
                                                
                                                <div class="col-md-12 push_installment <?=$classHidden?>">
                                                   <div class="col-md-12">
                                                      <a href="javascript:;" disabled class="btn btn-success btn-block"> القسط  <?= $installment_value->installment_type.' '.$installment_value->value ?></a>
                                                   </div>
                                                   
                                   
                                                   <?php if(!empty($invoices)){ $totalPaid=array(); $z=0; foreach ($invoices as $key => $value){ 
                                                      $totalPaid[] = $value->invoice_value;
                                                      $z++; ?>
                                                      <div class="col-md-6">
                                                         <p style="padding-top: 0px;margin: 0px;">تم دفع مبلغ وقدرة <?=$value->invoice_value?></p>
                                                         <span style="color: #ccc;">تحريرا فى  <?=returnMonthFormate($value->created_in)?></span>
                                                      </div>
                                                      <div class="col-md-6">
                                                         <a href="<?=base_url().'accounts/print_invoices/installment/'.$student_id.'/'.$value->invoice_id.'/'.$value->invoice_log_id?>" target="_blank" class="btn btn-info btn-block"> طباعة ايصال القسط <?=wordIn($installment_value->type)?> رقم <?=$z?> <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                                      </div>
                                                   <?php  } } if(!empty($invoices) && isset($totalPaid)){ // CHECK INSTALLMENT INVOICE AND PAIED vALUE 
                                                      $total = array_sum($totalPaid);// COUNT PAIED VALUES
                                                      $Residual = $installment_value->value - $total; // COUNT RESIDUAL vALUE
                                                      if($Residual == 0){$disabledClass = 'disabled';} else{$disabledClass = '';}
                                                   ?>
                                                   <span style="font-size: 14px;font-weight: bold;color: red;">اجمالى المدفوع من القسط  <?=$installment_value->installment_type ?> <?=$total?></span>
                                                   <p style="font-weight: bold">المتبقى <?=$Residual ?></p>
                                                   <?php } ?>
                                                   <div class="col-md-6">
                                                      <a href="javascript:;" <?=$disabledClass=='disabled' ? $disabledClass : 'data-toggle="modal" data-target="#tab_modal"'?> data-value="<?=$installment_value->installment_id?>" data-number="<?=$Residual > 0 ? $Residual : $installment_value->value?>" year-tb="<?=$year_tb->year_tb_id?>" data-type="قيمة  <?=$installment_value->installment_type ?>" class="btn btn-primary btn-block installment"> دفع  <?=$installment_value->installment_type ?></a>
                                                   </div>
                                                </div>

                                                <div class="p-t-0" style="width: 100%;float: right;padding: 0px 0px;border: 1px dotted;"></div>
                                                <hr>
                                             <?php } } } }else{ ?>
                                             <div class="col-md-12" id="resultDiv">
                                             <?php if(!empty($remission)){ foreach($remission as $remission_val){ ?>
                                                <?php if($remission_val->type == 'remission'){ ?>
                                                   <span style="font-size: 14px;font-weight: bold;color: #0c3263;">اعفاء  مبلغ  ( <?=$remission_val->value?> )</span>
                                                   <br>
                                                <?php }else{  ?>
                                                   <span style="font-size: 14px;font-weight: bold;color: #0c3263;">المصروفات المقدرة ( <?=$remission_val->value?> )</span>
                                                   <br>
                                                <?php } ?>
                                             <?php } } ?>

                                             <span style="font-size: 14px;font-weight: bold;color: red;">اجمالى المدفوع <?=$cash_invoice->invoice_value_ar ?> ( <?=$cash_invoice->invoice_value?> )</span>
                                             <?php if($cash_invoice->residual != 0){ ?>
                                             <br>
                                             <span style="font-size: 14px;font-weight: bold;color: red;">المتبقى  ( <?=$cash_invoice->residual?> )</span>

                                             <a href="javascript:;" url="<?=base_url().'accounts/pay_residual'?>" data-toggle="modal" data-target="#tab_modal" data-value="<?=$tuition_fees->tuition_fees_id?>" data-number="<?=$cash_invoice->residual?>" year-tb="<?=$year_tb->year_tb_id?>" data-type="قيمة  المصروفات" class="btn btn-primary btn-block installment"> دفع  المتبقى</a>

                                             <?php } ?>

                                                <a href="<?=base_url().'accounts/print_invoices/cash/'.$student_id.'/'.$cash_invoice->invoice_id?>" class="btn btn-info btn-block" target="_blank"> طباعة ايصال المصروفات <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                             </div>
                                             <?php }?>
                                          </div>
                                       </div>
                                       
                                    </div>
                                    <div class="tab-pane fadeIn" id="profile-timeline">
                                       <div class="row">
                                          <div class="col-xs-12 col-sm-11 col-sm-offset-1">
                                             <div class="card card-transparent">
                                          <header class="card-heading">
                                             <h2 class="card-title">بيانات الدراسة</h2>
                                          </header>
                                          <div class="card-body p-t-0">
                                             <div class="p-l-30">
                                             <dl class="dl-horizontal">
                                                   <dt>كود الطالب</dt>
                                                   <dd class="bill_student_code"><?=@$student_code?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>الفرقة</dt>
                                                   <dd><?=$class?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>القسم</dt>
                                                   <dd><?=$section?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>نوع الدراسة</dt>
                                                   <dd><?=$study_type?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>حالة الدراسة</dt>
                                                   <dd><?=$study_status?></dd>
                                                </dl>
                                             </div>
                                          </div>
                                       </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-xs-12 col-sm-11 col-sm-offset-1">
                                             <div class="card card-transparent">
                                          <header class="card-heading">
                                             <h2 class="card-title">البيانات الشخصية</h2>
                                          </header>
                                          <div class="card-body p-t-0">
                                             <div class="p-l-30">
                                                <dl class="dl-horizontal">
                                                   <dt>الاسم</dt>
                                                   <dd><?=$student_name?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>هاتف الطالب  </dt>
                                                   <dd>
                                                      <a href="tel:<?=@json_decode($student_phone)[0]?>" target="_blank"><?=@json_decode($student_phone)[0]?></a>
                                                   </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>هاتف والى الامر </dt>
                                                   <dd>

                                                      <a href="tel:<?=@json_decode($student_phone)[1]?>" target="_blank"><?=@json_decode($student_phone)[1]?></a>
                                                   </dd>
                                                </dl>

                                                <dl class="dl-horizontal">
                                                   <dt>هاتف اخر للطالب </dt>
                                                   <dd>
                                                      <a href="tel:<?=@json_decode($student_phone)[2]?>" target="_blank"><?=@json_decode($student_phone)[2]?></a>
                                                   </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>البريد الالكترونى</dt>
                                                   <dd><a href="mailto:<?=$student_email?>"><?=$student_email?></a></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>محل الميلاد</dt>
                                                   <dd><?=$birth_place?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>المحافظة</dt>
                                                   <dd><?=$state?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>المدينة</dt>
                                                   <dd><?=$city?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>العنوان</dt>
                                                   <dd><?=$address?></dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                   <dt>رقم البطاقة</dt>
                                                   <dd><?=$national_id?></dd>
                                                </dl>
                                             </div>
                                          </div>
                                       </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fadeIn" id="student-profile-files">
                                       	<div class="card card-transparent m-b-0">
	                                       	<div class="card-body p-t-0" style="min-height:270px">
	                                       	<header class="card-heading">
                                                <h2 class="card-title m-t-0">ملفات الطالب</h2>
                                             </header>
                                             <?php if(isset($cash_invoice->invoice_value) OR (isset($installment) && !empty($installment))):?>
                                             
                                             <div class=" col-sm-6 printBTN">
                                                 <a href="<?=$printed == 1 ? 'javascript:;' : base_url().'accounts/student_card/'.$student_id?> "
                                                 <?=$printed == 1 ? 'disabled' : ''?> class="btn btn-info btn-block" target="_blank">طباعة الكارنية <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                             </div>
                                             <div class=" col-sm-6 printBTN">
                                                <div class="checkbox inline-block">
                                                   <label class="login-label">
                                                   <input type="checkbox" <?=$printed == 1 ? 'disabled' : ''?> <?=$printed == 1 ? 'checked' : ''?> name="printed" id="card_printed" data-student="<?=$student_id?>" url="<?=base_url().'accounts/printedCard'?>" class="checkbox-inline" value="1">
                                                  تم طباعة الكارنية
                                                   </label>
                                                </div>
                                             </div>
                                             
                                             <?php endif; ?>
                                             <?php if(isset($invoice_file->invoice_file_id)): ?>
                                                <div class=" col-sm-12 printBTN">
                                                   <a href="<?=base_url().'accounts/print_invoices/open_file/'.$student_id.'/'.@$invoice_file->invoice_file_id?>" class="btn btn-info btn-block" target="_blank"> طباعة ايصال فتح الملف <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                             </div>
                                          <?php endif; ?>
                                             <div class=" col-sm-12 printBTN">
                                                 <a href="<?=base_url().'reception/print_student_file/'.$student_id;?>" class="btn btn-info btn-block print_file_url" target="_blank"> طباعة الملف <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                             </div>
                                             <div class=" col-sm-6 printBTN">
                                                 <a href="<?=base_url().'reception/future_institue_student_file/'.$student_id;?>" class="btn btn-info btn-block print_future_institue_student_file" target="_blank">استمارة معهد المستقبل للجودة <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                             </div>

			                                </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </header>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


   <div class="modal fade" id="tab_modal" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 
                 <h4 class="modal-title" id="myModalLabel-2">رسوم دراسية</h4>
                 <ul class="card-actions icons left-top">
                     <li>
                     <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                         <i class="zmdi zmdi-close"></i>
                     </a>
                 </li>
             </ul>
         </div>
         <form class="form_ajax" method="POST" result-data="form_result" action="<?=base_url().'accounts/insert_installment'?>">
            
         <div class="modal-body p-0" id="">
             <div class="tabpanel">
             </div>
             
                 <div class="tab-pane fadeIn active" id="tab-1">
                    <div class="form-group">
                       <label for="installment_input" class="col-sm-4 control-label installment_label"> </label>
                       <div class="col-sm-8">
                           <input id="installment_input" type="text" name="" disabled placeholder="" value=""  class="form-control" >
                           <input id="year_tb_id" type="hidden" name="year_tb_id" placeholder="" value=""  class="form-control" >
                           <input id="installment_id" type="hidden" name="installment_id" placeholder="" value=""  class="form-control" >



                           <input id="installment_value" type="hidden" name="installment_value" placeholder="" value=""  class="form-control" >
 
                           <input type="hidden" name="student_id" value="<?=$student_id?>">
 
                       </div>
                   </div>   
                   <div class="checkbox inline-block col-sm-offset-4">
                     <label class="login-label">
                        <input type="checkbox" id="installment_checkBox" name="checkbox" class="checkbox-inline" value="1">
                        <span class="installment_parte"></span>                       
                     </label>
                   </div>
                   <div class="form-group installment_parte_input hidden">
                       <label for="installment_parte_input" class="col-sm-4 control-label installment_label2"> </label>
                       <div class="col-sm-8">
                           <input id="installment_parte_input" min="0" type="number" name="installment_parte_input" placeholder="" value="0"  class="form-control hidden" >
                       </div>
                   </div>
                         
             </div>
         </div>
         <div class="form_result col-md-12"></div>
         <div class="clearfix"></div>
         <div class="modal-footer">
             <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">اغلاق</button>
             <button type="submit" class="btn btn-primary">حفظ</button>
         </div>
         </form>
         </div>
         <!-- modal-content -->
     </div>
         <!-- modal-dialog -->
     </div>




