    <div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <h1> أضافة إيصال <?=$type_str?></h1>
                <a class="btn btn-primary search-student-btn hidden" href="javascript:;"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></a>     
              </header>
            </div>
          </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="card p-b-20">
                  <header class="card-heading ">
                    <h2 class="card-title" style="display: inline-block;"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> أضافة إيصال جديد</h2>
                      <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'accounts/expenses/'.$type?>"> رجوع <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>     

                  </header>
                  <div class="card-body">
                    <form class="form-horizontal insert_expenses" method="POST" action="<?=base_url().'accounts/get_expenses_form'?>">
                      <div class="form-group">
                          <label for="" class="col-sm-2 control-label">نوع الايصال</label>
                        <?php if($type == 'normal'){ ?>
                        <label class="radio-inline control-label col-md-2 col-sm-6 normal" style="padding-top: 8px !important;">
                          <input type="radio" name="expense_type" class="normal" checked="checked" id="expense_type" value="0"><span class="circle"></span><span class="check"></span> عادى
                        </label>
                        <?php }else{?>

                        <label class="radio-inline control-label col-md-2 col-sm-6 honesty" style="padding-top: 8px !important;">
                          <input type="radio" name="expense_type" checked="checked" class="honesty" id="expense_type" value="1"><span class="circle"></span><span class="check"></span> امانة
                        </label>
                        <?php } ?>
                      </div>

                      <div class="form-group">
                          <label for="expenses_id" class="col-sm-2 control-label">بند المصروفات</label>
                          <div class="col-sm-10">
                              <select name="expenses_id" id="expenses_id" class="form-control">
                                  <?php foreach ($expenses as $key => $value): ?>
                                      <option <?=$value->expenses_id == $expense_invoice->expenses_id ? 'selected="selected"' : ''?> value="<?=$value->expenses_id ?>"> <?=$value->expenses_name ?></option>
                                  <?php endforeach;?>
                              </select>
                          </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="receiver_name" class="col-md-2 control-label">اسم المستلم</label>
                        <div class="col-md-10">
                          <input type="text" name="receiver_name" value="<?=$expense_invoice->receiver_name?>" class="form-control" id="receiver_name" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="اسم المستلم">
                        </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="  receiver_address" class="col-md-2 control-label">عنوان المستلم</label>
                        <div class="col-md-10">
                          <input type="text" name="receiver_address" value="<?=$expense_invoice->receiver_address?>" class="form-control" id="  receiver_address" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="عنوان المستلم">
                        </div>
                      </div>
                      <input type="hidden" name="ex_invoice_id" value="<?=$ex_invoice_id?>">

                      <div class="form-group is-empty">
                        <label for="receiver_national_id" class="col-md-2 control-label">الرقم القومى</label>
                        <div class="col-md-10">
                          <input type="text" name="receiver_national_id" value="<?=$expense_invoice->receiver_national_id?>" class="form-control" id="receiver_national_id"  minlength="14"  data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="الرقم القومى">
                        </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="  ex_invoice_value" class="col-md-2 control-label">المبلغ</label>
                        <div class="col-md-10">
                          <input type="number" name="ex_invoice_value" value="<?=str_replace('.00','',$expense_invoice->ex_invoice_value)?>" min="0" class="form-control" data-rule-required="true" data-msg-required="هذا الحقل الزامى" id=" ex_invoice_value" placeholder="المبلغ">
                        </div>
                      </div>

                      
                      <div class="form-group is-empty">
                        <label for="comment" class="col-md-2 control-label">ملاحظات</label>
                        <div class="col-md-10">
                          <textarea class="form-control" name="comment" rows="3" id="comment"><?=$expense_invoice->comment?></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary"> حفظ <i class="zmdi zmdi-save zmdi-hc-fw"></i></button>
                          </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    