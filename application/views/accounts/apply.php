    <div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <h1>تقديم الطلاب</h1>
                <a class="btn btn-primary search-student-btn hidden" href="javascript:;"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></a>     
              </header>
            </div>
          </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
                <div class="applyArea">
                    <div class="col-xs-12 col-sm-12">
                        <div class="card">
                          <div class="card-body">
                            <form class="form_insert" result-data="form_result" method="POST" action="<?=base_url().'accounts/apply_student'?>">
                                <div class="form-group label-floating is-empty">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                    <label class="control-label">اسم الطالب</label>
                                    <input type="text" name="student_name" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group label-floating is-empty">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-labels zmdi-hc-fw"></i></span>
                                    <label class="control-label">رقم البطاقة</label>
                                    <input type="text" name="national_id" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group label-floating is-empty">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-receipt zmdi-hc-fw"></i></span>
                                    <label class="control-label">رقم الايصال</label>
                                    <input type="text" name="open_file_bill_number" class="form-control">
                                  </div>
                                </div>
                            
                                <div class="card-footer text-center">
                                    <button class="btn btn-info"> بحث <i class="zmdi zmdi-search"></i></button>
                                </div>
                                <div class="form_result"></div>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    