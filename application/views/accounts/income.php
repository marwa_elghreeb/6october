<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الايرادات </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">قائمة الايرادات</h2>              

              <ul class="card-actions icons left-top">
                
                <li>
                  <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="Filter Products">
                    <i class="zmdi zmdi-filter-list"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div class="card-body p-0">
              <div class="col-md-12">
                <div class="col-md-6">
                  <a href="javascript:;" id="add_income_type" class="btn btn-info">اضافة بند  <i class="zmdi zmdi-plus zmdi-hc-fw"></i></a>
                </div>
              </div>
              <div class="table-responsive">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>نوع الايرااد</th>
                      <th>القيمة المالية</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 0; foreach($income as $income_value): $i++; ?>
                    <?php if($income_value->feature != 'tuition_fees'): ?>
                    <tr>
                      <td><?=$i?></td>
                      <td><?=$income_value->income_type?></td>
                      <td><?=$income_value->income_amount ?></td>
                      <td>
                          <a ata-toggle="tooltip" data-table="income" data-id="<?=$income_value->income_id?>" <?=$income_value->feature == 'tuition_fees' ? 'disabled' : '' ?> data-placement="top" data-type="<?=$income_value->income_type?>" data-amount="<?=$income_value->feature == 'tuition_fees' ? '--' : $income_value->income_amount ?>"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="تعديل" href="javascript:;" class="btn btn-info btn-fab <?=$income_value->feature == 'tuition_fees' ? '' : 'edit_income' ?> "><i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                          </a>

                          <a ata-toggle="tooltip" data-table="income" data-url="<?=base_url().'accounts/deleteBoolen/'?>" data-id="<?=$income_value->income_id?>" data-column="income_id" <?=$income_value->feature == 'tuition_fees' ? 'disabled' : '' ?> data-placement="top" style="min-width:38px;width:38px;height:38px;font-size:22px" title=" حذف" href="javascript:;" class="btn btn-danger btn-fab <?=$income_value->feature != 'tuition_fees' ? 'delete' : '' ?>"><i class="zmdi zmdi-delete zmdi-hc-fw"></i>
                          </a>
                      </td> 
                    </tr>
                  <?php endif; ?>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <!-- Add Income Type -->
  <button class="btn btn-info btn-block hidden add_income_type" data-toggle="modal" data-target="#add_income_popup"></button>
  <div class="modal fade" id="add_income_popup" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel-2">اضافة بند </h4>
            <ul class="card-actions icons left-top">
               <li>
                  <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                  <i class="zmdi zmdi-close"></i>
                  </a>
               </li>
            </ul>
         </div>
         <form class="insertData" method="POST" action="<?=base_url().'accounts/add_income_type'?>">
            <div class="modal-body">
                <div class="form-group">
                  <label for="income_type" class="col-sm-4 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> اسم البند </label>
                  <div class="col-sm-8">
                     <input id="income_type" type="text" data-rule-required="true"  data-msg-required="اكتب اسم  البند" name="income_type" placeholder="اكتب اسم البند"  class="form-control" >
                  </div>
                </div>
                <div class="clearfix"></div>
               <div class="form-group">
                  <label for="income_amount" class="col-sm-4 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> قيمة البند </label>
                  <div class="col-sm-8">
                     <input id="income_amount" type="number" min="0" data-rule-required="true"  data-msg-required="اكتب قيمة  البند" name="income_amount" placeholder="اكتب قيمة  البند"  class="form-control" >
                  </div>
               </div>

            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
               <button type="submit" class="btn btn-primary">حفظ</button>
            </div>
         </form>
      </div>
      <!-- modal-content -->
   </div>
   <!-- modal-dialog -->
  </div>
  <!-- End -->

<!-- Edit Income -->
<button class="btn btn-info btn-block hidden edit_income_popup" data-toggle="modal" data-target="#basic_modal"></button>
<div class="modal fade" id="basic_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel-2">تغير بيانات الايراد</h4>
            <ul class="card-actions icons left-top">
               <li>
                  <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                  <i class="zmdi zmdi-close"></i>
                  </a>
               </li>
            </ul>
         </div>
         <form class="insertData" method="POST" action="<?=base_url().'accounts/edit_income_type'?>">
            <div class="modal-body">
                <div class="form-group">
                  <label for="income_type" class="col-sm-4 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> اسم الايراد </label>
                  <div class="col-sm-8">
                     <input id="income_type" type="text" data-rule-required="true"  data-msg-required="اكتب اسم الايراد" name="income_type" placeholder="اكتب اسم الايراد"  class="form-control" >
                  </div>
                </div>
                <input type="hidden" id="income_id" name="income_id" value="">
                <div class="clearfix"></div>
               <div class="form-group">
                  <label for="income_amount" class="col-sm-4 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> قيمة الايراد </label>
                  <div class="col-sm-8">
                     <input id="income_amount" type="number" min="0" data-rule-required="true"  data-msg-required="اكتب قيمة الايراد" name="income_amount" placeholder="اكتب قيمة الايراد"  class="form-control" >
                  </div>
               </div>

            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
               <button type="submit" class="btn btn-primary">حفظ</button>
            </div>
         </form>
      </div>
      <!-- modal-content -->
   </div>
   <!-- modal-dialog -->
</div>