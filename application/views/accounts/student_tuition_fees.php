<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>ايرادات الطلاب </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'accounts/income_invoices'?>"> رجوع <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>       
              <h2 class="card-title">الايرادات </h2>
              <div class="col-md-12">
                <form class="student_search" action="<?=base_url().'accounts/student_tuition_fees'?>" method="GET">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="student_name" class="col-sm-3 control-label">اسم الطالب</label>
                    <div class="col-sm-9">
                        <input id="student_name" type="text" name="student_name" placeholder="اسم الطالب" value="<?=$this->input->get('student_name') != null ? $this->input->get('student_name') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="student_code" class="col-sm-3 control-label">كود الطالب</label>
                    <div class="col-sm-9">
                        <input id="student_code" type="text" name="student_code" placeholder="كود الطالب" value="<?=$this->input->get('student_code') != null ? $this->input->get('student_code') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="national_id" class="col-sm-3 control-label">الرقم القومى</label>
                    <div class="col-sm-9">
                        <input id="national_id" type="text" name="national_id" placeholder="الرقم القومى" value="<?=$this->input->get('national_id') != null ? $this->input->get('national_id') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                      <label for="class_id" class="col-sm-3 control-label">الفرقة</label>
                      <div class="col-sm-9">
                          <select name="class_id" id="class_id" class=" form-control">
                              <option value="">اختر الفرقة</option>
                              <?php foreach ($class as $class_key => $class_value): ?>
                                  <option <?=$this->input->get('class_id') == $class_value->class_id ? 'selected' : '' ?> value="<?=$class_value->class_id?>"><?=$class_value->class_name?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="section_id" class="col-sm-3 control-label">القسم</label>
                      <div class="col-sm-9">
                          <select name="section_id" id="section_id" class=" form-control">
                              <option value="">اختر القسم</option>
                              <?php foreach ($section as $section_key => $section_value): ?>
                                  <option <?=$this->input->get('section_id') == $section_value->section_id ? 'selected' : '' ?> value="<?=$section_value->section_id?>"><?=$section_value->section_name?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label for="year_type" class="col-sm-3 control-label">السنة الدراسية</label>
                      <div class="col-sm-9">
                          <select name="school_year" id="school_year" class=" form-control">
                              <option value="">اختر السنة الدراسية</option>
                              <?php foreach ($year_studey as $year_studey_key => $year_studey_value): ?>
                                  <option <?=$this->input->get('school_year') == $year_studey_value->year_type ? 'selected' : '' ?> value="<?=$year_studey_value->year_type?>"><?=$year_studey_value->year_type?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label style="color: #bbbbbb" for="" class="col-sm-2 control-label">المبلغ المدفوع <i class="zmdi zmdi-money zmdi-hc-fw"></i></label>
                    
                </div>  
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="col-sm-3 control-label">اقل من</label>
                    <div class="col-sm-9">
                        <input id="value_min" type="number" name="value_min" placeholder="اقل من" value="<?=$this->input->get('value_min') != null ? $this->input->get('value_min') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="col-sm-3 control-label">اكثر من</label>
                    <div class="col-sm-9">
                        <input id="value_max" type="number" name="value_max" placeholder="اكثر من" value="<?=$this->input->get('value_max') != null ? $this->input->get('value_max') : ''?>" class="form-control" >
                    </div>
                </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                      </div>
                  </div>
                </div>
                </form>
                <div class="clearfix"></div>
                
              </div>
            </header>
            <div class="clearfix"></div>
            <div class="card-body p-0">

              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" page="<?=base_url().'accounts/student_tuition_feesAjax'?>"  data-type="<?=str_replace('"',"'",json_encode($this->input->get()))?>" class="mdl-data-table product-table m-t-30 TableAjax" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                        <th class="col-xs-2">اسم الطالب </th>
                        <th class="col-xs-2">كود الطالب </th>
                        <th class="col-xs-2"> الرقم القومى </th>
                        <th class="col-xs-2">الفرقة </th>
                        <th class="col-xs-2">الشعبة </th>
                        <th class="col-xs-2">نوع الايصال</th>
                        <th class="col-xs-2">المبلغ المدفوع</th>
                        <th class="col-xs-2">المتبقى</th>
                      <th class="col-xs-2">الايصالات</th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  </tbody>
                </table>
                
              </div>

              <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>