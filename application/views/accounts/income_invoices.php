
<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>جميع الايرادات </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">الايرادات</h2>              
              <div class="col-md-12">
                <form class="student_search" action="<?=base_url().'accounts/income_invoices'?>" method="GET">
                <div class="col-md-6">
                  <div class="form-group is-empty">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i> التاريخ من</span>
                        <input type="date" class="form-control" value="<?=$this->input->get('date_from') ? $this->input->get('date_from') : ''?>" style="text-align: right;" name="date_from" type="date" placeholder="التاريخ من ...">
                      </div>
                    </div>  
                </div>
                <div class="col-md-6">
                  <div class="form-group is-empty">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i> التاريخ الى</span>
                        <input type="date" class="form-control" value="<?=$this->input->get('date_to') ? $this->input->get('date_to') : ''?>" style="text-align: right;" name="date_to" type="date" placeholder="التاريخ الى ...">
                      </div>
                    </div>  
                </div>
                <!-- <div class="col-md-6">
                  <div class="form-group">
                      <label for="class_id" class="col-sm-3 control-label">الفرقة</label>
                      <div class="col-sm-9">
                          <select name="class_id" id="class_id" class="select form-control">
                              <option>اختر الفرقة</option>
                              <?php foreach ($class as $class_key => $class_value): ?>
                                  <option <?=$this->input->get('class_id') == $class_value->class_id ? 'selected' : '' ?> value="<?=$class_value->class_id?>"><?=$class_value->class_name?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div> -->
                <!-- <div class="col-md-6">
                  <div class="form-group">
                      <label for="section_id" class="col-sm-3 control-label">القسم</label>
                      <div class="col-sm-9">
                          <select name="section_id" id="section_id" class="select form-control">
                              <option>اختر القسم</option>
                              <?php foreach ($section as $section_key => $section_value): ?>
                                  <option <?=$this->input->get('section_id') == $section_value->section_id ? 'selected' : '' ?> value="<?=$section_value->section_id?>"><?=$section_value->section_name?></option>
                              <?php endforeach; ?>

                          </select>
                      </div>
                  </div>  
                </div> -->
                
                <div class="col-md-6">
                  <div class="form-group">
                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                      </div>
                  </div>
                </div>
                </form>
                
                
              </div>
              <hr>
              <!-- <ul class="card-actions icons left-top">
                
                <li>
                  <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="Filter Products">
                    <i class="zmdi zmdi-filter-list"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul> -->

            </header>
            <div class="clearfix"></div>
            <div class="card-body p-0">

              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" data-type="<?=str_replace('"',"'",json_encode($this->input->get()))?>" class="mdl-data-table product-table m-t-30 TableAjax" page="<?=base_url().'accounts/incomeInvoicesAjax'?>" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                        <th class="col-xs-2">التاريخ</th>
                        <th class="col-xs-2">الاجمالى</th>
                      <?php foreach ($income as $income_key => $income_value): ?>
                        <th class="col-xs-2"><?=$income_value->income_type?></th>
                      <?php endforeach; ?>
                        <th class="col-xs-2">الايصالات</th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  </tbody>
                </table>
                
              </div>

              <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
