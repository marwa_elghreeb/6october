<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>جميع الايرادات </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'accounts/income_invoices'?>"> رجوع <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>    
              <ul class="card-actions icons right-top">
                <li>
                  <a href="<?=base_url().'accounts/print_income_invoices/'.$this->uri->segment('3')?>" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="طباعة">
                    <i class="zmdi zmdi-print"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div>
            <header class="card-heading">
              <h2 class="card-title">الايرادات </h2> 
              <h6>التاريخ : <span style="font-weight: bold;"><?=$date?></span> </h6>             
              <h6>مجموع الايراد : <span style="font-weight: bold;color: green;"><?=$total?></span> جنية مصرى </h6>      
              <h6>مجموع  المصروفات: <span style="font-weight: bold;color: red;"><?=$OTV?></span> جنية مصرى </h6>      
              <h6 style="font-weight: bold">الاجمالى <span style="font-weight: bold;color: green;"><?php if($OTV > $total){echo "-";}?><?=$total - $OTV?></span> جنية مصرى </h6>      
            </header>

            <div class="card-body p-0">

              <div class="table-responsive hover" style="overflow-x: auto;">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                        <th class="col-xs-2">التاريخ</th>
                        <th class="col-xs-2">كود الطالب </th>
                        <th class="col-xs-2">اسم الطالب </th>
                        <th class="col-xs-2">الفرقة </th>
                        <th class="col-xs-2">الشعبة </th>
                        <th class="col-xs-2">نوع الرسوم</th>
                        <th class="col-xs-2">ملاحظات</th>
                        <th class="col-xs-2">المبلغ</th>
                        <th class="col-xs-2">اضف بواسطة</th>
                      <th class="col-xs-2">الطباعة</th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  	<?php $i=0; foreach ($invoices as $invoices_key => $invoices_value){ $i++;?>
                  		
                  		<tr>
                  			<td><?=$i?></td>
                  			<td><?=$invoices_value->date?> <br> <span style="color: green;"><?=$invoices_value->time?></span></td>
                  			<td><?=$invoices_value->student_code?></td>
                  			<td><?=$invoices_value->student_name?></td>
                  			<td><?=$invoices_value->class?></td>
                  			<td><?=$invoices_value->section?></td>
                        <td><?=$invoices_value->income_invoice_payment?></td>
                        <td><?=$invoices_value->comment?></td>
                        <td><?=$invoices_value->income_invoice_value?></td>
                  			<td><?=$invoices_value->income_invoice_created_by?></td>
                  			<td><a target="_blank" ata-toggle="tooltip" data-placement="top"  style="min-width:38px;width:38px;height:38px;font-size:22px" title="الايصالات" href="<?=base_url().'accounts/print_income_invoice/'.$invoices_value->income_invoice_id?>" class="btn btn-primary btn-fab"><i class="zmdi zmdi-print zmdi-hc-fw"></i></a></td>
                  		</tr>
                  		
                  	<?php } ?>
                  </tbody>
                </table>
                
              </div>

              <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
              </div>

            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>