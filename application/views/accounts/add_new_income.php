    <div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <h1> أضافة إيراد جديد</h1>
              </header>
            </div>
          </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="card p-b-20">
                  <header class="card-heading ">
                    <h2 class="card-title" style="display: inline-block;"><i class="zmdi zmdi-plus zmdi-hc-fw"></i>أضافة إيراد جديد</h2>
                      <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'accounts/income_invoices/'?>"> الايرادات <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>     

                  </header>
                  <div class="card-body">
                    <form class="form-horizontal insert_income_invoice" method="POST" action="<?=base_url().'accounts/add_income_invoice'?>">

                      <div class="form-group is-empty">
                        <label for="student_name" class="col-md-2 control-label">اسم الطالب</label>
                        <div class="col-md-10">
                          <input type="text" name="" class="form-control" id="student_name" disabled="disabled" value="<?=$student_data->student_name?>">
                        </div>
                      </div>
                      <input type="hidden" name="student_name" value="<?=$student_data->student_name?>">

                      <div class="form-group is-empty">
                        <label for="student_code" class="col-md-2 control-label">كود الطالب</label>
                        <div class="col-md-10">
                          <input type="text" name="student_code" class="form-control" id="student_code" disabled="disabled" value="<?=$student_data->student_code?>">
                        </div>
                      </div>
                      <input type="hidden" name="student_id" id="student_id" value="<?=$student_data->student_id?>">
                      <div class="form-group is-empty">
                        <label for="class_id" class="col-md-2 control-label">الفرقة</label>
                        <div class="col-md-10">
                          <select name="class_id" id="class_id" class="form-control">
                            <?php foreach($class as $class_val): ?>
                              <option value="<?=$class_val->class_id?>" <?=$student_data->class_id == $class_val->class_id ? 'selected' : ''?>> <?=$class_val->class_name ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>


                      <div class="form-group is-empty">
                        <label for="section" class="col-md-2 control-label">القسم</label>
                        <div class="col-md-10">
                          <input type="text" name="section" class="form-control" id="section" disabled="disabled" value="<?=$student_data->section?>">
                        </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="student_year" class="col-md-2 control-label">السنة الدراسية</label>
                        <div class="col-md-10">
                          <input type="text" name="student_year" class="form-control" id="student_year" disabled value="<?=student_year()?>">
                        </div>
                      </div>

                      <div class="form-group" style="padding: 0px">
                          <label for="income_id" class="col-sm-2 control-label">الايراد</label>
                          <div class="col-sm-10">
                              <select name="income_id" id="income_id" class="form-control">
                                  <option value="">اختر الايراد</option>
                                  <?php foreach ($income as $key => $value){
                                      if($value->feature == 'tuition_fees')
                                          $link = base_url('accounts/get_student_tuitionFees_details');
                                      else
                                          $link = '';
                                      if($value->feature != NULL){
                                        ?>
                                      <option link="<?=$link?>" amount="<?=$value->income_amount?>" <?=$value->feature == 'section' ?'url="'.base_url().'home/get_sections"' : ''?> value="<?=$value->income_id?>" income_type="<?=$value->feature?>"> <?=$value->income_type ?></option>

                                  <?php } else{
                                  ?>
                                      <option link="<?=$link?>" amount="<?=$value->income_amount?>" value="<?=$value->income_id ?>"> <?=$value->income_type ?></option>
                                  <?php } } ?>
                              </select>
                          </div>
                      </div>
                      
                      <div class="new_result"></div>
                      
                      <div class="form-group is-empty tuition_amount hidden">
                        <label for="tuition_amount" class="col-md-2 control-label">القيمة</label>
                        <div class="col-md-10">
                          <input type="number" name="tuition_amount" min="0" class="form-control" id="tuition_amount" disabled value="">
                        </div>
                      </div>
                        <?php if($this->userData->owner == 1): ?>
                      <div class="form-group is-empty">
                        <label for="remission" class="col-md-2 control-label">اعفاء</label>
                        <div class="col-md-10">
                          <input type="number" name="remission" min="0" class="form-control" id="remission" disabled value="">
                        </div>
                      </div>
                        <?php endif; ?>
                      <div class="form-group is-empty">
                        <label for="comment" class="col-md-2 control-label">ملاحظات</label>
                        <div class="col-md-10">
                          <textarea name="comment" class="form-control" id="comment"></textarea>
                        </div>
                      </div>

                      <div class="select_info">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-10">
                          <span id="income_amount" style="font-size: 13px;font-weight: bold;color: #989898;"></span>
                        </div>
                      </div>

                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <div id="form_result"></div>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary"> تاكيد الدفع <i class="zmdi zmdi-save zmdi-hc-fw"></i></button>
                          </div>
                      </div>
                    </form>
                    <div id="modal-html"></div>
                    <div id="modal-button"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>



    