
<style type="text/css">
    .disabled{
        pointer-events:none;
        opacity:0.4;
    }
    .bTnProgressUpload{
        z-index:99999;
    }
</style>
          <div id="content_wrapper" class="card-overlay">
          <div id="header_wrapper" class="header-md ">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <header id="header">
                    <h1>فتح ملف طالب</h1>
                    <ol class="breadcrumb">
                      <li><a href="index.html">الاستقبال</a></li>
                      <li class="active">فتح الملف</li>
                      <li class="get_breadcrumb"></li>
                    </ol>
                  </header>
                </div>
              </div>
            </div>
          </div>
          <div id="content" class="container">
            <div class="content-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="card" id="rootwizard">
                    <div class="card-heading">
                        <div class="form-wizard-nav">
                          <div class="progress" style="width: 75%;">
                            <div class="progress-bar" style="width:0%;"></div>
                          </div>
                          <ul class="nav nav-justified nav-pills">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab" aria-expanded="true">
                                    <span class="step">1</span>
                                    <span class="title">بيانات الطالب </span>
                                </a>
                            </li>

                            <li class="tab2 disabled">
                                <a href="" data-toggle="tab" aria-expanded="false">
                                    <span class="step">2</span>
                                    <span class="title">ملفات الطالب</span>
                                </a>
                            </li>
                            
                            <li class="tab3 disabled">
                                <a href="" data-toggle="tab" aria-expanded="false">
                                    <span class="step">3</span>
                                    <span class="title">طباعة ملف الطالب</span>
                                </a>
                            </li>
                          </ul>
                        </div>
                      </div>

                      <div class="card-body p-0">
                        <div class="form-wizard form-wizard-horizontal">
                          <div class="tab-content clearfix p-30">
                            <div class="tab-pane active" id="tab1">
                                <div class="card-body">
                                    <form id="form-horizontal" result-data="formResult" edit-class="student_Input" method="POST" tabActive="1" action="<?=base_url()?>reception/add_student"  class="form-horizontal form_insert">
                                        <div class="col-md-12">
                                            <h2 style="display: inline-block;border-bottom: 1px solid;padding-bottom: 10px;font-weight: bold">البيانات الشخصية</h2>
                                        </div>

                                        <div class="form-group">
                                            <label for="student_name" class="col-sm-2 control-label">اسم الطالب</label>
                                            <div class="col-sm-10">
                                                <input id="student_name" type="text" name="student_name" placeholder="اسم الطالب" minlength="5"  data-rule-required="true" data-msg-required="يجب كتابة اسم الطالب"  class="form-control" >
                                            </div>
                                        </div>

                                        

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">جنسية الطالب</label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6 studnet_cuntry" style="padding-top: 8px !important;">
                                            <input type="radio" name="nationality" class="studnet_cuntry" checked="checked" id="inlineRadio3" value="مصرى"><span class="circle"></span><span class="check"></span> مصرى
                                          </label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6 studnet_new_cuntry" style="padding-top: 8px !important;">
                                            <input type="radio" name="nationality" class="studnet_new_cuntry" id="inlineRadio4" value="1"><span class="circle"></span><span class="check"></span> اجنبى
                                          </label>
                                        </div>
                                        <div class="form-group studentCuntry hidden">
                                            <label for="national_id" class="col-sm-2 control-label">اسم البلد</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="" id="student_cuntry" placeholder="اسم البلد"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">الديانة</label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="religion" checked="checked" id="inlineRadio1" value="0"><span class="circle"></span><span class="check"></span> مسلم
                                          </label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="religion" id="inlineRadio2" value="1"><span class="circle"></span><span class="check"></span> مسيحى
                                          </label>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-2 control-label">الجنس</label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="gender" checked="checked" id="inlineRadio1" value="0"><span class="circle"></span><span class="check"></span> ذكر <i class="zmdi zmdi-male zmdi-hc-fw"></i>
                                          </label>
                                          <label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                            <input type="radio" name="gender" id="inlineRadio2" value="1"><span class="circle"></span><span class="check"></span> انثى <i class="zmdi zmdi-female zmdi-hc-fw"></i>
                                          </label>
                                        </div>


                                        <div class="form-group">
                                            <label for="national_id" class="col-sm-2 control-label  national_id_label">رقم البطاقة</label>
                                            <div class="col-sm-10">
                                                <input id="national_id" type="text" name="national_id" placeholder="رقم البطاقة" minlength="14"  data-rule-required="true" data-msg-required="هذا الحقل الزامى"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="national_place" class="col-sm-2 control-label  national_place_label">جهة صدورها </label>
                                            <div class="col-sm-10">
                                                <input id="national_place" type="text" name="national_place" placeholder="جهة صدورها "   data-rule-required="true" data-msg-required="هذا الحقل الزامى"  class="form-control" >
                                            </div>
                                        </div>

                                        

                                        <div class="form-group">
                                            <label for="guardian_name" class="col-sm-2 control-label  guardian_name_label"> اسم والى الامر</label>
                                            <div class="col-sm-10">
                                                <input id="guardian_name" type="text" name="guardian_name" placeholder=" اسم والى الامر"   data-rule-required="true" data-msg-required="هذا الحقل الزامى"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="guardian_work" class="col-sm-2 control-label  guardian_work_label">وظيفة والى الامر</label>
                                            <div class="col-sm-10">
                                                <input id="guardian_work" type="text" name="guardian_work" placeholder="وظيفة والى الامر"   data-rule-required="true" data-msg-required="هذا الحقل الزامى"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="relative_relation" class="col-sm-2 control-label  relative_relation_label">صلة القرابة </label>
                                            <div class="col-sm-10">
                                                <input id="relative_relation" type="text" name="relative_relation" placeholder="صلة القرابة "   data-rule-required="true" data-msg-required="هذا الحقل الزامى"  class="form-control" >
                                            </div>
                                        </div>


                                        

                                        
                                        <div class="form-group">
                                            <label for="phone" class="col-sm-2 control-label">الهاتف</label>
                                            <div class="col-sm-3">
                                                <input id="phone" type="text" name="student_phone[]" placeholder="رقم الطالب"  minlength="11"  data-rule-required="true"  data-msg-required="يجب كتابة الهاتف"  class="form-control" >
                                            </div>
                                            <div class="col-sm-3">
                                                <input id="Anotherphone" type="text" name="student_phone[]" minlength="11"  placeholder="رقم والى الامر" class="form-control" >
                                            </div>
                                            <div class="col-sm-3">
                                                <input id="Anotherphone" type="text" name="student_phone[]" minlength="11" placeholder="رقم اخر" class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="student_email" class="col-sm-2 control-label">البريد الالكترونى</label>
                                            <div class="col-sm-10">
                                                <input id="student_email" type="email" name="student_email" placeholder="البريد الالكترونى"  class="form-control" >
                                            </div>
                                        </div>

                                        
                                        <hr>
                                        <div class="col-md-12">
                                            <h2 style="display: inline-block;border-bottom: 1px solid;padding-bottom: 10px;font-weight: bold">محل الميلاد</h2>
                                        </div>

                                        <div class="form-group">
                                            <label for="birth_place_state" class="col-sm-2 control-label">المحافظة</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="birth_place_governorate" value="" placeholder="المحافظة" class="form-control hidden birth_place_governorate_input" disabled="disabled">
                                                <select name="birth_place_governorate" id="birth_place_state" data-url="<?=base_url('home/changeSelect')?>" data-table="city" data-resultArea="birth_place" data-tr="name" data-get-id="city_id" data-get-name="city_name" data-column="governorate_id" class="form-control changeAjax birth_place_governorate">
                                                    <option>اختر المحافظة</option>
                                                    <?php foreach ($governorate as $key => $value): ?>
                                                        <option value="<?=$value->governorate_id ?>"> <?=$value->governorate_name ?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="birth_place" class="col-sm-2 control-label">محل الميلاد</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="birth_place" value="" placeholder="محل الميلاد" class="form-control hidden birth_place_input" disabled="disabled">
                                                <select name="birth_place" id="birth_place" class="form-control birth_place">
                                                    <option>اختر محل الميلاد</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="Date Of bith" class="col-sm-2 control-label">تاريخ الميلاد</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control datepicker" name="date_of_birth" style="text-align: right;" id="md_input_date"  data-rule-required="true" data-msg-required="يجب اختيار تاريخ الميلاد"  placeholder="تاريخ الميلاد" data-dtp="dtp_DUFhz">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-md-12">
                                            <h2 style="display: inline-block;border-bottom: 1px solid;padding-bottom: 10px;font-weight: bold">العنوان</h2>
                                        </div>

                                        <div class="form-group">
                                            <label for="address" class="col-sm-2 control-label">العنوان</label>
                                            <div class="col-sm-10">
                                                <input id="address" type="text" name="address" placeholder="العنوان" data-rule-required="true" data-msg-required="يجب كتابة العنوان"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="area_name" class="col-sm-2 control-label">المركز</label>
                                            <div class="col-sm-10">
                                                <input id="area_name" type="text" name="area_name" placeholder="المركز"  data-rule-required="true"  data-msg-required="يجب كتابة المركز"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="state" class="col-sm-2 control-label">المحافظة</label>
                                            <div class="col-sm-10">
                                                <select name="state" id="state" data-url="<?=base_url('home/changeSelect')?>" data-table="city" data-resultArea="studnet_city" data-get-id="city_id" data-get-name="city_name" data-column="governorate_id" class="form-control changeAjax" data-rule-required="true"  data-msg-required="يجب اختيار المحافظة">
                                                    <option>اختر المحافظة</option>
                                                    <?php foreach ($governorate as $key => $value): ?>
                                                        <option value="<?=$value->governorate_id ?>"> <?=$value->governorate_name ?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="city" class="col-sm-2 control-label">المدينة</label>
                                            <div class="col-sm-10">
                                                <select name="city" id="city" data-rule-required="true"  data-msg-required="يجب اختيار المدينة" class="form-control studnet_city">
                                                    <option>اختر المدينة</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group city_text_area hidden">
                                            <label for="city_text" class="col-sm-2 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> المدينة </label>
                                            <div class="col-sm-10">
                                                <input id="city_text" type="text" name="city_text" disabled="disabled" placeholder="اكتب المدينة"  class="form-control" >
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-md-12">
                                            <h2 style="display: inline-block;border-bottom: 1px solid;padding-bottom: 10px;font-weight: bold">المؤهل الدراسى</h2>
                                        </div>
                                        <div class="form-group">
                                            <label for="qualification" class="col-sm-2 control-label">المؤهل الدراسى</label>
                                            <div class="col-sm-10">
                                                <select name="qualification" id="qualification" class="form-control">
                                                    <option>اختر المؤهل الدراسى</option>
                                                    <?php $i=0; $z=count($qualification); foreach ($qualification as $key => $value){ $i++;?>
                                                        <option data-choose="<?=$value->choosen?>" value="<?=$value->qualification_id ?>"> <?=$value->qualification ?></option>
                                                    <?php }?>
                                                    <option data-choose="1" value="last"> اخرى </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group qualification_text_area hidden">
                                            <label for="qualification_text" class="col-sm-2 control-label">المؤهل الدراسى</label>
                                            <div class="col-sm-10">
                                                <input id="qualification_text" data-rule-required="true"  data-msg-required="يجب كتابة المؤهل الدراسى" type="text" name="qualification" disabled="disabled" placeholder="اكتب المؤهل الدراسى"  class="form-control" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="qualification_grade" class="col-sm-2 control-label">المجموع</label>
                                            <div class="col-sm-10">
                                                <input id="qualification_grade" data-rule-required="true"  data-msg-required="يجب كتابة المجموع" type="text" name="qualification_grade" placeholder="المجموع"  class="form-control" >
                                            </div>
                                        </div>

                                        
                                        
                                        <div class="form-group">
                                            <label for="state_administration" class="col-sm-2 control-label">محافظة الادارة التعليمية</label>
                                            <div class="col-sm-10">
                                                <select id="state_administration" name="state_administration" data-url="<?=base_url('home/changeSelect')?>" data-table="administrations" data-resultArea="management_learning" data-get-id="administration_id" data-get-name="administration_name" data-column="governorate_id" class=" form-control changeAjax ">
                                                    <option>اختر المحافظة</option>
                                                    <?php foreach ($governorate as $key => $value){ ?>
                                                        <option value="<?=$value->governorate_id?>" > <?=$value->governorate_name ?> </option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="management_learning" class="col-sm-2 control-label">الاداراة التعليمية</label>
                                            <div class="col-sm-10">
                                                <select name="management_learning" id="management_learning" data-rule-required="true"  data-msg-required="يجب اختيار الاداراة التعليمية" class="form-control management_learning">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group management_learning_text_area hidden">
                                            <label for="management_learning_text" class="col-sm-2 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> الاداراة التعليمية </label>
                                            <div class="col-sm-10">
                                                <input id="management_learning_text" type="text" data-rule-required="true"  data-msg-required="هذا الحقل الزامى" name="management_learning_text" disabled="disabled" placeholder="اكتب الاداراة التعليمية"  class="form-control" >
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="student_school" class="col-sm-2 control-label">المدرسة</label>
                                            <div class="col-sm-10">
                                                <select name="student_school" id="student_school" data-rule-required="true"  data-msg-required="يجب كتابة المدرسة"  class="form-control student_school">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group student_school_text_area hidden">
                                            <label for="student_school_text" class="col-sm-2 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> المدرسة  </label>
                                            <div class="col-sm-10">
                                                <input id="student_school_text" type="text" data-rule-required="true"  data-msg-required="هذا الحقل الزامى" name="student_school_text" disabled="disabled" placeholder="يجب كتابة المدرسة  " class="form-control" >
                                            </div>
                                        </div>

                                        



                                        <div class="form-group">
                                            <label for="class_id" class="col-sm-2 control-label">الفرقة</label>
                                            <div class="col-sm-10">
                                                <select name="class_id" id="class_id" data-rule-required="true"  data-msg-required="يجب اختيار الفرقة" class="form-control">
                                                        <option selected="selected" value="1">الفرقة الاولى</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="section_id" class="col-sm-2 control-label">القسم</label>
                                            <div class="col-sm-10">
                                                <select name="section_id" data-rule-required="true"  data-msg-required="يجب اختيار القسم" id="section_id" class="form-control">
                                                    <option>اختر القسم</option>
                                                    <?php foreach ($section as $section_key => $section_value): ?>
                                                        <option value="<?=$section_value->section_id?>"><?=$section_value->section_name?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="study_type" class="col-sm-2 control-label">نوع الدراسة</label>
                                            <div class="col-sm-10">
                                                <select name="study_type" id="study_type" data-rule-required="true"  data-msg-required="يجب اختيار نوع الدراسة" class="form-control">
                                                        <option value="1" selected>انتظام</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="study_status" class="col-sm-2 control-label">حالة الدراسة</label>
                                            <div class="col-sm-10">
                                                <select name="study_status" data-rule-required="true"  data-msg-required="يجب حالة الدراسة" id="study_status" class="form-control">
                                                    <option>اختر حالة الدراسة</option>
                                                    <?php foreach ($study_status as $study_status_key => $study_status_value): ?>
                                                        <option data-concentrated="<?=$study_status_value->concentrated?>" value="<?=$study_status_value->status_id?>"><?=$study_status_value->study_status?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group study_status_date hidden">
                                            <label for="study_status_text" class="col-sm-2 control-label">تاريخ التخرج</label>
                                            <div class="col-sm-10">
                                                <select name="graduation_date" data-rule-required="true"  data-msg-required="تاريخ التخرج" id="study_status_text" disabled class="form-control">
                                                    <option value="يونيو <?=student_codeDate()?>">يونيو <?=student_codeDate()?></option>
                                                    <option value="اكتوبر <?=student_codeDate()?>">اكتوبر <?=student_codeDate()?></option>
                                                    <option value="فبراير <?=student_codeDate()?>">فبراير <?=(student_codeDate()+1)?></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="certificate_type" class="col-sm-2 control-label">الشهادة المراد الحصول عليها</label>
                                            <div class="col-sm-10">
                                                <select name="certificate_type" data-rule-required="true"  data-msg-required="يجب اختيار الشهادة المراد الحصول عليها" id="certificate_type" class="form-control">
                                                    <option>اختر الشهادة المراد الحصول عليها</option>
                                                    <?php foreach ($certificate_type as $certificate_type_key => $certificate_type_value): ?>
                                                        <option value="<?=$certificate_type_value->certificate_id?>"><?=$certificate_type_value->certificate_type?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="acquaintance_way" class="col-sm-2 control-label">وسيلة التعارف</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="acquaintance_student_name" value="" placeholder="اكتب اسم الطالب" class="form-control hidden acquaintanceInput">
                                                <select name="acquaintance_way" id="acquaintance_way" class="form-control acquaintanceSelect">
                                                    <option>اختر وسيلة التعارف</option>
                                                    <?php foreach ($acquaintance as $acquaintance_key => $acquaintance_value): ?>
                                                        <option value="<?=$acquaintance_value->acquaintance_id?>"><?=$acquaintance_value->acquaintance_name?></option>
                                                    <?php endforeach; ?>
                                                    <option value="another">اخرى</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="checkbox inline-block col-sm-offset-2">
                                            <label class="login-label acquaintance_student">
                                              <input type="checkbox" name="acquaintance_student" id="acquaintance_student" class="checkbox-inline" value="0">
                                              عن طريق طالب
                                            </label>
                                        </div>
                                        <div class="student_Input"></div>
                                        <div class="formResult col-sm-offset-2"></div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <button type="submit" class="btn btn-primary"> الخطوة التالية <i class="zmdi zmdi-long-arrow-return zmdi-hc-fw"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end #tab1 -->
                            <div class="tab-pane" id="tab2">
                                <div class="card-body">
                                <div class="col-md-3 ManualDZ">
                                    <div class="dz-default dz-message"></div>
                                    <span>الصورة الشخصية</span>
                                    <form action="<?=base_url().'reception/upload'?>" method="post" enctype="multipart/form-data">
                                    <div class="student_Input"></div>
                                        <a href="javascript:;" class="btn btn-info btn-fab bTnProgressUpload"><i class="zmdi zmdi-cloud-upload receptionUpBtn"></i><div class="ripple-container"></div></a>
                                        <input type="file" title="upload" class="hidden InputFile" name="profile_photo">
                                        <div id="progress-U"><div class="progress-barUpload"></div ><div class="status">0%</div></div>
                                        <div id="output"><!-- error or success results --></div>
                                    </form>                                    
                                </div>
                                <div class="col-md-3 ManualDZ">
                                    <div class="dz-default dz-message"></div>
                                    <span> صورة البطاقة </span>
                                    <form action="<?=base_url().'reception/upload'?>" method="post" enctype="multipart/form-data">
                                    <div class="student_Input"></div>
                                        <a href="javascript:;" class="btn btn-info btn-fab bTnProgressUpload"><i class="zmdi zmdi-cloud-upload receptionUpBtn"></i><div class="ripple-container"></div></a>
                                        <input type="file" title="upload" class="hidden InputFile" name="national_id_photo">
                                        <div id="progress-U"><div class="progress-barUpload"></div ><div class="status">0%</div></div>
                                        <div id="output"><!-- error or success results --></div>
                                    </form>                                    
                                </div>

                                <div class="col-md-3 ManualDZ">
                                    <div class="dz-default dz-message"></div>
                                    <span>صورة شهادة الميلاد </span>
                                    <form action="<?=base_url().'reception/upload'?>" method="post" enctype="multipart/form-data">
                                    <div class="student_Input"></div>
                                        <a href="javascript:;" class="btn btn-info btn-fab bTnProgressUpload"><i class="zmdi zmdi-cloud-upload receptionUpBtn"></i><div class="ripple-container"></div></a>
                                        <input type="file" title="upload" class="hidden InputFile" name="birth_certificate_photo">
                                        <div id="progress-U"><div class="progress-barUpload"></div ><div class="status">0%</div></div>
                                        <div id="output"><!-- error or success results --></div>
                                    </form>                                    
                                </div>
                                <div class="col-md-3 ManualDZ">
                                    <div class="dz-default dz-message"></div>
                                    <span>صورة المؤهل الدراسى </span>
                                    <form action="<?=base_url().'reception/upload'?>" method="post" enctype="multipart/form-data">
                                    <div class="student_Input"></div>
                                        <a href="javascript:;" class="btn btn-info btn-fab bTnProgressUpload"><i class="zmdi zmdi-cloud-upload receptionUpBtn"></i><div class="ripple-container"></div></a>
                                        <input type="file" title="upload" class="hidden InputFile" name="study_file_photo">
                                        <div id="progress-U"><div class="progress-barUpload"></div ><div class="status">0%</div></div>
                                        <div id="output"><!-- error or success results --></div>
                                    </form>                                    
                                </div>
                                <form id="form-horizontal" result-data="formResult" method="POST" tabActive="2" action="<?=base_url()?>reception/checkFiles"  class="form-horizontal form_insert">
                                    <div class="form-group">
                                    <div class="student_Input"></div>
                                        <div class="col-sm-offset-2 col-sm-4">
                                          <button type="submit" class="btn btn-primary"> الخطوة التالية  <i class="zmdi zmdi-long-arrow-return zmdi-hc-fw"></i></button>
                                        </div>
                                    </div>
                                    <div class="formResult col-sm-offset-2"></div>
                                </form>

                                </div>
                            </div>
                            <!--end #tab2 -->
                            <!--end #tab3 -->
                            
                            <div class="tab-pane" id="tab3">
                                <div class="card-body MinHeight">
                                    <div class="alert alert-success" role="alert"> <span> تم تسجيل بيانات الطالب</span>
                                    </div>
                                    <form id="form-horizontal" result-data="formResult" method="POST" tabActive="3" action="<?=base_url()?>reception/openStudentFile"  class="form-horizontal form_insert">
                                    
                                    <div class=" col-sm-4 printBTN hidden">
                                        <a href="javascript:;" class="btn btn-info btn-block print_file_url" target="_blank"> طباعة الملف <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                    </div>
                                    <div class=" col-sm-4 hidden future_institue_student_file">
                                        <a href="<?=base_url().'reception/future_institue_student_file'?>" class="btn btn-info btn-block print_future_institue_student_file" target="_blank">استمارة معهد المستقبل للجودة <i class="zmdi zmdi-print zmdi-hc-fw"></i></a>
                                    </div>

                                
                                    <div class=" col-sm-4 printBTN hidden">
                                        <a onclick="location.href='<?=base_url().'reception'?>'" href="javascript:;" class="btn btn-primary btn-block"> اضافة طالب اخر <i class="zmdi zmdi-save zmdi-hc-fw"></i></a>
                                    </div>
                                    <div class="modal fade" id="tab_modal" tabindex="-1" role="dialog" aria-labelledby="tab_modal">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        
                                                        <h4 class="modal-title" id="myModalLabel-2">ايصال فتح الملف</h4>
                                                        <ul class="card-actions icons left-top">
                                                            <li>
                                                            <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                                                <i class="zmdi zmdi-close"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="modal-body p-0" id="printSpace">
                                                    <div class="tabpanel">
                                                    </div>
                                                    <div class="tab-content PrinterArea p-20">
                                                        <div class="tab-pane fadeIn active" id="tab-1">
                                                            <div class="bill_header">
                                                                <div class="Billlogo">
                                                                    <img src="<?=base_url().'assets/img/logo/6oct-academy- logo.jpg'?>" alt="Academy Logo" class="img-thumbnail pull-left m-r-10 " />
                                                                </div>
                                                                <div class="AcademyInfo">
                                                                    <span  class="InfoSpan">رقم الايصال : <span class="randbillnumber"> </span></span>
                                                                    <br>
                                                                    <span class="InfoSpan">اسم الموظف : <?=$this->userData->real_name?></span>
                                                                    <br>
                                                                    <span  class="InfoSpan">تاريخ الايصال : <?=date("Y-m-d")?></span>
                                                                    
                                                                </div>
                                                                <div class="billInfo">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover billTable">
                                                                      <thead>
                                                                        <tr>
                                                                          <th>#</th>
                                                                          <th>اسم الطالب</th>
                                                                          <th>القسم</th>
                                                                          <th>المبلغ</th>
                                                                        </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                        <tr>
                                                                          <td>#</td>
                                                                          <td class="bill_student_name"></td>
                                                                          <td class="bill_section_name"> </td>
                                                                          <td class="open_file_amount"></td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                  </div>                                                                    
                                                                </div>
                                                                <div class="billFooter">
                                                                    <span  class="InfoSpanFooter">تاريخ الاستحقاق : <?=date("Y-m-d")?></span>
                                                                    <br>
                                                                    <span  class="InfoSpanFooter">المجموع : <span class="open_file_amount"></span></span>
                                                                    <br>
                                                                    <span  class="InfoSpanFooter">مدفوع : <span class="open_file_amount"></span></span>
                                                                    <br>
                                                 
                                                                </div>
                                                                
                                                            </div>    
                                                        </div>
                                                                
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">اغلاق</button>
                                                    <a href="javascript:;" id="print" data-print="printSpace" class="btn btn-primary">طباعة</a>
                                                </div>
                                                </div>
                                                <!-- modal-content -->
                                            </div>
                                                <!-- modal-dialog -->
                                            </div>
                                        </form>
                                    </div>
                                </div>  
                                <!--end #tab4 -->
                          </div>
                        </div>
                      </div>


                  </div>
                </div>
              </div>
            </div>
            </div>

            <!--وسيلة التعارف -->
            <button class="btn btn-info btn-block hidden acquaintance_modal" data-toggle="modal" data-target="#acquaintance_modal">Trigger</button>
            <div class="modal fade" id="acquaintance_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            
                            <h4 class="modal-title" id="myModalLabel-2">إضافة وسيلة التعارف</h4>
                            <ul class="card-actions icons left-top">
                            <li>
                                <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form class="insertData" method="POST" data-result-id="acquaintance_way" action="<?=base_url().'reception/add_acquaintance_way'?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="birth_place_text" class="col-sm-4 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> وسيلة التعارف</label>
                                <div class="col-sm-8">
                                    <input id="acquaintance_way" type="text" data-rule-required="true"  data-msg-required="يجب كتابة المدينة" name="acquaintance_way" placeholder="اكتب وسيلة التعارف"  class="form-control" >
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                    </div>
                    <!-- modal-content -->
                </div>
                <!-- modal-dialog -->
            </div>
            <!--وسيلة التعارف -->
            <button class="btn btn-info btn-block hidden basic_modal" data-toggle="modal" data-target="#basic_modal">Trigger</button>
            <div class="modal fade" id="basic_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            
                            <h4 class="modal-title" id="myModalLabel-2">إضافة اسم المدينة</h4>
                            <ul class="card-actions icons left-top">
                            <li>
                                <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form class="insertData" method="POST" data-result-id="" action="<?=base_url().'reception/add_city_name'?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="birth_place_text" class="col-sm-4 control-label"><i class="zmdi zmdi-edit zmdi-hc-fw"></i> اسم المدينة</label>
                                <div class="col-sm-8">
                                    <input id="birth_place_text" type="text" data-rule-required="true"  data-msg-required="يجب كتابة المدينة" name="birth_place_text" placeholder="اكتب اسم المدينة"  class="form-control" >
                                    <input type="hidden" id="birth_place_gid" name="birth_place_gid" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                    </div>
                    <!-- modal-content -->
                </div>
                <!-- modal-dialog -->
            </div>
