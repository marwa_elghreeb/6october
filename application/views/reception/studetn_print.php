<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الاستقبال </h1>
            <button class="btn btn-primary" onclick="location.href='<?=base_url().'reception/add_new_student'?>'">فتح ملف طالب جديد <i class="zmdi zmdi-file-text zmdi-hc-fw"></i></button>                    
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">قائمة الطلاب</h2>              
              <div class="card-search">
                <div id="productsTable_wrapper" class="form-group label-floating is-empty">
                  <i class="zmdi zmdi-search search-icon-left"></i>
                  <input type="text" class="form-control filter-input" placeholder="بحث ..." autocomplete="off">
                  <a href="javascript:void(0)" class="close-search" data-card-search="close" data-toggle="tooltip" data-placement="top" title="Close"><i class="zmdi zmdi-close"></i></a>
                </div>
              </div>
              <ul class="card-actions icons left-top">
                <li id="deleteItems" style="display: none;">
                  <span class="label label-info pull-left m-t-5 m-r-10 text-white"></span>
                  <a href="javascript:void(0)" id="confirmDelete" data-toggle="tooltip" data-placement="top" data-original-title="حذف الطالب ">
                    <i class="zmdi zmdi-delete"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="Filter Products">
                    <i class="zmdi zmdi-filter-list"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div class="card-body p-0">
              
              <div class="table-responsive">
                <table id="productsTable" class="mdl-data-table product-table m-t-30" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="col-xs-2">الصورة الشخصية</th>
                      <th class="col-xs-2">اسم الطالب</th>
                      <th class="col-xs-2">كود الطالب</th>
                      <th class="col-xs-2">الفرقة</th>
                      <th class="col-xs-2">تاكيد الطباعة</th>
                      <th class="col-xs-2">
                      </th>
                    </tr>
                  </thead>
                  <tbody class="">
                    <?php $i=0; foreach ($students as $key => $value): ?>
                    <?php $i++; ?>
                    <tr>
                        <td><?=$i?></td>
                      <td><img style="max-height: 135px;max-width: 135px;" src="<?=base_url().'assets/uploads/'.$value->profile_photo?>"></td>
                      <td><?=$value->student_name?></td>
                      <td><?=$value->student_code?></td>
                      <td><?=$value->class_id == 1 ? 'الفرقة الاولى' : 'الفرقة الثانية'?></td>
                   
                      <td><?=$value->printed == 1 ? '<i style="font-size:35px;color:green" class="zmdi zmdi-badge-check zmdi-hc-fw"></i>' :  ''  ?></td>
                      <td><a ata-toggle="tooltip" data-placement="top"  style="height:43px;" title="تقديم" href="<?=base_url().'accounts/apply_student_page/'.$value->student_id?>" class="btn btn-primary">تقديم <i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i></a>
                        </td>
                    </tr>
                      
                    <?php endforeach; ?> 
                    <tr></tr>
                  </tbody>
                </table>
                <div class="" style="text-align: center;padding-top: 20px;">
                  
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>