<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الاستقبال </h1>
            <button class="btn btn-primary" onclick="location.href='<?=base_url().'reception/add_new_student'?>'">فتح ملف طالب جديد <i class="zmdi zmdi-file-text zmdi-hc-fw"></i></button>                    
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">قائمة الطلاب</h2>    
              <div class="col-md-12">
                <form class="student_search" action="<?=base_url().'reception'?>" method="GET">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="student_name" class="col-sm-3 control-label">اسم الطالب</label>
                        <div class="col-sm-9">
                            <input id="student_name" type="text" name="student_name" placeholder="اسم الطالب" value="<?=$this->input->get('student_name') != null ? $this->input->get('student_name') : ''?>" class="form-control" >
                        </div>
                    </div>  
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="national_id" class="col-sm-3 control-label">الرقم القومى</label>
                        <div class="col-sm-9">
                            <input id="national_id" type="text" name="national_id" placeholder="الرقم القومى" value="<?=$this->input->get('national_id') != null ? $this->input->get('national_id') : ''?>" class="form-control" >
                        </div>
                    </div>  
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                          </div>
                      </div>
                    </div>
                </form>
                
                
              </div>          
              <div class="card-search">
                <div id="productsTable_wrapper" class="form-group label-floating is-empty">
                  <i class="zmdi zmdi-search search-icon-left"></i>
                  <input type="text" class="form-control filter-input" placeholder="بحث ..." autocomplete="off">
                  <a href="javascript:void(0)" class="close-search" data-card-search="close" data-toggle="tooltip" data-placement="top" title="Close"><i class="zmdi zmdi-close"></i></a>
                </div>
              </div>
              <ul class="card-actions icons left-top">
                <li id="deleteItems" style="display: none;">
                  <span class="label label-info pull-left m-t-5 m-r-10 text-white"></span>
                  <a href="javascript:void(0)" id="confirmDelete" data-toggle="tooltip" data-placement="top" data-original-title="حذف الطالب ">
                    <i class="zmdi zmdi-delete"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="Filter Products">
                    <i class="zmdi zmdi-filter-list"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div class="card-body p-0">
              
              <div class="table-responsive">
                <table id="productsTable" post_data="<?=str_replace('"',"'",json_encode($this->input->get()))?>" class="mdl-data-table product-table m-t-30 TableAjax" page="<?=base_url().'reception/studentAjax'?>" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="col-xs-2">الصورة الشخصية</th>
                      <th class="col-xs-2">اسم الطالب</th>
                      <th class="col-xs-2">القسم</th>
                      <th class="col-xs-2">الفرقة</th>
                      <th class="col-xs-2">اضف بواسطة</th>
                      <th class="col-xs-2">التاريخ</th>
                      <th class="col-xs-2">
                      </th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  </tbody>
                </table>
                <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
                  
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>