<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الاستفسارات </h1>
            <button class="btn btn-primary" id="add_new_enquiry">اضافة  استفسار<i class="zmdi zmdi-file-text zmdi-hc-fw"></i></button>                    
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">قائمة الطلاب</h2>    
              <div class="col-md-12">
                <form class="student_search" action="<?=base_url().'reception/enquiry'?>" method="GET">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="date" class="col-sm-3 control-label">التاريخ</label>
                        <div class="col-sm-9">
                            <input id="date" type="date" name="date" placeholder="التاريخ" value="<?=$this->input->get('date') != null ? $this->input->get('date') : ''?>" class="form-control" >
                        </div>
                    </div>  
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                          </div>
                      </div>
                    </div>
                </form>
                
                
              </div>          
              <div class="card-search">
                <div id="productsTable_wrapper" class="form-group label-floating is-empty">
                  <i class="zmdi zmdi-search search-icon-left"></i>
                  <input type="text" class="form-control filter-input" placeholder="بحث ..." autocomplete="off">
                  <a href="javascript:void(0)" class="close-search" data-card-search="close" data-toggle="tooltip" data-placement="top" title="Close"><i class="zmdi zmdi-close"></i></a>
                </div>
              </div>
              <ul class="card-actions icons left-top">
                <li id="deleteItems" style="display: none;">
                  <span class="label label-info pull-left m-t-5 m-r-10 text-white"></span>
                  <a href="javascript:void(0)" id="confirmDelete" data-toggle="tooltip" data-placement="top" data-original-title="حذف الطالب ">
                    <i class="zmdi zmdi-delete"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-card-search="open" data-toggle="tooltip" data-placement="top" data-original-title="Filter Products">
                    <i class="zmdi zmdi-filter-list"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div class="card-body p-0">
              
              <div class="table-responsive">
                <table id="productsTable" post_data="<?=str_replace('"',"'",json_encode($this->input->get()))?>" class="mdl-data-table product-table m-t-30 TableAjax" page="<?=base_url().'reception/enquiryAjax'?>" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="col-xs-2" style="min-width: 120px;">التاريخ </th>
                      <th class="col-xs-2">اسم الطالب</th>
                      <th class="col-xs-2">المؤهل</th>
                      <th class="col-xs-2">المنطقة</th>
                      <th class="col-xs-2">رقم الهاتف</th>
                      <th class="col-xs-2">الغرض من الزيارة </th>
                      <th class="col-xs-2" style="min-width: 120px;">المكالمة الاولى</th>
                      <th class="col-xs-2" style="min-width: 120px;">المكالمة الثانية</th>
                      <th class="col-xs-2"></th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  </tbody>
                </table>
                <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
                  
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <button class="btn btn-info btn-block hidden enquiry_modal" data-toggle="modal" data-target="#enquiry_modal">Trigger</button>
  <div class="modal fade" id="enquiry_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel-2">إضافة  بيانات الطالب</h4>
                            <ul class="card-actions icons left-top">
                            <li>
                                <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form class="form_ajax" method="POST" result-data="form_result" action="<?=base_url().'reception/insert_enquiry'?>">
                        <div class="modal-body">
	                            <div class="form-group">
	                                <label for="day" class="col-sm-4 control-label">اليوم </label>
	                                <div class="col-sm-8">
	                                    <input id="day" type="text" data-rule-required="true"  data-msg-required="يجب كتابة  اليوم" name="day" placeholder="اكتب  اليوم" value="<?=dayAr(date('D'))?>" class="form-control" >
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="date" class="col-sm-4 control-label">التاريخ </label>
	                                <div class="col-sm-8">
	                                    <input id="date" type="text" data-rule-required="true"  data-msg-required="يجب كتابة  التاريخ" name="date" placeholder="اكتب  التاريخ" value="<?=date('Y-m-d')?>"  class="form-control" >
	                                </div>
	                            </div>
	                        	<div class="form-group">
		                            <label for="name" class="col-sm-4 control-label"> الاسم </label>
	                                <div class="col-sm-8">
	                                    <input id="name" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   الاسم" name="name" placeholder="الاسم" value=""  class="form-control" >
	                                </div>
		                        </div>

	                            <div class="form-group">
	                                <label for="qualification" class="col-sm-4 control-label">المؤهل </label>
	                                <div class="col-sm-8">
	                                    <input id="qualification" type="text" name="qualification" placeholder="المؤهل" value=""  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="grade" class="col-sm-4 control-label">المجموع </label>
	                                <div class="col-sm-8">
	                                    <input id="grade" type="text" name="grade" placeholder="المجموع" value=""  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="graduate_year" class="col-sm-4 control-label">سنة التخرج </label>
	                                <div class="col-sm-8">
	                                    <input id="graduate_year" type="text" name="graduate_year" placeholder="سنة التخرج" value=""  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="school" class="col-sm-4 control-label">المدرسة </label>
	                                <div class="col-sm-8">
	                                    <input id="school" type="text" name="school" placeholder="المدرسة" value=""  class="form-control" >
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="date" class="col-sm-4 control-label">سيلة التعارف </label>
	                                <div class="col-sm-8">
										<select name="acquaintance_way" id="acquaintance_way" class="form-control acquaintanceSelect">
                                            <option>اختر وسيلة التعارف</option>
                                            <?php foreach ($acquaintance as $acquaintance_key => $acquaintance_value): ?>
                                                <option value="<?=$acquaintance_value->acquaintance_id?>"><?=$acquaintance_value->acquaintance_name?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="phone" class="col-sm-4 control-label">رقم الهاتف </label>
	                                <div class="col-sm-8">
	                                    <input id="phone" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   رقم الهاتف   " name="phone" min="11" placeholder="رقم الهاتف" value=""  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group">
	                                <label for="area" class="col-sm-4 control-label">المنطقة </label>
	                                <div class="col-sm-8">
	                                    <input id="area" type="text" name="area" placeholder="المنطقة" value=""  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group col-md-12">
                                    <label for="" class="col-sm-4 control-label" style="padding-right: 0px;">زيارة معهد اخر</label>
                                  	<label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                    <input type="radio" name="another_academy" checked="checked" id="inlineRadio3" value="0"><span class="circle"></span><span class="check"></span> نعم
                                  	</label>
                                  	<label class="radio-inline control-label col-md-2 col-sm-6" style="padding-top: 8px !important;">
                                    <input type="radio" name="another_academy"  id="inlineRadio4" value="1"><span class="circle"></span><span class="check"></span> لا
                                  </label>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="" class="col-sm-4 control-label" style="padding-right: 0px;">الغرض من الزيارة</label>
                                  	<label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
                                    <input type="radio" name="reasone" checked="checked" id="inlineRadio3" value="0"><span class="circle"></span><span class="check"></span> استفسار
                                  	</label>
                                  	<label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
                                    <input type="radio" name="reasone"  id="inlineRadio4" value="1"><span class="circle"></span><span class="check"></span> استفسار بورق
                                  </label>
								
									<label for="" class="col-sm-4 control-label" style="padding-right: 0px;"></label>

                                  <label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
                                    <input type="radio" name="reasone"  id="inlineRadio4" value="2"><span class="circle"></span><span class="check"></span> تقديم
                                  </label>
                                  <label class="radio-inline control-label col-md-4 col-sm-6" style="padding-top: 8px !important;">
                                    <input type="radio" name="reasone"  id="inlineRadio4" value="3"><span class="circle"></span><span class="check"></span> تقديم بورق
                                  </label>
                                </div>


                        </div>
                        <div class="form_result col-md-12"></div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="margin-top: 30px;background: whitesmoke;">
                            <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                    </div>
                    <!-- modal-content -->
                </div>
                <!-- modal-dialog -->
            </div>

<button class="btn btn-info btn-block hidden edit_enquiryModal" data-toggle="modal" data-target="#edit_enquiryModal">Trigger</button>
  <div class="modal fade" id="edit_enquiryModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel-2">تعديل   الاستفسار</h4>
                            <ul class="card-actions icons left-top">
                            <li>
                                <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="edit_enquiry_result"></div>
                    </div>
                    <!-- modal-content -->
                </div>
                <!-- modal-dialog -->
            </div>