<aside id="app_sidebar-left">
      <div id="logo_wrapper">
        <ul>
          <li class="logo-icon">
            <a href="<?=base_url()?>">
              <div class="logo">
                <img src="<?=base_url()?>assets/img/logo/logo-icon.png" alt="Logo">
              </div>
              <h1 class="brand-text">أكاديمية 6 اكتوبر</h1>
            </a>
          </li>
          
        </ul>
      </div>
      <nav id="app_main-menu-wrapper" class="scrollbar">
        <div class="sidebar-inner sidebar-push">
          <ul class="nav nav-pills nav-stacked">
            <li class="sidebar-header">NAVIGATION</li>
            

            <?php if(in_array('reception',json_decode($this->userData->permission))){ ?>
            <li class="nav-dropdown <?=$this->uri->segment(1) == 'reception' ? 'active open': ''?>"><a href="#"><i class="zmdi zmdi-receipt zmdi-hc-fw"></i> الاستقبال </a>
              <ul class="nav-sub">

                <li class="<?=$this->uri->segment(1) == 'reception' && $this->uri->segment(2) == 'enquiry' ? 'active': ''?>"><a href="<?=base_url().'reception/enquiry'?>">الاستفسار</a></li>

                <li class="<?=$this->uri->segment(1) == 'reception' && $this->uri->segment(2) == 'add_new_student' ? 'active': ''?>"><a href="<?=base_url().'reception/add_new_student'?>">فتح ملف </a></li>
                <li class="<?=$this->uri->segment(1) == 'reception' && $this->uri->segment(2) == '' ? 'active': ''?>"><a href="<?=base_url().'reception'?>">الطلاب</a></li>
              </ul>
            </li>
            
            <?php } if(in_array('accounts',json_decode($this->userData->permission))){ ?>
            


            <li class="nav-dropdown <?=$this->uri->segment(1) == 'accounts' ? 'active open': ''?>"><a href="#"><i class="zmdi zmdi-dialpad zmdi-hc-fw"></i> الحسابات </a>
              <ul class="nav-sub">

                <li class="<?=$this->uri->segment(1) == 'accounts' && $this->uri->segment(2) == 'reception_student' ? 'active': ''?>"><a href="<?=base_url().'accounts/reception_student'?>">فتح الملف  </a></li>

                <li class=" <?=$this->uri->segment(1) == 'accounts' && $this->uri->segment(2) == 'apply' ? 'active': ''?> "><a href="<?=base_url().'accounts/apply'?>"> التقديم </a></li>
            

                <li class="nav-dropdown  <?php if($this->uri->segment(2) == 'income' OR $this->uri->segment(2) == 'pay_tuition_fees' OR $this->uri->segment(2) =='add_income' OR $this->uri->segment(2) == 'add_new_income' OR $this->uri->segment(2) == 'income_invoices' OR $this->uri->segment(2) == 'income_day_invoices'  OR $this->uri->segment(2) == 'student_income' OR $this->uri->segment(2) == 'student_tuition_fees' ){echo 'open';}else{echo '';}?>" >
                    <a href="#" title="Level 2.3">
                      <i class="fa fa-fw fa-folder-open"></i> الايرادات
                    </a>
                    <ul class="nav-sub" style="    border-bottom: 1px solid rgba(253, 69, 130, 0.2)">

                      <li class=""><a
                        <?=$this->uri->segment(1) == 'accounts' && $this->uri->segment(2) == 'income' ? 'style="color:#FD4582;"': ''?>
                        href="<?=base_url().'accounts/income'?>"><i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> اضافة بند  </a>
                      </li>

                      <li class=""><a 
                        <?=$this->uri->segment(1) == 'accounts' && $this->uri->segment(2) == 'add_income' ? 'style="color:#FD4582;"': ''?>
                        href="<?=base_url().'accounts/add_income'?>"><i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> الايرادات </a>
                      </li>

                      <!-- <li class=""><a 
                        <?=$this->uri->segment(1) == 'accounts' && $this->uri->segment(2) == 'pay_tuition_fees' ? 'style="color:#FD4582;"': ''?>
                        href="<?=base_url().'accounts/pay_tuition_fees'?>"><i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> دفع المصروفات </a>
                      </li>
 -->
                      <li class=""><a <?=$this->uri->segment(1) == 'accounts' && ($this->uri->segment(2) == 'income_invoices' OR $this->uri->segment(2) == 'income_day_invoices') ? 'style="color:#FD4582;"': ''?>
                        href="<?=base_url().'accounts/income_invoices/'?>"><i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> اليومية </a></li>


                      <li class=""><a <?=$this->uri->segment(1) == 'accounts' && ($this->uri->segment(2) == 'student_income') ? 'style="color:#FD4582;"': ''?>
                        href="<?=base_url().'accounts/student_income/'?>"><i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> المصروفات الاضافية</a></li>

                      <li class=""><a <?=$this->uri->segment(1) == 'accounts' && ($this->uri->segment(2) == 'student_tuition_fees') ? 'style="color:#FD4582;"': ''?>
                        href="<?=base_url().'accounts/student_tuition_fees/'?>"><i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> المصروفات الدراسية</a></li>
                    </ul>
                  </li>




                <li class="nav-dropdown  <?php if($this->uri->segment(2) == 'expenses' OR $this->uri->segment(2) =='add_expense'){echo 'open';}else{echo '';}?>" >
                    <a href="#" title="Level 2.3">
                      <i class="fa fa-fw fa-folder-open"></i> المصروفات
                    </a>
                    <ul class="nav-sub" style="    border-bottom: 1px solid rgba(253, 69, 130, 0.2)">
                      <li>
                        <a   <?=$this->uri->segment(3) == 'normal' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'accounts/expenses/normal'?>" title="ايصال عادى">
                          <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> ايصال عادى 
                        </a>
                      </li>

                      <li>
                        <a <?=$this->uri->segment(3) == 'honesty' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'accounts/expenses/honesty'?>" title="ايصال امانة">
                          <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i> ايصال امانة
                        </a>
                      </li>

                    </ul>
                  </li>

               <!--  <li><a href="<?=base_url().'reception/print_cards'?>">طباعة الكارنيها</a></li> -->
               <li><a <?=$this->uri->segment(2) == 'tuition_fees_setting' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'accounts/tuition_fees_setting'?>"> اعدادات المصروفات</a></li>
              </ul>

            </li>

             <?php } if(in_array('security',json_decode($this->userData->permission))){ ?>
              <li class="nav-dropdown <?=$this->uri->segment(1) == 'security' ? 'active open': ''?>"><a href="#"><i class="zmdi zmdi-videocam zmdi-hc-fw"></i> الامن </a>
                <ul class="nav-sub">
                  <li><a <?=$this->uri->segment(2) == 'entry_machine' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'security/entry_machine'?>">مكنة الدخول</a></li>
                  <li><a <?=$this->uri->segment(2) == 'visit' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'security/visit'?>">الزيارات </a></li>
                </ul>
              </li>

            <?php } if(in_array('students_affairs',json_decode($this->userData->permission))){ ?>

            <li class="nav-dropdown <?=$this->uri->segment(1) == 'students_affairs' ? 'active open': ''?>"><a href="#"><i class="zmdi zmdi-account-box-mail zmdi-hc-fw"></i> شئون الطلاب </a>
              <ul class="nav-sub">
                <li><a href="<?=base_url().'students_affairs'?>">الطلاب</a></li>
                <li><a href="<?=base_url().'students_affairs/graduates'?>">الطلاب الخريجين</a></li>
              </ul>
            </li>

            <?php } if(in_array('statistics',json_decode($this->userData->permission))){ ?>

            <li class="nav-dropdown <?=$this->uri->segment(1) == 'statistics' ? 'active open': ''?>"><a href="#"><i class="zmdi zmdi-chart zmdi-hc-fw"></i> الاحصائيات </a>
              <ul class="nav-sub">
                <li><a <?=$this->uri->segment(2) == 'acquaintance' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'statistics/acquaintance'?>">احصائية وسيلة التعارف</a></li>
                <li><a <?=$this->uri->segment(2) == 'statistics_citys' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'statistics/statistics_citys'?>">احصائيات المناطق</a></li>
                <li><a <?=$this->uri->segment(2) == 'statistics_administration' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'statistics/statistics_administration'?>">االادارات التعليمية </a></li>
                <li><a <?=$this->uri->segment(2) == 'statistics_schools' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'statistics/statistics_schools'?>">المدارس  </a></li>
                <li><a <?=$this->uri->segment(2) == 'section_statistics' ? 'style="color:#FD4582;"': ''?> href="<?=base_url().'statistics/section_statistics'?>">الاقسام  </a></li>
              </ul>
            </li>

              <?php } if(in_array('members',json_decode($this->userData->permission))){ ?>

            <li class=" <?=$this->uri->segment(1) == 'members' ? 'active': ''?> "><a href="<?=base_url().'members'?>"><i class="zmdi zmdi-accounts-add zmdi-hc-fw"></i> المستخدمين </a></li>

           <!--  <li><a href="javascript:void(0)" data-trigger="sidebar-overlay-open"><i class="zmdi zmdi-invert-colors"></i>Theme Settings</a></li> -->


                <?php //var_dump($this->userData->permission); die(); ?>
              <?php } if(in_array('call_center',json_decode($this->userData->permission))){ ?>

              <li class=" <?=$this->uri->segment(2) == 'callCenter' ? 'active': ''?> ">
                  <a href="<?=base_url().'callCenter'?>"><i class="zmdi zmdi-accounts-add zmdi-hc-fw"></i> Call Center </a></li>
              <?php } ?>


            </ul>
          </div>
        </nav>
      </aside>


      <section id="content_outer_wrapper" class="">
      