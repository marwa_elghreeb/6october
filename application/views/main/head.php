<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/logo/logo-icon.png"/>
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/logo/logo-icon.png"/>
	<title>اكاديمية 6 اكتوبر</title>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/vendor.bundle.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/app.bundle.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/<?=$this->userData->theme_setting != '' ? $this->userData->theme_setting : 'theme-g.css'?>">
	<link rel="stylesheet" href="<?=base_url()?>assets/rtl/bootstrap-rtl.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css">
		<!--link rel="stylesheet" href="<?=base_url()?>assets/css/export_style.css"-->

	<!-- SWEET ALERT REQUIRE-->
	<link href="<?=base_url()?>assets/vendor/sweet-alert.css" rel="stylesheet">
</head>

<body class="rtl">
	<div id="app_wrapper">