<!DOCTYPE html>
<html>
<head>
		<meta name="viewport" content="width=device-width, minimum-scale=0.1">
		<link rel="stylesheet" href="<?=base_url()?>assets/css/vendor.bundle.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/css/app.bundle.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/rtl/bootstrap-rtl.css">
		<title><?=$image?></title>
</head>
<body style="margin: 0px;text-align: center; background: #0e0e0e;">
	<div class="col-md-12">
		<a href="<?=base_url().'home/downloadimage/'.$image.'/'.$this->uri->segment(4)?>" class="btn btn-primary" target="_blank">تحميل <i class="zmdi zmdi-download"></i></a>
	</div>
	<img style="-webkit-user-select: none;background-position: 0px 0px, 10px 10px;background-size: 20px 20px;background-image:linear-gradient(45deg, #eee 25%, transparent 25%, transparent 75%, #eee 75%, #eee 100%),linear-gradient(45deg, #eee 25%, white 25%, white 75%, #eee 75%, #eee 100%);" 
	src="<?=base_url().'assets/uploads/'.$image?>">
</body>
	<footer>
		<script src="<?=base_url()?>assets/js/vendor.bundle.js"></script>
    	<script src="<?=base_url()?>assets/js/app.bundle.js"></script>
		<script src="<?=base_url()?>assets/js/responsivejs.js"></script>
	</footer>
</html>