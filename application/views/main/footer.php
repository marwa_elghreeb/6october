<footer id="footer_wrapper">
    <div class="footer-content">
            <div class="row copy-wrapper col-md-12">
                <div class="col-xs-8">
                    <p class="copy">&copy; Copyright 6 October Academy - All Rights Reserved</p>
                </div>
                <div class="col-xs-4">
                    <p class="copy">Develop By <a href="http://epicapps.net" style="color: #ed5547">Epic Apps <img src="http://epicapps.net/images/elogo.png" style="width: 18px"></a></p>
                </div>
            </div>
        </div>
    </footer>
</section>
<aside id="app_sidebar-right">
    <div class="sidebar-inner sidebar-overlay">
        <div class="tabpanel">
            <ul class="nav nav-tabs nav-justified">
                <li class="active" role="presentation"><a href="#sidebar_chat" data-toggle="tab" aria-expanded="true">Chat</a></li>
                <li role="presentation"><a href="#sidebar_activity" data-toggle="tab">Activity</a></li>
                <li role="presentation"><a href="#sidebar_settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="sidebar_chat">
                    <form class="m-l-15 m-r-15 m-t-30">
                        <div class="input-group search-target">
                            <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                            <div class="form-group is-empty">
                                <input type="text" value="" placeholder="بحث" class="form-control" data-search-trigger="open">
                            </div>
                        </div>
                    </form>
                    <ul class="description">
                        <li class="title">
                            Online
                        </li>
                    </ul>
                    <ul class="list-group p-0">
                        <li class="list-group-item" data-chat="open" data-chat-name="John Smith">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/07.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini success status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">John Smith</div>
                                <div class="list-group-item-text">New York, NY</div>
                            </div>
                        </li>
                        <li class="list-group-item" data-chat="open" data-chat-name="Allison Grayce">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/05.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini success status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Allison Grayce</div>
                                <div class="list-group-item-text">Seattle, WA</div>
                            </div>
                        </li>
                        <li class="list-group-item" data-chat="open" data-chat-name="Ashley Ford">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/18.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini success status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Ashley Ford</div>
                                <div class="list-group-item-text">Denver, CO</div>
                            </div>
                        </li>
                        <li class="list-group-item" data-chat="open" data-chat-name="Johanna Kollmann">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/11.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini success status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Johanna Kollmann </div>
                                <div class="list-group-item-text">Palo Alto, Ca</div>
                            </div>
                        </li>
                    </ul>
                    <ul class="description">
                        <li class="title">
                            Busy
                        </li>
                    </ul>
                    <ul class="list-group p-0">
                        <li class="list-group-item" data-chat="open" data-chat-name="Mike Jones">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/03.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini warning status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Mike Jones </div>
                                <div class="list-group-item-text">San Francisco, CA</div>
                            </div>
                        </li>
                        <li class="list-group-item" data-chat="open" data-chat-name="Nikki Clark">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/06.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini warning status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Nikki Clark </div>
                                <div class="list-group-item-text">Sarasota, FL</div>
                            </div>
                        </li>
                        <li class="list-group-item" data-chat="open" data-chat-name="Jason Kendall">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/15.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini warning status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Jason Kendall </div>
                                <div class="list-group-item-text">New York, NY</div>
                            </div>
                        </li>
                    </ul>
                    <ul class="description">
                        <li class="title">
                            Offline
                        </li>
                    </ul>
                    <ul class="list-group p-0">
                        <li class="list-group-item" data-chat="open" data-chat-name="Josh Hemsley">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/16.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini danger status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Josh Hemsley</div>
                                <div class="list-group-item-text">Salem, MA</div>
                            </div>
                        </li>
                        <li class="list-group-item" data-chat="open" data-chat-name="James Hart">
                            <span class="pull-right"><img src="<?=base_url()?>assets/img/profiles/09.jpg" alt="" class="img-circle max-w-40 m-r-10 "></span>
                            <i class="badge mini danger status"></i>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">James Hart</div>
                                <div class="list-group-item-text">Salem, MA</div>
                            </div>
                        </li>
                    </ul>
                    
                </div>
                <div class="tab-pane fade" id="sidebar_activity">
                    <div class="sidebar-timeline">
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">15 minutes ago</small>
                                <p><a href="#" class="accent">Mike Jones</a> fixed z-index conflict sidebar.scss</p>
                            </div>
                        </div>
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">30 minutes ago</small>
                                <p><a href="javascript:void(0)" class="accent">Hazel    Dean</a> right a comment on product page designs.</p>
                                <p><em>"Yuccie shoreditch trust fund, artisan tumblr sustainable cronut unicorn blog seitan. "</em></p>
                            </div>
                        </div>
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">45 minutes ago</small>
                                <p><a href="javascript:void(0)" class="accent">Molly</a> requested time off for training.</p>
                                <p><em>"Snackwave church-key cardigan you probably haven't heard of them, asymmetrical microdosing cronut "</em></p>
                            </div>
                        </div>
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">3 hours ago</small>
                                <p><a href="javascript:void(0)" class="accent">Frederick    Roy</a> commented your post.</p>
                                <p><em>"Skateboard dreamcatcher la croix, edison bulb sustainable sriracha vexillologist kombucha master cleanse."</em></p>
                            </div>
                        </div>
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">1 hour ago</small>
                                <p><a href="javascript:void(0)" class="accent">Holly Cobb</a> Uploaded 6 new photos.</p>
                            </div>
                        </div>
                        <div class="time-item">
                            <div class="item-info">
                                <small class="text-muted">5 hours ago</small>
                                <p><a href="javascript:void(0)" class="accent">Neal Stephens</a> setup a meeting with<a href="#" class="text-success"> Jason Kendall</a>.</p>
                                <p><em>"Authentic aesthetic tattooed, PBR&B squid tote bag schlitz vaporware glossier yr man braid direct trade disrupt poke.  "</em></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="sidebar_settings">
                    <div class="color-container">
                        <h3 class="title">Preset Color Options</h3>
                        <div class="row">
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-a.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-a"></span>
                                            <span class="color bg-shade-theme-a"></span>
                                        </span>
                                        <span class="color bg-menu-darkBlue"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-b.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-b"></span>
                                            <span class="color bg-shade-theme-b"></span>
                                        </span>
                                        <span class="color  bg-menu-darkBlue"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-c.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-c"></span>
                                            <span class="color bg-shade-theme-c"></span>
                                        </span>
                                        <span class="color  bg-menu-darkBlue"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-d.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-d"></span>
                                            <span class="color bg-shade-theme-d"></span>
                                        </span>
                                        <span class="color  bg-menu-darkBlue"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-20">
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-e.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme" checked="checked">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-e"></span>
                                            <span class="color bg-shade-theme-e"></span>
                                        </span>
                                        <span class="color bg-menu-white"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-f.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-f"></span>
                                            <span class="color bg-shade-theme-f"></span>
                                        </span>
                                        <span class="color  bg-menu-white"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-g.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-g"></span>
                                            <span class="color bg-shade-theme-g"></span>
                                        </span>
                                        <span class="color  bg-menu-white"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3 p-0">
                                <div class="color-option-check">
                                    <label data-load-css="<?=base_url()?>assets/css/theme-h.css" id="theme-color" data-url="<?=base_url().'home/theme_setting'?>">
                                        <input type="radio" name="setting-theme">
                                        <span class="icon-check"></span>
                                        <span class="split">
                                            <span class="color bg-primary-theme-h"></span>
                                            <span class="color bg-shade-theme-h"></span>
                                        </span>
                                        <span class="color  bg-menu-white"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- <h3 class="title m-t-30">Layout Mode</h3>
                        <ul class="description">
                            <li>
                                <div class="radio block"><label><input type="radio" name="layoutMode" value="" checked>FULL WIDTH</label></div>
                            </li>
                            <li>
                                <div class="radio block"><label><input type="radio" name="layoutMode" value="boxed-layout">BOXED</label></div>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>
<button class="btn btn-info waves-effect" id="sa-warning" style="display:none;"></button>
<section id="chat_compose_wrapper">
    <div class="tippy-top">
        <div class="recipient">Allison Grayce</div>
        <ul class="card-actions icons  left-top">
            <li>
                <a href="javascript:void(0)">
                    <i class="zmdi zmdi-videocam"></i>
                </a>
            </li>
            <li class="dropdown">
                <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu btn-primary dropdown-menu-left">
                    <li>
                        <a href="javascript:void(0)">Option One</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Option Two</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">Option Three</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:void(0)" data-chat="close">
                    <i class="zmdi zmdi-close"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class='chat-wrapper scrollbar'>
        <div class='chat-message scrollbar'>
            <div class='chat-message chat-message-recipient'>
                <img class='chat-image chat-image-default' src='<?=base_url()?>assets/img/profiles/05.jpg' />
                <div class='chat-message-wrapper'>
                    <div class='chat-message-content'>
                        <p>Hey Mike, we have funding for our new project!</p>
                    </div>
                    <div class='chat-details'>
                        <span class='today small'></span>
                    </div>
                </div>
            </div>
            <div class='chat-message chat-message-sender'>
                <img class='chat-image chat-image-default' src='<?=base_url()?>assets/img/profiles/02.jpg' />
                <div class='chat-message-wrapper '>
                    <div class='chat-message-content'>
                        <p>Awesome! Photo booth banh mi pitchfork kickstarter whatever, prism godard ethical 90's cray selvage.</p>
                    </div>
                    <div class='chat-details'>
                        <span class='today small'></span>
                    </div>
                </div>
            </div>
            <div class='chat-message chat-message-recipient'>
                <img class='chat-image chat-image-default' src='<?=base_url()?>assets/img/profiles/05.jpg' />
                <div class='chat-message-wrapper'>
                    <div class='chat-message-content'>
                        <p> Artisan glossier vaporware meditation paleo humblebrag forage small batch.</p>
                    </div>
                    <div class='chat-details'>
                        <span class='today small'></span>
                    </div>
                </div>
            </div>
            <div class='chat-message chat-message-sender'>
                <img class='chat-image chat-image-default' src='<?=base_url()?>assets/img/profiles/02.jpg' />
                <div class='chat-message-wrapper'>
                    <div class='chat-message-content'>
                        <p>Bushwick letterpress vegan craft beer dreamcatcher kickstarter.</p>
                    </div>
                    <div class='chat-details'>
                        <span class='today small'></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="compose-footer">
        <form class="form-horizontal compose-form">
            <ul class="card-actions icons right-bottom">
                <li>
                    <a href="javascript:void(0)">
                        <i class="zmdi zmdi-attachment-alt"></i>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="zmdi zmdi-mood"></i>
                    </a>
                </li>
            </ul>
            <div class="form-group m-10 p-l-75 is-empty">
                <div class="input-group">
                    <label class="sr-only">Leave a comment...</label>
                    <input type="text" class="form-control form-rounded input-lightGray" placeholder="Leave a comment..">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-blue btn-fab  btn-fab-sm">
                            <i class="zmdi zmdi-mail-send"></i>
                        </button>
                    </span>
                </div>
            </div>
        </form>
    </footer>
</section>

</div>
<div class="modal fade" id="schedule_modal" tabindex="-1" role="dialog" aria-labelledby="schedule_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel-2">Title goes here</h4>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in ligula id sem tristique ultrices eget id neque. Duis enim turpis, tempus at accumsan vitae, lobortis id sapien. Pellentesque nec orci mi, in pharetra ligula. Nulla facilisi. Nulla
                    facilisi. Mauris convallis venenatis massa, quis consectetur felis ornare quis. Sed aliquet nunc ac ante molestie ultricies. Nam pulvinar ultricies bibendum.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success">Ok</button>
                </div>
            </div>
            <!-- modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
    
    <a href="javascript:void(0)" class="btn btn-warning btn-sm hidden alertSweet sweet-warning"></a>
    <script src="<?=base_url()?>assets/js/vendor.bundle.js"></script>
    <script src="<?=base_url()?>assets/js/app.bundle.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.validate.js"></script>
    <script src="<?=base_url()?>assets/vendor/sweet-alert.min.js"></script>
    <script src="<?=base_url()?>assets/js/ajax.js"></script>
    <script src="<?=base_url()?>assets/js/responsivejs.js"></script>
    <script src="<?=base_url()?>assets/js/ajax.form.js"></script>
    <script src="<?=base_url()?>assets/js/printer.js" type="text/javascript"></script>
        
    <?php
    if(isset($script)){
        if(gettype($script)=='array'){
            foreach ($script as $page) {
                $this->load->view($page);
            }
        }else{
            $this->load->view($script);
        }
    }
    ?>
    
    <script type="text/javascript">
            /*
             * Notifications
             */

            function notify(from, align, icon, type, animIn, animOut){
                $.growl({
                    icon: icon,
                    title: ' Bootstrap Growl ',
                    message: 'Turning standard Bootstrap alerts into awesome notifications',
                    url: ''
                },{
                        element: 'body',
                        type: type,
                        allow_dismiss: true,
                        placement: {
                                from: from,
                                align: align
                        },
                        offset: {
                            x: 20,
                            y: 85
                        },
                        spacing: 10,
                        z_index: 1031,
                        delay: 2500,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: false,
                        animate: {
                                enter: animIn,
                                exit: animOut
                        },
                        icon_type: 'class',
                        template: '<div data-growl="container" class="alert" role="alert">' +
                                        '<button type="button" class="close" data-growl="dismiss">' +
                                            '<span aria-hidden="true">&times;</span>' +
                                            '<span class="sr-only">Close</span>' +
                                        '</button>' +
                                        '<span data-growl="icon"></span>' +
                                        '<span data-growl="title"></span>' +
                                        '<span data-growl="message"></span>' +
                                        '<a href="#" data-growl="url"></a>' +
                                    '</div>'
                });
            };
            
            $('.notifications-success > div > .btn').click(function(e){
                e.preventDefault();
                var nFrom = $(this).attr('data-from');
                var nAlign = $(this).attr('data-align');
                var nIcons = $(this).attr('data-icon');
                var nType = $(this).attr('data-type');
                var nAnimIn = $(this).attr('data-animation-in');
                var nAnimOut = $(this).attr('data-animation-out');
                
                notify(nFrom, nAlign, nIcons, nType, nAnimIn, nAnimOut);
            });


            /*
             * Dialogs
             */

            //Basic
            $('#sa-basic').click(function(){
                swal("Here's a message!");
            });

            //A title with a text under
            $('#sa-title').click(function(){
                swal("Here's a message!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis")
            });

            //Success Message
            $('#sa-success').click(function(){
                swal("Good job!", "", "success")
            
                /*setTimeout(function(){ location.href='http://localhost/as/demo.php'; }, 2000);*/
            });

            //Warning Message
            $('#sa-warning').click(function(){
                swal({   
                    title: "هل تريد الحذف ؟",   
                    text: "اذا تم الحذف لا يمكن استرجاع البيانات",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "نعم !",   
                    cancelButtonText: "لا",   
                    closeOnConfirm: false 
                }, function(){   
                    swal("تم الحذف!", "", "success"); 
                });
            });
            
            //Parameter
            $('#sa-params').click(function(){
                swal({   
                    title: "Are you sure?",   
                    text: "You will not be able to recover this imaginary file!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes, delete it!",   
                    cancelButtonText: "No, cancel plx!",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");   
                    } else {     
                        swal("Cancelled", "Your imaginary file is safe :)", "error");   
                    } 
                });
            });

            //Custom Image
            $('#sa-image').click(function(){
                swal({   
                    title: "Sweet!",   
                    text: "Here's a custom image.",   
                    imageUrl: "img/thumbs-up.png" 
                });
            });

            //Auto Close Timer
            $('#sa-close').click(function(){
                 swal({   
                    title: "Auto close alert!",   
                    text: "I will close in 2 seconds.",   
                    timer: 2000,   
                    showConfirmButton: false 
                });
            });

        </script>

    <script type="text/javascript">
        $(document).ready(function(){
            if($(window).width() <= 1280){
                $('#content_outer_wrapper').css({
                    'padding-right' : '0px',
                })
            }
        });
    </script>
     <script type="text/javascript">

        $(document).ready(function(){
            $('#openModal').click();
        });
        $('body').on('click','#print',function(){
            setTimeout(function(){
                window.close();
            }, 1000);
        });

     </script>
     <div class="alertify hidden"><div class="dialog"><div><nav id="alertNav" style="text-align: center;"></nav></div></div></div>
     <div class="alertify-logs"></div>
</body>

</html>
