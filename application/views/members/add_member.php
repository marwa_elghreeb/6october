    <div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <h1> أضافة مستخدم جديد</h1>
              </header>
            </div>
          </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div class="card p-b-20">
                  <header class="card-heading ">
                    <h2 class="card-title" style="display: inline-block;"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> أضافة مستخدم</h2>
                      <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'members/'?>"> رجوع <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>     

                  </header>
                  <div class="card-body">
                    <form class="form-horizontal insert_data" result-data="form_result" method="POST" action="<?=base_url().'members/insert_member'?>">

                      <div class="form-group is-empty">
                        <label for="real_name" class="col-md-2 control-label">الاسم</label>
                        <div class="col-md-10">
                          <input type="text" name="real_name" class="form-control" id="real_name" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="الاسم بالكامل">
                        </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="username" class="col-md-2 control-label">اسم المستخدم</label>
                        <div class="col-md-10">
                          <input type="text" name="username" pattern="[a-zA-Z0-9-]+" class="form-control" id="username" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="اسم المستخدم / username">
                        </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="password" class="col-md-2 control-label">كلمة المرور</label>
                        <div class="col-md-10">
                          <input type="text" name="password" class="form-control" id="password" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="كلمة المرور">
                        </div>
                      </div>

                      <div class="form-group is-empty">
                        <label for="conf_password" class="col-md-2 control-label">تاكيد كلمة المرور</label>
                        <div class="col-md-10">
                          <input type="text" name="conf_password" class="form-control" id="conf_password" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="تاكيد كلمة المرور">
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-12">
                          <label for="" class="control-label"><i class="zmdi zmdi-lock zmdi-hc-fw"></i> الصلاحيات </label>
                        </div>
                      </div>
                        <?php foreach($controllers as $permission_value): ?>
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-12">
                            <label style="text-align: right;" class="checkbox-inline control-label col-md-6 col-sm-6 studnet_cuntry" style="padding-top: 8px !important;">
                              <input type="checkbox" name="permission[]" class="studnet_cuntry" id="inlinecheckbox3" value="<?=$permission_value['name']?>"><span class="circle"></span><span class="check"></span> <?=$permission_value['ar']?>
                            </label>
                        </div>
                      </div>
                        <?php endforeach; ?>
                      
                      <div class="form-group">
                        <div class="form_result"></div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary"> اضافة <i class="zmdi zmdi-save zmdi-hc-fw"></i></button>
                          </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    