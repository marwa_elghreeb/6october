<div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <header id="header">
                        <h1> أضافة  متابعه جديده</h1>
                    </header>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="card p-b-20">
                        <header class="card-heading ">
                            <h2 class="card-title" style="display: inline-block;"><i class="zmdi zmdi-plus zmdi-hc-fw"></i> أضافة متابعه</h2>
                            <a class="btn btn-default search-student-btn" style="float: left;margin: 0px;" href="<?=base_url().'CallCenter/'?>"> رجوع <i class="zmdi zmdi-arrow-left zmdi-hc-fw"></i></a>

                        </header>
                        <div class="card-body">
                            <form class="form-horizontal insert_data" result-data="form_result" method="POST" action="<?=base_url().'CallCenter/add_call'?>">

                                <div class="form-group is-empty">
                                    <label for="real_name" class="col-md-2 control-label">الاسم</label>
                                    <div class="col-md-10">
                                        <input type="text" name="name" class="form-control" id="name" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="الاسم بالكامل">
                                    </div>
                                </div>

                                <div class="form-group is-empty">
                                    <label for="username" class="col-md-2 control-label">رقم الهاتف </label>
                                    <div class="col-md-10">
                                        <input type="text" name="phone"  class="form-control" id="phone" data-rule-required="true" data-msg-required="هذا الحقل الزامى" placeholder="رقم الهاتف">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="birth_place_state" class="col-sm-2 control-label">المحافظة</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="birth_place_governorate" value="" placeholder="المحافظة" class="form-control hidden birth_place_governorate_input" disabled="disabled">
                                        <select name="state" id="state" data-url="<?=base_url('home/changeSelect')?>" data-table="city" data-resultArea="birth_place" data-tr="name" data-get-id="city_id" data-get-name="city_name" data-column="governorate_id" class="form-control changeAjax birth_place_governorate">
                                            <option>اختر المحافظة</option>
                                            <?php foreach ($governorate as $key => $value): ?>
                                                <option value="<?=$value->governorate_id ?>"> <?=$value->governorate_name ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="birth_place" class="col-sm-2 control-label"> المدينه</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="city" value="" placeholder=" المدينه" class="form-control hidden birth_place_input" disabled="disabled">
                                        <select name="city" id="city" class="form-control birth_place">
                                            <option>اختر  المدينه</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="qualification" class="col-sm-2 control-label">المؤهل الدراسى</label>
                                    <div class="col-sm-10">
                                        <select name="qualification" id="qualification" class="form-control">
                                            <option>اختر المؤهل الدراسى</option>
                                            <?php $i=0; $z=count($qualification); foreach ($qualification as $key => $value){ $i++;?>
                                                <option data-choose="<?=$value->choosen?>" value="<?=$value->qualification_id ?>"> <?=$value->qualification ?></option>
                                            <?php }?>
                                            <option data-choose="1" value="last"> اخرى </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group qualification_text_area hidden">
                                    <label for="qualification_text" class="col-sm-2 control-label">المؤهل الدراسى</label>
                                    <div class="col-sm-10">
                                        <input id="qualification_text" data-rule-required="true"  data-msg-required="يجب كتابة المؤهل الدراسى" type="text" name="qualification" disabled="disabled" placeholder="اكتب المؤهل الدراسى"  class="form-control" >
                                    </div>
                                </div>



                                <div class="form-group is-empty">
                                    <label for="real_name" class="col-md-2 control-label">المتابعه</label>
                                    <div class="col-md-10">
                                        <textarea type="text" name="real_name" class="form-control" id="real_name"
                                               data-rule-required="true" data-msg-required="هذا الحقل الزامى"
                                               placeholder="الاسم بالكامل">
                                        </textarea>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="form_result"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary"> اضافة <i class="zmdi zmdi-save zmdi-hc-fw"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
