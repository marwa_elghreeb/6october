<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <title>اكاديمية 6 اكتوبر</title>
  <link rel="stylesheet" href="<?=base_url()?>assets/css/vendor.bundle.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/app.bundle.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/theme-a.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/rtl/bootstrap-rtl.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/custom.css">
</head>
<body class="rtl" id="auth_wrapper">
  <div id="login_wrapper">
    <div class="logo">
      <img src="<?=base_url()?>assets/img/logo/logo-icon%402x.png" alt="logo" class="logo-img">
    </div>
    <div id="login_content">
      <h1 class="login-title">
        تسجيل الدخول الى الحساب
      </h1>
      <div class="login-body">
        <form class="form_in" result-data="form_result" action="<?=base_url()?>auth/login">
          <div class="form-group label-floating is-empty">
            <label class="control-label">اسم المستخدم</label>
            <input type="text" name="username" data-rule-required="true" data-msg-required="يجب كتابة اسم المستخدم" class="form-control">
          </div>
          <div class="form-group label-floating is-empty">
            <label class="control-label">كلمة المرور</label>
            <input type="password" name="password" data-rule-required="true" data-msg-required="يجب  كتابة كلمة المرور" class="form-control">
          </div>
          <div class="checkbox inline-block">
            <label class="login-label">
              <input type="checkbox" name="remember" class="checkbox-inline" value="1">
              تذكرنى
            </label>
          </div>
          <div class="form-control form_result"></div>
          <button class="btn btn-info btn-block m-t-40">تسجيل الدخول</button>
        </form>
      </div>
    </div>
  </div>
  <script src="<?=base_url()?>assets/js/vendor.bundle.js"></script>
  <script src="<?=base_url()?>assets/js/app.bundle.js"></script>
  <script src="<?=base_url()?>assets/js/ajax.js"></script>
  <script src="<?=base_url()?>assets/js/jquery.validate.js"></script>
</body>
</html>
