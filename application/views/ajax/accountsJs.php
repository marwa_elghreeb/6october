<script>


// CLOSE MODAL WITH REFRESH PAGE
$('body').on('click','.close_bill_modal',function(){
	location.reload();
});
// PRINTED CHECKBOX

$('body').on('click','#card_printed',function(){

	if($(this).is(":checked")){
		url = $(this).attr('url');
		val = $(this).val();
		student_id = $(this).attr('data-student');
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data:{val:val,student_id:student_id},
			success: function(data){
				if(data.response == true){
					//SCROLL TOP
				}
			},
		});

	}

});
//END PRINTED AJAX CHECKBOX

// REPLACE TABLE WITH SEARCH
$('body').on('click','.search-student-btn',function(){
	if(!$(this).hasClass('hidden')){

		if($(this).attr('data-type') == 'income')
		{

			$('.applyArea').html(
				'<div class="col-xs-12 col-sm-12">'+
	                '<div class="card">'+
	                  '<div class="card-body">'+
	                   '<form class="form_insert" result-data="form_result" method="POST" action="<?=base_url().'accounts/apply_student'?>">'+
	                        '<div class="form-group label-floating is-empty">'+
	                          '<div class="input-group">'+
	                            '<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>'+
	                            '<label class="control-label">اسم الطالب</label>'+
	                            '<input type="text" name="student_name" class="form-control">'+
	                          '</div>'+
	                        '</div>'+
	                        '<div class="form-group label-floating is-empty">'+
	                          '<div class="input-group">'+
	                            '<span class="input-group-addon"><i class="zmdi zmdi-receipt zmdi-hc-fw"></i></span>'+
	                            '<label for="student_code" class="control-label">كود الطالب</label>'+
	                            '<input type="text" name="student_code" class="form-control">'+
	                          '</div>'+
	                        '</div>'+
	                        '<input type="hidden" name="income_search" value="<?=md5('income')?>">'+
	                        '<div class="form-group label-floating is-empty">'+
	                          '<div class="input-group">'+
	                            '<span class="input-group-addon"><i class="zmdi zmdi-labels zmdi-hc-fw"></i></span>'+
	                            '<label for="national_id" class="control-label">رقم البطاقة</label>'+
	                            '<input type="text" name="national_id" class="form-control">'+
	                          '</div>'+
	                        '</div>'+
	                        '<div class="card-footer text-center">'+
	                            '<button class="btn btn-info"> بحث <i class="zmdi zmdi-search"></i></button>'+
	                        '</div>'+
	                        '<div class="form_result"></div>'+
	                    '</form>'+
	                  '</div>'+
	                '</div>'+
	            '</div>');
		}
		else
		{
			$('.applyArea').html(
				'<div class="col-xs-12 col-sm-12">'+
	                '<div class="card">'+
	                  '<div class="card-body">'+
	                   '<form class="form_insert" result-data="form_result" method="POST" action="<?=base_url().'accounts/apply_student'?>">'+
	                        '<div class="form-group label-floating is-empty">'+
	                          '<div class="input-group">'+
	                            '<span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>'+
	                            '<label class="control-label">اسم الطالب</label>'+
	                            '<input type="text" name="student_name" class="form-control">'+
	                          '</div>'+
	                        '</div>'+
	                        '<div class="form-group label-floating is-empty">'+
	                          '<div class="input-group">'+
	                            '<span class="input-group-addon"><i class="zmdi zmdi-labels zmdi-hc-fw"></i></span>'+
	                            '<label class="control-label">رقم البطاقة</label>'+
	                            '<input type="text" name="national_id" class="form-control">'+
	                          '</div>'+
	                        '</div>'+
	                        '<div class="form-group label-floating is-empty">'+
	                          '<div class="input-group">'+
	                            '<span class="input-group-addon"><i class="zmdi zmdi-receipt zmdi-hc-fw"></i></span>'+
	                            '<label class="control-label">رقم الايصال</label>'+
	                            '<input type="text" name="open_file_bill_number" class="form-control">'+
	                          '</div>'+
	                        '</div>'+

	                        '<div class="card-footer text-center">'+
	                            '<button class="btn btn-info"> بحث <i class="zmdi zmdi-search"></i></button>'+
	                        '</div>'+
	                        '<div class="form_result"></div>'+
	                    '</form>'+
	                  '</div>'+
	                '</div>'+
	            '</div>');
		}
	}
	$(this).addClass('hidden');
	if($(window).width() > 768){
		$('.applyArea').css({'width' : '50%','margin' : '0 auto',});
	}
});


/// FORM INSERT STEP -> 1
$("body").on('submit','.form_insert',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $this.attr("action");
	resultClass = $this.attr('result-data');
	method = $this.attr('method');
	type = $this.attr('data-type');
	$this.find(':submit').prop('disabled',false);
	$this.find('.'+resultClass+'').html('');
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $this.serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){
					//SCROLL TOP
					topFunction();
					$('.applyArea').css({'width':'100%',});
					$('.applyArea').html(tableContent());
					$('.search-student-btn').removeClass('hidden');
					// APPEND TABLE CONTENT
					i=0;
					if(type == 'income')
					{
						$('.thead_append').append(
						'<tr>'+
	              			'<th>#</th>'+
	              			'<th class="col-xs-2">الصورة الشخصية</th>'+
	              			'<th class="col-xs-2">اسم الطالب</th>'+
	              			'<th class="col-xs-2">كود الطالب</th>'+
	              			'<th class="col-xs-2">رقم البطاقة</th>'+
	              			'<th class="col-xs-2">القسم</th>'+
	              			'<th class="col-xs-2">الفرقة</th>'+
	              			'<th class="col-xs-2">'+
	              			'</th>'+
	            		'</tr>');
					}
					else
					{
						$('.thead_append').append(
						'<tr>'+
	              			'<th>#</th>'+
	              			'<th class="col-xs-2">الصورة الشخصية</th>'+
	              			'<th class="col-xs-2">اسم الطالب</th>'+
	              			'<th class="col-xs-2">رقم البطاقة</th>'+
	              			'<th class="col-xs-2">القسم</th>'+
	              			'<th class="col-xs-2">الفرقة</th>'+
	              			'<th class="col-xs-2">رقم ايصال فتح الملف</th>'+
	              			'<th class="col-xs-2">'+
	              			'</th>'+
	            		'</tr>');
					}
					// INCOME TABLE CODE
					if(type == 'income')
					{
						$.each(data.result,function(key,value){
						i++;
							$('.TableResult').append(
							'<tr role="row" class="">'+
	             			'<td>'+i+'</td>'+
	                      	'<td><img src="'+value.profile_photo+'" alt="" class="img-thumbnail" /></td>'+
	                      	'<td>'+value.student_name+'</td>'+
	                      	'<td>'+value.student_code+'</td>'+
	                      	'<td>'+value.national_id+'</td>'+
	                      	'<td>'+value.section+'</td>'+
	                      	'<td>'+value.class+'</td>'+
	                      	'<td><a ata-toggle="tooltip" data-placement="top"  style="height:43px;" title="تقديم" href="<?=base_url().'accounts/add_new_income/'?>'+value.student_id+'" class="btn btn-success"> اضافة إيراد <i class="zmdi zmdi-money zmdi-hc-fw"></i></a>'+
	                      	'</td>'+
	                    '</tr>');
						});
					}
					// APPLY TABLE CODE
					else
					{
						$.each(data.result,function(key,value){
						i++;
							$('.TableResult').append(
							'<tr role="row" class="">'+
	             			'<td>'+i+'</td>'+
	                      	'<td><img src="'+value.profile_photo+'" alt="" class="img-thumbnail" /></td>'+
	                      	'<td>'+value.student_name+'</td>'+
	                      	'<td>'+value.national_id+'</td>'+
	                      	'<td>'+value.section+'</td>'+
	                      	'<td>'+value.class+'</td>'+
	                      	'<td>'+value.open_file_bill_number+'</td>'+
	                      	'<td><a ata-toggle="tooltip" data-placement="top"  style="height:43px;" title="تقديم" href="<?=base_url().'accounts/apply_student_page/'?>'+value.student_id+'" class="btn btn-primary">تقديم <i class="zmdi zmdi-assignment-check zmdi-hc-fw"></i></a>'+
	                      	'</td>'+
	                    '</tr>');
						});
					}



				}
				else{
					$this.find('.'+resultClass+'').html(alertHtml('danger',data.message));
				}
			},
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});
	}
	return false;

});


//RETURN AJAX TO GENERATE STUDENT CODE
function returnAjax(student_id,invoice_id,url){
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {invoice_id:invoice_id,student_id:student_id},
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){

					//SCROLL TOP
					topFunction();
					// EMPTY RESULT DIV
					$this.find('.'+resultClass+'').html('');
					if(data.redirect != null)
						window.location.href=''+data.redirect+'';


				}
				else{

					returnAjax(data.message.student_id, data.message.invoice_id, data.message.link);

				}
			},

		});
}



/// FORM INSERT STEP -> 1
$("body").on('submit','.form_ajax',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.find(':input[type=submit]').prop('disabled', true);
	url = $(this).attr("action");
	resultClass = $(this).attr('result-data');
	method = $(this).attr('method');
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){

					//SCROLL TOP
					topFunction();
					// EMPTY RESULT DIV
					$this.find('.'+resultClass+'').html('');

					if(data.window != null)
						window.open(data.window,'_blank');

					if(data.redirect != null)
						window.location.href=''+data.redirect+'';


				}
				else{

					$this.find(':input[type=submit]').prop('disabled', false);
					returnAjax(data.message.student_id, data.message.invoice_id, data.message.link);

				}
			},
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});

	return false;

});

//// ACCOUNT ADD EXPENSES INVOICE START __>>
// Fees Form
$("body").on('submit','.insert_expenses',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{

	url = $this.attr('action');
	//$this.find(':input[type=submit]').prop('disabled', true);
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: $this.serialize(),
			beforeSend: function(){
				$('.alertify').removeClass('hidden');
				$('#alertNav').html(perloader('blue','xs'));
				headerLoader(true);
			},
			success: function(data){
				// REMOVE HEADER LOADER
				headerLoader(false);

				if(data.response == true){
						$('#alertNav').html('<div class="expenses_msg"><div class="alert alert-success">'+data.message+'</div></div>');
					if(data.result.expense_type == '1'){
						$('#alertNav').append('<a target="_blank" href="'+data.redirect+'" class="ok btn btn-primary" style="margin-left: 60px;" tabindex="1">طباعة الايصال <i class="zmdi zmdi-print"></i></a><a href="'+data.result.back+'" class="ok btn btn-default" tabindex="1">رجوع <i class="zmdi zmdi-arrow-left"></i></a>');
					}else{
						setTimeout(function() {
							if(data.redirect != null)
							window.location.href=''+data.redirect+'';
						}, 1000);

					}

				}
				else{

					$this.find(':input[type=submit]').prop('disabled', false);
					$('.'+dataResult+'').html(alertHtml('danger',data.message));
				}
			},
		});
	}
	return false;
});
// EXPENSES INVOICE END

// Fees Form
$("body").on('click','#tuition_fees_payment',function(e) {
	var $this = $(this);

	url 		 = $this.attr('data-url');
	tuition_fees = $this.attr('data-fees');
	value 		 = $this.attr('data-value');
	student_id 	 = $this.attr('data-student');
	dataResult   = $this.attr('data-result');
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data:{tuition_fees:tuition_fees,value:value,student_id:student_id},
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){
					//SCROLL TOP
					$('.'+dataResult+'').html(alertHtml('success',data.message));
					$('.'+dataResult+' > a').attr('href',data.result);
					$('#'+dataResult+'').removeClass('hidden');
					if(data.redirect != null)
						window.location.href=''+data.redirect+'';
				}
				else{
					$('.'+dataResult+'').html(alertHtml('danger',data.message));
				}
			},
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});

	return false;

});

$('body').on('click','#push_installment',function(){

	$('.push_installment').removeClass('hidden');
	$('.push_cash').addClass('hidden');

});
$('body').on('click','#push_cash',function(){
	$('.push_installment').addClass('hidden');
	$('.push_cash').removeClass('hidden');

});
$('body').on('click','.installment',function(){
	type = $(this).attr('data-type');
	value = $(this).attr('data-value');
	year = $(this).attr('year-tb');
	number = $(this).attr('data-number');
	url = $(this).attr('url');

	if (typeof url !== typeof undefined && url !== false) {
		$('.form_ajax').attr('action',url);
	}


	$('#year_tb_id').attr('value',year);
	$('#installment_id').attr('value',value);

	$('.installment_parte').html('دفع جزء');
	$('.installment_label2').html('اكتب المبلغ');
	$('.installment_label').html(type);
	$('#installment_input').attr('value',number);
	$('#installment_value').attr('value',number);

});

$('body').on('click','#installment_checkBox',function(){

	if($(this).is(":checked")){
		$('.form_ajax').find(':input[type=submit]').prop('disabled', true);
		$('#installment_parte_input').removeClass('hidden');
		$('.installment_parte_input').removeClass('hidden');
	}
	else{
		$('.form_ajax').find(':input[type=submit]').prop('disabled', false);

		$('.installment_parte_input').addClass('hidden');
		$('#installment_parte_input').addClass('hidden');
		$('#installment_input').attr('value',$('#installment_value').attr('value'));
	}
});

$('body').on('input propertychange paste keyup keydown','#installment_parte_input',function(){
	var value = $(this).val();
	var old_value = $('#installment_value').val();

	if(value > 0){
		$('.form_ajax').find(':input[type=submit]').prop('disabled', false);
	}else{
		$('.form_ajax').find(':input[type=submit]').prop('disabled', true);

	}
	if((parseInt(value) >= parseInt(old_value))){

		$(this).val('');
		$('#installment_input').value == old_value;
		$('#installment_checkBox').click();

	}else{
		if(value != ''){
			newValue = parseInt(old_value) - parseInt(value);
		$('#installment_input').attr('value',newValue);

		}
	}

});


// STUDENT TABLE AJAX
$(document).ready(function(){
	if($('.TableAjax').length > 0){
		tableAjax(1,$('.TableAjax').attr('page'),$('.TableAjax').attr('data-type'));
	}
});

$('body').on('click','.paginat',function(){
	$this = $(this);
	page = $this.attr('data-page');
	$('.TableAjax').attr('data-num',page);
	tableAjax(page,$('.TableAjax').attr('page'),$('.TableAjax').attr('data-type'));
	headerLoader(true);
});

function tableAjax(page,url,data_type){
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {page:page,data_type:data_type},
			success: function(data){
				if(data.response == true){
					topFunction();
					headerLoader(false);
					$('.TableResult').html(data.result.table);
					$('.paginationAjax').html(data.result.pagination);
				}
			}
		});

	return false;
}

// INCOME FUNCTIONS
$('body').on('click','#add_income_type',function(){
	$('.add_income_type').click();
});
$('body').on('click','.edit_income',function(){

	var tr=$(this).parent();
	$('#income_id').attr('value',$(this).attr('data-id'));
	$('#income_type').attr('value',$(this).attr('data-type'));
	$('#income_amount').attr('value',$(this).attr('data-amount').replace('.00',''));
	$('.edit_income_popup').click();

});

$('#income_id').change(function(){
	type = $('option:selected', this).attr('income_type');
	link = $('option:selected', this).attr('link');
	amount = $('option:selected', this).attr('amount');
	$('.new_result').html('');
	$('#income_amount').html('');
	$('.tuition_amount').addClass('hidden');
	$('#tuition_amount').prop('disabled',true);

	if(typeof amount !== 'undefined')
		$('#income_amount').html('قيمة إلايراد '+amount+' جنية مصرى فقط');
	if(type == 'section'){
		var url = $('option:selected', this).attr('url');
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			success: function(data){
				$('.new_result').html('<div class="form-group" style="padding: 0px">'+
                          	'<label for="section_id" class="col-sm-2 control-label">اسم القسم المراد التحويل الية</label>'+
                          	'<div class="col-sm-10">'+
                              	'<select name="section_id" id="section_id" class="form-control">'+
                              	'</select>'+
                          	'</div>'+
                      	'</div>');
				$.each(data.result, function(i, item) {

					if($('#section').val() != item.section_name)
					{
						$('#section_id').append('<option data-id="'+item.section_id+'" value="'+item.section_id+'">'+item.section_name+'</option>');
					}
				});

			},
		});

	}
	else if(type == 'fines')
	{
		$('.new_result').html('<div class="form-group" style="padding: 0px">'+
                          	'<label for="fines" class="col-sm-2 control-label">قيمة الغرامة</label>'+
                          	'<div class="col-sm-10">'+
                              	'<input type="number"  data-rule-required="true" data-msg-required="هذا الحقل الزامى" name="fines" class="form-control" id="fines" min="0" value="">'+
                          	'</div>'+
                      	'</div>');

	}
	else if(type == 'tuition_fees')
	{
		$('.tuition_amount').removeClass('hidden');
		$('#tuition_amount').prop('disabled',false);


		class_id = $('#class_id').val();
		var student_id = $('#student_id').val()
        var tuition_fees = type;
		var url = $(this)
        $.ajax({
            cache: false,
            type: "POST",
            url: link,
            dataType:'json',
            data: {student_id:student_id, class_id:class_id},
            beforeSend: function(){
                headerLoader(true);
            },
            success: function(data){
                // REMOVE HEADER LOADER
                headerLoader(false);
                if(data.response == true)
                {
                    $('#modal-html').html(data.result);
                    $('#modal-button').html('<a href="javascript:;" id="open-modal" class="btn hidden btn-info btn-block" data-toggle="modal" data-target="#fullscreen_modal">Trigger</a>');
                    $('#open-modal').click();
                }
                else
                {
                    alert('حدث خطاء');
                }
            },
        });


	}
	$('#remission').prop('disabled',false)
});

$("body").on('submit','.insert_income_invoice',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{

	url = $this.attr('action');
	//$this.find(':input[type=submit]').prop('disabled', true);
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: $this.serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				// REMOVE HEADER LOADER
				headerLoader(false);

				if(data.response == true){
						$('.alertify').removeClass('hidden');
						$('#alertNav').html('<div class="expenses_msg"><div class="alert alert-success">'+data.message+'</div></div>');
						$('#alertNav').append('<a target="_blank" href="'+data.redirect+'" class="ok btn btn-primary" style="margin-left: 60px;" tabindex="1">طباعة الايصال <i class="zmdi zmdi-print"></i></a><a href="'+data.result.back+'" class="ok btn btn-default" tabindex="1">رجوع <i class="zmdi zmdi-arrow-left"></i></a>');
					}
				else{


					$this.find(':input[type=submit]').prop('disabled', false);
					$('#form_result').html(data.message);
				}
			},
		});
	}
	return false;
});

// END INCOME FUNTIONS

// INSERT CITY NAME
$("body").on('submit','.insertData',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $(this).attr("action");
	resultId = $(this).attr('data-result-id');
	method = $(this).attr('method');
	$this.find(':submit').prop('disabled',true);
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),

			success: function(data){
				location.reload();

			},

		});
	}
	return false;

});


$("body").on('submit','.form_in2',function(e) {
	e.preventDefault();
	var $this = $(this);

	url = $(this).attr("action");
	resultClass = $(this).attr('result-data');
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			success: function(data){
				if(data.response == true){
					$('.'+resultClass+'').html(alertHtml('success',data.message));
					if(data.redirect != ''){
						setTimeout(function(){
							window.location.href = ''+data.redirect+'';
						});
					}
				}
				else
					$('.'+resultClass+'').html(alertHtml('danger',data.message));
			}
		});

	return false;

});

// Tuitios fees setting

$('body').on('click','#deleteInstallment',function(){
	$(this).closest('.form-group').remove();
});

function installment($num)
{
	if($num == 1)
		return 'الاول';
	else if($num == 2)
		return 'الثانى';
	else if($num == 3)
		return 'الثالث';
	else if($num == 4)
		return 'الرابع';
	else if($num == 5)
		return 'الخامس';
	else if($num == 6)
		return 'السادس';
}

$('body').on('click','.add_installment_btn',function(){
	id = $(this).attr('data-id');
	idNum = $(this).attr('data-id-num');

	input_length = $('.'+id).length + 1;

	$('#'+id).append('<div class="form-group is-empty '+id+'">'+
        '<label for="" class="col-md-4 control-label">القسط  '+installment(input_length)+'</label>'+
        '<div class="col-md-5">'+
        '<input type="number" name="installment['+idNum+'][]" class="form-control" value="">'+
        '</div>'+
        '<div class="col-md-3">'+
        '<a class="btn btn-danger btn-fab btn-fab-sm" href="javascript:;" id="deleteInstallment"> <i class="zmdi zmdi-close"></i> </a>'+
        '</div>'+
        '</div>');
});
</script>
