<script>

/// FORM INSERT STEP -> 1
$("body").on('submit','.insert_data',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
		// FORM PROPERTIES
		url = $(this).attr("action");	
		resultClass = $(this).attr('result-data');
		method = $(this).attr('method');
		$this.find(':submit').prop('disabled',false);

		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data)
			{
				if(data.response == true)
				{
					// EMPTY RESULT DIV
					$this.find('.'+resultClass+'').html('');
					if(data.redirect != null)
						window.location.href=''+data.redirect+'';
				}
				else
				{
					$this.find('.'+resultClass+'').html(alertHtml('danger',data.message));
				}
			},	
		});
	}
	
	return false;

});


</script>