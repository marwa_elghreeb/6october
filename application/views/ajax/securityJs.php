<script type="text/javascript">
	$('body').on('click','#add_new_enquiry',function(){
	$('.enquiry_modal').click();

	setTimeout(function(){ $(".input-name").focus(); },1000);
});

$('body').on('click','.edit_visit',function(){
$('.edit_visitModal').click();

url = $(this).attr('data-url');
id = $(this).attr('data-id');

$.ajax({
		cache: false,
		type: "POST",
		url: url,
		data: {id:id},
	
		success: function(data){
			$('.edit_visit_result').html(data);
		},	

	});

return false;


});

/// FORM INSERT STEP -> 1
$("body").on('submit','.form_ajax',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.find(':input[type=submit]').prop('disabled', true);
	url = $(this).attr("action");	
	resultClass = $(this).attr('result-data');
	method = $(this).attr('method');
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){
					$this.find('.'+resultClass).html(data.message);

					if(data.window != null)
						window.open(data.window,'_blank');

					if(data.redirect != null)
						window.location.href=''+data.redirect+'';
					
				}
				else{

					$this.find(':input[type=submit]').prop('disabled', false);
					returnAjax(data.message.student_id, data.message.invoice_id, data.message.link);
				
				}
			},	
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});
	
	return false;

});

$(document).ready(function(){
	if($('.TableAjax').length > 0){
		tableAjax(1,$('.TableAjax').attr('page'),$('.TableAjax').attr('post_data'));
	}
});
	
$('body').on('click','.paginat',function(){
	$this = $(this);
	page = $this.attr('data-page');
	$('.TableAjax').attr('data-num',page);
	tableAjax(page,$('.TableAjax').attr('page'),$('.TableAjax').attr('post_data'));
	headerLoader(true);
});

function tableAjax(page,url,post_data){
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {page:page,post_data:post_data},
			success: function(data){
				if(data.response == true){
					topFunction();
					headerLoader(false);
					$('.TableResult').html(data.result.table);
					$('.paginationAjax').html(data.result.pagination);
				}
			}	
		});
	
	return false;
}




</script>