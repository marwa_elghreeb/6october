<script>
/// acquaintance CHECKBOX
$('body').on('click','#acquaintance_student',function(){
	if($(this).is(":checked")){
		$('.acquaintanceInput').removeClass('hidden');
		$('.acquaintanceSelect').addClass('hidden');
	}
	else{
		$('.acquaintanceSelect').removeClass('hidden');
		$('.acquaintanceInput').addClass('hidden');
	}
});

$('body').on('click','#payment',function(){
	if($(this).is(":checked")){
		$('#checkPayment').prop('disabled',false);
	}
	else{
		$('#checkPayment').prop('disabled',true);
	}
});


$('.student_school').change(function(){
	if($(this).val() == 'not_exist'){
		$('.student_school_text_area').removeClass('hidden');
		$('#student_school_text').prop('disabled',false);
	}
	else
	{
		$('.student_school_text_area').addClass('hidden');
		$('#student_school_text').prop('disabled',true);

	}
});


// acquaintance_way SELECT 
$('#acquaintance_way').change(function(){
	if($(this).val() == 'another'){
		$('.acquaintance_modal').click();
	}
});

// IMPORT STUDNET MONEY LEAVE ACADEMY
$('body').on('click','#import_checkBox',function(){
	
	if($(this).is(":checked")){
		$('.form_ajax').find(':input[type=submit]').prop('disabled', true);
		$('#import_sm_input').removeClass('hidden');
		$('.import_sm_input').removeClass('hidden');
	}
	else{
		$('.form_ajax').find(':input[type=submit]').prop('disabled', false);

		$('.import_sm_input').addClass('hidden');
		$('#import_sm_input').addClass('hidden');
		$('#mv_value_input').attr('value',$('#mv_value').attr('value'));
	}
});
$('body').on('click','#gsii',function(){

	student_id = $(this).attr('data-id');
	url = $(this).attr('url');
	headerLoader(true);


	$('#openImportPopup').click();
	$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {student_id:student_id},
			success: function(data){
				if(data.response == true){
					headerLoader(false);
					$('#importHtmlResult').html(data.html);
					$('#mv_value_input').attr('value',data.total);
					$('.countminus').attr('value',data.total);
				}
			}	
		});
	
	return false;

});


$('body').on('input propertychange paste keyup keydown','#import_sm_input',function(){
	var value = $(this).val();
	var old_value = $('#mv_value_input').val();

	if(value > 0){
		$('.form_ajax').find(':input[type=submit]').prop('disabled', false);
	}else{
		$('.form_ajax').find(':input[type=submit]').prop('disabled', true);

	}
	if((parseInt(value) >= parseInt(old_value))){
		
		$(this).val('');
		$('.countminus').attr('value',old_value);
		$('#import_sm_input').value == old_value; 
		$('#import_checkBox').click();

	}else{
		if(value != ''){
			newValue = parseInt(old_value) - parseInt(value);
			$('.countminus2').attr('value',newValue); 
			
		}
	}

});




// STUDNET CUNTRY CHOOSE AREA
$('body').on('click','.studnet_new_cuntry',function(){
	if($(this).is(":checked")){
		$('.national_id_label').html('رقم جواز السفر');
		$('#national_id').attr('placeholder','رقم جواز السفر');
		$('#national_id').attr('minlength','4');
		$('.studentCuntry').removeClass('hidden');

		$('.birth_place_governorate_input').prop('disabled',false);
		$('.birth_place_governorate_input').removeClass('hidden');
		$('.birth_place_governorate').prop('disabled',true);
		$('.birth_place_governorate').addClass('hidden');
		$('.birth_place_governorate').next('.dropdownjs').addClass('hidden');


		$('.birth_place_input').prop('disabled',false);
		$('.birth_place_input').removeClass('hidden');
		$('.birth_place').prop('disabled',true);
		$('.birth_place').addClass('hidden');
		$('.birth_place').next('.dropdownjs').addClass('hidden');
	}
});

$('body').on('click','.studnet_cuntry',function(){
if($(this).is(":checked")){
		$('.national_id_label').html('الرقم القومى');
		$('#national_id').attr('placeholder','الرقم القومى');
		$('#national_id').attr('minlength','14');
		$('.studentCuntry').addClass('hidden');

		$('.birth_place_governorate_input').prop('disabled',true);
		$('.birth_place_governorate_input').addClass('hidden');
		$('.birth_place_governorate').prop('disabled',false);
		$('.birth_place_governorate').removeClass('hidden');
		$('.birth_place_governorate').next('.dropdownjs').removeClass('hidden');


		$('.birth_place_input').prop('disabled',true);
		$('.birth_place_input').addClass('hidden');
		$('.birth_place').prop('disabled',false);
		$('.birth_place').removeClass('hidden');
		$('.birth_place').next('.dropdownjs').removeClass('hidden');
	}
});

$('#student_cuntry').on('keyup keydown past',function(){
	$value = $(this).val();
	$('.studnet_new_cuntry').attr('value',$value);
});
// END CHOOSE COUNTRY


// START INSERT STUDENT NOT EXIST CITIES

// 1 - Birth place 
$('.birth_place').change(function(){

	if($(this).val() == 'not_exist'){
		$('.basic_modal').click();
		$('#birth_place_gid').attr('value',$('#birth_place_state').val());
	}
});
	

// 2 - City
$('.studnet_city').change(function(){

	if($(this).val() == 'not_exist'){
		$('.city_text_area').removeClass('hidden');
		$('#city_text').prop('disabled',false);
	}
	else{
		$('.city_text_area').addClass('hidden');
		$('#city_text').prop('disabled',true);

	}
});

// 3 - Mangment Learning
$('.management_learning').change(function(){
	if($(this).val() == 'not_exist'){
		$('.management_learning_text_area').removeClass('hidden');
		$('#management_learning_text').prop('disabled',false);
	}
	else{
		$('.management_learning_text_area').addClass('hidden');
		$('#management_learning_text').prop('disabled',true);

	}
});
//////////////

// CHOOSE qualification 
$('#qualification').change(function(){
	if($('option:selected', this).attr('data-choose') == 1){
		$(this).attr('name','qualification_');
		$('.qualification_text_area').removeClass('hidden');
		$('#qualification_text').prop("disabled",false);
	}else{
		$(this).attr('name','qualification');
		$('.qualification_text_area').addClass('hidden');
		$('#qualification_text').prop("disabled",true);

	}
});
// END qualification CHOOSEN 

// CHOOSE STUDY STATUS
$('#study_status').change(function(){
	if($('option:selected', this).attr('data-concentrated') == 1){
		$('.study_status_date').removeClass('hidden');
		$('#study_status_text').prop("disabled",false);
	}else{
		$('.study_status_date').addClass('hidden');
		$('#study_status_text').prop("disabled",true);

	}
});

// END STUDY STATUS CODE 
/// FORM INSERT STEP -> 1
$("body").on('submit','.form_insert',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $(this).attr("action");	
	resultClass = $(this).attr('result-data');
	tabActive   = $(this).attr('tabActive');
	EditClass   = $(this).attr('edit-class');
	Counter 	= parseInt(tabActive) + parseInt(1);
	Counter2 	= parseInt(tabActive) + parseInt(2);
	nextTab 	= 'tab'+ Counter;
	secondTab 	= 'tab'+ Counter2;
	method = $(this).attr('method');
	$this.find(':submit').prop('disabled',false);
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){
					
					//SCROLL TOP
					topFunction();
					// EMPTY RESULT DIV
					$this.find('.'+resultClass+'').html('');
					// DISABLED SUBMIT BUTTON 
						$this.find(':submit').prop('disabled',false);
			
					//IF THIS FIRST FORM:: 1
					if(tabActive == 1){
						
						$this.find(':submit').html('تعديل البيانات <i class="zmdi zmdi-rotate-right zmdi-hc-fw"></i>');
						
						EditInput('.'+EditClass+'','<input type="hidden" name="student_id" value="'+data.result.student_id+'">');
						// APPEND DATA TO OPEN FILE INVOICE
						$('#open_file_amount').attr('value',data.result.open_file_amount.open_file_amount);
						$('#openfile_price').attr('value',data.result.open_file_amount.open_file_amount);
						$('#bill_study_year').attr('value',data.result.bill_study_year);
						$('#bill_student_name').attr('value',data.result.student_name);

						//APPEN DATA TO INVOICE TABLE 
						$('.open_file_amount').html(data.result.open_file_amount.open_file_amount);
						$('.bill_section_name').html(data.result.section_name);
						$('.bill_student_name').html(data.result.student_name);

						$('.get_breadcrumb').html(data.result.breadcrumb);
						$('.print_file_url').attr('href',data.result.print_file_url);
						$('.print_future_institue_student_file').attr('href',data.result.future_institue);
						$('.progress-bar').css('width','33.3333%');

					}
					else{
						//$('.randbillnumber').html(data.message);
						$('.billLink').attr('href',data.message);
					}
					
						$('.'+nextTab).removeClass('disabled');
						$('.'+nextTab+' > a').attr('href','#'+nextTab);
						$('.'+nextTab+' > a').click();

						$('.printBTN').removeClass('hidden');
				}
				else{
					$this.find('.'+resultClass+'').html(alertHtml('danger',data.message));
				}
			},	
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});
	}
	return false;

});
// APPEND EDIT INPUT FUNCTION 
function EditInput(className,Content){
	$(className).each(function(){
		$(this).html(Content);
	});
} 


// UPLOAD FUNCTIONS
$('body').on('click','.bTnProgressUpload',function(){
	$this = $(this);
	if($this.attr('disabled')){
	    
		$('.alertify-logs').html('<div class="success show">يرجى الانتظار من انتهاء رفع الملف</div>');
		setTimeout(function() {
			$('.alertify-logs').html('');
		}, 4000);
		
	}else{
	    $this.next('.InputFile').click();    
	}
	
});
$('.InputFile').change(function(){
	$this    = $(this);
	thisForm = $this.parents('form:first');
	url 	 = thisForm.attr('action');

	bar  = thisForm.find($('.progress-barUpload'));
    percent = thisForm.find($('.status'));
    
    // RESTORE DIV STYLE 
    thisForm.find('.progress-barUpload').css({'background-color':'rgba(33, 193, 57, 0.8)',});
    thisForm.find('#output').html('');

    // SUBMIT FORM 
    thisForm.ajaxForm({
    	cache: false,
    	dataType:'json',
        beforeSend: function() {
            $('.bTnProgressUpload').attr('disabled','disabled');
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        success:function(data){        	
        	if(data.response == true){
        		thisForm.prevAll('.dz-message').css({'background-image':'url('+data.result+')'});
        		$('.bTnProgressUpload').removeAttr("disabled");

        		thisForm.prevAll('.dz-message').attr('file_name',data.redirect);

        		<?php if(isset($student->student_id)){?>

        		$(imgButtons(thisForm.prevAll('.dz-message').attr('data-file'),<?=$student->student_id?>,thisForm.prevAll('.dz-message').attr('data-href')
        			+data.redirect+'/'+'<?=$this->router->fetch_class()?>',thisForm.prevAll('.dz-message').attr('data-type'),thisForm.attr('table'))).insertBefore(thisForm.prev());
        		
        		<?php }?>

        		thisForm.css('visibility','hidden');
        		thisForm.prevAll('.dz-message')
        		thisForm.css('visibility','hidden');

        	}else{
        		$('.bTnProgressUpload').removeAttr("disabled");
        		thisForm.prevAll('.dz-message').css({'background-image':'url('+data.result+')'});
        		thisForm.find('.progress-barUpload').css({'background-color':'red',});
        		thisForm.find('#output').html(data.message);
        	}
        },
        complete: function(xhr) {
            //status.html(xhr.responseText);
        }
    });
thisForm.trigger('submit');
});



// STUDENT TABLE AJAX 
$(document).ready(function(){
	if($('.TableAjax').length > 0){
		tableAjax(1,$('.TableAjax').attr('page'),$('.TableAjax').attr('post_data'));
	}
});
	
$('body').on('click','.paginat',function(){
	$this = $(this);
	page = $this.attr('data-page');
	$('.TableAjax').attr('data-num',page);
	tableAjax(page,$('.TableAjax').attr('page'),$('.TableAjax').attr('post_data'));
	headerLoader(true);
});

function tableAjax(page,url,post_data){
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {page:page,post_data:post_data},
			success: function(data){
				if(data.response == true){
					topFunction();
					headerLoader(false);
					$('.TableResult').html(data.result.table);
					$('.paginationAjax').html(data.result.pagination);
				}
			}	
		});
	
	return false;
	}


// INSERT CITY NAME
$("body").on('submit','.insertData',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $(this).attr("action");	
	resultId = $(this).attr('data-result-id');
	method = $(this).attr('method');
	$this.find(':submit').prop('disabled',true);

		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
		
			success: function(data){
				$this.find(':submit').prop('disabled',false);
				$('#closeModal').click();

				$('#'+resultId+' option', this).removeClass('selected');
				if(resultId == 'city'){
					$('#'+resultId).html('<option class="selected" data-id="'+data.governorate_id+'" selected="selected" value="'+data.city_id+'">'+data.city_name+'</option>');
					
				}
				else if(resultId == 'acquaintance_way'){
					$('#'+resultId).html('<option class="selected" data-id="'+data.acquaintance_id+'" selected="selected" value="'+data.acquaintance_id+'">'+data.acquaintance_name+'</option>');
				}
				else{
					$('#'+resultId).html('<option class="selected" data-id="'+data.governorate_id+'" selected="selected" value="'+data.city_name+'">'+data.city_name+'</option>');
					
				}

				
			},	

		});
	}
	return false;

});


// function checkIfRemoteFileExists(link){
// $.ajax({
//         url:link,
//         error: function()
//         {
//            return false;
//         },
//         success: function()
//         {
//             return true;
//         }
//     });
// }

// Upload images box setting 
function imgButtons(file_name,student_id,data_href,data_type,table)
{
	return '<div class="iMgHover">'+
    '<a target="_blank" href="'+data_href+'" class="btn btn-info btn-fab">'+
       '<i class="zmdi zmdi-eye zmdi-hc-fw" style="margin-top: 15px;"></i>'+
       '<div class="ripple-container"></div>'+
    '</a>'+

    '<a data-toggle="tooltip" data-placement="top" title="حذف" href="javascript:;" img-name="'+file_name+'" data-url="<?=base_url().'home/delete/'?>" data-id="'+student_id+'" data-table="'+table+'" data-column="student_id" data-type="image" type-name="'+data_type+'" class="btn btn-danger btn-fab delete">'+
       '<i class="zmdi zmdi-delete" style="margin-top: 15px;"></i>'+
       '<div class="ripple-container"></div>'+
    '</a>'+
 	'</div>';
}

<?php if($this->uri->segment(2) == 'edit_student' OR $this->uri->segment(2) == 'edit_graduated_student'): ?>
$(document).ready(function(){

	$('.dz-message').each(function(i, obj) {
   		
   		$this = $(this);
	   	file_path = $this.attr('data-file');
	   	file_name = $this.attr('file_name');
	   	data_href = $this.attr('data-href');
   		
   		if($this.attr('file_name').length > 0)
   		{

	   		
	   		$.ajax({
			    url: $('.dz-message').eq(i).attr('data-file'),
			    type:'HEAD',
			    error: function(data)
			    {
			        $('.dz-message').eq(i).removeAttr('style');
			    },
			    success: function(data)
			    {
	   				$(imgButtons($('.dz-message').eq(i).attr('data-file'),<?=$student->student_id?>,$('.dz-message').eq(i).attr('data-href')+$('.dz-message').eq(i).attr('file_name')+'/'+'<?=$this->router->fetch_class()?>',$('.dz-message').eq(i).attr('data-type'),$('.imgForm').eq(i).attr('table'))).insertAfter($('.dz-message').eq(i)).find('span');
			     	$('.dz-message').eq(i).nextAll().eq(2).css('visibility','hidden');   
			    	// alert(data);
			      //  	$('sad').insertAfter($this).find('span');
			    }
			});

   		}
   		else
   		{
   			$('.dz-message').eq(i).removeAttr('style');
   		}
	});
});
<?php endif; ?>

</script>