<script>

$('body').on('click','.edit_enquiry',function(){
	$('.edit_enquiryModal').click();

	url = $(this).attr('data-url');
	id = $(this).attr('data-id');

	$.ajax({
			cache: false,
			type: "POST",
			url: url,
			data: {id:id},
		
			success: function(data){
				$('.edit_enquiry_result').html(data);
			},	

		});
	
	return false;


});


/// acquaintance CHECKBOX
$('body').on('click','#acquaintance_student',function(){
	if($(this).is(":checked")){
		$('.acquaintanceInput').removeClass('hidden');
		$('.acquaintanceSelect').addClass('hidden');
	}
	else{
		$('.acquaintanceSelect').removeClass('hidden');
		$('.acquaintanceInput').addClass('hidden');
	}
});

$('body').on('click','#payment',function(){
	if($(this).is(":checked")){
		$('#checkPayment').prop('disabled',false);
	}
	else{
		$('#checkPayment').prop('disabled',true);
	}
});


$('body').on('click','#add_new_enquiry',function(){
	$('.enquiry_modal').click();
});

// acquaintance_way SELECT 
$('#acquaintance_way').change(function(){
	if($(this).val() == 'another'){
		$('.acquaintance_modal').click();
	}
});

// STUDNET CUNTRY CHOOSE AREA
$('body').on('click','.studnet_new_cuntry',function(){
	if($(this).is(":checked")){
		$('.national_id_label').html('رقم جواز السفر');
		$('#national_id').attr('placeholder','رقم جواز السفر');
		$('#national_id').attr('minlength','4');
		$('.studentCuntry').removeClass('hidden');

		$('.birth_place_governorate_input').prop('disabled',false);
		$('.birth_place_governorate_input').removeClass('hidden');
		$('.birth_place_governorate').prop('disabled',true);
		$('.birth_place_governorate').addClass('hidden');
		$('.birth_place_governorate').next('.dropdownjs').addClass('hidden');


		$('.birth_place_input').prop('disabled',false);
		$('.birth_place_input').removeClass('hidden');
		$('.birth_place').prop('disabled',true);
		$('.birth_place').addClass('hidden');
		$('.birth_place').next('.dropdownjs').addClass('hidden');
	}
});


$('body').on('click','.studnet_cuntry',function(){
if($(this).is(":checked")){
		$('.national_id_label').html('الرقم القومى');
		$('#national_id').attr('placeholder','الرقم القومى');
		$('#national_id').attr('minlength','14');
		$('.studentCuntry').addClass('hidden');

		$('.birth_place_governorate_input').prop('disabled',true);
		$('.birth_place_governorate_input').addClass('hidden');
		$('.birth_place_governorate').prop('disabled',false);
		$('.birth_place_governorate').removeClass('hidden');
		$('.birth_place_governorate').next('.dropdownjs').removeClass('hidden');


		$('.birth_place_input').prop('disabled',true);
		$('.birth_place_input').addClass('hidden');
		$('.birth_place').prop('disabled',false);
		$('.birth_place').removeClass('hidden');
		$('.birth_place').next('.dropdownjs').removeClass('hidden');
	}
});

$('#student_cuntry').on('keyup keydown past',function(){
	$value = $(this).val();
	$('.studnet_new_cuntry').attr('value',$value);
});
// END CHOOSE COUNTRY

// START INSERT STUDENT NOT EXIST CITIES

// 1 - Birth place 
$('.birth_place').change(function(){

	if($(this).val() == 'not_exist'){
		$('.basic_modal').click();

		 $('.insertData').attr('data-result-id','birth_place');

		$('#birth_place_gid').attr('value',$('#birth_place_state').val());
	}
});
	

// 2 - City
$('.studnet_city').change(function(){

	if($(this).val() == 'not_exist'){
		$('.basic_modal').click();

		 $('.insertData').attr('data-result-id','city');

		$('#birth_place_gid').attr('value',$('#state').val());
	}


	// if($(this).val() == 'not_exist'){
	// 	$('.city_text_area').removeClass('hidden');
	// 	$('#city_text').prop('disabled',false);
	// }
	// else{
	// 	$('.city_text_area').addClass('hidden');
	// 	$('#city_text').prop('disabled',true);

	// }
});

// 3 - Mangment Learning
$('.management_learning').change(function(){
	if($(this).val() == 'not_exist'){
		$('.management_learning_text_area').removeClass('hidden');
		$('#management_learning_text').prop('disabled',false);
	}
	else{
		$('.management_learning_text_area').addClass('hidden');
		$('#management_learning_text').prop('disabled',true);

	}
});

$('.student_school').change(function(){
	if($(this).val() == 'not_exist'){
		$('.student_school_text_area').removeClass('hidden');
		$('#student_school_text').prop('disabled',false);
	}
	else
	{
		$('.student_school_text_area').addClass('hidden');
		$('#student_school_text').prop('disabled',true);

	}
});



//////////////

// CHOOSE qualification 
$('#qualification').change(function(){
	if($('option:selected', this).attr('data-choose') == 1){
		$(this).attr('name','qualification_');
		$('.qualification_text_area').removeClass('hidden');
		$('#qualification_text').prop("disabled",false);
	}else{
		$(this).attr('name','qualification');
		$('.qualification_text_area').addClass('hidden');
		$('#qualification_text').prop("disabled",true);

	}
});
// END qualification CHOOSEN 

// CHOOSE STUDY STATUS
$('#study_status').change(function(){
	if($('option:selected', this).attr('data-concentrated') == 1){
		$('.study_status_date').removeClass('hidden');
		$('#study_status_text').prop("disabled",false);
	}else{
		$('.study_status_date').addClass('hidden');
		$('#study_status_text').prop("disabled",true);

	}
});

// END STUDY STATUS CODE 
/// FORM INSERT STEP -> 1
$("body").on('submit','.form_insert',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $(this).attr("action");	
	resultClass = $(this).attr('result-data');
	tabActive   = $(this).attr('tabActive');
	EditClass   = $(this).attr('edit-class');
	Counter 	= parseInt(tabActive) + parseInt(1);
	Counter2 	= parseInt(tabActive) + parseInt(2);
	nextTab 	= 'tab'+ Counter;
	secondTab 	= 'tab'+ Counter2;
	method = $(this).attr('method');
	$this.find(':submit').prop('disabled',false);
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){
					
					//SCROLL TOP
					topFunction();
					// EMPTY RESULT DIV
					$this.find('.'+resultClass+'').html('');
					// DISABLED SUBMIT BUTTON 
						$this.find(':submit').prop('disabled',false);
			
					//IF THIS FIRST FORM:: 1
					if(tabActive == 1){
						
						$this.find(':submit').html('تعديل البيانات <i class="zmdi zmdi-rotate-right zmdi-hc-fw"></i>');
						
						EditInput('.'+EditClass+'','<input type="hidden" name="student_id" value="'+data.result.student_id+'">');
						// APPEND DATA TO OPEN FILE INVOICE
						$('#open_file_amount').attr('value',data.result.open_file_amount.open_file_amount);
						$('#openfile_price').attr('value',data.result.open_file_amount.open_file_amount);
						$('#bill_study_year').attr('value',data.result.bill_study_year);
						$('#bill_student_name').attr('value',data.result.student_name);

						//APPEN DATA TO INVOICE TABLE 
						$('.open_file_amount').html(data.result.open_file_amount.open_file_amount);
						$('.bill_section_name').html(data.result.section_name);
						$('.bill_student_name').html(data.result.student_name);

						$('.get_breadcrumb').html(data.result.breadcrumb);
						$('.print_file_url').attr('href',data.result.print_file_url);

							
						
						if(data.result.future_institue != '')
						{
							$('.future_institue_student_file').removeClass('hidden');
							$('.print_future_institue_student_file').attr('href',data.result.future_institue);
						}
						
						$('.progress-bar').css('width','33.3333%');

					}
					else{
						//$('.randbillnumber').html(data.message);
						$('.billLink').attr('href',data.message);
					}
					
						$('.'+nextTab).removeClass('disabled');
						$('.'+nextTab+' > a').attr('href','#'+nextTab);
						$('.'+nextTab+' > a').click();

						$('.printBTN').removeClass('hidden');
				}
				else{
					$this.find('.'+resultClass+'').html(alertHtml('danger',data.message));
				}
			},	
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});
	}
	return false;

});
// APPEND EDIT INPUT FUNCTION 
function EditInput(className,Content){
	$(className).each(function(){
		$(this).html(Content);
	});
} 


// UPLOAD FUNCTIONS
$('body').on('click','.bTnProgressUpload',function(){
	$this = $(this);
	if($this.attr('disabled')){
	    
		$('.alertify-logs').html('<div class="success show">يرجى الانتظار من انتهاء رفع الملف</div>');
		setTimeout(function() {
			$('.alertify-logs').html('');
		}, 4000);
		
	}else{
	    $this.next('.InputFile').click();    
	}
	
});
$('.InputFile').change(function(){
	$this    = $(this);
	thisForm = $this.parents('form:first');
	url 	 = thisForm.attr('action');

	bar  = thisForm.find($('.progress-barUpload'));
    percent = thisForm.find($('.status'));
    
    // RESTORE DIV STYLE 
    thisForm.find('.progress-barUpload').css({'background-color':'rgba(33, 193, 57, 0.8)',});
    thisForm.find('#output').html('');

    // SUBMIT FORM 
    thisForm.ajaxForm({
    	cache: false,
    	dataType:'json',
        beforeSend: function() {
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
            $('.bTnProgressUpload').attr('disabled','disabled');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        success:function(data){        	
        	if(data.response == true){
        		thisForm.prevAll('.dz-message').css({'background-image':'url('+data.result+')'});
        		$('.bTnProgressUpload').removeAttr("disabled");
        	}else{
        		$('.bTnProgressUpload').removeAttr("disabled");
        		thisForm.prevAll('.dz-message').css({'background-image':'url('+data.result+')'});
        		thisForm.find('.progress-barUpload').css({'background-color':'red',});
        		thisForm.find('#output').html(data.message);
        	}
        },
        complete: function(xhr) {
            //status.html(xhr.responseText);
        }
    });
thisForm.trigger('submit');
});



// STUDENT TABLE AJAX 
$(document).ready(function(){
	if($('.TableAjax').length > 0){
		tableAjax(1,$('.TableAjax').attr('page'),$('.TableAjax').attr('post_data'));
	}
});
	
$('body').on('click','.paginat',function(){
	$this = $(this);
	page = $this.attr('data-page');
	$('.TableAjax').attr('data-num',page);
	tableAjax(page,$('.TableAjax').attr('page'),$('.TableAjax').attr('post_data'));
	headerLoader(true);
});

function tableAjax(page,url,post_data){
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {page:page,post_data:post_data},
			success: function(data){
				if(data.response == true){
					topFunction();
					headerLoader(false);
					$('.TableResult').html(data.result.table);
					$('.paginationAjax').html(data.result.pagination);
				}
			}	
		});
	
	return false;
}

// INSERT CITY NAME
$("body").on('submit','.insertData',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.validate();
	if($this.valid())
	{
	url = $(this).attr("action");	
	resultId = $(this).attr('data-result-id');
	method = $(this).attr('method');
	$this.find(':submit').prop('disabled',true);

		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
		
			success: function(data){
				$this.find(':submit').prop('disabled',false);
				$('#closeModal').click();

				$('#'+resultId+' option', this).removeClass('selected');
				if(resultId == 'city'){
					$('#'+resultId).html('<option class="selected" data-id="'+data.governorate_id+'" selected="selected" value="'+data.city_id+'">'+data.city_name+'</option>');
					
				}
				else if(resultId == 'acquaintance_way'){
					$('#'+resultId).html('<option class="selected" data-id="'+data.acquaintance_id+'" selected="selected" value="'+data.acquaintance_id+'">'+data.acquaintance_name+'</option>');
				}
				else{
					$('#'+resultId).html('<option class="selected" data-id="'+data.governorate_id+'" selected="selected" value="'+data.city_name+'">'+data.city_name+'</option>');
					
				}

				
			},	

		});
	}
	return false;

});

$("body").on('click','#payOpenFilePayment',function(e) {
	e.preventDefault();
	var $this = $(this);
	var Element = $this.closest("tr");
	var id 		= $this.attr('data-id');
	var url 	= $this.attr('data-url');

	if(url != '' && id != '')
	{
		$('#sa-warning').click();
		$('body .sweet-alert h2').html('دفع رسوم فتح الملف');
		$('body .text-muted').html('');
		$('.confirm').click(function(){
		$('.confirm').remove();
		$.ajax({
			cache: false,
			type: "POST",
			url: url,
			dataType:'json',
			data: {student_id:id},
			success: function(data)
			{
				if(data.response == true)
				{
					Element.hide(1000);
					$('body .sweet-alert h2').html('تم الدفع');
					$('body .sweet-alert').append('<a target="_blank" href="'+data.message+'" class="btn btn-primary refresh">طباعة  الايصال</a>')
				}
				else
				{
					$('body .sweet-alert h2').html(data.message);
				}
			}
		});
			return false;
		});
	}
});

$('body').on('click','.refresh',function(){
	setTimeout(function(){
		location.reload();
	},100);
});


/// FORM INSERT STEP -> 1
$("body").on('submit','.form_ajax',function(e) {
	e.preventDefault();
	var $this = $(this);
	$this.find(':input[type=submit]').prop('disabled', true);
	url = $(this).attr("action");	
	resultClass = $(this).attr('result-data');
	method = $(this).attr('method');
		$.ajax({
			cache: false,
			type: ""+method+"",
			url: url,
			dataType:'json',
			data: $(this).serialize(),
			beforeSend: function(){
				headerLoader(true);
			},
			success: function(data){
				if(data.response == true){
					$this.find('.'+resultClass).html(data.message);

					if(data.window != null)
						window.open(data.window,'_blank');

					if(data.redirect != null)
						window.location.href=''+data.redirect+'';
					
				}
				else{

					$this.find(':input[type=submit]').prop('disabled', false);
					returnAjax(data.message.student_id, data.message.invoice_id, data.message.link);
				
				}
			},	
			complete:function(){
				// OPEN NEXT TAB

				// CLEAR LOADER PLACE
				headerLoader(false);
			}
		});
	
	return false;

});
</script>