<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الزيارات  </h1>
            <button class="btn btn-primary" id="add_new_enquiry">اضافة  زيارة <i class="zmdi zmdi-user"></i></button>                    
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">قائمة  الزيارات</h2>    
              <div class="col-md-12">
                <form class="student_search" action="<?=base_url().'reception/enquiry'?>" method="GET">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="date" class="col-sm-3 control-label">التاريخ</label>
                        <div class="col-sm-9">
                            <input id="date" type="date" name="date" placeholder="التاريخ" value="<?=$this->input->get('date') != null ? $this->input->get('date') : ''?>" class="form-control" >
                        </div>
                    </div>  
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></button>
                          </div>
                      </div>
                    </div>
                </form>
                
                
              </div>          
              
              
            </header>
            <div class="card-body p-0">
              
              <div class="table-responsive">
                <table id="productsTable" post_data="<?=str_replace('"',"'",json_encode($this->input->get()))?>" class="mdl-data-table product-table m-t-30 TableAjax" page="<?=base_url().'security/visitAjax'?>" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="col-xs-2" style="min-width: 120px;">التاريخ </th>
                      <th class="col-xs-2">اسم  الزائر </th>
                      <th class="col-xs-2">رقم البطاقة </th>
                      <th class="col-xs-2">رقم الهاتف </th>
                      <th class="col-xs-2">سبب الزيارة </th>
                      <th class="col-xs-2">مقابلة  </th>
                      <th class="col-xs-2">اسم الشركة   </th>
                      <th class="col-xs-2">يوم </th>
                      <th class="col-xs-2">الساعة </th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                  </tbody>
                </table>
                <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
                  
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <button class="btn btn-info btn-block hidden enquiry_modal" data-toggle="modal" data-target="#enquiry_modal">Trigger</button>
  <div class="modal fade" id="enquiry_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel-2">إضافة  بيانات  الزيارة </h4>
                            <ul class="card-actions icons left-top">
                            <li>
                                <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <form class="form_ajax" method="POST" result-data="form_result" action="<?=base_url().'security/insert_visit'?>">
                        <div class="modal-body">
	                            <div class="form-group text-center">
	                                <label for="day" class="col-sm-4 control-label">اليوم </label>
	                                <div class="col-sm-8">
	                                	<span><?=dayAr(date('D'))?></span>
	                                    <input id="day" type="hidden" data-rule-required="true"  data-msg-required="يجب كتابة  اليوم" name="day_ar" placeholder="اكتب  اليوم" value="<?=dayAr(date('D'))?>" class="form-control" >
	                                </div>
	                            </div>
	                            <div class="form-group text-center">
	                                <label for="date" class="col-sm-4 control-label">التاريخ </label>
	                                <div class="col-sm-8">
	                                	<span><?=date('Y-m-d')?></span>
	                                    <input id="date" type="hidden" data-rule-required="true"  data-msg-required="يجب كتابة  التاريخ" name="date" placeholder="اكتب  التاريخ" value="<?=date('Y-m-d')?>"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="form-group text-center">
	                                <label for="date" class="col-sm-4 control-label">الساعة  </label>
	                                <div class="col-sm-8">
	                                	<span><?=date('h:i:s A')?></span>
	                                    <input id="date" type="hidden" data-rule-required="true"  data-msg-required="يجب كتابة  الساعة " name="check_in" placeholder="اكتب  الساعة " value="<?=date('h:i:s A')?>"  class="form-control" >
	                                </div>
	                            </div>

	                            <div class="clearfix"></div>
		                        <hr>

	                        	<div class="form-group">
		                            <label for="name" class="col-sm-4 control-label"> الاسم </label>
	                                <div class="col-sm-8">
	                                    <input id="name" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   الاسم" name="name" placeholder="الاسم" value=""  class="input-name form-control" >
	                                </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="name" class="col-sm-4 control-label"> رقم البطاقة  </label>
	                                <div class="col-sm-8">
	                                    <input id="national_id" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   رقم البطاقة " name="national_id" placeholder="رقم البطاقة " value=""  class="form-control" >
	                                </div>
		                        </div>


		                        <div class="form-group">
	                                <label for="phone" class="col-sm-4 control-label">رقم الهاتف </label>
	                                <div class="col-sm-8">
	                                    <input id="phone" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   رقم الهاتف   " name="phone" min="11" placeholder="رقم الهاتف" value=""  class="form-control" >
	                                </div>
	                            </div>

		                        <div class="form-group">
		                            <label for="company" class="col-sm-4 control-label"> الشركة  </label>
	                                <div class="col-sm-8">
	                                    <input id="company" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   الشركة " name="company" placeholder="الشركة " value=""  class="form-control" >
	                                </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="meet" class="col-sm-4 control-label"> لمقابلة  </label>
	                                <div class="col-sm-8">
	                                    <input id="meet" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   لمقابلة " name="meet" placeholder="لمقابلة " value=""  class="form-control" >
	                                </div>
		                        </div>

		                        <div class="form-group">
		                            <label for="reasone" class="col-sm-4 control-label"> سبب الزيارة   </label>
	                                <div class="col-sm-8">
	                                    <input id="reasone" type="text" data-rule-required="true"  data-msg-required="يجب كتابة   سبب الزيارة  " name="reasone" placeholder="سبب الزيارة  " value=""  class="form-control" >
	                                </div>
		                        </div>

                        </div>
                        <div class="form_result col-md-12"></div>
                        <div class="clearfix"></div>
                        <div class="modal-footer" style="margin-top: 30px;background: whitesmoke;">
                            <button type="button" class="btn btn-default btn-flat" id="closeModal" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                    </div>
                    <!-- modal-content -->
                </div>
                <!-- modal-dialog -->
            </div>

<button class="btn btn-info btn-block hidden edit_visitModal" data-toggle="modal" data-target="#edit_visitModal">Trigger</button>
  <div class="modal fade" id="edit_visitModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="basic_modal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel-2">تعديل   الزيارة </h4>
                            <ul class="card-actions icons left-top">
                            <li>
                                <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="edit_visit_result"></div>
                    </div>
                    <!-- modal-content -->
                </div>
                <!-- modal-dialog -->
            </div>