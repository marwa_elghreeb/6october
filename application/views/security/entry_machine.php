    <div id="content_wrapper" class="card-overlay">
    <div id="header_wrapper" class="header-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <h1>ملف بيانات الدخول</h1>
                <a class="btn btn-primary search-student-btn hidden" href="javascript:;"> بحث <i class="zmdi zmdi-search zmdi-hc-fw"></i></a>     
              </header>
            </div>
          </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="content-body">
            <div class="row">
                <div class="applyArea">
                    <div class="col-xs-12 col-sm-12">
                        <div class="card">
                          <div class="card-body">
                            
                              <div class="card-footer text-center">
                                  <a href="<?=base_url().'security/download_machine_file'?>" target='_blank' class="btn btn-info"> تحميل الملف <i class="zmdi zmdi-download"></i></a>
                              </div>
                              <div class="form_result"></div>
                      
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    