<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الاحصائيات </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">احصائيات   المدارس  </h2>              
              <div class="col-md-12">
                <form class="statistics_acquaintance" action="<?=base_url().'statistics/print_statistics_schools'?>" method="POST">
                <div class="col-md-12">
                  <div class="form-group">
                    <?php $i=0; foreach ($years as $key => $value): $i++; ?>
                      <label style="width: 15%;padding: 0px;margin: 10px 0px !important;" class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox<?=$i?>" name="years[]" value="<?=$value->year_type?>" <?=$this->input->get('year_tb_id') == $value->year_tb_id ? 'checked' : ''?>> <?=$value->year_type?>
                      </label>
                    <?php endforeach; ?>
                </div> 
                <button type="submit" class="btn btn-primary">طباعة الاحصائيات </button> 
                </div>
                </form>
                
                
              </div>
              <div class="clearfix"></div>
              <hr>
            </header>
          </div>
        </div>
      </div>
    </div>
  </div>