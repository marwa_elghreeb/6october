<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الاحصائيات </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">احصائيات وسيلة التعارف </h2>              
              <div class="col-md-12">
                <form class="statistics_acquaintance" action="<?=base_url().'statistics/acquaintance'?>" method="GET">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="year" class="col-sm-3 control-label">السنة الدراسية</label>
                    <div class="col-sm-9">
                        <select id="year_tb_id_select" name="year_tb_id" class="form-control">
                          <?php foreach ($years as $key => $value): ?>
                            <option  <?=$this->input->get('year_tb_id') == $value->year_tb_id ? 'selected' : ''?> value="<?=$value->year_tb_id?>"><?=$value->year_type?></option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                </div>  
                </div>
                <div class="col-xs-12 col-md-6">
                  <button class="btn btn-primary">بحث  </button>
                </div>
                </form>
                
                
              </div>
              <hr>
              <ul class="card-actions icons left-top">
                
                <li>
                  <a href="<?=base_url().'statistics/print_acquaintance/'.$year_tb_id?>" target="_blank" data-toggle="tooltip" data-original-title="طباعة الاحصائيات">
                    <i class="zmdi zmdi-print"></i>
                  </a>
                </li>
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div class="card-body p-0">

              <div class="table-responsive">
                <table id="productsTable" class="mdl-data-table product-table m-t-30 TableAjax" width="100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="col-xs-2">وسيلة التعارف </th>
                      <th class="col-xs-2">عدد الطلاب  </th>
                      <th class="col-xs-2">اجمالى عدد الطلاب</th>
                    </tr>
                  </thead>
                  <tbody class="TableResult">
                    <?php $i=0; foreach ($acquaintance as $acquaintance_key => $acquaintance_value): $i++;?>                      
                      <tr>
                        <td><?=$i?></td>
                        <td><?=$acquaintance_value->acquaintance_name?></td>
                        <td><?=$acquaintance_value->num?></td>
                        <?php if($i == 1): ?>
                          <td><?=$total?></td>
                        <?php endif; ?>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
                <div class="paginationAjax" style="text-align: center;padding-top: 20px;">
                  
                  
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>