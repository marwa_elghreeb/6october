<div id="content_wrapper" class="card-overlay">
  <div id="header_wrapper" class="header-md">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <header id="header">
            <h1>الاحصائيات </h1>
          </header>
        </div>
      </div>
    </div>
  </div>
  <div id="content" class="container-fluid">
    <div class="content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="card card-data-tables ">
            <header class="card-heading">
              
              <h2 class="card-title">كشف احصائيات   المتقدمين للاقسام المختلفة  </h2>              
              <div class="col-md-12">
                <form class="statistics_acquaintance" action="<?=base_url().'statistics/print_sections_statistics'?>" method="GET">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="year" class="col-sm-3 control-label">السنة الدراسية</label>
                    <div class="col-sm-9">
                        <select id="year_tb_id_select" name="year_tb_id" class="form-control">
                          <option>اختر السنة الدراسية </option>
                          <?php foreach ($years as $key => $value): ?>
                            <option  <?=$this->input->get('year_tb_id') == $value->year_tb_id ? 'selected' : ''?> value="<?=$value->year_tb_id?>"><?=$value->year_type?></option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                </div>  
                </div>
                <div class="col-xs-12 col-md-6">
                  <button class="btn btn-primary">بحث  </button>
                </div>
                </form>
                
                
              </div>
              <hr>
              <ul class="card-actions icons left-top">
                
                <li>
                  <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="تصدير ملف Excel">
                    <i class="zmdi zmdi-inbox"></i>
                  </a>
                </li>
              </ul>
            </header>
            <div style="padding-bottom: 20px;" class="clearfix"></div>
            
          </div>
        </div>
      </div>
    </div>
  </div>