<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'حقل {field} مطلوب.';
$lang['form_validation_isset']			= 'يجب أن يحتوي %s قيمة.';
$lang['form_validation_valid_email']		= 'يجب أن يحتوي %s عنوان بريد إلكتروني صحيح.';
$lang['form_validation_valid_emails']		= 'يجب أن يحتوي الحقل %s على عناوين بريد إلكتروني صحيحة.';
$lang['form_validation_valid_url']		= 'يجب أن يحتوي الحقل %s عنوان URL صحيح.';
$lang['form_validation_valid_ip']		= 'يجب أن يحتوي الحقل %s عنوان IP صحيح.';
$lang['form_validation_min_length']		= 'يجب أن يكون عدد أحرف %s على الأقل %s.';
$lang['form_validation_max_length']		= 'يجب أن لا يزيد عدد أحرف %s عن %s.';
$lang['form_validation_exact_length']		= 'يجب أن يتكون %s من %s أحرف بالضبط.';
$lang['form_validation_alpha']			= 'يجب أن يحتوي %s أحرفاً فقط.';
$lang['form_validation_alpha_numeric']		= 'يجب أن يحتوي %s أحرفاً إنجليزية وأرقاماً فقط.';
$lang['form_validation_alpha_numeric_spaces']	= 'The {field} field may only contain alpha-numeric characters and spaces.';
$lang['form_validation_alpha_dash']		= 'يجب أن يحتوي %s على أرقام وأحرف وعلامتي - و _ فقط.';
$lang['form_validation_numeric']		= 'يجب أن يحتوي %s على أعداد فقط.';
$lang['form_validation_is_numeric']		= 'يجب أن يحتوي %s أعداد فقط.';
$lang['form_validation_integer']		= 'يجب أن يحتوي %s على عدد.';
$lang['form_validation_regex_match']		= 'الحقل %s ليس مكتوباً بالنسق الصحيح.';
$lang['form_validation_matches']		= 'قيمة %s لا تطابق قيمة %s.';
$lang['form_validation_differs']		= 'The {field} field must differ from the {param} field.';
$lang['form_validation_is_unique'] 		= 'The {field} field must contain a unique value.';
$lang['form_validation_is_natural']		= 'The {field} field must only contain digits.';
$lang['form_validation_is_natural_no_zero']	= 'The {field} field must only contain digits and must be greater than zero.';
$lang['form_validation_decimal']		= 'The {field} field must contain a decimal number.';
$lang['form_validation_less_than']		= 'The {field} field must contain a number less than {param}.';
$lang['form_validation_less_than_equal_to']	= 'The {field} field must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= 'The {field} field must contain a number greater than {param}.';
$lang['form_validation_greater_than_equal_to']	= 'The {field} field must contain a number greater than or equal to {param}.';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your field name {field}.';
$lang['form_validation_in_list']		= 'The {field} field must be one of: {param}.';
